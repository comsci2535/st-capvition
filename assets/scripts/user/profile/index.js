"use strict";
// alert(2);
$(document).ready(function(e) {

    $('form#frm-save').submit(function(event) {

      if($('#ff').val() != ""){

         var username = $("#username").val();
         var password = $("#password").val();
        
          
          if($('#password').val()!="" || $('#passwordOld').val()!="" ){
              
              if(password.length < 8){
                $('#password').focus();
                $('.alert_password').html('<font color=red>* รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร !</font>');
                return false;
              }
              if(!/[a-z]/.test(password)){
                  $('#password').focus();
                  $('.alert_password').html('<font color=red>* รหัสผ่านต้องประกอบด้วยตัวอักษร (a-z) !</font>');
                  return false;
              }
              if(!/[0-9]/.test(password)){
                  $('#password').focus();
                  $('.alert_password').html('<font color=red>* รหัสผ่านต้องประกอบด้วยตัวอักษร (0-9) !</font>');
                  return false;
              }
          }
          
         
          if($('#passwordConf').val() != $('#password').val()){

            $('#passwordConf').focus();
            $(".alert_passwordConf").html('<font color=red>* ยืนยันรหัสผ่านไม่ถูกต้อง</font>'); 
            return false;

          }

      }

        if($('#fname').val()==""){
            $('#fname').focus();
            $(".alert_name").html('<font color=red>* กรอกชื่อ</font>'); 
            return false;
        }
        if($('#lname').val()==""){
            $('#lname').focus();
            $(".alert_name").html('<font color=red>* กรอกนามสกุล</font>'); 
            return false;
        }

        $('#form-error-div').html(' ');
        $('#form-success-div').html(' ');
        $(".alert_passwordOld").html(' ');
        if(confirm("ยืนยันการบันทึกข้อมูล ?")){

            event.preventDefault();             
            var formObj = $(this);
            var formURL = formObj.attr("action");
            var formData = new FormData(this);
            $('#form-img-div').html('<img src="'+baseUrl+'assets/images/loading1.gif" alt="loading" title="loading" style="display:inline">');
            $.ajax({
              url: formURL,
              type: 'POST',
              data:formData,
              dataType:"json",
              mimeType:"multipart/form-data",
              contentType: false,
              cache: false,
              processData:false,
              success: function(data, textStatus, jqXHR){

                 if(data.info_txt == "error")
                  {
                       $('#form-img-div').html(' ');
                       $("#form-error-div").append("<p><strong><li class='text-red'></li>"+data.msg+"</strong>,"+data.msg2+"</p>");
                       $("#form-error-div").slideDown(400);
                      
                  }
                  if(data.info_txt == "error_passwordOld")
                  {
                       $('#form-img-div').html(' ');
                       $(".alert_passwordOld").append("<font color=red>* "+data.msg+"</font>");
                       $(".alert_passwordOld").slideDown(400);
                      
                  }
                  if(data.info_txt == "success")
                  {
                    $('#form-img-div').html(' ');
                    // $("#form-success-div").append('<p><strong><li class="text-green"></li>'+data.msg+'</strong>,'+data.msg2+'</p>');
                    // $("#form-success-div").slideDown(400);
                    // alert('แก้ไขข้อมูลสำเร็จ');

                    // setInterval(function(){
                    //     window.location.href= siteUrl+"user/profile";
                    //     },1000
                    // );

                    swal({ 
                      title: data.msg,
                      text: "",
                      type: "success" 
                    }).then(function() {
                        window.location.href = siteUrl+"user/profile";
                    })
                        
                  }
        
              },
              error: function(jqXHR, textStatus, errorThrown){
                 $('#form-img-div').html(' ');
                 $("#form-error-div").append("<p><strong><li class='text-red'></li>บันทึกข้อมูลไม่สำเร็จ</strong>,กรุณาลองใหม่อีกครั้ง!</p>");
                 $("#form-error-div").slideDown(400);
              }
        
            });
        }else{
          return false;
        }

    });

});

function isEnglishchar(str){   
    var orgi_text="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890._-";   
    var str_length=str.length;   
    var isEnglish=true;   
    var Char_At="";   
    for(i=0;i<str_length;i++){   
        Char_At=str.charAt(i);   
        if(orgi_text.indexOf(Char_At)==-1){   
            isEnglish=false;   
            break;
        }      
    }   
    return isEnglish; 

    //alert(str);
}  
function onloadCallback () {
 
    grecaptcha.render('html_element', {
      'sitekey' : recaptcha_sitekey,
      'callback' : correctCaptcha,
      'expired-callback': expiredCallback
    });

};
function correctCaptcha(response) {
    $('input[name="robot"]').val(response);
    $('input[name="robot"]').next('span').addClass('hidden');
};
function expiredCallback(response) {
    grecaptcha.reset();
    $('input[name="robot"]').val("");
    $('input[name="robot"]').next('span').removeClass('hidden');
};