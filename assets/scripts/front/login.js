"use strict";



$(document).ready(function () {

    $('form#frm-login').submit(function(event) {

        var username = $("#username_login").val();
        var password = $("#password_login").val();
        
        if($('#username_login').val() == ""){

          $('#username_login').focus();
          $(".alert_username_login").html('<font color=red>* กรอกชื่อผู้ใช้งาน/Username</font>'); 
          return false;

        }
       
        
        if($('#password_login').val()==""){
            $('#password_login').focus();
            $(".alert_password_login").html('<font color=red>* กรอกรหัสผ่าน</font>'); 
            return false;
        }
        

         $('#form-error-div_login').html(' ');
          $('#form-success-div_login').html(' ');

        

            event.preventDefault();             
            var formObj = $(this);
            var formURL = formObj.attr("action");
            var formData = new FormData(this);
            $('#form-img-div_login').html('<img src="'+baseUrl+'assets/images/loading1.gif" alt="loading" title="loading" style="display:inline">');
            $.ajax({
              url: formURL,
              type: 'POST',
              data:formData,
              dataType:"json",
              mimeType:"multipart/form-data",
              contentType: false,
              cache: false,
              processData:false,
              success: function(data, textStatus, jqXHR){

                 if(data.info_txt == "error")
                  {
                       $('#form-img-div_login').html(' ');
                       $("#form-error-div_login").append("<p><strong><li class='text-red'></li>"+data.msg+"</strong>,"+data.msg2+"</p>");
                       $("#form-error-div_login").slideDown(400);
                      
                  }
                  if(data.info_txt == "success")
                  {
                    $('#form-img-div_login').html(' ');
                    // $("#form-success-div").append('<p><strong><li class="text-green"></li>'+data.msg+'</strong>,'+data.msg2+'</p>');
                    // $("#form-success-div").slideDown(400);

                    setInterval(function(){
                        //window.location.href= siteUrl+"home";
                        window.location = $("#after_login_url").val();
                        },1000
                    );
                        
                  }
        
              },
              error: function(jqXHR, textStatus, errorThrown){
                 $('#form-img-div_login').html(' ');
                 $("#form-error-div_login").append("<p><strong><li class='text-red'></li>เข้าสู่ระบบไม่สำเร็จ</strong>,กรุณาลองใหม่อีกครั้ง!</p>");
                 $("#form-error-div_login").slideDown(400);
              }
        
            });
       

    });

    // $('#frm-register').validate({
    //     rules: {
    //         email: {required: true, isEmail: true},
    //         password: {required: true},
    //         rePassword: {required: true, equalTo: '#password'}
    //     }
    // });

    // $('#frm-forgot').validate({
    //     rules: {
    //         email: {required: true, isEmail: true},
    //     }
    // });

   
});
