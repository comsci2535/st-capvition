// JavaScript Document
$(document).ready(function($) {
	   $("#owl-example").owlCarousel({
  	    	
			margin: 0,
        	loop: true,
			autoplay: true,
			nav : false,
			dots: false,
			autoplaySpeed: 3000,
        	responsive: {
          	0: {
            	items: 1
          	},
          	600: {
            	items: 1
          	},
          	1000: {
            	items:1
          	}
         }
		
    	});
	
	 	$(".owl-demo3").owlCarousel({
  	    	
			margin: 10,
        	loop: false,
			autoplay: false,
			nav : true,
			dots: false,
        	responsive: {
          	0: {
            	items: 1
          	},
          	600: {
            	items: 2
          	},
          	1000: {
            	items: 4
          	}
         }
		
    	});
		
		$("#owl-demo4").owlCarousel({
  	    	
			margin: 10,
        	loop: true,
			autoplay: false,
			nav : true,
			dots: false,
        	responsive: {
          	0: {
            	items: 1
          	},
          	600: {
            	items: 4
          	},
          	1000: {
            	items: 4
          	}
         }
		
    	});
	  
	   
		$("#owl-demo5").owlCarousel({
  	    	
			margin: 0,
        	loop: false,
			autoplay: false,
			nav : true,
			dots: false,
        	responsive: {
          	0: {
            	items: 2
          	},
          	600: {
            	items: 2
          	},
          	1000: {
            	items: 2
          	}
         }
		
    	});
	 
	 
	  $("#owl-demo6").owlCarousel({
  	    	
			margin: 0,
        	loop: true,
			autoplay: false,
			nav : true,
			dots: false,
        	responsive: {
          	0: {
            	items: 2
          	},
          	600: {
            	items: 3
          	},
          	1000: {
            	items: 5
          	}
         }
		
    	});
	  $("#owl-example6").owlCarousel({
		 pagination:false,
		 autoPlay: false,  
		 navigation : true,
		 slideSpeed : 300,
         paginationSpeed : 1000,
		 items : 9,
         itemsDesktop : [1199,9],
		 itemsTablet: [768,10],
		 itemsMobile : [640,4],
		 
	  }
	  );
	  
	  $("#owl-example7").owlCarousel({
		 pagination:false,
		 autoPlay: false,  
		 navigation : true,
		 slideSpeed : 300,
         paginationSpeed : 1000,
		 items : 3,
         itemsDesktop : [1199,3],
		 itemsTablet: [768,3],
		 itemsMobile : [640,1],
		 
	  }
	  );
	  $(".owl-example8").owlCarousel({
		 pagination:false,
		 autoPlay: false,  
		 navigation : true,
		 slideSpeed : 700,
         paginationSpeed : 1000,
		 items : 2,
         itemsDesktop : [1199,2],
		 itemsTablet: [768,2],
		 itemsMobile : [640,2],
		 
	  }
	  );
	  $("#owl-example9").owlCarousel({
		 pagination:false,
		 autoPlay: false,  
		 navigation : true,
		 slideSpeed : 700,
         paginationSpeed : 1000,
		 items : 5,
         itemsDesktop : [1199,5],
		 itemsTablet: [768,4],
		 itemsMobile : [640,1],
		 
	  }
	  );
	  
    });