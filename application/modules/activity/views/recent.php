
    <!-- ##### Upcoming Events Start ##### -->
    <section class="upcoming-events section-padding-100-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h3>กิจกรรมล่าสุด</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Upcoming Events -->
                <?php foreach ($info_recent as $key => $rs) { ?>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-upcoming-events mb-50 wow fadeInUp" data-wow-delay="250ms">
                        <!-- Events Thumb -->
                        <div class="events-thumb">
                            <img src="<?php echo $rs['image']; ?>" alt="">
                            <h6 class="event-date"><?php echo $rs['date']; ?></h6>
                            <h4 class="event-title"><?php echo $rs['title']; ?></h4>
                        </div>
                        <!-- Date & Fee -->
                        <div class="date-fee d-flex justify-content-between">
                            <div class="date">
                                <p><i class="fa fa-map-marker"></i> <?php echo $rs['location']; ?></p>
                            </div>
                            <div class="events-fee">
                                <a href="<?php echo site_url("activity/detail/{$rs['linkId']}");?>">
                                   <?php if($rs['promotion']!=""){ ?>
                                      <?php echo number_format($rs['promotion']);?> บาท
                                    <?php }else{ ?>
                                        <?php echo number_format($rs['price']);?> บาท
                                   <?php } ?>
                                        
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                
            </div>
        </div>
    </section>
    <!-- ##### Upcoming Events End ##### -->