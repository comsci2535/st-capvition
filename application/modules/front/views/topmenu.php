<!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Top Header Area -->
        <div class="top-header-area d-flex justify-content-between align-items-center">
            <!-- Contact Info -->
            <div class="contact-info">
                <a href="#"><span>Phone:</span> +44 300 303 0266</a>
                <a href="#"><span>Email:</span> info@clever.com</a>
            </div>
            <!-- Follow Us -->
            <div class="follow-us">
                <span>Follow us</span>
                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="clever-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <!-- Menu -->
                <nav class="classy-navbar justify-content-between" id="cleverNav">

                    <!-- Logo -->
                    <a class="nav-brand" href="<?php echo site_url();?>"><img src="<?php echo base_url("assets/website/") ?>images/logo.png" alt="" style="height: 55px;"></a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- Close Button -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul>
                                <li ><a class="<?php if(!empty($home_act)){ echo $home_act; } ?> " href="<?php echo site_url();?>">หน้าหลัก</a></li>
                                <li><a class="<?php if(!empty($article_act)){ echo $article_act; } ?> " href="<?php echo site_url('article');?>">บทความ</a></li>
                                <li><a class="<?php if(!empty($course_act)){ echo $course_act; } ?> " href="<?php echo site_url('course');?>">คอร์สเรียน</a></li>
                                <li><a class="<?php if(!empty($activity_act)){ echo $activity_act; } ?> " href="<?php echo site_url('activity');?>">กิจกรรม</a></li>
                                <li><a class="<?php if(!empty($contact_act)){ echo $contact_act; } ?> " href="<?php echo site_url('contact');?>">ติดต่อเรา</a></li>
                            </ul>

                            <!-- Search Button -->
                            <!-- <div class="search-area">
                                <form action="#" method="post">
                                    <input type="search" name="search" id="search" placeholder="Search">
                                    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </div> -->

                            <!-- Register / Login -->
                            <div class="register-login-area">
                                <a href="#" class="btn">สมัครสมาชิก</a>
                                <a href="index-login.html" class="btn active">เข้าสู่ระบบ</a>
                            </div>

                        </div>
                        <!-- Nav End -->
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->