<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends MX_Controller {

    public $language = array("th" => "ไทย", /*"en" => "English"*/);
    public $curLang;

    public function __construct() {
        parent::__construct();
        $this->load->model('front_m');
        $this->load->library('facebook');
       // ini_set('session.cookie_lifetime', 0);
       
    }

    public function layout($data) {
        if ( ENVIRONMENT == 'development' )
            $this->output->enable_profiler(FALSE);
        //breadcrumb
        $this->breadcrumbs->push('หน้าหลัก', site_url("admin/"));
        if (isset($data['breadcrumb']))
            $this->_breadcrumbs($data['breadcrumb']);
        //view
        if (!isset($data['contentView']))
            $data['contentView'] = 'empty';  
        //page header
        
        $data['pageTitle'] = "The CAP Vision Academy";
        $siteTitle=$this->_get_config('general','siteTitle');
        if($siteTitle){
            $data['pageTitle'] = $siteTitle['value'];
        }

        $data['header'] = $data['pageTitle'];

       

        $data['metaDescription'] = "The CAP Vision Academy";
        $metaDescription=$this->_get_config('general','metaDescription');
        if($metaDescription){
            $data['metaDescription'] = $metaDescription['value'];
        }

        $data['metaKeyword'] = "The CAP Vision Academy";
        $metaKeyword=$this->_get_config('general','metaKeyword');
        if($metaKeyword){
            $data['metaKeyword'] = $metaKeyword['value'];
        }

         if (isset($data['metaDescription_'])) {
            $data['metaDescription'] = $data['metaDescription_'];
        } 
        if (isset($data['metaKeyword_'])) {
            $data['metaKeyword'] = $data['metaKeyword_'];
        } 
        
        if (isset($data['pageHeader'])) {
            $data['pageTitle'] = $data['pageTitle'] . " | " . $data['pageHeader'];
        } else {
            $data['pageHeader'] = "test";
        }

        // if (isset($data['metaTitle'])) {
        //     $data['pageTitle'] = $data['pageTitle'] . " | " . $data['metaTitle'];
        // } else {
        //     $data['pageTitle'] = $data['pageTitle'];
        // }

        

        

        $data['idLine'] = "";
        $idLine=$this->_get_config('general','idLine');
        if($idLine){
            $data['idLine'] = $idLine['value'];
        }

        $data['facebook'] = "";
        $facebook=$this->_get_config('general','facebook');
        if($facebook){
            $data['facebook'] = $facebook['value'];
        }

        $data['phoneNumber'] = "";
        $phoneNumber=$this->_get_config('general','phoneNumber');
        if($phoneNumber){
            $data['phoneNumber'] = $phoneNumber['value'];
        }

        $data['phoneContact'] = "";
        $phoneContact=$this->_get_config('general','phoneContact');
        if($phoneContact){
            $data['phoneContact'] = $phoneContact['value'];
        }

        $data['mailDefault'] = "";
        $input_c['type'] = 'mail';
        $input_c['variable'] = 'mailDefault';
        $data['mailDefault'] = $this->front_m->getConfig($input_c)->row();

        $data['payment'] = "";
        $payment=$this->_get_config('general','payment');
        if($payment){
            $data['payment'] = $payment['value'];
        }

        $data['recaptcha_sitekey'] = $this->config->item('recaptcha_sitekey');
        $data['recaptcha_secretkey'] = $this->config->item('recaptcha_secretkey');
        //page script
        if ( !isset($data['pageScript']) ) {
             $data['pageScript'] = $this->_page_script();
        } else {
            $data['pageScript'] = '<script src="' . base_url($data['pageScript']) . '?v='.rand(0,100).'"></script>';
        }
        
        $data['isLogin'] = $this->_set_users();
        $data['language'] = $this->language;

      

        $data['authUrl'] =  $this->facebook->login_url();

        $data['courseRegis']=0;
        $data['isCommunity']=1;
        if($this->session->member['userId']!=""){

            $data['courseRegis'] = $this->db
                                ->select('*')
                                ->from('course_member a')
                                ->where('a.userId',$this->session->member['userId'])
                                ->get()->num_rows();

            $data['isCommunity'] = $this->db
                                ->select('a.isCommunity')
                                ->from('user a')
                                ->where('a.userId',$this->session->member['userId'])
                                ->where('a.isCommunity',1)
                                ->get()->num_rows();

        }
        
        $code['codeId'] = $this->input->get('code');
        //update cart and redirect cart index
        if(!empty($code['codeId']) && $code['codeId']!=""){
           $this->session->set_userdata('code_data', $code); 
        }
        
        
        $this->load->view('layout', $data);
    }
    public function layout_blog($data) {
        if ( ENVIRONMENT == 'development' )
            $this->output->enable_profiler(FALSE);
        //breadcrumb
        $this->breadcrumbs->push('หน้าหลัก', site_url("admin/"));
        if (isset($data['breadcrumb']))
            $this->_breadcrumbs($data['breadcrumb']);
        //view
        if (!isset($data['contentView']))
            $data['contentView'] = 'empty';  
        //page header
        $data['pageTitle'] = "The CAP Vision Academy";
        $siteTitle=$this->_get_config('general','siteTitle');
        if($siteTitle){
            $data['pageTitle'] = $siteTitle['value'];
        }


        $data['header'] = $data['pageTitle'];

        if (isset($data['metaTitle'])) {
            $data['pageTitle'] = $data['pageTitle'] . " | " . $data['metaTitle'];
        } else {
            $data['pageTitle'] = $data['pageTitle'];
        }

        if (isset($data['metaDescription'])) {
            $data['description'] = $data['metaDescription'];
        } else {
            $data['description'] = $data['pageTitle'];
        }
        if (isset($data['metaKeyword'])) {
            $data['keywords'] = $data['metaKeyword'];
        } else {
            $data['keywords'] = $data['pageTitle'];
        }

        $data['min_price']=$this->get_price('ASC');
        $data['max_price']=$this->get_price('DESC');

        $data['idLine'] = "";
        $idLine=$this->_get_config('general','idLine');
        if($idLine){
            $data['idLine'] = $idLine['value'];
        }

        $data['phoneNumber'] = "";
        $phoneNumber=$this->_get_config('general','phoneNumber');
        if($phoneNumber){
            $data['phoneNumber'] = $phoneNumber['value'];
        }
        //page script
        if ( !isset($data['pageScript']) ) {
             $data['pageScript'] = $this->_page_script();
        } else {
            $data['pageScript'] = '<script src="' . base_url($data['pageScript']) . '?v='.rand(0,100).'"></script>';
        }
        
        $data['isLogin'] = $this->_set_users();
        $data['language'] = $this->language;
        
        $this->load->view('layout_blog', $data);
    }

    public function layout_iframe($data) {
        if ( ENVIRONMENT == 'development' )
            $this->output->enable_profiler(FALSE);
        //breadcrumb
        $this->breadcrumbs->push('หน้าหลัก', site_url("admin/"));
        if (isset($data['breadcrumb']))
            $this->_breadcrumbs($data['breadcrumb']);
        //view
        if (!isset($data['contentView']))
            $data['contentView'] = 'empty';  
        //page header
        $data['pageTitle'] = "The CAP Vision Academy";
        $siteTitle=$this->_get_config('general','siteTitle');
        if($siteTitle){
            $data['pageTitle'] = $siteTitle['value'];
        }


        $data['header'] = $data['pageTitle'];

        if (isset($data['metaTitle'])) {
            $data['pageTitle'] = $data['pageTitle'] . " | " . $data['metaTitle'];
        } else {
            $data['pageTitle'] = $data['pageTitle'];
        }

        if (isset($data['metaDescription'])) {
            $data['description'] = $data['metaDescription'];
        } else {
            $data['description'] = $data['pageTitle'];
        }
        if (isset($data['metaKeyword'])) {
            $data['keywords'] = $data['metaKeyword'];
        } else {
            $data['keywords'] = $data['pageTitle'];
        }

        $data['min_price']=$this->get_price('ASC');
        $data['max_price']=$this->get_price('DESC');

        $data['idLine'] = "";
        $idLine=$this->_get_config('general','idLine');
        if($idLine){
            $data['idLine'] = $idLine['value'];
        }

        $data['phoneNumber'] = "";
        $phoneNumber=$this->_get_config('general','phoneNumber');
        if($phoneNumber){
            $data['phoneNumber'] = $phoneNumber['value'];
        }
        //page script
        if ( !isset($data['pageScript']) ) {
             $data['pageScript'] = $this->_page_script();
        } else {
            $data['pageScript'] = '<script src="' . base_url($data['pageScript']) . '?v='.rand(0,100).'"></script>';
        }
        
        $data['isLogin'] = $this->_set_users();
        $data['language'] = $this->language;
        
        $this->load->view('layout_iframe', $data);
    }
    
    

    private function _breadcrumbs($breadcurmbs) {
        if ( empty($breadcurmbs) )
            return;
        foreach ( $breadcurmbs as $rs )
            $this->breadcrumbs->push($rs[0], $rs[1]);
        
        return true;
    }

    private function _page_script() {
        $file = $this->router->method;
        if ( in_array($this->router->method,array('create','edit')) ) 
            $file = 'form';
        $script = "assets/scripts/{$this->router->class}/{$file}.js";
        if ( is_file($script) ) {
            $pageScript = '<script src="' . base_url($script) . '?v='.rand(0,100).'"></script>';
        } else {
            $pageScript = '<!-- page no script -->';
        }
        return $pageScript;
    }

     private function _set_users()
    {
        // $member['userId'] = 0;
        // if ( !$this->session->has_userdata('member') ) {
        //     $this->session->set_userdata('member', $member);
        //     return false;
        // } else {
            if ( $this->session->member['userId'] > 0) {
                return true;
            } else {
                return false;
            }
        //}
        
    }

    private function _get_config($type,$variable)
    {
        $input['type'] = $type;
        $input['variable'] = $variable;
        $info=$this->front_m->getConfig($input)->row_array();
        return  $info;
        
    }
    private function get_price($type)
    {
        $input['order'] = $type;    
        $info=$this->front_m->getPrice($input)->row_array();
        //arr($info['price']);exit();
        
        return  $info['price'];
        
    }

    public function getLocation()
    {
       $input_ = $this->input->post(null, true);
       $input['grpContent']=$input_['type'];
       
       $info = $this->front_m->getLocation($input)->result_array();

       // $this->output
       //          ->set_content_type('application/json')
       //          ->set_output(json_encode($info));
       $text='<option value="">เลือกทำเลที่ตั้ง</option>';

       //if($info){ 
       foreach ($info as $key => $value) {
            $text.='<option value="'.$value['location'].'">'.$value['location'].'</option>';
        } 
            //}else{
       //      $text.='<option value="ยะลา">ยะลา</option>';
       //      $text.='<option value="ปัตตานี">ปัตตานี</option>';
       //      $text.='<option value="นราธิวาส">นราธิวาส</option>';
       // }

       echo $text;
        
    }

    public function getType()
    {
       $input_ = $this->input->post(null, true);
       $input['location']=$input_['location'];
       
       $info = $this->front_m->getType($input)->result_array();
       //arr($info); exit();
       // $this->output
       //          ->set_content_type('application/json')
       //          ->set_output(json_encode($info));
       $text='<option value="">เลือกประเภทสินค้า</option>';

       foreach ($info as $key => $value) {
            if($value['grpContent']=="house"){
                $title="บ้านมือสอง";
            }elseif ($value['grpContent']=="land") {
                $title="ที่ดิน";
            }elseif ($value['grpContent']=="rental") {
                $title="บ้านเช่า";
            }
            $text.='<option value="'.$value['grpContent'].'">'.$title.'</option>';
       } 

       echo $text;
        
    }
    
}
