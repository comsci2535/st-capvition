<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class House extends MX_Controller {
    
    private $_grpContent = 'house';

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('house_m');
       
    }
    
    public function index()
    {
        $input = $this->input->get();
        $input['grpContent'] = $this->_grpContent;
        $input['statusBuy'] = 0;

        $data['info'] = '';
        $info = $this->house_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ($info as &$rs) {
                $rs['linkId'] = str_replace(" ","-",$rs['title']);
                $input_u['contentId'] = $rs['contentId'];
                $input_u['grpContent'] = $this->_grpContent;
                $uplode = $this->house_m->get_uplode($input_u)->result_array();
                $rs['image']=base_url("assets/images/no_image2.png");
                if (!empty($uplode)){
                    foreach ($uplode as $key => $value) {
                        if($value['grpType']=='coverImage'){
                            $coverImage_path=$value['path'];
                            $coverImage_filename=$value['filename'];
                            $rs['image'] = base_url($coverImage_path.'thumbnail/'.$coverImage_filename);
                        }
                    }
                }
                
                $rs['createDate'] = date_language($rs['createDate'], TRUE, 'th');
            }

            $data['info'] = $info;
        }
        
       //print_r($data['info']);exit();
        $this->load->view('index', $data); 
    }

    public function datalist()
    {
        $input = $this->input->get();
        $input['grpContent'] = $this->_grpContent;


        $info = $this->house_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ($info as &$rs) {
                $input_u['contentId'] = $rs['contentId'];
                $input_u['grpContent'] = $this->_grpContent;
                $uplode = $this->house_m->get_uplode($input_u)->result_array();
                $rs['image']=base_url("assets/images/no_image2.png");
                if (!empty($uplode)){
                    $rs['image'] = array();
                    foreach ($uplode as $key => $value) {
                        
                        if($value['grpType']=='galleryImage'){
                            $coverImage_path=$value['path'];
                            $coverImage_filename=$value['filename'];
                            $rs['image'][] = base_url($coverImage_path.'thumbnail/'.$coverImage_filename);
                        }
                    }
                }
                
                $rs['createDate'] = date_language($rs['createDate'], TRUE, 'th');
            }

            $data['info'] = $info;
           //print"<pre>";print_r($data['info']);exit();
            $this->load->view('datalist', $data); 
        }
       
    }

    

    public function detail($id=0)
    {
        
        $this->load->module('front');
       
        //$input['contentId'] = $id;
        //print_r($id);exit();
        $input['keyword'] = str_replace("-"," ",$id);
        $input['grpContent'] = $this->_grpContent;

        

        $info=$this->house_m->get_rows($input)->row_array();
        if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "The CAP Vision Academy");

        //print"<pre>";print_r($info); exit(); 
        if (!empty($info)) {
            //foreach ( $info as $key=>&$rs ) {

                $input_u['grpContent'] = $this->_grpContent;
                $input_u['contentId'] = $info['contentId'];
                $info['image'] = base_url("assets/images/no_image3.png");
                
                $uplode = $this->house_m->get_uplode($input_u)->result_array();
                $data['image_g']=base_url("assets/images/no_image2.png");
                if (!empty($uplode)){
                    $data['image_g'] = array();
                    foreach ($uplode as $key => $value) {
                        
                        if($value['grpType']=='galleryImage'){
                            $coverImage_path=$value['path'];
                            $coverImage_filename=$value['filename'];
                            $data['image_g'][] = base_url($coverImage_path.$coverImage_filename);
                        }
                    }
                    //$data['JSON_img_arr']=json_encode($data['image_g']);
                }

                $file = $this->house_m->get_uplode($input_u)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $info['image'] = base_url($file['path'].$file['filename']);
                    $imageSeo = base_url($file['path'].'seo/'.$file['filename']);
                }

                

            $data['info'] = $info;

           // }

        }
        $this->house_m->plus_view($info['contentId']);
        Modules::run('track/front',$info['contentId']);
        
        $data['contentHeader'] = "house/header";
        $data['contentView'] = "house/detail";
        $data['sideBarView'] = "house/detailSideBar";
        $data['metaTitle'] = $info['metaTitle'];
        $data['metaDescription'] = $info['metaDescription'];
        $data['metaKeyword'] = $info['metaKeyword'];

        $share['ogTitle']=$info['metaTitle'];
        $share['ogDescription']=$info['metaDescription'];
        $share['ogUrl']= 'house/detail/'.$id;
        $share['ogImage']= $imageSeo;
        $this->_social_share($share);
        $this->front->layout_blog($data);
    }
    public function getImgG(){
        $input = $this->input->post(null, true);
        $input['grpContent'] = $this->_grpContent;
        
        $uplode = $this->house_m->get_uplode($input)->result_array();
        $data['image_g']=base_url("assets/images/no_image2.png");
        if (!empty($uplode)){
            $data['image_g'] = array();
            foreach ($uplode as $key => $value) {
                
                if($value['grpType']=='galleryImage'){
                    $coverImage_path=$value['path'];
                    $coverImage_filename=$value['filename'];
                    $data['image_g'][] = base_url($coverImage_path.'thumbnail/'.$coverImage_filename);
                }
            }
            //echo json_encode($data['image_g']);
        }
    }

    public function relate($contentId=0)
    {
        $input['grpContent'] = $this->_grpContent;
        $input['length']=10;
        $input['start']=0;
        $input['statusBuy'] = 0;
        
        $input['exclude']=$contentId;
        
        $info = $this->house_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ($info as &$rs) {
                $input_u['contentId'] = $rs['contentId'];
                $input_u['grpContent'] = $this->_grpContent;
                $uplode = $this->house_m->get_uplode($input_u)->result_array();
                $rs['linkId'] = str_replace(" ","-",$rs['title']);
                $rs['image']=base_url("assets/images/no_image2.png");
                if (!empty($uplode)){
                    foreach ($uplode as $key => $value) {
                        if($value['grpType']=='coverImage'){
                            $coverImage_path=$value['path'];
                            $coverImage_filename=$value['filename'];
                            $rs['image'] = base_url($coverImage_path.'thumbnail/'.$coverImage_filename);
                        }
                    }
                }
                
                $rs['createDate'] = date_language($rs['createDate'], TRUE, 'th');
            }
        }
        $data['info'] = $info;
       //print"<pre>"; print_r($data['info']); exit();
        if (!empty($info)) $this->load->view('relate', $data);
    }

    public function list()
    {
        Modules::run('track/front','');
        $this->load->module('front');
        $input = $this->input->get();
        $input['grpContent'] = $this->_grpContent;
        $input['statusBuy'] = 0;
        

        ///pagination///
        $uri='house/list';
        $total=$this->house_m->get_count($input);
        $segment=3;
        $per_page = 10;
        $data["links"] = $this->pagin($uri, $total, $segment, $per_page);
        ///pagination///
        
        $page = ($this->input->get('per_page')) ? ($per_page*($this->input->get('per_page')-1)) : 0;

        $input['length']=$per_page;
        $input['start']=$page;

        $info=$this->house_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ( $info as $key=>&$rs ) {
                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $rs['contentId'];
                $rs['linkId'] = str_replace(" ","-",$rs['title']);
                $rs['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->house_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $rs['image'] = base_url($file['path'].$file['filename']);
                }
                

            }
         
            $data['info'] = $info;
           
        }
       // print"<pre>"; print_r($data['info']); exit();
        
        $data['contentHeader'] = "house/header";
        $data['contentView'] = "house/list";
        $data['sideBarView'] = "house/sideBar";
        $data['metaTitle'] = "บ้านมือสอง";
        $data['metaDescription'] = "บ้านมือสอง";
        $data['metaKeyword'] = "บ้านมือสอง";

        $share['ogTitle']="บ้านมือสอง";
        $share['ogDescription']="บ้านมือสอง";
        $share['ogUrl']= 'house';
        $share['ogImage']= config_item('metaOgImage');
        $this->_social_share($share);

        $this->front->layout_blog($data);
        // $data['info'] = '';
        // $this->load->view('index', $data); 
    }

    public function getTrack(){
        $input = $this->input->post(null, true);
        Modules::run('track/front',$input['contentId'],'detail',$this->_grpContent);
        // print_r($input);exit();
    }

    public function _social_share($input)
    {
        $param['ogType'] = "webiste";
        $param['ogUrl'] = $input['ogUrl'];
        $param['ogTitle'] = $input['ogTitle'];
        $param['ogDescription'] = $input['ogDescription'];
        $param['ogImage'] = $input['ogImage'];
        $param['twImage'] = $input['ogImage'];
        
        Modules::run('social/set_share', $param);
    } 

    public function pagin($uri, $total, $segment, $per_page = 30) {

        $config['base_url'] = site_url($uri);
        $config['per_page'] = $per_page;
        $config['total_rows'] = $total;
        $config['uri_segment'] = $segment;
        //$config['num_links'] = $num_links;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination center-align">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = FALSE; //'<span aria-hidden="true">«</span>';
        $config['last_link'] = FALSE; //'<span aria-hidden="true">»</span>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span aria-hidden="true"><i class="fa fa-angle-left"></i></span>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<span aria-hidden="true"><i class="fa fa-angle-right"></i></span>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    } 

    
}
