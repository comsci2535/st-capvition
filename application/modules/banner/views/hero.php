<!-- <section class="hero-area bg-img bg-overlay-2by5" style="background-image: url(<?php echo base_url("assets/website/") ?>img/bg-img/bg1.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="hero-content text-center">
                        <h2>Let's Study Together</h2>
                        <a href="#" class="btn clever-btn">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
</section> -->


<section class="hero-area bg-img bg-overlay-2by5"  id="slider">
  <div id="owl-example" class="owl-carousel owl-theme">
    <?php foreach ($info as $rs) :?>
    
    <div class="item ">
       <?php if ($rs->link){ ?>
          <a href="<?php echo $rs->link ?>" >
       
            <img src="<?php echo $rs->image['coverImage'] ?>" class="img-responsive">
          
          </a>
        <?php }else{ ?>
          
              <img src="<?php echo $rs->image['coverImage'] ?>" class="img-responsive">
         
        <?php } ?>
    </div>
    
   
    <?php endforeach; ?>
  </div> 
</section> 