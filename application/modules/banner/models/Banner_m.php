<?php defined('BASEPATH') OR exit('No direct script access allowed.');

class Banner_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_list($param)
    {
        $this->_condition($param);
    
        $query = $this->db
                        ->select('a.*')
                        ->from('banner a')
                        ->get();
                        
        return $query;
    }

     private function _condition($param) 

    {   
        // $this->db->group_start()
        //                     ->where('a.startDate',NULL)
        //                     ->or_where('NOW() between DATE_FORMAT(a.startDate, "%Y-%m-%d %H:%i:%s") AND DATE_FORMAT(a.endDate, "%Y-%m-%d %H:%i:%s")')
        //                     ->or_where('DATE_FORMAT(a.startDate, "%Y-%m-%d %H:%i:%s") = "0000-00-00 00:00:00" AND DATE_FORMAT(a.endDate, "%Y-%m-%d %H:%i:%s") = "0000-00-00 00:00:00"  ')
        //                     ->or_where('DATE_FORMAT(a.startDate, "%Y-%m-%d %H:%i:%s") = "0000-00-00 00:00:00"  AND NOW() <= DATE_FORMAT(a.endDate, "%Y-%m-%d %H:%i:%s")')
        //                     ->or_where('NOW() >= DATE_FORMAT(a.startDate, "%Y-%m-%d %H:%i:%s")  AND DATE_FORMAT(a.endDate, "%Y-%m-%d %H:%i:%s") = "0000-00-00 00:00:00" ')
        //         ->group_end();
        $this->db->where("((a.startDate ='0000-00-00' AND a.endDate ='0000-00-00') OR  (a.startDate <='".date('Y-m-d')."' AND a.endDate >='".date('Y-m-d')."'))");
        $this->db->where('a.type', $param['type']);
        
        if ( isset($param['active']) ) {
            $this->db->where('a.active', $param['active']);
        }   
         if ( isset($param['page']) ) {
            $this->db->where('a.page', $param['page']);
        }         
        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

        if ( isset($param['social']) ){
            $this->db->order_by('a.createDate', 'asc');
        }else{
            $this->db->order_by('a.createDate', 'desc');
        }
            

         

    }

    public function get_uplode($param) 
    {
        $this->_condition_uplode($param);
        $query = $this->db
                        ->select('a.*,b.*')
                        ->from('upload a')
                        ->join('upload_content b', 'a.uploadId = b.uploadId', 'left')
                        ->get();
        return $query;
    }

    private function _condition_uplode($param) 
    {

        $this->db->where('a.grpContent', $param['grpContent']);
        
        if ( isset($param['contentId']) ) 
             $this->db->where('b.contentId', $param['contentId']);

        if ( isset($param['grpType']) ) 
             $this->db->where('b.grpType', $param['grpType']);

    }
        
}