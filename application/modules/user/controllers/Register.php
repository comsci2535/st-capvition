<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MX_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_m');
        $this->load->library('encryption');
        $this->load->model('user_m');
        
    }


    public function index()
    {

        $this->load->module('front');
        $data['frmAction'] = site_url("user/register/save");
        $data['contentView'] = 'user/register/form';
        $data['pageScript'] = 'assets/scripts/user/register/index.js';
        
        $this->front->layout($data);
    }
    public function save()
    {

        $input = $this->input->post();

	    $ip = $_SERVER['REMOTE_ADDR'];

	    
    	if($this->input->post('robot') != null){
    		$captcha = "ok";
    		$responseKeys["success"] = 1;
    	} else {
    		$captcha = false;
    		$responseKeys["success"] = false;
    	}


	    if(intval($responseKeys["success"]) !== 1||!$captcha) {
		    echo json_encode(array('info_txt'=>'error','msg' => '* Captcha error','msg2'=>'กรุณาลองใหม่อีกครั้ง!'));
	    } else {
        
	        $username=$this->check_username($input['username']);
	        //arr($username);exit();
	        if($username == 1){

	            $resp_msg = array('info_txt'=>"error",'msg'=>'พบ Username ซ้ำในระบบ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
	            echo json_encode($resp_msg);
	            return false;
	            
	        }else{

                $value = $this->_build_data($input);
                $userId = $this->user_m->insert($value);
                if ( $userId ) {
                    $input_l['username']=$value['username'];
                    $info = $this->login_m->get_by_username($input_l);
                    $this->check_coupon($userId);

                    
                    //if ( $info['password'] == $value['password'] ) {
                        $image = base_url('uploads/user.png');
                        $upload = Modules::run('admin/upload/get_upload', $info['userId'], 'user', 'coverImage');
                        if ( $upload->num_rows() != 0 ) {
                            $row = $upload->row();
                            if ( is_file("{$row->path}/{$row->filename}") )
                                $image = base_url("{$row->path}{$row->filename}");
                        }
                        $member['userId'] = $info['userId'];
                        $member['name'] = $info['firstname'];//." ".$info['lastname'];
                        $member['image'] = $image;
                        $member['email'] = $info['email'];
                        $member['sectionId'] = $info['sectionId'];
                        $member['partyId'] = $info['partyId'];
                        $member['positionId'] = $info['positionId'];
                        $member['degree'] = $info['degree'];
                        $member['type'] = $info['type'];
                        $member['policyId'] = $info['policyId'];
                        $member['isBackend'] = TRUE;
                        $this->session->set_userdata('member', $member);
                        $this->session->set_flashdata('firstTime', '1');
                        $this->login_m->update_last_login();

                    //}

                     $resp_msg = array('info_txt'=>"success",'msg'=>'สมัครสมาชิกสำเร็จ','msg2'=>'กรุณารอสักครู่...');
                        echo json_encode($resp_msg);
                        return false;
                } else {
                    $resp_msg = array('info_txt'=>"error",'msg'=>'สมัครสมาชิกไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
                    echo json_encode($resp_msg);
                    return false;
                }

            }

	        
	    }
    }

    public function success()
    {

        $this->load->module('front');
        
        $data['contentView'] = 'user/register/success';
        
        
        $this->front->layout($data);
    }

    private function _build_data($input) {
        
        $value['firstname'] = $input['fname'];
        $value['lastname'] = $input['lname'];
        $value['phone'] = $input['tel'];
        $value['email'] = $input['email'];
        $value['username'] = $input['username'];
        $value['oauth_provider'] = 'register';
        $value['createDate'] = db_datetime_now();
        $value['password'] = $this->encryption->encrypt($input['password']);
        $value['type'] = "member";
        $value['verify'] = 1;
        $value['updateDate'] = db_datetime_now();
        $value['updateBy'] = $this->session->user['userId'];
       
        return $value;
    }

    public function check_username($username)
    {
        
        $input['recycle'] = array(0,1);
        $input['username'] = $username;
        $info = $this->user_m->get_rows($input);
        //arrx($info->num_rows());
        if ( $info->num_rows() > 0 ) {         
            $rs = 1;
        } else {
            $rs =  0;
        }
        return $rs;
    }

    private function check_coupon($userId)
    {
        //arrx($oauth_uid);
        
        $input['userId'] = $userId;
        $info = $this->user_m->get_rows($input)->row();
        // arr($info->result());exit();
        //arrx($info->num_rows());
        if ( $info->couponCode =="") {         
           $chars = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
            $res = "";
            for ($i = 0; $i < 7; $i++) {
                $res .= $chars[mt_rand(0, strlen($chars)-1)];
            }

            $v['couponCode']=$res.$userId;
            $this->user_m->update($userId,$v);
        } 
    }



}