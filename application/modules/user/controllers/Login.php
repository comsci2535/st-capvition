<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_m');
        $this->load->model('user_m');
        $this->load->library('encryption');
        $this->load->library('facebook');
        // $this->load->library('facebookSDK');
    }
    
    public function index()
    {

         $this->load->view('formLogin',$data); 
    }
    
    public function do_login()
    {
        $input = $this->input->post();
        $info = $this->login_m->by_username($input['username']);

        if ( empty($info) ) {

            $rs['info_txt'] = "error";
            $rs['text'] = "ไม่พบผู้ใช้งาน: {$input['username']}";
        } else {
            $password = md5($input['password']);
            if ( $password == $info['memberPassword'] ) {
                if ( $info['memberVerifyStatus'] == 0 ) {
                    

                    $rs['info_txt'] = "error";
                    $rs['text'] = "กรุณายืนยันการสมัครสมาชิกทางอีเมล์";
                } else if ( $info['memberActive'] == 0 ) {
                    

                    $rs['info_txt'] = "error";
                    $rs['text'] = "ถูกระงับการใช้งาน";
                } else {
                    
                    $rs['info_txt'] = "success";
                    $rs['text'] = "";
                    $this->_set_member($info);
                }
            } else {
                

                $rs['info_txt'] = "error";
                $rs['text'] = "รหัสผ่านไม่ถูกต้อง";
            }
            
        }   
       
            echo json_encode($rs);
            return false;
    }
    
    private function _set_member($member)
    {
        $value['memberId'] = $member['memberId'];
        $value['firstname'] = $member['memberName'];
        $value['lastname'] = $member['memberSurname'];
        $value['email'] = $member['memberEmail'];
        $value['username'] = $member['memberUsername'];
        $value['name'] = $member['memberName']." ".$member['memberSurname'];
        $value['memberFullName'] = $member['memberName'].' '.$member['memberSurname'];
        $this->session->set_userdata('member', $value);
        $this->session->set_flashdata('showMember','1');
        $value = array();
        $value['memberLastLogin'] = db_datetime_now();
        $this->login_m->update($value, $member['memberId']);
        return true;
    }
    
    
    public function _build_member($input)
    {
        $values = array();
        $dob = NULL;
        if ($input['bod']) {
            $bod = db_date($input['bod'][0].'-'.$input['bod'][1].'-'.$input['bod'][2]);
        }
        $values['firstname'] = html_escape($input['firstname']);
        $values['lastname'] = html_escape($input['lastname']);
        $values['email'] = $input['email'];
        $values['mobile'] = html_escape($input['mobile']);
        $values['gender'] = ($input['gender']);
        $values['occupation'] = html_escape($input['ocupation']);
        $values['country'] = html_escape($input['country']);
        $values['province'] = html_escape($input['province']);
        $values['district'] = html_escape($input['city']);
        $values['zip'] = html_escape($input['post_code']);
        $values['address'] = html_escape($input['address']);
        $values['bod'] = $bod;
        if ( $input['mode'] == 'create' ) {
            $values['createDate'] = db_datetime_now();  
        } else {
            $values['modifyDate'] = $values['lastLogin'] = db_datetime_now();
            $values['modifyBy'] = (int)$input['id'];
        }
        return($values);
    }


    public function check() {
        $input = $this->input->post();
        $info_ = $this->login_m->get_by_username($input);

        
        if (!empty($info_)) {
            $this->check_coupon($info_['userId']);
            $info = $this->login_m->get_by_username($input);
            if ($info['verify'] == 1 || $info['type'] == 'developer') {
                if ($info['active'] == 1 || $info['type'] == 'developer') {
                    $pass = $this->encryption->decrypt($info['password']);
                    if ( $input['password'] == $pass ) {

                        $user_login_status_exist=$info['user_login_status'];  // เก็บสถานะล็อกอิน
                        $user_datetime_using=$info['user_datetime_using']; // เก็บเวลาที่ใช้อยู่ล่าสุด
                        
                        // ถ้ามีผู้ใช้ค้างสถานะล็อกอินชื่อนี้อยู่
                        if($user_login_status_exist==1){     
                            // arr(time());exit();         
                            // ถ้าเวลาที่ใช้อยู่ บวกอีก 10 วินาที มากกว่าหรือเท่ากับเวลาในปัจจุบัน
                            //if(time() <= strtotime(date("Y-m-d H:i:s",strtotime($user_datetime_using." +20 minutes"))) ){
 
                            // $resp_msg = array('info_txt'=>"error",'msg'=>'ผู้ใช้นี้ กำลังใช้งานอยู่ !!!' ,'msg2'=>'');   


                           // }else{ // ถ้าน้อยกว่า หรือไม่ได้ใช้งานแล้ว
                               
                                $this->db->where('id', $info['session_id'])->delete('ci_sessions');

                                $image = base_url('uploads/user.png');
                                $upload = Modules::run('admin/upload/get_upload', $info['userId'], 'user', 'coverImage');
                                if ( $upload->num_rows() != 0 ) {
                                    $row = $upload->row();
                                    if ( is_file("{$row->path}/{$row->filename}") )
                                        $image = base_url("{$row->path}{$row->filename}");
                                }
                                $data['error'] = false;
                                $data['message'] = site_url();
                                $member['userId'] = $info['userId'];
                                $member['name'] = $info['firstname'];//." ".$info['lastname'];
                                $member['fullname'] = $info['firstname']." ".$info['lastname'];
                                $member['image'] = $image;
                                $member['email'] = $info['email'];
                                $member['sectionId'] = $info['sectionId'];
                                $member['partyId'] = $info['partyId'];
                                $member['positionId'] = $info['positionId'];
                                $member['degree'] = $info['degree'];
                                $member['type'] = $info['type'];
                                $member['policyId'] = $info['policyId'];
                                $member['couponCode'] = $info['couponCode'];
                                $member['isBackend'] = TRUE;
                                $this->session->set_userdata('member', $member);
                                $this->session->set_flashdata('firstTime', '1');
                                $this->login_m->update_last_login();

                              

                                 $resp_msg = array('info_txt'=>"success",'msg'=>'','msg2'=>'');                                                
                            //}
                        }else{ // ถ้า ไม่มี  ผู้ใช้ล็อกอินชื่อนี้อยู่
                                

                                $image = base_url('uploads/user.png');
                                $upload = Modules::run('admin/upload/get_upload', $info['userId'], 'user', 'coverImage');
                                if ( $upload->num_rows() != 0 ) {
                                    $row = $upload->row();
                                    if ( is_file("{$row->path}/{$row->filename}") )
                                        $image = base_url("{$row->path}{$row->filename}");
                                }
                                $data['error'] = false;
                                $data['message'] = site_url();
                                $member['userId'] = $info['userId'];
                                $member['name'] = $info['firstname'];//." ".$info['lastname'];
                                 $member['fullname'] = $info['firstname']." ".$info['lastname'];
                                $member['image'] = $image;
                                $member['email'] = $info['email'];
                                $member['sectionId'] = $info['sectionId'];
                                $member['partyId'] = $info['partyId'];
                                $member['positionId'] = $info['positionId'];
                                $member['degree'] = $info['degree'];
                                $member['type'] = $info['type'];
                                $member['policyId'] = $info['policyId'];
                                $member['couponCode'] = $info['couponCode'];
                                $member['isBackend'] = TRUE;
                                $this->session->set_userdata('member', $member);
                                $this->session->set_flashdata('firstTime', '1');
                                $this->login_m->update_last_login();

                                

                                $resp_msg = array('info_txt'=>"success",'msg'=>'','msg2'=>'');             
                        }




                        
                    } else {
                        // $data['error'] = true;
                        // $data['message'] = "รหัสผ่านไม่ถูกต้อง";
                        $resp_msg = array('info_txt'=>"error",'msg'=>'รหัสผ่านไม่ถูกต้อง','msg2'=>'');
                       
                    }
                } else {
                    // $data['error'] = true;
                    // $data['message'] = "บัญชีของท่านถูกระงับการใช้งาน";
                    $resp_msg = array('info_txt'=>"error",'msg'=>'บัญชีของท่านถูกระงับการใช้งาน','msg2'=>'');
                }
            } else {
                // $data['error'] = true;
                // $data['message'] = 'กรุณายืนยันตัวตนอีเมล์ <a href="">ส่งอีกครั้ง</a>';

                $resp_msg = array('info_txt'=>"error",'msg'=>'กรุณายืนยันตัวตนอีเมล์','msg2'=>'');
            }
        } else {
            // $data['error'] = true;
            // $data['message'] = 'ไม่พบบัญขีของท่านกรุณาลงทะเบียน';

            $resp_msg = array('info_txt'=>"error",'msg'=>'ไม่พบบัญขีของท่านกรุณาลงทะเบียน','msg2'=>'');

        }

         echo json_encode($resp_msg);
         return false;
        // $this->output
        //         ->set_content_type('application/json')
        //         ->set_output(json_encode($data));
    }
    public function checkloginuser(){

            if(isset($this->session->member['userId']) && $this->session->member['userId']!="")
            {
                // ให้อัพเดทเวลาที่ใช้อยู่ปัจจุบัน กับ สถานะการล็อกอิน
                $this->login_m->update_last_login_2();
                echo date("Y-m-d H:i:s");   
            }else{ // ถ้าไม่ได้ใช้แล้วหรือล็อกเอาท์หรืออื่นๆ ส่งค่ากลับเป็น 0
                echo 0;
            }
    }

    public function updateCommunity(){

          $value['isCommunity'] = 1;
          $this->user_m->update($this->session->member['userId'],$value);
    }

    public function check_facebook(){
        $url="home";
        if(isset($this->session->urlreffer['url']) && $this->session->urlreffer['url']!=""){
            $url=$this->session->urlreffer['url'];
        }
        
        $userProfile = array();
        if($this->facebook->is_authenticated()){
            $userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture.width(200).height(200)');
            
           //  arr($userProfile);exit();

             $input_ = array(
                        'oauth_uid' => $userProfile['id'] , 
                        'fname' => $userProfile['first_name'] , 
                        'lname' => $userProfile['last_name'] ,
                        'email' => !empty($userProfile['email']) ? $userProfile['email'] : "",
                        'picture' => $userProfile['picture']['data']['url']
                    );
           
            $oauth_uid=$this->check_oauth_uid($input_['oauth_uid']);
            
            
            if($oauth_uid == 0){

               $value = $this->_build_data($input_);
               $userId = $this->user_m->insert($value);
                
            }
            $input_u['oauth_uid'] = $userProfile['id'];

            $image = base_url('uploads/user.png');
            $info_ = $this->login_m->get_by_oauth_uid($input_u);
            $this->check_coupon($info_['userId']);
            $this->facebook->destroy_session();

            $info = $this->login_m->get_by_oauth_uid($input_u);
            
            
            $user_login_status_exist=$info['user_login_status'];  // เก็บสถานะล็อกอิน
            $user_datetime_using=$info['user_datetime_using']; // เก็บเวลาที่ใช้อยู่ล่าสุด
            
            // ถ้ามีผู้ใช้ค้างสถานะล็อกอินชื่อนี้อยู่
            if($user_login_status_exist==1){     
                   //    arr(time());exit();         
                // ถ้าเวลาที่ใช้อยู่ บวกอีก 10 วินาที มากกว่าหรือเท่ากับเวลาในปัจจุบัน +2 second
                //if(time() <= strtotime(date("Y-m-d H:i:s",strtotime($user_datetime_using." +20 minutes"))) ){
                     // $resp_msg = array('info_txt'=>"error",'msg'=>'ชื่อล็อกอินนี้ กำลังใช้งานอยู่ !!!' ,'msg2'=>'');   
                    // echo "<script>alert('ชื่อล็อกอินนี้ กำลังใช้งานอยู่ !!!');</script>";
                    //redirect('user/login/user_not_found');    
                    //$this->load->view('user/formLogin');

                //}else{ // ถ้าน้อยกว่า หรือไม่ได้ใช้งานแล้ว
                    $this->db->where('id', $info['session_id'])->delete('ci_sessions');
                    $value2 = $this->_build_data2($input_);
                    $this->user_m->update($info['userId'],$value2);
                    
                    $upload = Modules::run('admin/upload/get_upload', $info['userId'], 'user', 'coverImage');
                    if ( $upload->num_rows() != 0 ) {
                        $row = $upload->row();
                        if ( is_file("{$row->path}/{$row->filename}") )
                            $image = base_url("{$row->path}{$row->filename}");
                    }else{
                        if($info['picture']!=""){
                             $image = $info['picture'];
                        }else{
                            $image = $input_['picture'];
                        }
                        
                    }
                    $member['userId'] = $info['userId'];
                    $member['name'] = $info['firstname'];//." ".$info['lastname'];
                     $member['fullname'] = $info['firstname']." ".$info['lastname'];
                    $member['email'] = $info['email'];
                    $member['type'] = $info['type'];
                    $member['couponCode'] = $info['couponCode'];
                    $member['image'] = $image;

                    
                    
                    $this->session->set_userdata('member', $member);
                    $this->session->set_flashdata('firstTime', '1');
                    $this->login_m->update_last_login();
                    $this->session->unset_userdata('urlreffer');

                    redirect($url);                                                
                //}
            }else{ // ถ้า ไม่มี  ผู้ใช้ล็อกอินชื่อนี้อยู่
                    
                    $value2 = $this->_build_data2($input_);
                    $this->user_m->update($info['userId'],$value2);

                    $upload = Modules::run('admin/upload/get_upload', $info['userId'], 'user', 'coverImage');
                    if ( $upload->num_rows() != 0 ) {
                        $row = $upload->row();
                        if ( is_file("{$row->path}/{$row->filename}") )
                            $image = base_url("{$row->path}{$row->filename}");
                    }else{
                        if($info['picture']!=""){
                             $image = $info['picture'];
                        }else{
                            $image = $input_['picture'];
                        }
                        
                    }
                    $member['userId'] = $info['userId'];
                    $member['name'] = $info['firstname'];//." ".$info['lastname'];
                     $member['fullname'] = $info['firstname']." ".$info['lastname'];
                    $member['email'] = $info['email'];
                    $member['type'] = $info['type'];
                    $member['couponCode'] = $info['couponCode'];
                    $member['image'] = $image;

                    
                    
                    $this->session->set_userdata('member', $member);
                    $this->session->set_flashdata('firstTime', '1');
                    $this->login_m->update_last_login();
                    $this->session->unset_userdata('urlreffer');

                    redirect($url);            
            }

            

        }
        else
        {
            $data['authUrl'] =  $this->facebook->login_url();
            redirect($url);     
        }
        //$this->load->view('login',$data);
    }

    private function _build_data($input) {
       
        
        $value['firstname'] = $input['fname'];
        $value['lastname'] = $input['lname'];
        $value['oauth_uid'] = $input['oauth_uid'];
        $value['email'] = $input['email'];
        $value['picture'] = $input['picture'];
        $value['oauth_provider'] = 'facebook';
        $value['createDate'] = db_datetime_now();
        //$value['password'] = $this->encryption->encrypt($input['password']);
        $value['type'] = "member";
        $value['verify'] = 1;
        
        return $value;
    }

    private function _build_data2($input) {
       
        
        // $value['firstname'] = $input['fname'];
        // $value['lastname'] = $input['lname'];
        $value['picture'] = $input['picture'];
        
        return $value;
    }

    public function check_oauth_uid($oauth_uid)
    {
        //arrx($oauth_uid);
        $input['recycle'] = array(0,1);
        $input['oauth_uid'] = $oauth_uid;
        $info = $this->user_m->get_rows($input);
        // arr($info->result());exit();
        //arrx($info->num_rows());
        if ( $info->num_rows() > 0 ) {         
            $rs = 1;
        } else {
            $rs =  0;
        }
        return $rs;
    }

    private function check_coupon($userId)
    {
        //arrx($oauth_uid);
        
        $input['userId'] = $userId;
        $info = $this->user_m->get_rows($input)->row();
        // arr($info->result());exit();
        //arrx($info->num_rows());
        if ( $info->couponCode =="") {         
           $chars = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
            $res = "";
            for ($i = 0; $i < 7; $i++) {
                $res .= $chars[mt_rand(0, strlen($chars)-1)];
            }

            $v['couponCode']=$res.$userId;
            $this->user_m->update($userId,$v);
        } 
    }

     public function user_not_found()
    {

        $this->load->view('user/formLogin');

    } 

            
    
}
