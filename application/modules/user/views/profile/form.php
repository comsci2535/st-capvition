<style type="text/css">
	.span-d{
		cursor: pointer;
    color: #eceeef;
    text-align: center;
    padding: 0px 8px;
    text-decoration: none;
    /* font-size: 14px; */
    margin: 2px;
    border-radius: 25px;
    background-color: #ff0000;
	}
</style>
<section class="section blog-area">
	<div class="container">
		<div class="row">
			<div class=" col-sm-12 text-center">
                <span class="topnav-article">
                  <a class="active"  href="<?php echo site_url("user/profile");?>">ข้อมูลส่วนตัว</a>
                  <a  href="<?php echo site_url("user/profile/deposit");?>">เงินสะสม/ประวัติการถอน</a>
                 <!--  <a   href="<?php echo site_url("user/profile/deposit");?>">ประวัติการถอน</a> -->
                </span>
            </div>
			<div class="col-lg-3 col-md-3">
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="blog-register">
					<div class="title">
						<!-- <h3>ข้อมูลส่วนตัว</h3> -->
						<!-- <div class="separator"></div> -->
					</div>
					<div class="row" style="padding-top: 25px;">
						<?php echo form_open($frmAction, array('class' => '', 'id'=>'frm-save' , 'method' => 'post')) ?>
						     <div class="img-profile">
						      <img alt="K SME" src="<?php echo $this->session->member['image'];?>">
						    </div>
							<ul class="form-style-1">
								<li>
							        <label><span style="cursor: pointer;" data-toggle="tooltip" data-placement="bottom"
       title="" data-original-title="รหัสคูปองส่วนลด 200 บาท สำหรับเพื่อนของคุณ หากใครใช้รหัสนี้ในการสมัครคอร์ส คุณจะได้รับทันที 20% จากค่าสมัคร"
       class="red-tooltip">รหัสสมาชิก </span> <span data-toggle="tooltip" data-placement="bottom"
       title="" data-original-title="รหัสคูปองส่วนลด 200 บาท สำหรับเพื่อนของคุณ หากใครใช้รหัสนี้ในการสมัครคอร์ส คุณจะได้รับทันที 20% จากค่าสมัคร"
       class="red-tooltip span-d" > 
	?</span></label> 
							        <input type="text"  class="field-long" value="<?php echo $profile->couponCode; ?>" disabled/>
							         
							        <div class="alert_username"></div>
							    </li>
								<?php if($profile->oauth_provider!="facebook"){ ?>
								 <li>
							        <label>ชื่อผู้ใช้งาน/Username </label>
							        <input type="text" name="username" id="username" class="field-long" value="<?php echo $profile->username; ?>" disabled/>
							        <div class="alert_username"></div>
							    </li>
							     <li>
							        <label>รหัสผ่านเดิม </label>
							        <input type="password" name="passwordOld" id="passwordOld" class="field-long" />
							         <div class="alert_passwordOld"></div>
							    </li>
							    <li>
							        <label>รหัสผ่านใหม่ </label>
							        <input type="password" name="password" id="password" class="field-long" />
							         <div class="alert_password"></div>
							    </li>
							     <li>
							        <label>ยืนยันรหัสผ่าน </label>
							        <input type="password" name="passwordConf" id="passwordConf" class="field-long" />
							        <div class="alert_passwordConf"></div>
							    </li>
							    <input type="hidden" name="ff" id="ff" value="1" />
							    <?php }else{ ?>
							    	 <input type="hidden" name="ff" id="ff" value="" />
							    <?php } ?>
							    <li>
							    	
							    </li>
							    <li><label>ชื่อ-นามสกุล <span class="required">*</span></label>
							    	<input type="text" name="fname" id="fname" class="field-divided" placeholder="ชื่อ" value="<?php echo $profile->firstname; ?>"/>&nbsp;<input type="text" name="lname" id="lname" class="field-divided" placeholder="นามสกุล" value="<?php echo $profile->lastname; ?>"/>
							    	<div class="alert_name"></div>
							    </li>
							    <li>
							        <label>Email</label>
							        <input type="email" name="email" id="email" class="field-long" value="<?php echo $profile->email; ?>"/>
							    </li>
							    <li>
							        <label>เบอร์โทรศัพท์ </label>
							        <input type="text" name="tel" class="field-long" value="<?php echo $profile->phone; ?>"/>
							    </li>
							     <li>
							     	<input type="hidden" name="userId" id="userId" value="<?php echo $profile->userId; ?>" />
							     	<input type="hidden" name="mode" value="create" />
							          <div id="html_element" style="margin-bottom: 10px"></div>
					                  <input type="hidden" name="robot"  class="form-control">
					                 <div id="form-success-div" class="text-success"></div> 
                                    <div id="form-error-div" class="text-danger"></div>
					             </li>
							    <li>
							        <button type="submit" class="btn btn-flat btn-register"><span id="form-img-div"></span> แก้ไขข้อมูล</button>
							    </li>
							</ul>
						<?php echo form_close() ?>
					</div><!-- row -->
				</div><!-- blog-posts -->
			</div><!-- col-lg-4 -->

			<div class="col-lg-3 col-md-3">
			</div>
		</div><!-- row -->
	</div><!-- container -->
</section><!-- section -->