<style>
#customers-t {
    border-collapse: collapse;
    width: 100%;
}

#customers-t td, #customers-t th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers-t tr:nth-child(even){background-color: #f2f2f2;}

#customers-t tr:hover {background-color: #ddd;}

#customers-t th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
</style>
<section class="section blog-area">
	<div class="container">
		<div class="row">
			<div class=" col-sm-12 text-center">
                <span class="topnav-article">
                  <a  href="<?php echo site_url("user/profile");?>">ข้อมูลส่วนตัว</a>
                  <a class="active"  href="<?php echo site_url("user/profile/deposit");?>">เงินสะสม/ประวัติการถอน</a>
                 <!--  <a   href="<?php echo site_url("user/profile/deposit");?>">ประวัติการถอน</a> -->
                </span>
            </div>
            <div class="" >
				<span class="copy-container" style="padding-left: 17px;">
                            <form action="">
                              <div style="font-weight: 600;font-size: 1.2em;">คัดลอกลิงก์เพื่อแชร์</div>
                              <span data-toggle="tooltip" data-placement="bottom"
       title="" data-original-title="แชร์แล้วหากใครคลิกลิงค์นี้ แล้วสมัครคอร์สภายในเว็บ รับเงินทันที 20% จากค่าสมัคร"
       class="red-tooltip" ><input  type="text"  name="codeId" id="codeId" value="<?php echo site_url().'?code='.$this->session->member['couponCode'];?>" disabled></span>
                              <button type="button" data-toggle="tooltip" data-placement="bottom"
       title="" data-original-title="แชร์แล้วหากใครคลิกลิงค์นี้ แล้วสมัครคอร์สภายในเว็บ รับเงินทันที 20% จากค่าสมัคร"
       class="red-tooltip" onclick="myFunctionCopy()"><i class="fa fa-copy"></i></button>
                            </form>
                          </span>
                          
		    </div>
		   
			
			<div class="col-12">

					<div  style="padding-top: 25px;">
						

						<div class="t-pc">
							<table id="customers-t" <div style="padding-top: 20px; max-height:500px;overflow-y: scroll;">
								<tr>
									<th class="text-center">เลขที่อ้างอิง</th>
									<th class="text-center">วันเวลาทำรายการ</th>
									<th class="text-center">ประเภทรายการ</th>
									<th class="text-center">จำนวนเงินเข้า/ถอน</th>
									<th class="text-center">สถานะ</th>
								</tr>
								<?php foreach ($info as $key => $value) { ?>
								
								<tr>
									<td class="text-center"><?php echo $value['code']; ?></td>
									<td class="text-center"><?php echo $value['date']; ?></td>
									<td class="text-center"><?php echo $value['action']; ?></td>
									<td class="text-center"><?php echo $value['amount']; ?></td>
									<td class="text-success text-center"><?php echo $value['status']; ?></td>
								</tr>
							    <?php } ?>
							    <tr>
									<td class="text-center" colspan="3"><b>รวมเงินสะสม</b></td>
									
									<td class="text-center"><b><?php echo number_format($amount); ?></b></td>
									<td class="text-success text-center">
										 <a   class="btn various5 fancybox.iframe"  href="<?php echo site_url('user/profile/withdraw');?>" >ถอนเงิน
										 </a>
									</td>
								</tr>

							</table>
						</div>
						<div class="t-mobile">
							<div style="padding-top: 20px;max-height:500px;overflow-y: scroll;">
								<?php foreach ($info as $key => $value) { ?>
								<div class="row" >
									<div class="col-12 col-md-6" >
										<span style="font-weight: 700;">เลขที่อ้างอิง : </span>
									<!-- </div>
									<div class="col-5 col-md-6"> -->
										<?php echo $value['code']; ?>
									</div>
								</div>
								<div class="row" >
									<div class="col-12 col-md-6" >
										<span style="font-weight: 700;">วันเวลาทำรายการ : </span>
									<!-- </div>
									<div class="col-5 col-md-6"> -->
										<?php echo $value['date']; ?>
									</div>
								</div>
								<div class="row" >
									<div class="col-12 col-md-6" >
										<span style="font-weight: 700;">ประเภทรายการ : </span>
									<!-- </div>
									<div class="col-5 col-md-6"> -->
										<?php echo $value['action']; ?>
									</div>
								</div>
								<div class="row" >
									<div class="col-12 col-md-6" >
										<span style="font-weight: 700;">จำนวนเงินเข้า/ถอน : </span>
									<!-- </div>
									<div class="col-5 col-md-6"> -->
										<?php echo $value['amount']; ?>
									</div>
								</div>
								<div class="row" >
									<div class="col-12 col-md-6" >
										<span style="font-weight: 700;">สถานะ : </span>
									<!-- </div>
									<div class="col-5 col-md-6"> -->
										<?php echo $value['status']; ?>
									</div>
								</div>
								<br>
								 <?php } ?>
								 <div class="row" style="padding-top: 25px" >
									<div class="col-12 col-md-6" >
										<span style="font-weight: 700;">รวมเงินสะสม : </span>
									<!-- </div>
									<div class="col-5 col-md-6"> -->
										<?php echo number_format($amount); ?>
									</div>
								</div>
								 <div class="row" style="padding-top: 25px" >
									<div class="col-12 col-md-12" >
										 <a   class="btn various5 fancybox.iframe"  href="<?php echo site_url('user/profile/withdraw');?>" >ถอนเงิน
										 </a>
									</div>
									
								</div>
							
						    </div>
							</div>
					   </div>
			</div><!-- col-lg-4 -->

			
		</div><!-- row -->
	</div><!-- container -->
</section><!-- section -->

<script type="text/javascript">
  	function myFunctionCopy() {
	  /* Get the text field */
	  var copyText = document.getElementById("codeId");

	  /* Select the text field */
	  copyText.select();

	  /* Copy the text inside the text field */
	  document.execCommand("copy");

	  copyStringToClipboard(copyText.value);

	  /* Alert the copied text */
	  //alert("Copied the text: " + copyText.value);
	}
	function copyStringToClipboard (str) {
	   // Create new element
	   var el = document.createElement('textarea');
	   // Set value (string to be copied)
	   el.value = str;
	   // Set non-editable to avoid focus and move outside of view
	   el.setAttribute('readonly', '');
	   el.style = {position: 'absolute', left: '-9999px'};
	   document.body.appendChild(el);
	   // Select text inside element
	   el.select();
	   // Copy text to clipboard
	   document.execCommand('copy');
	   // Remove temporary element
	   document.body.removeChild(el);
	}
  </script>