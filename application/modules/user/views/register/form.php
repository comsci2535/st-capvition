<section class="section blog-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3">
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="blog-register">
					<div class="title">
						<h3>สมัครสมาชิก</h3>
						<!-- <div class="separator"></div> -->
					</div>
					<div class="row">
						<?php echo form_open($frmAction, array('class' => '', 'id'=>'frm-save' , 'method' => 'post')) ?>
							<ul class="form-style-1">
								 <li>
							        <label>ชื่อผู้ใช้งาน/Username <span class="required">*</span></label>
							        <input type="text" name="username" id="username" class="field-long" />
							        <div class="alert_username"></div>
							    </li>
							     <li>
							        <label>รหัสผ่าน <span class="required">*</span></label>
							        <input type="password" name="password" id="password" class="field-long" />
							       <!--  <span class="required">* ต้องไม่น้อยกว่า 8 ตัวอักษร ประกอบด้วย a-z และ 0-9</span> -->
							         <div class="alert_password"></div>
							    </li>
							     <li>
							        <label>ยืนยันรหัสผ่าน <span class="required">*</span></label>
							        <input type="password" name="passwordConf" id="passwordConf" class="field-long" />
							        <div class="alert_passwordConf"></div>
							    </li>
							    <li><label>ชื่อ-นามสกุล <span class="required">*</span></label>
							    	<input type="text" name="fname" id="fname" class="field-divided" placeholder="ชื่อ" />&nbsp;<input type="text" name="lname" id="lname" class="field-divided" placeholder="นามสกุล" />
							    	<div class="alert_name"></div>
							    </li>
							    <li>
							        <label>Email</label>
							        <input type="email" name="email" id="email" class="field-long" />
							    </li>
							    <li>
							        <label>เบอร์โทรศัพท์ </label>
							        <input type="text" name="tel" class="field-long" />
							    </li>
							     <li>
							     	<input type="hidden" name="mode" value="create" />
							          <div id="html_element" style="margin-bottom: 10px"></div>
					                  <input type="hidden" name="robot"  class="form-control">
					                 <div id="form-success-div" class="text-success"></div> 
                                    <div id="form-error-div" class="text-danger"></div>
					             </li>
							    <li>
							        <span id="form-img-div"><button type="submit" class="btn btn-flat btn-register">สมัครสมาชิก</button></span> 
							    </li>
							</ul>
						<?php echo form_close() ?>
					</div><!-- row -->
				</div><!-- blog-posts -->
			</div><!-- col-lg-4 -->

			<div class="col-lg-3 col-md-3">
			</div>
		</div><!-- row -->
	</div><!-- container -->
</section><!-- section -->