<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>404 Page not found</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/Ionicons/css/ionicons.min.css">
<link rel="stylesheet" href="<?php echo base_url("assets/") ?>dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url("assets/") ?>dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
    body{
      font-family: 'Kanit';
    }
  </style>
</head>
<body class="layout-top-nav" >


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   

    <!-- Main content -->
    <section class="content">
      <div style="padding-top: 100px;"></div>
     
        

        <div class="error-content text-center">
          <!-- <h1 class="headline text-yellow" style="font-size: 70px"> <img src="<?php echo base_url("assets/website/") ?>images/logo.jpg" class="ls-bg" alt="" style="width: 45px;"/> <a href="<?php echo base_url() ?>"><b>The CAP Vision Academy</b></h1> -->
          <h3 style="font-family: Kanit;"><i class="fa fa-warning text-yellow"></i> ขออภัย <br><br> ผู้ใช้นี้ กำลังใช้งานอยู่!</h3>
          <br>
          
          <p>
             <a class="btn btn-info btn-flat" href="<?php echo site_url();?>">กลับไปหน้าหลัก</a>
             
          </p>

         <!--  <form class="search-form">
            <div class="input-group">
              <input type="text" name="search" class="form-control" placeholder="Search">

              <div class="input-group-btn">
                <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
                </button>
              </div>
            </div>
           
          </form> -->
       
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  




</body>
</html>
