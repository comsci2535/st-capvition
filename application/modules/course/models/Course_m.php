<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Course_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

      public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                         ->select('b.title as instructor_name ')
                        ->from('course a')
                        ->join('instructor b','b.instructorId=a.instructorId','left')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                         ->select('b.title as instructor_name ')
                        ->from('course a')
                        ->join('instructor b','b.instructorId=a.instructorId','left')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.createDate,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updateDate,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        

        if ( isset($param['title_link']) ) {
            $this->db->like('a.title_link', $param['title_link']);
        }

         if ( isset($param['title']) ) 
            $this->db->where('a.title', $param['title']);
        
        if ( isset($param['courseId']) ) 
            $this->db->where('a.courseId', $param['courseId']);

        if ( isset($param['recycle']) ){
            if (is_array($param['recycle'])) {
                $this->db->where_in('a.recycle', $param['recycle']);
            } else {
                $this->db->where('a.recycle', $param['recycle']);
            }
           
        }

        $this->db->order_by('a.recommend', 'DESC');
       
        $this->db->order_by('a.updateDate', 'DESC');
         $this->db->order_by('a.createDate', 'DESC');


    }

    public function plus_view($id)
    {
        $sql = "UPDATE course SET view = (view+1) WHERE courseId=?";
        $this->db->query($sql, array($id));
    }
    
    public function get_uplode($param) 
    {
        $this->_condition_uplode($param);
        $query = $this->db
                        ->select('a.*,b.*')
                        ->from('upload a')
                        ->join('upload_content b', 'a.uploadId = b.uploadId', 'left')
                        ->get();
        return $query;
    }

    private function _condition_uplode($param) 
    {

        $this->db->where('a.grpContent', $param['grpContent']);
        if ( isset($param['contentId']) ) 
             $this->db->where('b.contentId', $param['contentId']);

        if ( isset($param['grpType']) ) 
             $this->db->where('b.grpType', $param['grpType']);

    }

    public function get_rows_instructor($param) 
    {
        $this->_condition_instructor($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('instructor a')
                        ->get();
        return $query;
    }

 

    private function _condition_instructor($param) 
    {   
           
        
        $this->db->where('a.active', 1);
                
        if ( isset($param['instructorId']) ) 
            $this->db->where('a.instructorId', $param['instructorId']);

        $this->db->where('a.recycle', 0);

    }

    public function get_promotion_rows($param) 
    {
        $this->_condition_promotion($param); 
          
        $query = $this->db
                        ->select('a.*')
                        ->from('promotion a')
                        ->get();
        return $query;
    }

    private function _condition_promotion($param) 

    {   
   
        $this->db->where("((a.startDate ='0000-00-00' AND a.endDate ='0000-00-00') OR  (a.startDate <='".date('Y-m-d')."' AND a.endDate >='".date('Y-m-d')."'))");
       
       if ( isset($param['courseId']) ) 
            $this->db->where('a.courseId', $param['courseId']);


        $this->db->where('a.active', 1);
               
        $this->db->where('a.recycle',0);

       

    }
    public function get_promotion_rows2($param) 
    {
        if ( isset($param['courseId']) ) 
            $this->db->where('a.courseId', $param['courseId']);
        if ( isset($param['promotionId']) ) 
            $this->db->where('a.promotionId', $param['promotionId']);
          
        $query = $this->db
                        ->select('a.*')
                        ->from('promotion a')
                        ->get();
        return $query;
    }

    

    public function get_promotion_content($promotionId) 
    {
        
        $this->db->where('a.promotionId', $promotionId);
        $query = $this->db
                        ->select('a.*')
                        ->select('b.*')
                        ->from('promotion_content a')
                        ->join('course b', 'a.courseId = b.courseId', 'left')
                        ->get();
        return $query;
    }
    public function insertCourseRegis($value) 
    {
        // $this->db->insert('course_register', $value);
        // return $this->db->insert_id();

        if($this->db->insert('course_register', $value)){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
    public function get_CourseRegis($id) 
    {
        
        $this->db->where('a.course_registerId', $id);
        $query = $this->db
                        ->select('a.*')
                       // ->select('b.*,b.price as priceCourse')
                        ->select('c.*')
                        ->from('course_register a')
                        //->join('course b', 'a.courseId = b.courseId', 'left')
                        ->join('promotion c', 'a.promotionId = c.promotionId', 'left')
                        ->get();
        return $query;
    }
    public function get_CourseRegis2($id) 
    {
        
        $this->db->where('a.course_registerId', $id);
        $query = $this->db
                        ->select('a.*')
                        ->select('b.firstname,b.lastname,b.email')
                        //->select('c.*')
                        ->from('course_register a')
                        ->join('user b', 'a.userId = b.userId', 'left')
                        //->join('promotion c', 'a.promotionId = c.promotionId', 'left')
                        ->get();
        return $query;
    }

    public function get_CourseRegisByUserId($id) 
    {
        
        $this->db->where('a.userId', $id);
        $this->db->where('a.status !=', 2);
        $this->db->where('a.recycle', 0);
        $this->db->order_by('a.course_registerId', 'DESC');
        $query = $this->db
                        ->select('a.*')
                        // ->select('b.*,b.price as priceCourse')
                        //->select('c.*')
                        ->from('course_register a')
                        //->join('course b', 'a.courseId = b.courseId', 'left')
                        //->join('promotion c', 'a.promotionId = c.promotionId', 'left')
                        ->get();
        return $query;
    }

    public function get_course_member($courseId,$userId) 
    {
        $this->db->where('a.courseId', $courseId);
        $this->db->where('a.userId', $userId);
        $this->db->where('a.active',1);
        $this->db->where('a.recycle',0);
        $query = $this->db
                        ->select('a.*')
                        ->from('course_member a')
                        ->get();
        return $query;
    }

    public function get_course_member_regis($courseId,$userId) 
    {
        $this->db->where('a.courseId', $courseId);
        $this->db->where('a.userId', $userId);
        
        $this->db->where('a.recycle',0);
        $query = $this->db
                        ->select('a.*')
                        ->from('course_register a')
                        ->get();
        return $query;
    }

    public function get_reviews($courseId) 
    {
      
        $this->db->where('a.courseId', $courseId);
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $this->db->order_by('a.createDate', 'DESC');
        $query = $this->db
                        ->select('a.*')
                        ->from('reviews a')
                        ->get();
        return $query;
    }

    /////////คูปอง/////////


    public function getCoupon($param) 
    {
      
        $this->db->where('a.couponCode', $param['couponCode']);
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $this->db->where("((a.startDate ='0000-00-00' AND a.endDate ='0000-00-00') OR  (a.startDate <='".date('Y-m-d')."' AND a.endDate >='".date('Y-m-d')."'))");
        $query = $this->db
                        ->select('a.*')
                        ->from('coupon a')
                        ->get();
        return $query;
    }

    public function getCouponMember($param) 
    {
      
        $this->db->where('a.couponCode', $param['couponCode']);
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);

        if ( isset($param['userId']) ){
           $this->db->where('a.userId !=', $param['userId']);
        }
        // $this->db->where("((a.startDate ='0000-00-00' AND a.endDate ='0000-00-00') OR  (a.startDate <='".date('Y-m-d')."' AND a.endDate >='".date('Y-m-d')."'))");
        $query = $this->db
                        ->select('a.*')
                        ->from('user a')
                        ->get();
        return $query;
    }

    public function insert_point_log($value) {

        $this->db->insert('point_log', $value);

        $id = $this->db->insert_id();

        return $id;

    }

    public function insert_payment($value) {

        $this->db->insert('course_payment', $value);

        $id = $this->db->insert_id();

        return $id;

    }

    public function get_CoursePayment($id) 
    {
        
        $this->db->where('a.course_paymentId', $id);
        $query = $this->db
                        ->select('a.*')
                        ->select('b.title,b.branch,b.accountNumber,b.type,b.name')
                        ->from('course_payment a')
                        ->join('bank b', 'a.bankId = b.bankId', 'left')
                        ->get();
        return $query;
    }
    public function get_course_payment_by_regis($id) 
    {
        
        $this->db->where('a.course_registerId', $id);
        $query = $this->db
                        ->select('a.*')
                        ->select('b.title,b.branch,b.accountNumber,b.type,b.name')
                        ->from('course_payment a')
                        ->join('bank b', 'a.bankId = b.bankId', 'left')
                        ->get();
        return $query;
    }
    public function get_bank() 
    {
        
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $query = $this->db
                        ->select('a.*')
                        ->from('bank a')
                        ->get();
        return $query;
    }

     public function get_reviews_stars_($courseId) 
    {
        
        $this->db->where('a.courseId', $courseId);
         $this->db->order_by('a.createDate', 'DESC');
        $query = $this->db
                        ->select('a.*,b.firstname,b.lastname,b.picture')
                        ->from('reviews_stars a')
                        ->join('user b', 'a.userId = b.userId', 'left')
                        ->get();
        return $query;
    }

    public function save_reviews_stars($courseId,$userId,$value) {
         $this->db
                ->where('userId', $userId)
                ->where('courseId', $courseId)
                ->delete('reviews_stars');

        return $this->db->insert('reviews_stars', $value);

    }
    

}
