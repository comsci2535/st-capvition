<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MX_Controller {

    private $_grpContent = 'payment';

    public function __construct() {
        parent::__construct();
        $this->load->model("course_m");
        $this->load->model("course_content_m");
        $this->load->model('front/front_m');
        $this->load->model("admin/upload_m");
        $this->load->library('image_moo');

        $this->load->model("admin/order_list_m");
        $this->load->model("admin/course_m",'ca');
    }

    public function index($id) {

        $this->load->module('front');
        
        $this->session->unset_userdata('course_data');

        $bank=$this->course_m->get_bank()->result();
        
        foreach ($bank as $key => $value) {
                $input_b['grpContent'] = "bank";
                $input_b['contentId'] = $value->bankId;
                $value->image = "";
                
                $file_bank = $this->course_m->get_uplode($input_b)->row();
               
                if (!empty($file_bank) && is_file($file_bank->path.$file_bank->filename)) {
                    $value->image = base_url($file_bank->path.$file_bank->filename);
                   
                }
        }

        $data['bank']=$bank;
      

        $data['course_register']=$this->course_m->get_CourseRegis2($id)->row();
        //  arr($data['course_register']);exit();
///
        $input['active']='1';
        $input['recycle']='0';
        $input['courseId'] = $data['course_register']->courseId;
        //$input['keyword']=
        $input['grpContent'] = $this->_grpContent;
        

        $info=$this->course_m->get_rows($input)->row_array();
        if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "The CAP Vision Academy");

        //print"<pre>";print_r($data['course_register']); exit(); 
        if (!empty($info)) {
            //foreach ( $info as $key=>&$rs ) {

                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $info['courseId'];
                $info['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->course_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $info['image'] = base_url($file['path'].$file['filename']);
                    $imageSeo = base_url($file['path'].$file['filename']);
                }

                $input_p['courseId'] = $info['courseId'];
                $info['promotion']=$this->course_m->get_promotion_rows($input_p)->row_array();

                $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
                foreach ($promotionContent as $key => $value_) {
                    $input_['grpContent'] = $this->_grpContent;
                    $input_['contentId'] = $value_->courseId;
                    $value_->image = base_url("assets/images/no_image3.png");
                    
                    $file_ = $this->course_m->get_uplode($input_)->row_array();
                   
                    if (!empty($file_) && is_file($file_['path'].$file_['filename'])) {
                       $value_->image= base_url($file_['path'].$file_['filename']);
                      
                    }
                }
                $data['promotionContent']=$promotionContent;

                $data['info'] = $info;

           // }

        }

        //arr($data['info']);exit();
         $data['course_act'] = 'active';
         $data['frmAction'] = site_url("course/payment/payment_save");
       
         $data['contentView'] = "course/payment";
          $data['pageScript'] = 'assets/scripts/course/payment.js';
         $this->front->layout($data);

    }
    public function register($id=0){
        
       
        //print"<pre>";print_r($course_data); exit(); 
        $this->load->module('front');
        $input['active']='1';
        $input['recycle']='0';
        $input['courseId'] = $id;//$course_data['courseId'];
        //$input['keyword']=
        $input['grpContent'] = 'course';

        $bank=$this->course_m->get_bank()->result();
        
        foreach ($bank as $key => $value) {
                $input_b['grpContent'] = "bank";
                $input_b['contentId'] = $value->bankId;
                $value->image = "";
                
                $file_bank = $this->course_m->get_uplode($input_b)->row();
               
                if (!empty($file_bank) && is_file($file_bank->path.$file_bank->filename)) {
                    $value->image = base_url($file_bank->path.$file_bank->filename);
                   
                }
        }

        $data['bank']=$bank;
        

        $info=$this->course_m->get_rows($input)->row_array();
        if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "The CAP Vision Academy");

        
        if (!empty($info)) {
            //foreach ( $info as $key=>&$rs ) {
                $input_i['grpContent'] = 'Instructor';
                $input_i['instructorId'] = $info['instructorId'];
         
                $info_i = $this->course_m->get_rows_instructor($input_i)->row_array();
                

                if (!empty($info_i)) {
                    
                    $input_i['contentId'] = $info['instructorId'];
                    $data['instructor_image'] = base_url("assets/images/no_image3.png");
                    
                    $file_i = $this->course_m->get_uplode($input_i)->row_array();
                   
                    if (!empty($file_i) && is_file($file_i['path'].$file_i['filename'])) {
                        $data['instructor_image'] = base_url($file_i['path'].$file_i['filename']);
                    }
                    
                    $data['instructor'] = $info_i;
                    
                }

                $input['grpContent'] = 'course';
                $input['contentId'] = $info['courseId'];
                $info['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->course_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $info['image'] = base_url($file['path'].$file['filename']);
                    $imageSeo = base_url($file['path'].$file['filename']);
                }

                $input_p['courseId'] = $id;//$info['courseId'];
               // $info['promotion']=$this->course_m->get_promotion_rows($input_p)->row_array();

                $date=date('Y-m-d');
                //arr($date); exit();
                $promotion_ = $this->db
                                ->select('*')
                                ->from('promotion a')
                                ->where('a.courseId', $id)
                                ->where('a.active', 1)
                                ->where('a.recycle', 0)
                                ->where("((a.startDate ='0000-00-00' AND a.endDate ='0000-00-00') OR  (a.startDate <='".($date)."' AND a.endDate >='".($date)."'))")
                                ->get()->row_array();
                 $dddd=str_replace('-','', $promotion_['endDate']);
                 $dddd2=str_replace('-','', $date);
                 // && intval($dddd) > intval($dddd2)
                if(!empty($promotion_)){
                    $info['promotion']=$promotion_;
                    $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
                    foreach ($promotionContent as $key => $value_) {
                        $input_['grpContent'] = 'course';
                        $input_['contentId'] = $value_->courseId;
                        $value_->image = base_url("assets/images/no_image3.png");
                        
                        $file_ = $this->course_m->get_uplode($input_)->row_array();
                       
                        if (!empty($file_) && is_file($file_['path'].$file_['filename'])) {
                           $value_->image= base_url($file_['path'].$file_['filename']);
                          
                        }
                    }
                    $data['promotionContent']=$promotionContent;

                }
                

                $data['info'] = $info;

           // }

        }
        // arr($data['info']);exit();

         $data['course_act'] = 'active';
         $data['frmAction'] = site_url("course/payment/register_save");
         $data['frmActionOmise'] = site_url("course/payment/register_save_omise");
         $data['contentView'] = "course/payment/register";
         $data['pageScript'] = 'assets/scripts/course/payment/register.js';
         $this->front->layout_iframe($data);

    }

    public function register_save(){

        $input = $this->input->post();
        $file=$this->upload_ci_r('fileUpload');

        $input['docAttachId']=array();
        $input['docAttachId'][]=$file['insertId'];

        $value=$this->_build_data($input);
        //arr($input);exit();
        $result = $this->course_m->insertCourseRegis($value);
        if($result){
             $value = $this->_build_upload_content_r($result, $input);
             Modules::run('admin/upload/update_content', $value);

            $data['course_register']=$this->course_m->get_CourseRegis2($result)->row();
    ///

            $input['grpContent'] = "course_register";
            $input['contentId'] = $result;
            $data['course_register']->file = "";
            
            $file_f = $this->course_m->get_uplode($input)->row_array();
           
            if (!empty($file_f) && is_file($file_f['path'].$file_f['filename'])) {
               $data['course_register']->file = base_url($file_f['path'].$file_f['filename']);
               
            }

            $input['active']='1';
            $input['recycle']='0';
            $input['courseId'] = $data['course_register']->courseId;
            //$input['keyword']=
            $input['grpContent'] = "course";


            

            $info=$this->course_m->get_rows($input)->row_array();
           
            //print"<pre>";print_r($data['course_register']); exit(); 
            if (!empty($info)) {
                //foreach ( $info as $key=>&$rs ) {



                    $input['grpContent'] = "course";
                    $input['contentId'] = $info['courseId'];
                    $info['image'] = base_url("assets/images/no_image3.png");
                    
                    $file = $this->course_m->get_uplode($input)->row_array();
                   
                    if (!empty($file) && is_file($file['path'].$file['filename'])) {
                        $info['image'] = base_url($file['path'].$file['filename']);
                       
                    }

                    $input_p['courseId'] = $info['courseId'];
                    $info['promotion']=$this->course_m->get_promotion_rows($input_p)->row_array();

                    $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
                    foreach ($promotionContent as $key => $value_) {
                        $input_['grpContent'] = "course";
                        $input_['contentId'] = $value_->courseId;
                        $value_->image = base_url("assets/images/no_image3.png");
                        
                        $file_ = $this->course_m->get_uplode($input_)->row_array();
                       
                        if (!empty($file_) && is_file($file_['path'].$file_['filename'])) {
                           $value_->image= base_url($file_['path'].$file_['filename']);
                          
                        }
                    }
                    $data['promotionContent']=$promotionContent;

                    $data['info'] = $info;

               // }

            }

            $email=$data['course_register']->email;

            $input['type'] = 'mail';

            $input['variable'] = 'SMTPserver';
            $SMTPserver=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPusername';
            $SMTPusername=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPpassword';
            $SMTPpassword=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPport';
            $SMTPport=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'senderEmail';
            $senderEmail=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'senderName';
            $senderName=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'mailDefault';
            $mailDefault=$this->front_m->getConfig($input)->row();
            
            $textEmail = $this->load->view('course/mail/mailer_form_r.php',$data , TRUE);
                          
                           

              require 'application/third_party/phpmailer/PHPMailerAutoload.php';
              $mail = new PHPMailer;

              //$mail->SMTPDebug = 3;                               // Enable verbose debug output

              $mail->isSMTP();                                      // Set mailer to use SMTP
              $mail->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
              $mail->SMTPAuth = true;                               // Enable SMTP authentication
              $mail->Username = $SMTPusername->value;                // SMTP username
              $mail->Password = $SMTPpassword->value;                          // SMTP password
              $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
              $mail->Port = $SMTPport->value;                                           // TCP port to connect to
              $mail->CharSet = 'UTF-8';
              $mail->From = $senderEmail->value;
              $mail->FromName = $senderName->value;
              $mail->addAddress($email);               // Name is optional
              
              $mail->isHTML(true);                                  // Set email format to HTML

              $mail->Subject = $email.' : ลงทะเบียนเรียน';
              $mail->Body    = $textEmail;
              $mail->AltBody = $textEmail;
              $mail->send();

              $textEmail2 = $this->load->view('course/mail/mailer_form_r_admin.php',$data , TRUE);

                $mail2 = new PHPMailer;
                
                $mail2->isSMTP();                                      // Set mailer to use SMTP
                $mail2->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
                $mail2->SMTPAuth = true;                               // Enable SMTP authentication
                $mail2->Username = $SMTPusername->value;                // SMTP username
                $mail2->Password = $SMTPpassword->value;                          // SMTP password
                $mail2->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail2->Port = $SMTPport->value;                                           // TCP port to connect to
                $mail2->CharSet = 'UTF-8';
                $mail2->From = $senderEmail->value;
                $mail2->FromName = $senderName->value;
                $mail2->addAddress($mailDefault->value);               // Name is optional

                
                $mail2->isHTML(true);                                  // Set email format to HTML

                $mail2->Subject = $email.' : ลงทะเบียนเรียน';
                $mail2->Body    = $textEmail2;
                $mail2->AltBody = $textEmail2;
                $mail2->send();

            $this->session->unset_userdata('course_data');
            
            $resp_msg = array('info_txt'=>"success",'msg'=>'ลงทะเบียนเรียบร้อย','msg2'=>'กรุณารอสักครู่...','result'=>$result);
                echo json_encode($resp_msg);
                return false;
        } else {
            $resp_msg = array('info_txt'=>"error",'msg'=>'ลงทะเบียนไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
                echo json_encode($resp_msg);
               return false;
        }
        //arr($result);exit();

    }
    public function register_save_paypal(){

        $input = $this->input->post();
        $input['paypal']="1";

        $value=$this->_build_data($input);
        //arr($input);exit();
        $result = $this->course_m->insertCourseRegis($value);
        if($result){
            
            $input['course_registerId'] =  $result;
                    
            $course_register=$this->course_m->get_CourseRegis2($result)->row();
            $input_p['promotionId'] = $course_register->promotionId;
            $info['promotion']=$this->course_m->get_promotion_rows2($input_p)->row_array();

            $courseId  = array();
            $courseId[]  = $course_register->courseId;
            $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
            
            if(!empty($promotionContent)){ 
                foreach ($promotionContent as $key => $value_) {
                     $courseId[]  = $value_->courseId;
                }
            }
     //arr($courseId);exit();
            $this->order_list_m->del_course_member($course_register->code);
           // if($status==1){

            foreach ($courseId as $key_ => $value) {
                //$this->order_list_m->plus_learner($value);
                $value_i['courseId']=$value;
                $value_i['userId']=$course_register->userId;
                $value_i['code']=$course_register->code;
                $value_i['active']=1;
                $value_i['recycle']=0;
                $value_i['createDate'] = db_datetime_now();
                $this->order_list_m->insert_course_member($value_i);
            }

            $data['course_register']=$this->course_m->get_CourseRegis2($result)->row();
    ///

            $input['grpContent'] = "course_register";
            $input['contentId'] = $result;
            $data['course_register']->file = "";
            
            $file_f = $this->course_m->get_uplode($input)->row_array();
           
            if (!empty($file_f) && is_file($file_f['path'].$file_f['filename'])) {
               $data['course_register']->file = base_url($file_f['path'].$file_f['filename']);
               
            }

            $input['active']='1';
            $input['recycle']='0';
            $input['courseId'] = $data['course_register']->courseId;
            //$input['keyword']=
            $input['grpContent'] = "course";


            

            $info=$this->course_m->get_rows($input)->row_array();
           
            //print"<pre>";print_r($data['course_register']); exit(); 
            if (!empty($info)) {
                //foreach ( $info as $key=>&$rs ) {



                    $input['grpContent'] = "course";
                    $input['contentId'] = $info['courseId'];
                    $info['image'] = base_url("assets/images/no_image3.png");
                    
                    $file = $this->course_m->get_uplode($input)->row_array();
                   
                    if (!empty($file) && is_file($file['path'].$file['filename'])) {
                        $info['image'] = base_url($file['path'].$file['filename']);
                       
                    }

                    $input_p['courseId'] = $info['courseId'];
                    $info['promotion']=$this->course_m->get_promotion_rows($input_p)->row_array();

                    $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
                    foreach ($promotionContent as $key => $value_) {
                        $input_['grpContent'] = "course";
                        $input_['contentId'] = $value_->courseId;
                        $value_->image = base_url("assets/images/no_image3.png");
                        
                        $file_ = $this->course_m->get_uplode($input_)->row_array();
                       
                        if (!empty($file_) && is_file($file_['path'].$file_['filename'])) {
                           $value_->image= base_url($file_['path'].$file_['filename']);
                          
                        }
                    }
                    $data['promotionContent']=$promotionContent;

                    $data['info'] = $info;

               // }

            }

            $email=$data['course_register']->email;

            $input['type'] = 'mail';

            $input['variable'] = 'SMTPserver';
            $SMTPserver=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPusername';
            $SMTPusername=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPpassword';
            $SMTPpassword=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPport';
            $SMTPport=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'senderEmail';
            $senderEmail=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'senderName';
            $senderName=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'mailDefault';
            $mailDefault=$this->front_m->getConfig($input)->row();
            
            $textEmail = $this->load->view('course/mail/mailer_form_rp.php',$data , TRUE);
                          
                           

              require 'application/third_party/phpmailer/PHPMailerAutoload.php';
              $mail = new PHPMailer;

              //$mail->SMTPDebug = 3;                               // Enable verbose debug output

              $mail->isSMTP();                                      // Set mailer to use SMTP
              $mail->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
              $mail->SMTPAuth = true;                               // Enable SMTP authentication
              $mail->Username = $SMTPusername->value;                // SMTP username
              $mail->Password = $SMTPpassword->value;                          // SMTP password
              $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
              $mail->Port = $SMTPport->value;                                           // TCP port to connect to
              $mail->CharSet = 'UTF-8';
              $mail->From = $senderEmail->value;
              $mail->FromName = $senderName->value;
              $mail->addAddress($email);               // Name is optional
              
              $mail->isHTML(true);                                  // Set email format to HTML

              $mail->Subject = $email.' : ลงทะเบียนเรียน';
              $mail->Body    = $textEmail;
              $mail->AltBody = $textEmail;
              $mail->send();

              $textEmail2 = $this->load->view('course/mail/mailer_form_rp_admin.php',$data , TRUE);

                $mail2 = new PHPMailer;
                
                $mail2->isSMTP();                                      // Set mailer to use SMTP
                $mail2->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
                $mail2->SMTPAuth = true;                               // Enable SMTP authentication
                $mail2->Username = $SMTPusername->value;                // SMTP username
                $mail2->Password = $SMTPpassword->value;                          // SMTP password
                $mail2->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail2->Port = $SMTPport->value;                                           // TCP port to connect to
                $mail2->CharSet = 'UTF-8';
                $mail2->From = $senderEmail->value;
                $mail2->FromName = $senderName->value;
                $mail2->addAddress($mailDefault->value);               // Name is optional

                
                $mail2->isHTML(true);                                  // Set email format to HTML

                $mail2->Subject = $email.' : ลงทะเบียนเรียน';
                $mail2->Body    = $textEmail2;
                $mail2->AltBody = $textEmail2;
                $mail2->send();

            $this->session->unset_userdata('course_data');
            
            $resp_msg = array('info_txt'=>"success",'msg'=>'ลงทะเบียนเรียบร้อย','msg2'=>'กรุณารอสักครู่...','result'=>$result);
                echo json_encode($resp_msg);
                return false;
        } else {
            $resp_msg = array('info_txt'=>"error",'msg'=>'ลงทะเบียนไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
                echo json_encode($resp_msg);
               return false;
        }
        //arr($result);exit();

    }


    public function register_save_omise(){

        require_once 'application/libraries/omise-php/lib/Omise.php';
        define('OMISE_PUBLIC_KEY', 'pkey_test_5e6hs53uxjjf886yf7b');
        define('OMISE_SECRET_KEY', 'skey_test_5e6hrjz2ueqpgeqsfvs');

        $input = $this->input->post();
        $input['paypal']="2";


        $charge = OmiseCharge::create(array(
          'amount' => $input["priceOmise"],
          'currency' => 'thb',
          'card' => $input["omiseToken"]
        ));

        if ($charge['status'] == 'successful') {
            // arr($input);exit();
            $value=$this->_build_data($input);
            $result = $this->course_m->insertCourseRegis($value);
            if($result){
                
                $input['course_registerId'] =  $result;
                        
                $course_register=$this->course_m->get_CourseRegis2($result)->row();
                $input_p['promotionId'] = $course_register->promotionId;
                $info['promotion']=$this->course_m->get_promotion_rows2($input_p)->row_array();

                $courseId  = array();
                $courseId[]  = $course_register->courseId;
                $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
                if(!empty($promotionContent)){ 
                    foreach ($promotionContent as $key => $value_) {
                         $courseId[]  = $value_->courseId;
                    }
                }
         //arr($courseId);exit();
                $this->order_list_m->del_course_member($course_register->code);
               // if($status==1){

                foreach ($courseId as $key_ => $value) {
                    //$this->order_list_m->plus_learner($value);
                    $value_i['courseId']=$value;
                    $value_i['userId']=$course_register->userId;
                    $value_i['code']=$course_register->code;
                    $value_i['active']=1;
                    $value_i['recycle']=0;
                    $value_i['createDate'] = db_datetime_now();
                    $this->order_list_m->insert_course_member($value_i);
                }

                $data['course_register']=$this->course_m->get_CourseRegis2($result)->row();
        ///

                $input['grpContent'] = "course_register";
                $input['contentId'] = $result;
                $data['course_register']->file = "";
                
                $file_f = $this->course_m->get_uplode($input)->row_array();
               
                if (!empty($file_f) && is_file($file_f['path'].$file_f['filename'])) {
                   $data['course_register']->file = base_url($file_f['path'].$file_f['filename']);
                   
                }

                $input['active']='1';
                $input['recycle']='0';
                $input['courseId'] = $data['course_register']->courseId;
                //$input['keyword']=
                $input['grpContent'] = "course";


                

                $info=$this->course_m->get_rows($input)->row_array();
               
                //print"<pre>";print_r($data['course_register']); exit(); 
                if (!empty($info)) {
                    //foreach ( $info as $key=>&$rs ) {
                        $input['grpContent'] = "course";
                        $input['contentId'] = $info['courseId'];
                        $info['image'] = base_url("assets/images/no_image3.png");
                        
                        $file = $this->course_m->get_uplode($input)->row_array();
                       
                        if (!empty($file) && is_file($file['path'].$file['filename'])) {
                            $info['image'] = base_url($file['path'].$file['filename']);
                           
                        }

                        $input_p['courseId'] = $info['courseId'];
                        $info['promotion']=$this->course_m->get_promotion_rows($input_p)->row_array();

                        $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
                        foreach ($promotionContent as $key => $value_) {
                            $input_['grpContent'] = "course";
                            $input_['contentId'] = $value_->courseId;
                            $value_->image = base_url("assets/images/no_image3.png");
                            
                            $file_ = $this->course_m->get_uplode($input_)->row_array();
                           
                            if (!empty($file_) && is_file($file_['path'].$file_['filename'])) {
                               $value_->image= base_url($file_['path'].$file_['filename']);
                              
                            }
                        }
                        $data['promotionContent']=$promotionContent;

                        $data['info'] = $info;

                   // }

                }

                $email=$data['course_register']->email;

                $input['type'] = 'mail';

                $input['variable'] = 'SMTPserver';
                $SMTPserver=$this->front_m->getConfig($input)->row();

                $input['variable'] = 'SMTPusername';
                $SMTPusername=$this->front_m->getConfig($input)->row();

                $input['variable'] = 'SMTPpassword';
                $SMTPpassword=$this->front_m->getConfig($input)->row();

                $input['variable'] = 'SMTPport';
                $SMTPport=$this->front_m->getConfig($input)->row();

                $input['variable'] = 'senderEmail';
                $senderEmail=$this->front_m->getConfig($input)->row();

                $input['variable'] = 'senderName';
                $senderName=$this->front_m->getConfig($input)->row();

                $input['variable'] = 'mailDefault';
                $mailDefault=$this->front_m->getConfig($input)->row();
                
                $textEmail = $this->load->view('course/mail/mailer_form_rp.php',$data , TRUE);
                              
                               

                  require 'application/third_party/phpmailer/PHPMailerAutoload.php';
                  $mail = new PHPMailer;

                  //$mail->SMTPDebug = 3;                               // Enable verbose debug output

                  $mail->isSMTP();                                      // Set mailer to use SMTP
                  $mail->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
                  $mail->SMTPAuth = true;                               // Enable SMTP authentication
                  $mail->Username = $SMTPusername->value;                // SMTP username
                  $mail->Password = $SMTPpassword->value;                          // SMTP password
                  $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                  $mail->Port = $SMTPport->value;                                           // TCP port to connect to
                  $mail->CharSet = 'UTF-8';
                  $mail->From = $senderEmail->value;
                  $mail->FromName = $senderName->value;
                  $mail->addAddress($email);               // Name is optional
                  
                  $mail->isHTML(true);                                  // Set email format to HTML

                  $mail->Subject = $email.' : ลงทะเบียนเรียน';
                  $mail->Body    = $textEmail;
                  $mail->AltBody = $textEmail;
                  $mail->send();

                  $textEmail2 = $this->load->view('course/mail/mailer_form_rp_admin.php',$data , TRUE);

                    $mail2 = new PHPMailer;
                    
                    $mail2->isSMTP();                                      // Set mailer to use SMTP
                    $mail2->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
                    $mail2->SMTPAuth = true;                               // Enable SMTP authentication
                    $mail2->Username = $SMTPusername->value;                // SMTP username
                    $mail2->Password = $SMTPpassword->value;                          // SMTP password
                    $mail2->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                    $mail2->Port = $SMTPport->value;                                           // TCP port to connect to
                    $mail2->CharSet = 'UTF-8';
                    $mail2->From = $senderEmail->value;
                    $mail2->FromName = $senderName->value;
                    $mail2->addAddress($mailDefault->value);               // Name is optional

                    
                    $mail2->isHTML(true);                                  // Set email format to HTML

                    $mail2->Subject = $email.' : ลงทะเบียนเรียน';
                    $mail2->Body    = $textEmail2;
                    $mail2->AltBody = $textEmail2;
                    $mail2->send();

                $this->session->unset_userdata('course_data');
                
                redirect('course/payment/register_success_paypal/'.$result, 'refresh');

            } else {
                // $resp_msg = array('info_txt'=>"error",'msg'=>'ลงทะเบียนไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
                //     echo json_encode($resp_msg);
                //    return false;
            }
        } else {
          echo 'Fail';
        }

        // print('<pre>');
        // print_r($charge);
        // print('</pre>');

        
        
        //arr($result);exit();

    }

    public function register_success($id){

        $this->load->module('front');

        $this->update_point_log($id);
        
        $this->session->unset_userdata('course_data');
        $data['course_register']=$this->course_m->get_CourseRegis2($id)->row();
///
        $input['active']='1';
        $input['recycle']='0';
        $input['courseId'] = $data['course_register']->courseId;
        //$input['keyword']=
        $input['grpContent'] = $this->_grpContent;
        

        $info=$this->course_m->get_rows($input)->row_array();
        if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "The CAP Vision Academy");

        //print"<pre>";print_r($data['course_register']); exit(); 
        if (!empty($info)) {
            //foreach ( $info as $key=>&$rs ) {

                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $info['courseId'];
                $info['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->course_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $info['image'] = base_url($file['path'].$file['filename']);
                    $imageSeo = base_url($file['path'].$file['filename']);
                }

                $input_p['courseId'] = $info['courseId'];
                $info['promotion']=$this->course_m->get_promotion_rows($input_p)->row_array();

                $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
                foreach ($promotionContent as $key => $value_) {
                    $input_['grpContent'] = $this->_grpContent;
                    $input_['contentId'] = $value_->courseId;
                    $value_->image = base_url("assets/images/no_image3.png");
                    
                    $file_ = $this->course_m->get_uplode($input_)->row_array();
                   
                    if (!empty($file_) && is_file($file_['path'].$file_['filename'])) {
                       $value_->image= base_url($file_['path'].$file_['filename']);
                      
                    }
                }
                $data['promotionContent']=$promotionContent;

                $data['info'] = $info;

           // }

        }

        //arr($data['info']);exit();
         $data['course_act'] = 'active';
       
         $data['contentView'] = "course/payment/register_success";
         $this->front->layout_iframe($data);

    }

    public function register_success_paypal($id){

        $this->load->module('front');

        $this->update_point_log($id);
        
        $this->session->unset_userdata('course_data');
        $data['course_register']=$this->course_m->get_CourseRegis2($id)->row();
///
        $input['active']='1';
        $input['recycle']='0';
        $input['courseId'] = $data['course_register']->courseId;
        //$input['keyword']=
        $input['grpContent'] = $this->_grpContent;
        

        $info=$this->course_m->get_rows($input)->row_array();
        if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "The CAP Vision Academy");

        //print"<pre>";print_r($data['course_register']); exit(); 
        if (!empty($info)) {
            //foreach ( $info as $key=>&$rs ) {

                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $info['courseId'];
                $info['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->course_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $info['image'] = base_url($file['path'].$file['filename']);
                    $imageSeo = base_url($file['path'].$file['filename']);
                }

                $input_p['courseId'] = $info['courseId'];
                $info['promotion']=$this->course_m->get_promotion_rows($input_p)->row_array();

                $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
                foreach ($promotionContent as $key => $value_) {
                    $input_['grpContent'] = $this->_grpContent;
                    $input_['contentId'] = $value_->courseId;
                    $value_->image = base_url("assets/images/no_image3.png");
                    
                    $file_ = $this->course_m->get_uplode($input_)->row_array();
                   
                    if (!empty($file_) && is_file($file_['path'].$file_['filename'])) {
                       $value_->image= base_url($file_['path'].$file_['filename']);
                      
                    }
                }
                $data['promotionContent']=$promotionContent;

                $data['info'] = $info;

           // }

        }

        //arr($data['info']);exit();
         $data['course_act'] = 'active';
       
         $data['contentView'] = "course/payment/register_success_paypal";
         $this->front->layout_iframe($data);

    }

    public function update_point_log($id){

        $course_register=$this->course_m->get_CourseRegis2($id)->row();

        if($course_register->couponCode!=""){
            $input['couponCode']=$course_register->couponCode;
            $result = $this->course_m->getCouponMember($input)->row();

            if(!empty($result)){

                if($course_register->type==2){
                      $rt=($course_register->price*$course_register->discount)/100;
                      $total=$course_register->price-$rt;
                }else{
                      $total=$course_register->price-$course_register->discount;
                }

                $point=(20*$total)/100;

                $data['action']='add';
                $data['point']=$point;
                $data['userId']=$result->userId;
                $data['course_registerId']=$id;
                $data['courseId']=$course_register->courseId;
                $data['note']='ลงทะเบียนเรียน';
                $data['created_at']=db_datetime_now();
                $data['created_by']=$this->session->member['userId'];

                $this->course_m->insert_point_log($data);
            }

        }
    }

    public function getSLip($id){

        $this->load->module('front');

        $input['grpContent'] = "course_register";
        $input['contentId'] = $id;
       
        
        $file = $this->course_m->get_uplode($input)->row_array();
       
        if (!empty($file) && is_file($file['path'].$file['filename'])) {
            $data['extension'] = $file['extension'];
            $data['image'] = base_url($file['path'].$file['filename']);
            
        }

        
       
       
         $data['contentView'] = "course/payment/slip";
         $this->front->layout_iframe($data);

    }

     private function _build_data($input) {
       
        
        $value['userId'] = $this->session->member['userId'];
        $value['courseId'] = $input['courseId'];
        $value['promotionId'] = $input['promotionId'];
        $value['price'] = $input['price'];
        $value['code'] = "GD".time();
       

        if($input['type']!=""){
            $value['type'] = $input['type'];
            $value['couponCode'] = $input['couponCode'];
        }

        if($input['discount']!=""){
             $value['discount'] = $input['discount'];
        }
        
        if(isset($input['paypal']) && $input['paypal']=='1'){
             $value['status'] = 1;
             $value['isPaypal'] = 1;
        }else{
             $value['status'] = 0;
        }

        if(isset($input['paypal']) && $input['paypal']=='2'){
             $value['status'] = 1;
             $value['isPaypal'] = 2;
        }else{
             $value['status'] = 0;
        }
        
        $value['createDate'] = db_datetime_now();
        $value['createBy'] = $this->session->member['userId'];
        // $value['updateDate'] = db_datetime_now();
        // $value['updateBy'] = $this->session->member['userId'];
       
        return $value;
    }

    public function payment_save()
    {

        $input = $this->input->post();

        $ip = $_SERVER['REMOTE_ADDR'];

        
        if($this->input->post('robot') != null){
            $captcha = "ok";
            $responseKeys["success"] = 1;
        } else {
            $captcha = false;
            $responseKeys["success"] = false;
        }


        // if(intval($responseKeys["success"]) !== 1||!$captcha) {
        //     echo json_encode(array('info_txt'=>'error','msg' => '* Captcha error','msg2'=>'กรุณาลองใหม่อีกครั้ง!'));
        // } else {
            $file=$this->upload_ci('fileUpload');

            $input['docAttachId']=array();
            $input['docAttachId'][]=$file['insertId'];
//arr($file);
            $value = $this->_build_data_payment($input);
            $result = $this->course_m->insert_payment($value);
            if ( $result ) {
                 $value = $this->_build_upload_content($result, $input);
                 Modules::run('admin/upload/update_content', $value);

                $data['course_payment']=$this->course_m->get_CoursePayment($result)->row_array();
                
                $input_pm['grpContent'] = "payment";
                $input_pm['contentId'] = $result;
                $data['course_payment']['p_file'] = "";
                
                // $file_pm = $this->course_m->get_uplode($input_pm)->row_array();
               
                // if (!empty($file_pm) && is_file($file_pm['path'].$file_pm['filename'])) {
                //    $data['course_payment']['p_file'] = base_url($file_pm['path'].$file_pm['filename']);
                   
                // }
                if(!empty($file['file_name'])){
                   $data['course_payment']['p_file'] = base_url($file['file_name']); 
                }

                $input_b['grpContent'] = "bank";
                $input_b['contentId'] = $data['course_payment']['bankId'];
                $data['course_payment']['b_file'] = "";
                
                $file_p = $this->course_m->get_uplode($input_b)->row_array();
               
                if (!empty($file_p) && is_file($file_p['path'].$file_p['filename'])) {
                   $data['course_payment']['b_file'] = base_url($file_p['path'].$file_p['filename']);
                   
                }
 //arr($file_pm);exit();
                $data['course_register']=$this->course_m->get_CourseRegis2($input['course_registerId'])->row();
        ///
                $input['active']='1';
                $input['recycle']='0';
                $input['courseId'] = $data['course_register']->courseId;
                //$input['keyword']=
                $input['grpContent'] = $this->_grpContent;
                

                $info=$this->course_m->get_rows($input)->row_array();
               
                //print"<pre>";print_r($data['course_register']); exit(); 
                if (!empty($info)) {
                    //foreach ( $info as $key=>&$rs ) {

                        $input['grpContent'] = $this->_grpContent;
                        $input['contentId'] = $info['courseId'];
                        $info['image'] = base_url("assets/images/no_image3.png");
                        
                        $file = $this->course_m->get_uplode($input)->row_array();
                       
                        if (!empty($file) && is_file($file['path'].$file['filename'])) {
                            $info['image'] = base_url($file['path'].$file['filename']);
                           
                        }

                        $input_p['courseId'] = $info['courseId'];
                        $info['promotion']=$this->course_m->get_promotion_rows($input_p)->row_array();

                        $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
                        foreach ($promotionContent as $key => $value_) {
                            $input_['grpContent'] = $this->_grpContent;
                            $input_['contentId'] = $value_->courseId;
                            $value_->image = base_url("assets/images/no_image3.png");
                            
                            $file_ = $this->course_m->get_uplode($input_)->row_array();
                           
                            if (!empty($file_) && is_file($file_['path'].$file_['filename'])) {
                               $value_->image= base_url($file_['path'].$file_['filename']);
                              
                            }
                        }
                        $data['promotionContent']=$promotionContent;

                        $data['info'] = $info;

                   // }

                }



                $email=$data['course_register']->email;

            $input['type'] = 'mail';

            $input['variable'] = 'SMTPserver';
            $SMTPserver=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPusername';
            $SMTPusername=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPpassword';
            $SMTPpassword=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPport';
            $SMTPport=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'senderEmail';
            $senderEmail=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'senderName';
            $senderName=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'mailDefault';
            $mailDefault=$this->front_m->getConfig($input)->row();
            
            $textEmail = $this->load->view('course/mail/mailer_form_p.php',$data , TRUE);
                          
                           

              require 'application/third_party/phpmailer/PHPMailerAutoload.php';
              $mail = new PHPMailer;

              //$mail->SMTPDebug = 3;                               // Enable verbose debug output

              $mail->isSMTP();                                      // Set mailer to use SMTP
              $mail->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
              $mail->SMTPAuth = true;                               // Enable SMTP authentication
              $mail->Username = $SMTPusername->value;                // SMTP username
              $mail->Password = $SMTPpassword->value;                          // SMTP password
              $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
              $mail->Port = $SMTPport->value;                                           // TCP port to connect to
              $mail->CharSet = 'UTF-8';
              $mail->From = $senderEmail->value;
              $mail->FromName = $senderName->value;
              $mail->addAddress($email);               // Name is optional
              
              $mail->isHTML(true);                                  // Set email format to HTML

              $mail->Subject = $email.' : แจ้งชำระเงิน';
              $mail->Body    = $textEmail;
              $mail->AltBody = $textEmail;
              $mail->send();

              $textEmail2 = $this->load->view('course/mail/mailer_form_p_admin.php',$data , TRUE);

                $mail2 = new PHPMailer;
                
                $mail2->isSMTP();                                      // Set mailer to use SMTP
                $mail2->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
                $mail2->SMTPAuth = true;                               // Enable SMTP authentication
                $mail2->Username = $SMTPusername->value;                // SMTP username
                $mail2->Password = $SMTPpassword->value;                          // SMTP password
                $mail2->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail2->Port = $SMTPport->value;                                           // TCP port to connect to
                $mail2->addAttachment($file['file_name']); 
                $mail2->CharSet = 'UTF-8';
                $mail2->From = $senderEmail->value;
                $mail2->FromName = $senderName->value;
                $mail2->addAddress($mailDefault->value);               // Name is optional

                
                $mail2->isHTML(true);                                  // Set email format to HTML

                $mail2->Subject = $email.' : แจ้งชำระเงิน';
                $mail2->Body    = $textEmail2;
                $mail2->AltBody = $textEmail2;
                $mail2->send();

                $resp_msg = array('info_txt'=>"success",'msg'=>'แจ้งโอนสำเร็จ','msg2'=>'กรุณารอสักครู่...');
                    echo json_encode($resp_msg);
                    return false;
            } else {
                $resp_msg = array('info_txt'=>"error",'msg'=>'แจ้งโอนไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
                echo json_encode($resp_msg);
                return false;
            }
        //}
    }

    public function confirm($id) {

        //$data['course_payment']=$this->course_m->get_CoursePayment($id)->row_array();
                
        

        $value['status'] = 1;
        $result = $this->order_list_m->update($id, $value);

        //arr($data['course_payment']);exit();

        $input['course_registerId'] =  $id;//$course_registerId;
        //$info = $this->order_list_m->get_rows($input)->row();


        $course_register=$this->course_m->get_CourseRegis2($id)->row();

       
        $input_p['promotionId'] = $course_register->promotionId;
        $info['promotion']=$this->course_m->get_promotion_rows2($input_p)->row_array();

        $courseId  = array();
        $courseId[]  = $course_register->courseId;
        $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
        if(!empty($promotionContent)){ 
            foreach ($promotionContent as $key => $value_) {
                 $courseId[]  = $value_->courseId;
            }
        }
 //arr($courseId);exit();
        $this->order_list_m->del_course_member($course_register->code);
       // if($status==1){

            foreach ($courseId as $key => $value) {
                //$this->order_list_m->plus_learner($value);
                $value_i['courseId']=$value;
                $value_i['userId']=$course_register->userId;
                $value_i['code']=$course_register->code;
                $value_i['active']=1;
                $value_i['recycle']=0;
                $value_i['createDate'] = db_datetime_now();
                $this->order_list_m->insert_course_member($value_i);
            }
        //}
        //arr($data);exit();

            // $data['course_register']=$this->course_m->get_CourseRegis2($input['course_registerId'])->row();
        ///
            $input['active']='1';
            $input['recycle']='0';
            $input['courseId'] = $course_register->courseId;
            //$input['keyword']=
            $input['grpContent'] = $this->_grpContent;
            

            $info=$this->course_m->get_rows($input)->row_array();
           
            //print"<pre>";print_r($course_register); exit(); 
            if (!empty($info)) {
                //foreach ( $info as $key=>&$rs ) {

                    $input['grpContent'] = $this->_grpContent;
                    $input['contentId'] = $info['courseId'];
                    $info['image'] = base_url("assets/images/no_image3.png");
                    
                    $file = $this->course_m->get_uplode($input)->row_array();
                   
                    if (!empty($file) && is_file($file['path'].$file['filename'])) {
                        $info['image'] = base_url($file['path'].$file['filename']);
                       
                    }

                    $input_p['courseId'] = $info['courseId'];
                    $info['promotion']=$this->course_m->get_promotion_rows($input_p)->row_array();

                    $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
                    foreach ($promotionContent as $key => $value_) {
                        $input_['grpContent'] = $this->_grpContent;
                        $input_['contentId'] = $value_->courseId;
                        $value_->image = base_url("assets/images/no_image3.png");
                        
                        $file_ = $this->course_m->get_uplode($input_)->row_array();
                       
                        if (!empty($file_) && is_file($file_['path'].$file_['filename'])) {
                           $value_->image= base_url($file_['path'].$file_['filename']);
                          
                        }
                    }
                    $data['promotionContent']=$promotionContent;

                    $data['info'] = $info;

               // }

            }



            $email=$course_register->email;
            $data['course_register']=$course_register;

            $input['type'] = 'mail';

            $input['variable'] = 'SMTPserver';
            $SMTPserver=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPusername';
            $SMTPusername=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPpassword';
            $SMTPpassword=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPport';
            $SMTPport=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'senderEmail';
            $senderEmail=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'senderName';
            $senderName=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'mailDefault';
            $mailDefault=$this->front_m->getConfig($input)->row();
            
            $textEmail = $this->load->view('course/mail/mailer_form_c.php',$data , TRUE);
                          
                           

              require 'application/third_party/phpmailer/PHPMailerAutoload.php';
              $mail = new PHPMailer;

              //$mail->SMTPDebug = 3;                               // Enable verbose debug output

              $mail->isSMTP();                                      // Set mailer to use SMTP
              $mail->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
              $mail->SMTPAuth = true;                               // Enable SMTP authentication
              $mail->Username = $SMTPusername->value;                // SMTP username
              $mail->Password = $SMTPpassword->value;                          // SMTP password
              $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
              $mail->Port = $SMTPport->value;                                           // TCP port to connect to
              $mail->CharSet = 'UTF-8';
              $mail->From = $senderEmail->value;
              $mail->FromName = $senderName->value;
              $mail->addAddress($email);               // Name is optional
              
              $mail->isHTML(true);                                  // Set email format to HTML

              $mail->Subject = $email.' : ยืนยันการชำระเงิน';
              $mail->Body    = $textEmail;
              $mail->AltBody = $textEmail;
              $mail->send();

        $this->load->view('course/confirm', $data); 

    }

    private function _build_data_payment($input) {
       
        
        $value['course_registerId'] = $input['course_registerId'];
        $value['userId'] = $input['userId'];
        $value['courseId'] = $input['courseId'];
        $value['code'] = $input['code'];
        $value['phone'] = $input['phone'];
        $value['bankId'] = $input['bankId'];
        
        $value['transferDdate'] = date('Y-m-d', strtotime(str_replace('/', '-', $input['transferDdate'])));
        $value['transferTime'] = date("H:i:s", strtotime($input['transferTime']));
        $value['amount'] = str_replace(",","",$input['amount']);
        $value['note'] = $input['note'];
        $value['createDate'] = db_datetime_now();
        $value['updateDate'] = db_datetime_now();
     
      //arr($value);exit();
        return $value;
    }

     private function _build_upload_content($id, $input) {
        $value = array();
        if ( isset($input['coverImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId'],
                'title' => $input['coverImageTitle'],
            );
        if ( isset($input['contentImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'contentImage',
                'uploadId' => $input['contentImageId'],
                'title' => $input['contentImageTitle'],
            );
        if ( isset($input['galleryImageId']) ) {
            foreach ( $input['galleryImageId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'galleryImage',
                    'uploadId' => $rs,
                    'title' => $input['galleryImageTitle'][$key]
                );
            }
        }
        if ( isset($input['docAttachId']) ) {
            foreach ( $input['docAttachId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'docAttach',
                    'uploadId' => $rs,
                    'title' => $input['docAttachTitle'][$key]
                );
            }
        }
        return $value;
    }  

    private function _build_upload_content_r($id, $input) {
        $value = array();
        if ( isset($input['coverImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => "course_register",
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId'],
                'title' => $input['coverImageTitle'],
            );
        if ( isset($input['contentImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => "course_register",
                'grpType' => 'contentImage',
                'uploadId' => $input['contentImageId'],
                'title' => $input['contentImageTitle'],
            );
        if ( isset($input['galleryImageId']) ) {
            foreach ( $input['galleryImageId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => "course_register",
                    'grpType' => 'galleryImage',
                    'uploadId' => $rs,
                    'title' => $input['galleryImageTitle'][$key]
                );
            }
        }
        if ( isset($input['docAttachId']) ) {
            foreach ( $input['docAttachId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => "course_register",
                    'grpType' => 'docAttach',
                    'uploadId' => $rs,
                    'title' => $input['docAttachTitle'][$key]
                );
            }
        }
        return $value;
    }   

    public function upload_ci($inputFileName="userfile")  {
        $config = $this->_get_config();
        create_dir($config['upload_path']);
        $this->upload->initialize($config);
       // 

        if ( !$this->upload->do_upload($inputFileName) ) {
            $result['result'] = $this->upload->display_errors();
            $result['success'] = false;
        } else {
            $result = $this->upload->data();
            $result['success'] = true;
            $pathinfo = pathinfo("{$config['upload_path']}{$result['file_name']}");
            $result = array_merge($result, $pathinfo);      
            $value = array(
                'grpContent' => 'payment',
                'filename' => $result['file_name'],
                'path' => $result['dirname'].'/',
                'size' => $result['file_size'],
                'extension' => $result['extension'],
                'mime' => $result['file_type'],
                'createDate' => db_date_now(),
                'createBy' => $this->session->user['userId']
            );
            //$this->resizeImage($result['file_name']);
            $result['insertId'] = $this->upload_m->insert($value);
            $result['file_name'] = $result['dirname'].'/'.$result['file_name'];
        }

        //print_r($result);exit();
        return $result;
    }  

     public function upload_ci_r($inputFileName="userfile")  {
        $config = $this->_get_config();
        create_dir($config['upload_path']);
        $this->upload->initialize($config);
       // 

        if ( !$this->upload->do_upload($inputFileName) ) {
            $result['result'] = $this->upload->display_errors();
            $result['success'] = false;
        } else {
            $result = $this->upload->data();
            $result['success'] = true;
            $pathinfo = pathinfo("{$config['upload_path']}{$result['file_name']}");
            $result = array_merge($result, $pathinfo);      
            $value = array(
                'grpContent' => 'course_register',
                'filename' => $result['file_name'],
                'path' => $result['dirname'].'/',
                'size' => $result['file_size'],
                'extension' => $result['extension'],
                'mime' => $result['file_type'],
                'createDate' => db_date_now(),
                'createBy' => $this->session->user['userId']
            );
            //$this->resizeImage($result['file_name']);
            $result['insertId'] = $this->upload_m->insert($value);
            $result['file_name'] = $result['dirname'].'/'.$result['file_name'];
        }

        //print_r($result);exit();
        return $result;
    }  

    
    
    private function _get_config(){

        $config['upload_path'] = 'uploads/';
        $config['max_size'] = file_upload_max_size();
       // $config['resize'] = true;
        $config['upload_path'] = 'uploads/'.date('Y').'/'.date('m').'/'.'payment'.'/';
        $config['encrypt_name'] = true;
        $config['allowed_types'] = 'pdf|gif|jpg|png|jpeg|image/png';
        //$config['resizeOverwrite'] = true;
        //$config['quality']= '90%';
        
    
        
        return $config;
    } 


}