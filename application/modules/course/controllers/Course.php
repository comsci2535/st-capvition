<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends MX_Controller {

    private $_grpContent = 'course';

    public function __construct() {
        parent::__construct();
        $this->load->model("course_m");
        $this->load->model("course_content_m");
        $this->load->model('front/front_m');
        $this->load->model("admin/upload_m");
        $this->load->library('image_moo');
    }

    public function index() {
        $this->load->module('front');
        $data['contentView'] = 'course/index';
        $this->front->layout($data);
    }
    public function top_menu() {
       
        $input['active']='1';
        $input['recycle']='0';

        $info = $this->course_m->get_rows($input)->result();
       
        
        if ( !empty($info) ) {
            foreach ( $info as $key=>&$rs ) {
                $rs->linkId = str_replace(" ","-",$rs->title_link);
                $rs->icon = base_url("assets/website/images/icon/icon-compass.svg");
                   $input['grpContent'] = 'course';
                   $input['contentId'] =$rs->courseId;
                   $file = $this->course_m->get_uplode($input)->result();
                   
                    if (!empty($file)) {
                        foreach ($file as $key => $value) {
                           
                            if($value->grpType=='contentImage'){
                                $rs->icon = base_url($value->path.$value->filename);
                            }
                            
                        }
                    }
            }
            $data['top_menu'] = $info;
            $this->load->view('top_menu', $data); 
            
        }
    }
    public function home() {
       
        $input['active']='1';
        $input['recycle']='0';

        $info = $this->course_m->get_rows($input)->result();
       
        
        if ( !empty($info) ) {
            foreach ( $info as $key=>&$rs ) {
                $rs->linkId = str_replace(" ","-",$rs->title_link);
                $input_u['grpContent'] = $this->_grpContent;
                $input_u['contentId'] = $rs->courseId;
                $file_ = $this->course_m->get_uplode($input_u)->row();
                if (!empty($file_) && is_file($file_->path.$file_->filename)) {
                    $rs->image = base_url($file_->path.$file_->filename);
                
                }

                $numl = $this->db
                                ->select('*')
                                ->from('course_member a')
                                ->where('a.courseId', $rs->courseId)
                                ->where('a.active', 1)
                                ->where('a.recycle', 0)
                                ->get()->num_rows();
                $input_p['courseId'] = $rs->courseId;
                $rs->promotion=$this->course_m->get_promotion_rows($input_p)->row_array();
                $rs->learner2=($rs->learner+$numl);
                $rs->stars=$this->reviews_stars_home($rs->courseId);

                $rs->info_user=$this->course_m->get_course_member($rs->courseId,$this->session->member['userId'])->row_array();
            }
           
            $data['course_home'] = $info;
            //arr($data['course_home']);exit();
            $this->load->view('home', $data); 
            
        }
    }
    public function reviews_stars_home($courseId)
    {
       
        $info_r=$this->course_m->get_reviews_stars_($courseId)->result_array();
        $average=array();
        if ( !empty($info_r) ) {
            foreach ($info_r as $key => $info) {
                $average[]=$info['score'];
            }
        }

        if(!empty($average)){
            $v=round($this->average($average), 0, PHP_ROUND_HALF_UP);
        }else{
            $v='0';
        }

        if($v==1){
            $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star "></span>
<span class="fa fa-star "></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>';
        }else if($v==2){
            $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star "></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>';

        }else if($v==3){
            $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>';

        }else if($v==4){
            $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>';

        }else if($v==5){
            $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>';

        }else{
            $t='<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>';
        }

        return $t;
       
    }
    public function article_page() {
       
        $input['active']='1';
        $input['recycle']='0';

        $info = $this->course_m->get_rows($input)->result();
       
        
        if ( !empty($info) ) {
            foreach ( $info as $key=>&$rs ) {
                $rs->linkId = str_replace(" ","-",$rs->title_link);
                $input_u['grpContent'] = $this->_grpContent;
                $input_u['contentId'] = $rs->courseId;
                $file_ = $this->course_m->get_uplode($input_u)->row();
                if (!empty($file_) && is_file($file_->path.$file_->filename)) {
                    $rs->image = base_url($file_->path.$file_->filename);
                
                }
                $input_p['courseId'] = $rs->courseId;
                $rs->promotion=$this->course_m->get_promotion_rows($input_p)->row_array();
            }
           
            $data['course_home'] = $info;
            //arr($data['course_home']);exit();
            $this->load->view('article_page', $data); 
            
        }
    }
    public function article_page2() {
       
        $input['active']='1';
        $input['recycle']='0';

        $info = $this->course_m->get_rows($input)->result();
       
        
        if ( !empty($info) ) {
            foreach ( $info as $key=>&$rs ) {
                $rs->linkId = str_replace(" ","-",$rs->title_link);
                $input_u['grpContent'] = $this->_grpContent;
                $input_u['contentId'] = $rs->courseId;
                $file_ = $this->course_m->get_uplode($input_u)->row();
                if (!empty($file_) && is_file($file_->path.$file_->filename)) {
                    $rs->image = base_url($file_->path.$file_->filename);
                
                }
                $input_p['courseId'] = $rs->courseId;
                $rs->promotion=$this->course_m->get_promotion_rows($input_p)->row_array();
            }
           
            $data['course_home'] = $info;
            //arr($data['course_home']);exit();
            $this->load->view('article_page-2', $data); 
            
        }
    }

    public function detail($id=0)
    {
        $this->session->unset_userdata('course_data');
        
        $this->load->module('front');

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url('course/detail/').$id;
        $this->session->set_userdata('urlreffer', $urlreffer);
        
        $input['active']='1';
        $input['recycle']='0';
        $input['title_link'] = str_replace("-"," ",$id);
        //$input['keyword']=
        $input['grpContent'] = $this->_grpContent;
        $data['keyword'] = "";
        $data['linkId']=$id;


        $info=$this->course_m->get_rows($input)->row_array();
        if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "The CAP Vision Academy");

        //print"<pre>";print_r($info); exit(); 
        if (!empty($info)) {
                $data['info_user_regis']=$this->course_m->get_course_member_regis($info['courseId'],$this->session->member['userId'])->row_array();

                $data['info_user']=$this->course_m->get_course_member($info['courseId'],$this->session->member['userId'])->row_array();
                

                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $info['courseId'];
                $info['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->course_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $info['image'] = base_url($file['path'].$file['filename']);
                    $imageSeo = base_url($file['path'].$file['filename']);
                }

                $input_pa['courseId'] = $info['courseId'];
                $input_pa['detail'] = "detail";
                //$promotion_=$this->course_m->get_promotion_rows($input_pa)->row_array();
                
                $date=date('Y-m-d');
                //arr($date); exit();
                $promotion_ = $this->db
                                ->select('*')
                                ->from('promotion a')
                                ->where('a.courseId', $info['courseId'])
                                ->where('a.active', 1)
                                ->where('a.recycle', 0)
                                ->where("((a.startDate ='0000-00-00' AND a.endDate ='0000-00-00') OR  (a.startDate <='".($date)."' AND a.endDate >='".($date)."'))")
                                ->get()->row_array();
                 $dddd=str_replace('-','', $promotion_['endDate']);
                 $dddd2=str_replace('-','', $date);
                //arr($dddd2); exit(); && intval($dddd) > intval($dddd2)
                if(!empty($promotion_)){
                    $data['promotion']=$promotion_;
                    $promotionContent=$this->course_m->get_promotion_content($data['promotion']['promotionId'])->result();
                    //
                    foreach ($promotionContent as $key => $value_) {
                        $input_['grpContent'] = $this->_grpContent;
                        $input_['contentId'] = $value_->courseId;
                        $value_->image = base_url("assets/images/no_image3.png");
                        
                        $file_ = $this->course_m->get_uplode($input_)->row_array();
                       
                        if (!empty($file_) && is_file($file_['path'].$file_['filename'])) {
                           $value_->image= base_url($file_['path'].$file_['filename']);
                          
                        }
                    }
                     $data['promotionContent']=$promotionContent;
                }
                

                $data['info'] = $info;

           // }

        }

       // arr( $data['promotionContent']);exit();
        $this->course_m->plus_view($info['courseId']);
        Modules::run('track/front',$info['courseId']);
        
        $data['course_act'] = 'active';
       
        $data['contentView'] = "course/detail";
        $data['pageHeader'] = $info['metaTitle'];
        
        $data['metaTitle'] = $info['metaTitle'];
        $data['metaDescription_'] = $info['metaDescription'];
        $data['metaKeyword_'] = $info['metaKeyword'];

        $share['ogTitle']=$info['metaTitle'];
        $share['ogDescription']=$info['metaDescription'];
        $share['ogUrl']= 'course/detail/'.$id;
        $share['ogImage']= $imageSeo;
        $this->_social_share($share);
        $this->front->layout($data);
    }

    public function list_video($id=0,$listId=0)
    {
        
        $this->load->module('front');
        
        $input['active']='1';
        $input['recycle']='0';
        $input['title_link'] = str_replace("-"," ",$id);
        //$input['keyword']=
        $input['grpContent'] = $this->_grpContent;
        $data['keyword'] = "";
        $data['name']=$input['title_link'];
        $data['linkId']=$id;
        $userId=$this->session->member['userId'];

       

        $info=$this->course_m->get_rows($input)->row_array();
        if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "The CAP Vision Academy");

        //print"<pre>";print_r($info); exit(); 
        if (!empty($info)) {
            //foreach ( $info as $key=>&$rs ) {
                 $info_user=$this->course_m->get_course_member($info['courseId'],$userId)->row_array();
                 if (empty($info_user) || $this->session->member['userId']==0){
                    redirect(site_url(''));
                 } //show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "The CAP Vision Academy");


                $input_l['course_contentId'] = $listId;
                $input_l['courseId'] = $info['courseId'];
                $input_l['parentId'] = $info['courseId'];
                $input_l['recycle'] = 0;
                $input_l['active'] = 1;
                $data['course_content_l'] = $this->course_content_m->get_list_byid($input_l)->row_array();
                $input_f['contentId'] = $data['course_content_l']['course_contentId'];
                $input_f['grpContent'] ="course_content";
                $input_f['grpType'] = "docAttach";
                $data['course_content_l']['file'] = "";
                
                $file_f = $this->course_m->get_uplode($input_f)->row_array();
                
                if($data['course_content_l']['fileTypeUploadType']=='1' ){
                    if (!empty($file_f) && is_file($file_f['path'].$file_f['filename'])) {
                        $data['course_content_l']['file'] = site_url('course/download/'.$data['course_content_l']['course_contentId']);//base_url($file_f['path'].$file_f['filename']);
                    }
                }else if ($data['course_content_l']['fileTypeUploadType']=='2' && $data['course_content_l']['fileUrl']!='' ) {
                    $data['course_content_l']['file'] = $data['course_content_l']['fileUrl'];
                    //base_url($file_f['path'].$file_f['filename']);
                }

                $this->plus_view_video($data['course_content_l']['course_contentId'],$userId);
               
                $input['courseId'] = $info['courseId'];
                $input['recycle'] = 0;
                $input['active'] = 1;
         
                $course_content = $this->course_content_m->get_list($input)->result();
                
                foreach ($course_content as $key => $value) {
                    $input['parentId'] = $value->course_contentId;
                    $value->parent = $this->course_content_m->get_list($input)->result();
                    foreach ($value->parent as $key_ => $value_) {
                        
                            $input['contentId'] = $value_->course_contentId;
                            $input['grpContent'] ="course_content";
                            $input['grpType'] = "docAttach";
                            $value_->file = "";
                            $value_->active = $this->db
                                ->select('*')
                                ->from('course_content_member a')
                                ->where('a.course_contentId', $value_->course_contentId)
                                ->where('a.userId', $userId)
                                ->get()->row();
                            $file = $this->course_m->get_uplode($input)->row_array();
                           
                            if (!empty($file) && is_file($file['path'].$file['filename'])) {
                                $value_->file = base_url($file['path'].$file['filename']);
                            }
                    }
                }

                $data['info'] = $info;
                $data['course_content'] =$course_content;

                
               
                //arr($data['course_content_l']);exit();
           // }

        }

        //arr( $data['course_content_l']);exit();
        $this->course_m->plus_view($info['courseId']);
        Modules::run('track/front',$info['courseId']);
        
        $data['course_act'] = 'active';
       
        $data['contentView'] = "course/list_video";
        $data['pageHeader'] = $info['metaTitle'];
        
        $data['metaTitle'] = $info['metaTitle'];
        $data['metaDescription'] = $info['metaDescription'];
        $data['metaKeyword'] = $info['metaKeyword'];

        $share['ogTitle']=$info['metaTitle'];
        $share['ogDescription']=$info['metaDescription'];
        $share['ogUrl']= 'course/detail/'.$id;
        $share['ogImage']= $imageSeo;
        $this->_social_share($share);
        $this->front->layout($data);
    }

     public function get_course_content()
    {
        $input = $this->input->post(null, true);
        $input_l['course_contentId'] = $input['course_contentId'];
        $input_l['parentId'] = $input['course_contentId'];
        $input_l['recycle'] = 0;
        $input_l['active'] = 1;
        $data['course_content_l'] = $this->course_content_m->get_list_byid($input_l)->row_array();
        $input_f['contentId'] = $data['course_content_l']['course_contentId'];
        $input_f['grpContent'] ="course_content";
        $input_f['grpType'] = "docAttach";
        $data['course_content_l']['file'] = "";
        
        $file_f = $this->course_m->get_uplode($input_f)->row_array();
        
        if($data['course_content_l']['fileTypeUploadType']=='1' ){
            if (!empty($file_f) && is_file($file_f['path'].$file_f['filename'])) {
                $data['course_content_l']['file'] = '<div class="file-upload"><a target="_blank" href="'.site_url('course/download/'.$data['course_content_l']['course_contentId']).'"><span style="font-size: 20px"><i class="fa fa-file-text-o"></i></span> เอกสารดาวน์โหลด</a></div>';
            }
        }else if ($data['course_content_l']['fileTypeUploadType']=='2' && $data['course_content_l']['fileUrl']!='' ) {
            $data['course_content_l']['file'] =  '<div class="file-upload"><a target="_blank" href="'.$data['course_content_l']['fileUrl'].'"><span style="font-size: 20px"><i class="fa fa-file-text-o"></i></span> เอกสารดาวน์โหลด</a></div>';
            //base_url($file_f['path'].$file_f['filename']);
        }
        $data['course_content_l']['title_'] = '<div class="title-content"><h3>'.$data['course_content_l']['title'].'</h3></div>';

        $data['course_content_l']['descript'] = '<div class="detail-content">'.html_entity_decode($data['course_content_l']['descript']).'</div>';
        $data['course_content_l']['videoLink'] = str_replace('https://vimeo.com/', '', $data['course_content_l']['videoLink']);

        $userId=$this->session->member['userId'];
        $this->plus_view_video($input['course_contentId'],$userId);

        $data['course_content_l']['status'] = $this->get_view_video($input['course_contentId'],$userId);



        echo json_encode($data);
    }

    public function get_view_video($listId,$userId)
    {
        $info = $this->db
                        ->select('*')
                        ->from('course_content_member a')
                        ->where('a.course_contentId', $listId)
                        ->where('a.userId', $userId)
                        ->get()->row();
        return $info->status;
    }

    public function plus_view_video($listId,$userId)
    {
        $info = $this->db
                        ->select('*')
                        ->from('course_content_member a')
                        ->where('a.course_contentId', $listId)
                        ->where('a.userId', $userId)
                        ->get()->row();
        if(empty($info)){
            $value['course_contentId']=$listId;
            $value['userId']=$userId;
            $value['updateDate']=db_datetime_now();
           $this->db->insert('course_content_member', $value);
        }
    }
    public function plus_view_video2()
    {   
        $input = $this->input->post(null, true);
        $userId=$this->session->member['userId'];
        $listId=$input['course_contentId'];
           
       $value['updateDate']=db_datetime_now();
       $value['status']=1;
       $this->db
          ->where('course_contentId', $listId)
          ->where('userId', $userId)
          ->update('course_content_member', $value);

         $data['status'] = 1;



        echo json_encode($data);
    }

    public function download($id) {
        $this->load->helper('download');
        Modules::run('track/front',$id);

       $input_l['course_contentId'] = $id;
       $course_content_l = $this->course_content_m->get_list_byid($input_l)->row_array();

        $input['contentId'] = $id;
        $input['grpContent'] ="course_content";
        $input['grpType'] = "docAttach";
        
        
        $file = $this->course_m->get_uplode($input)->row_array();
       
        // if (!empty($file) && is_file($file['path'].$file['filename'])) {
        //     $value_->file = base_url($file['path'].$file['filename']);
        // }     
        // $info = $this->db->.....
        //$filePath = "";
        if (!empty($file) && is_file($file['path'].$file['filename'])) {
            $filePath = $file['path'].'/'.$file['filename'];
            $extension = $file['extension'];
            $this->_push_file($filePath,$extension,$course_content_l['title']);
        } else {
            $this->session->set_flashdata('downloadError', '1');
           // redirect(site_url("informationlist"));
        }
    }

    public function _push_file($path,$extension, $name)
    {     //print_r($path);exit();
          // make sure it's a file before doing anything!
          if(is_file($path))
          {
            // required for IE
            if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

            // get the file mime type using the file extension
            $this->load->helper('file');

            $mime = get_mime_by_extension($path);
            //print_r($mime);exit();

            // Build the headers to push out the file properly.
            header('Pragma: public');     // required
            header('Expires: 0');         // no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
            header('Cache-Control: private',false);
            header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
            header('Content-Disposition: attachment; filename="'.$name.'.'.$extension.'"');  // Add the file name
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.filesize($path)); // provide file size
            header('Connection: close');
            readfile($path); // push it out
            exit();
        }
    }

    public function registerCourse(){

        $this->session->unset_userdata('course_data');
        $course_data = $this->session->userdata('course_data');

        

        if($course_data == null || count($course_data) == 0){
            $course_data = array();

        }

        
        $courseId = $this->input->post('courseId');
       
        $course_data['courseId'] = $courseId;
           
        
 
        //update cart and redirect cart index
        $this->session->set_userdata('course_data', $course_data);
        exit();
        // redirect($this->data['lang_slug'].'publication/cart', 'refresh');

    }

    public function register($id=0){
        $course_data = $this->session->userdata('course_data');
        if($this->session->member['userId']==0 ||  $course_data==""){
            $this->session->unset_userdata('course_data');
            redirect('home', 'refresh');
        }
       
        //print"<pre>";print_r($course_data); exit(); 
        $this->load->module('front');
        $input['active']='1';
        $input['recycle']='0';
        $input['courseId'] = $id;//$course_data['courseId'];
        //$input['keyword']=
        $input['grpContent'] = $this->_grpContent;
        

        $info=$this->course_m->get_rows($input)->row_array();
        if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "The CAP Vision Academy");

        
        if (!empty($info)) {
            //foreach ( $info as $key=>&$rs ) {

                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $info['courseId'];
                $info['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->course_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $info['image'] = base_url($file['path'].$file['filename']);
                    $imageSeo = base_url($file['path'].$file['filename']);
                }

                $input_p['courseId'] = $id;//$info['courseId'];
               // $info['promotion']=$this->course_m->get_promotion_rows($input_p)->row_array();

                $date=date('Y-m-d');
                //arr($date); exit();
                $promotion_ = $this->db
                                ->select('*')
                                ->from('promotion a')
                                ->where('a.courseId', $id)
                                ->where('a.active', 1)
                                ->where('a.recycle', 0)
                                ->where("((a.startDate ='0000-00-00' AND a.endDate ='0000-00-00') OR  (a.startDate <='".($date)."' AND a.endDate >='".($date)."'))")
                                ->get()->row_array();
                 $dddd=str_replace('-','', $promotion_['endDate']);
                 $dddd2=str_replace('-','', $date);
                 // && intval($dddd) > intval($dddd2)
                if(!empty($promotion_)){
                    $info['promotion']=$promotion_;
                    $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
                    foreach ($promotionContent as $key => $value_) {
                        $input_['grpContent'] = $this->_grpContent;
                        $input_['contentId'] = $value_->courseId;
                        $value_->image = base_url("assets/images/no_image3.png");
                        
                        $file_ = $this->course_m->get_uplode($input_)->row_array();
                       
                        if (!empty($file_) && is_file($file_['path'].$file_['filename'])) {
                           $value_->image= base_url($file_['path'].$file_['filename']);
                          
                        }
                    }
                    $data['promotionContent']=$promotionContent;

                }
                

                $data['info'] = $info;

           // }

        }
        // arr($data['info']);exit();

         $data['course_act'] = 'active';
       
         $data['contentView'] = "course/register";
         $this->front->layout($data);

    }

    

    public function registerSave(){

        $input = $this->input->post();
        $value=$this->_build_data($input);
//arr($input);exit();
        $result = $this->course_m->insertCourseRegis($value);
        if($result){


            $data['course_register']=$this->course_m->get_CourseRegis2($result)->row();
    ///
            $input['active']='1';
            $input['recycle']='0';
            $input['courseId'] = $data['course_register']->courseId;
            //$input['keyword']=
            $input['grpContent'] = $this->_grpContent;
            

            $info=$this->course_m->get_rows($input)->row_array();
           
            //print"<pre>";print_r($data['course_register']); exit(); 
            if (!empty($info)) {
                //foreach ( $info as $key=>&$rs ) {

                    $input['grpContent'] = $this->_grpContent;
                    $input['contentId'] = $info['courseId'];
                    $info['image'] = base_url("assets/images/no_image3.png");
                    
                    $file = $this->course_m->get_uplode($input)->row_array();
                   
                    if (!empty($file) && is_file($file['path'].$file['filename'])) {
                        $info['image'] = base_url($file['path'].$file['filename']);
                        $imageSeo = base_url($file['path'].$file['filename']);
                    }

                    $input_p['courseId'] = $info['courseId'];
                    $info['promotion']=$this->course_m->get_promotion_rows($input_p)->row_array();

                    $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
                    foreach ($promotionContent as $key => $value_) {
                        $input_['grpContent'] = $this->_grpContent;
                        $input_['contentId'] = $value_->courseId;
                        $value_->image = base_url("assets/images/no_image3.png");
                        
                        $file_ = $this->course_m->get_uplode($input_)->row_array();
                       
                        if (!empty($file_) && is_file($file_['path'].$file_['filename'])) {
                           $value_->image= base_url($file_['path'].$file_['filename']);
                          
                        }
                    }
                    $data['promotionContent']=$promotionContent;

                    $data['info'] = $info;

               // }

            }

            $email=$data['course_register']->email;

            $input['type'] = 'mail';

            $input['variable'] = 'SMTPserver';
            $SMTPserver=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPusername';
            $SMTPusername=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPpassword';
            $SMTPpassword=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPport';
            $SMTPport=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'senderEmail';
            $senderEmail=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'senderName';
            $senderName=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'mailDefault';
            $mailDefault=$this->front_m->getConfig($input)->row();
            
            $textEmail = $this->load->view('course/mail/mailer_form_r.php',$data , TRUE);
                          
                           

              require 'application/third_party/phpmailer/PHPMailerAutoload.php';
              $mail = new PHPMailer;

              //$mail->SMTPDebug = 3;                               // Enable verbose debug output

              $mail->isSMTP();                                      // Set mailer to use SMTP
              $mail->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
              $mail->SMTPAuth = true;                               // Enable SMTP authentication
              $mail->Username = $SMTPusername->value;                // SMTP username
              $mail->Password = $SMTPpassword->value;                          // SMTP password
              $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
              $mail->Port = $SMTPport->value;                                           // TCP port to connect to
              $mail->CharSet = 'UTF-8';
              $mail->From = $senderEmail->value;
              $mail->FromName = $senderName->value;
              $mail->addAddress($email);               // Name is optional
              
              $mail->isHTML(true);                                  // Set email format to HTML

              $mail->Subject = $email.' : ลงทะเบียนเรียน';
              $mail->Body    = $textEmail;
              $mail->AltBody = $textEmail;
              $mail->send();

              $textEmail2 = $this->load->view('course/mail/mailer_form_r_admin.php',$data , TRUE);

                $mail2 = new PHPMailer;
                
                $mail2->isSMTP();                                      // Set mailer to use SMTP
                $mail2->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
                $mail2->SMTPAuth = true;                               // Enable SMTP authentication
                $mail2->Username = $SMTPusername->value;                // SMTP username
                $mail2->Password = $SMTPpassword->value;                          // SMTP password
                $mail2->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail2->Port = $SMTPport->value;                                           // TCP port to connect to
                $mail2->CharSet = 'UTF-8';
                $mail2->From = $senderEmail->value;
                $mail2->FromName = $senderName->value;
                $mail2->addAddress($mailDefault->value);               // Name is optional

                
                $mail2->isHTML(true);                                  // Set email format to HTML

                $mail2->Subject = $email.' : ลงทะเบียนเรียน';
                $mail2->Body    = $textEmail2;
                $mail2->AltBody = $textEmail2;
                $mail2->send();

            $this->session->unset_userdata('course_data');
            
            $resp_msg = array('info_txt'=>"success",'msg'=>'ลงทะเบียนเรียบร้อย','msg2'=>'กรุณารอสักครู่...','result'=>$result);
                echo json_encode($resp_msg);
                return false;
        } else {
            $resp_msg = array('info_txt'=>"error",'msg'=>'ลงทะเบียนไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
                echo json_encode($resp_msg);
               return false;
        }
        //arr($result);exit();

    }

    public function checkCoupon(){

        $input = $this->input->post();
        $value=$this->_build_data($input);
        //arr($input);exit();
        $result = $this->course_m->getCoupon($input)->row_array();
        if(!empty($result)){

            if($result['isCourse']==1){
                $courseNum = $this->db
                                ->select('*')
                                ->from('course_register a')
                                ->where('a.userId', $this->session->member['userId'])
                                ->where('a.couponCode', $input['couponCode'])
                                ->get()->num_rows();

                 // arr($result['isCourseNum']);exit();

                if($courseNum < $result['isCourseNum']){
                    $c=1;
                }else{
                    $c=0;
                }

            }else{
                $c=1;
            }

            if($result['isMember']==1){
                $memberNum = $this->db
                                ->select('*')
                                ->from('course_register a')
                                ->where('a.couponCode', $input['couponCode'])
                                ->get()->num_rows();

                if($memberNum < $result['isMemberNum']){
                    $m=1;
                }else{
                    $m=0;
                }

            }else{
                $m=1;
            }

            if($c==0 && $m==1){
                $resp_msg = array('info_txt'=>"error",'msg'=>'ท่านได้ใช้รหัสคูปองนี้ครบแล้ว','msg2'=>'กรุณาตรวจสอบอีกครั้ง !');
                echo json_encode($resp_msg);
                return false;
            }else if($c==1 && $m==0){
                 $resp_msg = array('info_txt'=>"error",'msg'=>'รหัสคูปองนี้ได้ลงทะเบียนครบ '.$result['isMemberNum'].' ท่านแรกแล้ว','msg2'=>'กรุณาตรวจสอบอีกครั้ง !');
                echo json_encode($resp_msg);
                return false;
            }else if($c==0 && $m==0){
                 $resp_msg = array('info_txt'=>"error",'msg'=>'รหัสคูปองนี้ได้ลงทะเบียนครบ '.$result['isMemberNum'].' ท่านแรกแล้ว','msg2'=>'กรุณาตรวจสอบอีกครั้ง !');
                echo json_encode($resp_msg);
                return false;
            }else{
                $resp_msg = array('info_txt'=>"success",'type_'=>$result['type'],'discount'=>$result['discount'],'result'=>$result);
                echo json_encode($resp_msg);
                return false;
            } 

            // $this->session->unset_userdata('course_data');
            
            
        } else {
            $input['userId']=$this->session->member['userId'];
            $result2 = $this->course_m->getCouponMember($input)->row_array();
            if(!empty($result2)){

                $resp_msg = array('info_txt'=>"success",'type_'=>1,'discount'=>200,'result'=>$result2);
                echo json_encode($resp_msg);
                return false;

             }else{

                $resp_msg = array('info_txt'=>"error",'msg'=>'รหัสคูปองนี้ไม่สามารถใช้งานได้','msg2'=>'กรุณาตรวจสอบอีกครั้ง !');
                    echo json_encode($resp_msg);
                   return false;
            }
        }
        //arr($result);exit();

    }

    public function register_success($id){

        $this->load->module('front');
        
        $this->session->unset_userdata('course_data');
        $data['course_register']=$this->course_m->get_CourseRegis2($id)->row();
///
        $input['active']='1';
        $input['recycle']='0';
        $input['courseId'] = $data['course_register']->courseId;
        //$input['keyword']=
        $input['grpContent'] = $this->_grpContent;
        

        $info=$this->course_m->get_rows($input)->row_array();
        if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "The CAP Vision Academy");

        //print"<pre>";print_r($data['course_register']); exit(); 
        if (!empty($info)) {
            //foreach ( $info as $key=>&$rs ) {

                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $info['courseId'];
                $info['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->course_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $info['image'] = base_url($file['path'].$file['filename']);
                    $imageSeo = base_url($file['path'].$file['filename']);
                }

                $input_p['courseId'] = $info['courseId'];
                $info['promotion']=$this->course_m->get_promotion_rows($input_p)->row_array();

                $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
                foreach ($promotionContent as $key => $value_) {
                    $input_['grpContent'] = $this->_grpContent;
                    $input_['contentId'] = $value_->courseId;
                    $value_->image = base_url("assets/images/no_image3.png");
                    
                    $file_ = $this->course_m->get_uplode($input_)->row_array();
                   
                    if (!empty($file_) && is_file($file_['path'].$file_['filename'])) {
                       $value_->image= base_url($file_['path'].$file_['filename']);
                      
                    }
                }
                $data['promotionContent']=$promotionContent;

                $data['info'] = $info;

           // }

        }

        //arr($data['info']);exit();
         $data['course_act'] = 'active';
       
         $data['contentView'] = "course/register_success";
         $this->front->layout($data);

    }

     

    public function course_history(){

        $this->load->module('front');
        
        if($this->session->member['userId']==0){
            
            redirect('home', 'refresh');
        }
        $id=$this->session->member['userId'];
        $course_register=$this->course_m->get_CourseRegisByUserId($id)->result();
        //arr($course_register);exit();
        foreach ($course_register as $key => $value) {
            $value->course_payment=$this->course_m->get_course_payment_by_regis($value->course_registerId)->row();


            $input['active']='1';
            $input['recycle']='0';
            $input['courseId'] = $value->courseId;
            //$input['keyword']=
            $input['grpContent'] = $this->_grpContent;
            

            $value->course=$this->course_m->get_rows($input)->row_array();
            $value->course['linkId'] = str_replace(" ","-",$value->course['title_link']);
            $input['grpContent'] = $this->_grpContent;
            $input['contentId'] = $value->courseId;
            $value->image = base_url("assets/images/no_image3.png");
            
            $file = $this->course_m->get_uplode($input)->row_array();
           
            if (!empty($file) && is_file($file['path'].$file['filename'])) {
                $value->image = base_url($file['path'].$file['filename']);
            }

            $input_p['courseId'] = $value->courseId;
            $value->promotion=$this->course_m->get_promotion_rows($input_p)->row_array();

            $promotionContent=$this->course_m->get_promotion_content($value->promotion['promotionId'])->result();
            foreach ($promotionContent as $key => $value_) {
                $input_['grpContent'] = $this->_grpContent;
                $input_['contentId'] = $value_->courseId;
                $value_->image = base_url("assets/images/no_image3.png");
                
                $file_ = $this->course_m->get_uplode($input_)->row_array();
               
                if (!empty($file_) && is_file($file_['path'].$file_['filename'])) {
                   $value_->image= base_url($file_['path'].$file_['filename']);
                  
                }
            }
            $value->promotionContent=$promotionContent;

           

               
        }
///
         $data['info'] = $course_register;

        //arr($data['info']);exit();
         //$data['course_act'] = 'active';
       
         $data['contentView'] = "course/course_history";
         $this->front->layout($data);

    }
    private function _build_data($input) {
       
        
        $value['userId'] = $this->session->member['userId'];
        $value['courseId'] = $input['courseId'];
        $value['promotionId'] = $input['promotionId'];
        $value['price'] = $input['price'];
        $value['code'] = "GD".time();
        $value['couponCode'] = $input['couponCode'];

        if(!empty($input['type'])){
            $value['type'] = $input['type'];
        }

        if(!empty($input['discount'])){
             $value['discount'] = $input['discount'];
        }
        
       
        $value['status'] = 0;
        $value['createDate'] = db_datetime_now();
        $value['createBy'] = $this->session->member['userId'];
        // $value['updateDate'] = db_datetime_now();
        // $value['updateBy'] = $this->session->member['userId'];
       
        return $value;
    }

    public function _social_share($input)
    {
        $param['ogType'] = "webiste";
        $param['ogUrl'] = $input['ogUrl'];
        $param['ogTitle'] = $input['ogTitle'];
        $param['ogDescription'] = $input['ogDescription'];
        $param['ogImage'] = $input['ogImage'];
        $param['twImage'] = $input['ogImage'];
        
        Modules::run('social/set_share', $param);
    }  

    public function instructor($instructorId=0)
    {
       
       
        $input['grpContent'] = 'Instructor';
        $input['instructorId'] = $instructorId;
 
        $info_ = $this->course_m->get_rows_instructor($input)->row_array();
        

        if (!empty($info_)) {
            
            $input['contentId'] = $instructorId;
            $data['image'] = base_url("assets/images/no_image3.png");
            
            $file = $this->course_m->get_uplode($input)->row_array();
           
            if (!empty($file) && is_file($file['path'].$file['filename'])) {
                $data['image'] = base_url($file['path'].$file['filename']);
            }
            
            $data['instructor'] = $info_;

            $this->load->view('instructor', $data); 
            
        }

       
    }

    public function instructor_detail($instructorId=0)
    {
       
       
        $input['grpContent'] = 'Instructor';
        $input['instructorId'] = $instructorId;
 
        $info_ = $this->course_m->get_rows_instructor($input)->row_array();
        

        if (!empty($info_)) {
            
            $input['contentId'] = $instructorId;
            $data['image'] = base_url("assets/images/no_image3.png");
            
            $file = $this->course_m->get_uplode($input)->row_array();
           
            if (!empty($file) && is_file($file['path'].$file['filename'])) {
                $data['image'] = base_url($file['path'].$file['filename']);
            }
            
            $data['instructor'] = $info_;

            $this->load->view('instructor_detail', $data); 
            
        }

       
    }

    public function instructor_contact($instructorId=0)
    {
       
       
        $input['grpContent'] = 'Instructor';
        $input['instructorId'] = $instructorId;
 
        $info_ = $this->course_m->get_rows_instructor($input)->row_array();
        

        if (!empty($info_)) {
            
            $input['contentId'] = $instructorId;
            $data['image'] = base_url("assets/images/no_image3.png");
            
            $file = $this->course_m->get_uplode($input)->row_array();
           
            if (!empty($file) && is_file($file['path'].$file['filename'])) {
                $data['image'] = base_url($file['path'].$file['filename']);
            }
            
            $data['instructor'] = $info_;

            $this->load->view('contact', $data); 
            
        }

       
    }

    public function couse_content($courseId=0)
    {
       
       
       
        $input['courseId'] = $courseId;
        $input['recycle'] = 0;
        $input['active'] = 1;
 
        $course_content = $this->course_content_m->get_list($input)->result();
        
        foreach ($course_content as $key => $value) {
            $input['parentId'] = $value->course_contentId;
            $value->parent = $this->course_content_m->get_list($input)->result();
            foreach ($value->parent as $key_ => $value_) {
                
                    $input['contentId'] = $value_->course_contentId;
                    $input['grpContent'] ="course_content";
                    $input['grpType'] = "docAttach";
                    $value_->file = "";
                    
                    $file = $this->course_m->get_uplode($input)->row_array();
                   
                   
                    if($value_->fileTypeUploadType=='1' ){
                        if (!empty($file) && is_file($file['path'].$file['filename'])) {
                            $value_->file = site_url('course/download/').$value_->course_contentId;
                        }
                    }else if ($value_->fileTypeUploadType=='2' && $value_->fileUrl!='' ) {
                       $value_->file =  $value_->fileUrl;
                        //base_url($file_f['path'].$file_f['filename']);
                    }
                    // if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    //     $value_->file = base_url($file['path'].$file['filename']);
                    // }
            }
        }
        $info=$course_content;
        if (!empty($info)) {
            
            //arr($info);
            $data['content'] = $info;
            $this->load->view('couse_content', $data); 
            
        }
         

       
    }

    public function play_video($course_contentId)
    {
       
       
       
        $input['course_contentId'] = $course_contentId;
        
 
        $course_content = $this->course_content_m->get_list_byid($input)->row();
        //arr($course_content);exit();
        $info=$course_content;
        if (!empty($info)) {
            
           
            
            $data['content'] = $info;
            $this->load->view('play_video', $data); 
            
        }
         

       
    }

    public function reviews($courseId)
    {
       
        $reviews = $this->course_m->get_reviews($courseId)->result();
        //
        $info=$reviews;
        if (!empty($info)) {

            foreach ($info as $key => $rs) {
                   $input['grpContent'] = 'reviews';
                   $input['contentId'] =$rs->reviewsId;
                   $data['image'] = base_url("assets/images/no_image3.png");
                    
                    $rs->file = $this->course_m->get_uplode($input)->result();
                   
                    if (!empty($rs->file)) {
                        foreach ($rs->file as $key => $value) {
                            if($value->grpType=='coverImage'){
                                $rs->file['coverImage'] = base_url($value->path.$value->filename);
                            }
                            if($value->grpType=='contentImage'){
                                $rs->file['contentImage'] = base_url($value->path.$value->filename);
                            }
                            
                        }
                        //$data['image'] = base_url($file['path'].$file['filename']);
                    }
            }
            $data['reviews'] = $info;
            $this->load->view('reviews', $data); 
            
        }
        // arr($data['reviews']);exit();

       
    }



    public function promotion() {
         
       
        $input['']="";
        $info=$this->course_m->get_promotion_rows($input)->result();
       
        
        if ( !empty($info) ) {
           // echo $info;
            foreach ($info as $key => $value) {
                 $value->dd=date('F d,Y', strtotime($value->endDate)).' 23:59:59'; 
            }
            $array =  (array) $info;
            //arr($array);exit();
            echo json_encode($array);
        }
         

    }

    public function promotionByid() {
         
       
        $input['courseId']=$this->input->post('courseId');
        $info=$this->course_m->get_promotion_rows($input)->row_array();
       
        
        if ( !empty($info) ) {
           // echo $info;
            //foreach ($info as $key => $value) {
            $info['dd']=date('F d,Y', strtotime($info['endDate'])).' 23:59:59'; 
            //}
            //$array =  (array) $info;
            //arr($array);exit();
            echo json_encode($info);
        }
         

    }

     public function reviews_stars() {
         
       
        $input=$this->input->post();
        $value['courseId']=$input['courseId'];
        $value['score']=$input['score'];
        $value['comment']=$input['comment'];
        $value['userId']=$this->session->member['userId'];
        $value['createDate']=db_datetime_now();
        $value['updateDate']=db_datetime_now();
        $info=$this->course_m->save_reviews_stars($input['courseId'],$this->session->member['userId'],$value);
       
        
        if ($info) {
            $rs=$this->get_reviews_stars($input['courseId']);

            echo json_encode($rs);
        }
         

    }

    public function get_reviews_stars2() {
        $input=$this->input->post();
        $value['courseId']=$input['courseId'];
        $rs=$this->get_reviews_stars($input['courseId']);
        echo json_encode($rs);
    }

    public function get_reviews_stars($courseId) {
         
        $info_r=$this->course_m->get_reviews_stars_($courseId)->result_array();
       // arr($rs);exit();
        $txt['t']="<br>";
        $average=array();
        $v1=0;
        $v2=0;
        $v3=0;
        $v4=0;
        $v5=0;
        if ( !empty($info_r) ) {
            foreach ($info_r as $key => $info) {
                $average[]=$info['score'];
                $image = base_url('uploads/user.png');
                $upload = Modules::run('admin/upload/get_upload', $info['userId'], 'user', 'coverImage');
                if ( $upload->num_rows() != 0 ) {
                    $row = $upload->row();
                    if ( is_file("{$row->path}/{$row->filename}") )
                        $image = base_url("{$row->path}{$row->filename}");
                }else{
                  
                    if($info['picture']!=""){
                         $image = $info['picture'];
                    }else{
                        $image = base_url('uploads/user.png');
                    }
                   
                }
                $sc1="";
                $sc2="";
                $sc3="";
                $sc4="";
                $sc5="";

                if($info['score']=="1"){
                   $sc1="checked"; 
                   $v1++;

                    $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star "></span>
<span class="fa fa-star "></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>';
                }
                if($info['score']=="2"){
                   $sc2="checked"; 
                   $v2++;

                    $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star "></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>';
                }
                if($info['score']=="3"){
                   $sc3="checked"; 
                   $v3++;

                    $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>';
                }
                if($info['score']=="4"){
                   $sc4="checked"; 
                   $v4++;

                    $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>';
                }
                if($info['score']=="5"){
                   $sc5="checked";
                   $v5++; 

                    $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>';
                }

                

                $txt['t'].='<section id="section-team" class="topmargin-lg">
                    <div class="row">
                        <div class="col-sm-2 col-md-2 col-4 text-center">
                            <img src="'.$image.'" alt="" style="width:40px;">
                        </div>
                        <div class="col-sm-4 col-md-3 col-8">
                        <span class="rating3">
                            '.$t.'
                        </span>
                        </div>
                        <div class="col-sm-6 col-md-7 col-12">
                          <div class="c-name"> by '.$info['firstname'].' '.$info['lastname'].'</div><div class="c-comment">'.$info['comment'].'</div>
                        </div>
                    </div>
                    
                    </section>';
             } 
        }
        if(!empty($average)){
            $txt['v']=$this->average($average);
        }else{
            $txt['v']='0';
        }
        
        $txt['v1']=$v1;
        $txt['v2']=$v2;
        $txt['v3']=$v3;
        $txt['v4']=$v4;
        $txt['v5']=$v5;

        $t=count($info_r);
        $txt['p1']='<div class="progress-bar progress-bar-yellow" style="width: '.(($v1*100)/$t).'%"></div>';
        $txt['p2']='<div class="progress-bar progress-bar-yellow" style="width: '.(($v2*100)/$t).'%"></div>';
        $txt['p3']='<div class="progress-bar progress-bar-yellow" style="width: '.(($v3*100)/$t).'%"></div>';
        $txt['p4']='<div class="progress-bar progress-bar-yellow" style="width: '.(($v4*100)/$t).'%"></div>';
        $txt['p5']='<div class="progress-bar progress-bar-yellow" style="width: '.(($v5*100)/$t).'%"></div>';
        
        return $txt;
         

    }

    public function average($arr) {
        $array_size = count($arr);

        $total = 0;
        for ($i = 0; $i < $array_size; $i++) {
            $total += $arr[$i];
        }

        $average = (float)($total / $array_size);
        return number_format($average,1);
    }


}
