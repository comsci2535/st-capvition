<!-- ##### Popular Courses Start ##### -->
<section class="popular-courses-area section-padding-100-0" style="background-image: url(img/core-img/texture.png);">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-heading">
                    <h3>คอร์สเรียนแนะนำ</h3>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- Single Popular Course -->
            <?php foreach ($course_home as $key => $rs) :?>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="single-popular-course mb-100 wow fadeInUp" data-wow-delay="250ms">
                    <img src="<?php echo $rs->image ?>" alt="">
                    <!-- Course Content -->
                    <div class="course-content">
                          <a href="<?php echo site_url("course/detail/{$rs->linkId}");?>">
                            <h4><?php echo $rs->title ?></h4>
                        </a>
                        <div class="meta d-flex align-items-center">
                            <a href="<?php echo site_url("course/detail/{$rs->linkId}");?>"><?php echo $rs->instructor_name ?></a>
                          <!--   <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                            <a href="<?php echo site_url("course/detail/{$rs->linkId}");?>">Art &amp; Design</a> -->
                        </div>
                        <p><?php echo $rs->excerpt ?></p>
                    </div>
                    <!-- Seat Rating Fee -->
                    <div class="seat-rating-fee d-flex justify-content-between">
                        <div class="seat-rating h-100 d-flex align-items-center">
                            <div class="seat">
                                <i class="fa fa-user" aria-hidden="true"></i> <?php echo $rs->learner2 ?>
                            </div>
                            <div class="rating">
                                <i class="fa fa-star" aria-hidden="true"></i> 4.5
                            </div>
                        </div>
                        <div class="course-fee h-100">
                            <a href="<?php echo site_url("course/detail/{$rs->linkId}");?>" class="free"><?php echo number_format($rs->price) ?></a>
                        </div>
                    </div>
                </div>
            </div>
             <?php endforeach; ?> 

            
        </div>
    </div>
</section>
