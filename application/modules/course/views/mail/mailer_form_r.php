<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>The CAP Vision Academy</title>
<style type="text/css">
  #customers {
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #6d6e72;
    color: white;
}
</style>
</head>

<body>
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left"  style="border: 2px solid #27160E;">
   <img src="<?php echo base_url("assets/website/") ?>images/logo.jpg" class="ls-bg" alt="" style="width: 45px;"/> 
    </td>
  </tr>

  <tr>
    <td align="center" valign="top" bgcolor="" style="border: 2px solid #27160E;font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
        <tr>
          <td align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#525252;">

          <div style="font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#000000;">เรียน คุณ <?php echo $course_register->firstname.' '.$course_register->lastname; ?></div>
            <div style="font-size:18px;">
                ท่านได้ลงทะเบียนเรียนเรียบร้อย 
                <br>เลขที่อ้างอิง <?php echo $course_register->code;?>
                <br>
                <br>
                ขอบคุณที่ใช้บริการ
                <br>
                ระบบจะอนุมัติ
                <br>
                รายการภายใน 10 ชม.
            </div>

            <div style="font-size:16px;">
                 รายละเอียด :<br>
        เลขที่สั่งซื้อ : <?php echo  $course_register->code ?><br>
        <table id="customers" style="font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#000000;width: 100% ">
                <tr >
                  <th >รายละเอียดคอร์สเรียน</th>
                  <th style="text-align: center;width: 15%">ราคาปกติ</th>
                  <th style="text-align: center;width: 15%">ราคาโปรโมชั่น</th>
                  <th style="text-align: center;width: 15%">คูปองส่วนลด</th>
                  <th style="text-align: center;width: 15%">ราคา</th>
                </tr>
                <tr >
                  <td  >
                  
                      <span style="width: 40%">
                          <img src="<?php echo $info['image'];?>" style="width: 100%">
                      </span>
                      <span style="width: 60%">
                            <h4><?php echo $info['title'] ?></h4>
                         <p><?php echo $info['excerpt'] ?></p> 
                     </span> 
                    
                  </td>
                  <td style="text-align: right;" valign="top">
                    
                    <?php if(!empty($info['promotion']) && $info['promotion']['discount']!='0'){ ?>
                       <span style="text-decoration: line-through;"><?php echo number_format($info['price'])?></span>
                    <?php }else{ ?>
                       <?php echo number_format($info['price'])?>
                    <?php } ?>
                  </td>
                 
                    <?php if(!empty($info['promotion']) && $info['promotion']['discount']!='0'){ ?>
                       <td style="text-align: right;" valign="top">
                      <?php echo number_format($info['promotion']['discount'])?>
                       </td>
                    <?php }else if(!empty($info['promotion']) && $info['promotion']['discount']=='0' && $info['promotion']['type']=='1'){ ?>
                       <td style="text-align: center;" valign="top">
                        โปรโมชั่นแถมคอร์ส
                      </td>
                       <?php }else if(!empty($info['promotion']) && $info['promotion']['discount']=='0' && $info['promotion']['type']=='2'){ ?>
                        
                         <td style="text-align: center;" valign="top">
                        โปรโมชั่นจ่ายเท่าไรก็ได้
                      </td>
                    <?php }else{ ?>
                       <td style="text-align: center;" valign="top">
                        ไม่มีโปรโมชั่น
                      </td>
                    <?php } ?>
                 
                  <td style="text-align: right;" valign="top">
                    <?php echo $course_register->couponCode?>
                  </td>
                  <td style="text-align: right;" valign="top">
                    <?php if(!empty($info['promotion']) && $info['promotion']['discount']!='0'){ ?>
                      <input type="hidden" name="price" id="price" value="<?php echo $info['promotion']['discount']; ?>">
                      <?php echo number_format($info['promotion']['discount'])?>
                    <?php }else if(!empty($info['promotion']) && $info['promotion']['discount']=='0' && $info['promotion']['type']=='2'){ ?>
                      <?php echo number_format($info['price'])?>
                    <?php }else{ ?>
                      <input type="hidden" name="price" id="price" value="<?php echo $info['price']; ?>">
                      <?php echo number_format($info['price'])?>
                    <?php } ?>
                  </td>
                </tr>
                

                <?php if(!empty($promotionContent)){ 
                  foreach ($promotionContent as $key => $value) {
                 ?>

                  <tr>
                   <!--  <td  style="width: 150px">
                      
                          <img src="<?php echo $value->image;?>" style="width: 150px">
                   </td>
                   <td  style="width: 50%"> 
                  
                        <h4><?php echo $value->title ?></h4>
                        <p><?php echo $value->excerpt ?></p>
                     
                    </td> -->
                    <td  >
                       <span style="width: 40%">
                            <img src="<?php echo $value->image;?>" style="width: 100%">
                        </span>
                         <span style="width: 60%">
                             <h4><?php echo $value->title ?></h4>
                        <p><?php echo $value->excerpt ?></p>
                       </span> 
                     
                        
                  
                  
                        
                     
                    </td>
                    <td style="text-align: center;">
                      
                      แถมฟรี
                    </td>
                   <td style="text-align: center;">
                      
                      -
                    </td>
                      
                   
                    <td style="text-align: center;">
                      -
                    </td>
                    <td style="text-align: center;">
                      -
                    </td>
                  </tr>

                <?php } } ?>
                 <?php if(!empty($course_register->couponCode)){ 
                    if($course_register->type==2){
                      $rt=($course_register->price*$course_register->discount)/100;
                      $total=$course_register->price-$rt;
                    }else{
                      $total=$course_register->price-$course_register->discount;
                    }
                  ?>
                  <tr>
                  <td colspan="3"></td>
                  
                  <td style="text-align: right;"><b style="font-weight: bold;">ส่วนลด</b></td>
                  <td style="text-align: right;"><b style="font-weight: bold;"><?php echo number_format($course_register->discount);?><?php if($course_register->type==2){ echo "%";} ?></b></td>
                </tr>
                 <?php }else{
                  $total=$course_register->price;
                 } ?>
                 <tr>
                  <td colspan="3" style="text-align: center;">
                    
                  </td>
                  
                  <td style="text-align: right;"><b style="font-weight: bold;">ราคาสุทธิ (รวมVAT)</b></td>
                  <td style="text-align: right;"><b style="font-weight: bold;"><?php echo number_format($total);?></b></td>
                </tr>
                <!-- <tr>
                  <td colspan="6" style="text-align: center;">
                    
                  </td>
                </tr> -->
                
              </table>

            </div>
            </td>
        </tr>

        
      </table>
      </td>
  </tr>


  <tr>

    <td align="left" valign="top" bgcolor="#27160E" style="background-color:#27160E;color:#FFF;">
    <table width="100%" border="0" cellspacing="0" cellpadding="15">
      <tr>
        <td align="left" valign="top" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:13px;line-height: 20px;text-align: center;"> The CAP Vision Academy
          </td>
      </tr>
    </table></td>
  </tr>

</table>
</body>
</html>
