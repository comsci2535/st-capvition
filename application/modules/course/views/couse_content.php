<hr>
<div class="blog-posts couse_content">
				
<p>เนื้อหาของคอร์ส</p>

<table class="couse-content">
<?php foreach ($content as $key => $rs) { ?>

  <tr>
    <th ><?php echo $rs->title ?></th>
  </tr>
  <?php if(!empty($rs->parent)){ foreach ($rs->parent as $key_ => $rs_) { ?> 	
	  <tr>
	  	<td>
	  		<div class="row" >
	  			<?php if($rs_->type==0){ ?>
				<div class="c-img col-lg-6 col-12">
					<p class="couse-title"><?php echo $rs_->title ?></p>
				</div>
				<div class="c-img col-lg-2 col-4" style="text-align: right;" >
					
			    	<span class="label label-success">ดูตัวอย่างฟรี</span>
			       
				</div>
				<div class="c-img col-lg-2 col-4" style="text-align: right;">
				 <?php }else{ ?>
				 	<div class="c-img col-lg-8 col-12">
						<p class="couse-title"><?php echo $rs_->title ?></p>
					</div>
					<div class="c-img col-lg-2 col-8" style="text-align: right;">
				 <?php } ?>
			
					<?php if($rs_->type==0){ ?>
	    	
		    	<?php if($rs_->file){ ?>
		    	<a target="_blank" href="<?php echo $rs_->file;?>">
		    		<!-- <img alt="" src="<?php echo base_url('assets/website/images/icon/file.png'); ?>" style="width:20px;"/> -->
		    		<span style="font-size: 20px"><i class="fa fa-file-text-o"></i></span>
		    	</a>&nbsp;
		       <?php } ?>
		        <?php if (empty($info_user)){ ?>
		       <!--  <a class="various5 fancybox.iframe " href="<?php echo site_url('course/play_video/'.$rs_->course_contentId); ?>"> -->
		       	 <a href="" data-toggle="modal" data-target="#contentVideo-<?php echo $rs_->course_contentId; ?>">
		        
		        	<span style="font-size: 24px"><i class="fa fa-play-circle"></i></span>
		        </a>
		         <?php }else{  ?>

	    	    	<a href="<?php echo site_url('course/list_video/'.$linkId.'/'.$rs_->course_contentId);?>">
	    	    		

	    	    		<span style="font-size: 24px"><i class="fa fa-play-circle"></i></span>
	    	    		 
	    	    	</a>
	    	    <?php } ?>
	        <?php }else{ ?>
	        	
	        	<?php if (empty($info_user)){ ?>

		        	<?php if($rs_->file){ ?>
		        	<a><span style="font-size: 24px"><i class="fa fa-file-text-o"></i></span></a>&nbsp;
		        	 <?php } ?>
	        	
	        	   <a ><span style="font-size: 24px"><i class="fa fa-play-circle"></i></span></a>
	    	      
	    	    <?php }else{  ?>
	    	    	<?php if($rs_->file){ ?>
		        	<a  target="_blank" href="<?php echo $rs_->file;?>"><span style="font-size: 24px"><i class="fa fa-file-text-o"></i></span></a>&nbsp;
		        	 <?php } ?>
	    	    	<a href="<?php echo site_url('course/list_video/'.$linkId.'/'.$rs_->course_contentId);?>"><span style="font-size: 24px"><i class="fa fa-play-circle"></i></span></a>
	    	    <?php } ?>


	        <?php } ?>
				</div>
				<div class="c-img col-lg-2 col-4 " >
					<p class="video-length"><?php echo $rs_->videoLength ?> น.</p>
				</div>
			</div>
	  	</td>
	  </tr>
      
      <?php if($rs_->type==0){ ?>
	  <!-- Modal -->
		<div class="modal fade" id="contentVideo-<?php echo $rs_->course_contentId ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document" >
				<div class="modal-content">
					<div class="modal-header">
						
						<h5 class="modal-title" id="exampleModalLabel"><?php echo $rs_->title ?></h5>
						<button type="button"   id="pause-button" onclick="pausePlayer(<?php echo $rs_->course_contentId ?>)">
							<!-- <span aria-hidden="true">&times;</span> -->
							<i class="fa fa-times"></i>
						</button>
					</div>
					<div class="modal-body">
						
								<div class="embed-container">
									    <div data-vimeo-id="<?php echo $rs_->videoLink; ?>"  id="handstick-c-<?php echo $rs_->course_contentId ?>"   class="handstick-c"></div>
									</div>
							
					</div>
					<div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        	<button type="button" class="btn btn-primary">Save changes</button> -->
        			</div>
    			</div>
			</div>
		</div>
	<?php } ?>

  <?php } } ?>
<?php } ?>
  
  
</table>
	

</div><!-- blog-posts -->