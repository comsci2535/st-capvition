
<!-- <div class="top_site_main" >
		<div class="page-title-wrapper">
			<div class="banner-wrapper container">
				<div class="text-title">
					<h2>คอร์สเรียนออนไลน์</h2>
					<h4>ไม่จำกัดเวลา เรียนจนกว่าเก่ง</h4>	
				</div>		
			</div>
		</div>
</div> -->
<section class="section blog-article" id="#detail-content">
		<div class="container">
			<div class="title">
				<h3><?php echo $info['title'] ?></h3>
				<!-- <div class="separator"></div> -->
			</div>
			<div class="row">
				<div class=" col-lg-8 col-md-8 col-ms-12">
					
					   
					 <!-- <div class="embed-container">
						<div data-vimeo-id="<?php echo $course_content_l['videoLink']; ?>"  id="handstick-<?php echo $course_content_l['course_contentId'];?>" class="vimeo-view">
							
						</div>
					</div> -->
<div class="embed-container">
<div class="dd">
	<!-- <div id="plyr-vimeo-example-<?php echo $course_content_l['course_contentId'];?>" data-plyr-provider="vimeo" data-plyr-embed-id="<?php echo $course_content_l['videoLink']; ?>" class="vimeo-view">
	</div> -->

    <div data-vimeo-id="<?php echo $course_content_l['videoLink']; ?>"  id="plyr-vimeo-example-<?php echo $course_content_l['course_contentId'];?>" class="vimeo-view" ></div>
</div>
</div>
<input type="hidden" name="course_contentId" id="course_contentId" value="<?php echo $course_content_l['course_contentId'];?>" >
					
					<div class="descript-list">
						<div class="title-content"><h3><?php echo $course_content_l['title'];?></h3></div>
						<h4>รายละเอียด</h4>
						<div class="detail-content"><?php echo html_entity_decode($course_content_l['descript']) ?></div>
					</div>
					<br>
					<div class="file-upload">
						<?php if(!empty($course_content_l['file'])){ ?>
				    	<a target="_blank" href="<?php echo $course_content_l['file'];?>"><span style="font-size: 20px"><i class="fa fa-file-text-o"></i></span> เอกสารดาวน์โหลด</a>&nbsp;
				       <?php }else{?>
				       	
				       <?php }?>
					</div>
				</div><!-- col-lg-4 -->

				<div class="col-lg-4 col-md-4 col-ms-12">
					<h4>เนื้อหาคอร์ส</h4>
			          <div class="body-link-vdo">

			            <ul style="height:386px;overflow-y: scroll;">
			            	<?php foreach ($course_content as $key => $rs) { ?>
			            	<li><h5><?php echo $rs->title ?></h5></li>
			            	<?php foreach ($rs->parent as $key_ => $rs_) { ?> 
			                <li class="item active">
			                   
			                    <div class="media">
			                    	
			                        <!-- <div class="media-left <?php if(empty($rs_->active)){ echo "media-left-gray"; }else{ echo "media-left-green"; };?>  col-xs-1 col-sm-1 col-md-1 col-1">
			                           <i class="fa fa-circle"></i>
			                        </div> -->
							        <div class="bg-circle">
									  <div id="circle-<?php echo $rs_->course_contentId ?>" class="circle-<?php echo $rs_->course_contentId ?> <?php if(!empty($rs_->active) && $rs_->active->status==0){ echo "circle-left"; }else if(!empty($rs_->active) && $rs_->active->status==1){ echo "circle-full"; }else{ echo "circle-none"; };?>"></div>
									</div>
			                        <div class="media-body col-xs-11 col-sm-11 col-md-11 col-11">
			                        	<!-- <a href="<?php echo site_url('course/list_video/'.$linkId.'/'.$rs_->course_contentId);?>"> -->
			                        		<a href="javascript:void(0)" id="vimeo-<?php echo $rs_->course_contentId ?>" class="vimeo-b" >
			                            <p class="color5 title fontLv8"><?php echo $rs_->title ?></p>
			                            <p class="clock"><i class="fa fa-clock-o"></i> <?php echo $rs_->videoLength ?> น.</p>
			                            </a>
			                        </div>
			                        
			                    </div>  
			                                     
			                </li>
			                <?php } ?>
			                <?php } ?>
			                
                        </ul>               
                      </div>

				</div>
				<input type="hidden" name="courseId" id="courseId" value="<?php echo $info['courseId']; ?>">


			</div>	
		</div>
</section>
<script>
	// var iframe = document.querySelector('iframe');
 //    var player = new Vimeo.Player(iframe);

 //    player.on('play', function() {
 //        console.log('played the video!');
 //    });

 //    player.getVideoTitle().then(function(title) {
 //        console.log('title:', title);
 //    });
</script>