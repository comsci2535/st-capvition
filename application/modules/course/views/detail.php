<!-- 
<div class="top_site_main" >
		<div class="page-title-wrapper">
			<div class="banner-wrapper container">
				<div class="text-title">
					<h2>คอร์สเรียนออนไลน์</h2>
					<h4>ไม่จำกัดเวลา เรียนจนกว่าเก่ง</h4>	
				</div>		
			</div>
		</div>
</div> -->
<?php echo Modules::run('banner/hero','course') ?>


<section class="section blog-article">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-ms-12">
					<div class="blog-posts">
						<?php if(!empty($info)){ ?>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-ms-12 ">
								<div class="content-activity">
									<h4 ><?php echo $info['title'] ?></h4>
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-ms-12 ">
								<?if ($info['recommendVideo']!="") { ?>
									<!-- <div class="resp-container">
									<iframe class="resp-iframe" src="<?php echo $info['recommendVideo']; ?>"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="autoplay; encrypted-media"></iframe>
									</div> -->
									<div class="embed-container">
									    <div data-vimeo-id="<?php echo $info['recommendVideo']; ?>"  id="handstick-d"  ></div>
									</div>

								<?php }else{ ?>
									
									<img src="<?php echo $info['image']; ?>" alt="Blog Image">
								<?php } ?>

								
								<div class="socila-link">
                                    <!-- <div class="sharethis-inline-share-buttons"></div> -->
                                     <?php echo Modules::run('social/share_button'); ?>
                                </div>

							</div>
							
							<div class="col-lg-12 col-md-12 col-ms-12  t-mobile">
					         <?php if (empty($this->session->member['userId']) || (empty($info_user) && empty($info_user_regis))){ ?>
						          <div class="sidebar-content">
							        <h3 class="sidebar-title">ราคา ณ เวลานี้</h3>
							        <div class="price">
							        	<?php if(!empty($promotion)){ ?>
							        	   <?php if($promotion['discount']!='0' && $promotion['type']=="1"){ ?>
							        		  <div class="p1 price-nopro"><?php echo number_format($info['price']); ?> บาท</div>
									          <div class="p2 price-pro"><?php echo number_format($promotion['discount']); ?> บาท</div>
									       <?php }else if($promotion['discount']=='0' && $promotion['type']=="2"){ ?>
							        		  <div class="p1 price-nopro"><?php echo number_format($info['price']); ?> บาท</div>
									          <div class="p2 price-pro">จ่ายเท่าไรก็ได้</div>
									      <?php }else{ ?>
									      		 <div class="p2 price-pro"><?php echo number_format($info['price']); ?> บาท</div>
									      <?php } ?>
									          <div class="box-pro" style="display: none;">
									          	หมดโปรใน <br>
									          	<span id="showRemain2" class="showRemain"></span>
									          </div>
							        	<?php }else{ ?>
							          	 <div class="p2 price-pro"><?php echo number_format($info['price']); ?> บาท</div>
							         <?php } ?>
							         <!--  <a  class="button-click-2 <?php if(!$isLogin){ echo "modalLogin";}else{ echo "register"; } ?>">ชำระเงินเพื่อเริ่มเรียน</a> -->
							          <a   <?php if(!$isLogin){ echo 'class="button-click-2 modalLogin"';}else{ echo 'class="button-click-2 various5 fancybox.iframe"';?>  href="<?php echo site_url('course/payment/register/'.$info['courseId']);?>" <?php } ?> >ชำระเงินเพื่อเริ่มเรียน</a>
							          <span>ไม่จำกัดเวลา เรียนซ้ำได้ไม่อั้น</span>
							        </div>
							      </div>
							      <?php if(!empty($promotionContent)){ ?>
								      <div class="sidebar-content">
								      <h3 class="sidebar-title">แถมคอร์สฟรี</h3>
								        <div class="box">
								            <?php foreach ($promotionContent as $key => $value) { ?>
									         <div class="">		
												<div class="hover13 column ">
												    <figure>
												    	<img src="<?php echo $value->image ?>" alt="Blog Image" style="width: 100%">
												    </figure>
												</div>
												<div class="">
													<h5><b><?php echo $value->title ?></b></h5>
											    </div>
											</div><!-- single-post -->
											<?php } ?>
								        </div>
								      </div>
								  <?php } ?>
						      <?php }else if(!empty($info_user) ){  ?>

						      	<div class="sidebar-content">
						       
						        <div class="price">
						        	
						          <a  class="button-click-2" href="<?php echo site_url('course/list_video/'.$linkId);?>">อนุมัติเรียบร้อย <br>เรียนให้สนุกนะครับ</a>
						          <span>ไม่จำกัดเวลา เรียนซ้ำได้ไม่อั้น</span>
						        </div>
						      </div>

						      <?php }else{ ?>

						      	<div class="sidebar-content">
						       
						        <div class="price not-buy">
						        	
						          <a  class="button-click-2 " >รออนุมัติ</a>
						          <span>ไม่จำกัดเวลา เรียนซ้ำได้ไม่อั้น</span>
						        </div>
						      </div>
						      <?php } ?>

						 </div>
							<div class="col-lg-12 col-md-12 col-ms-12 ">
								<div class="tabs">
									<ul class="tab-links">
										<li class="active"><a href="#tab1">รายละเอียด</a></li>
										<li><a href="#tab2">วิธีการชำระเงิน</a></li>
<!-- <li><a href="#tab3">ห้องสนทนา</a></li>-->		
 <?php $reviews=Modules::run('course/reviews',$info['courseId']) ?>
 <?php //if($reviews!=""){ ?>								
 <li><a href="#tab4">รีวิว</a></li>
<?php //} ?>
										<li><a href="#tab5">ติดต่อสอบถาม</a></li>
									</ul>

									<div class="tab-content">
										<div id="tab1" class="tab active">
											<div style="max-height:386px;overflow-y: scroll;">
											<?php echo html_entity_decode($info['detail']); ?>
											</div>
											<?php echo Modules::run('course/couse_content',$info['courseId']) ?>
										</div>

										<div id="tab2" class="tab">
											<div class="payment">
												<?php echo html_entity_decode($payment); ?>
											</div>
										</div>

										<!-- <div id="tab3" class="tab">
											<p>ห้องสนทนา</p>
											
										</div> -->

										<div id="tab4" class="tab">
											<?php echo Modules::run('course/reviews',$info['courseId']) ?>
				  <div class="row">
				  	<div class="col-sm-4 col-12">
				  		<div class="user-review-title" style="font-size: 20px; padding-bottom: 15px;">คะแนนเฉลี่ย</div>
				  	    <div class="progress-amount">
				  	    	<div id="p-value"></div>
				  	    </div>
				  	</div>
				  	<div class="col-sm-6 col-12">
				  		<div class="user-review-title" style="font-size: 16px; padding-bottom: 15px;">รายละเอียด</div>
				  		<div class="progress-group">
		                    <span class="progress-text">5 ดาว</span>
		                    <span class="progress-number"><b><div id="p-value5"></div></b></span>
		                    <div class="progress sm" id="p5"></div>
		                 </div>
		                 <div class="progress-group">
		                    <span class="progress-text">4 ดาว</span>
		                    <span class="progress-number"><b><div id="p-value4"></div></b></span>
		                    <div class="progress sm" id="p4"></div>
		                 </div>
		                 <div class="progress-group">
		                    <span class="progress-text">3 ดาว</span>
		                    <span class="progress-number"><b><div id="p-value3"></div></b></span>
		                    <div class="progress sm" id="p3"></div>
		                 </div>
		                 <div class="progress-group">
		                    <span class="progress-text">2 ดาว</span>
		                    <span class="progress-number"><b><div id="p-value2"></div></b></span>
		                    <div class="progress sm" id="p2"></div>
		                 </div>
		                 <div class="progress-group">
		                    <span class="progress-text">1 ดาว</span>
		                    <span class="progress-number"><b><div id="p-value1"></div></b></span>
		                    <div class="progress sm" id="p1"></div>
		                 </div>
				  	</div>
				   </div>
											<div class="user-review">
												 <div class="divider"></div>
												<?php if($isLogin){ ?>
											    <div class="user-review-title" style="font-size: 16px;">รีวิวบทเรียน</div>
											   
											    <div class="user-review-stars-container">
											        <div class="user-review-stars" >
														<fieldset class="rating">
														    <input type="radio" id="star5" name="rating" value="5"  /><label for="star5" title="gorgeous">5 stars</label>
														    <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="good">4 stars</label>
														    <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="regular">3 stars</label>
														    <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="poor">2 stars</label>
														    <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="bad">1 star</label>
														</fieldset>
											        <span style="margin: 8px;font-size: 16px;">กดเพื่อให้คะแนน</span>
											        </div>
											    </div>

											    <div class="review-form text-right">
											        
											       
											        <input type="hidden" id="review-form-title" class="form-control" placeholder="" value="">
											        <textarea id="review-form-comment" class="form-control" rows="3" cols="83" placeholder="แสดงความคิดเห็น"></textarea>
											        <div class="review-form-btns" style="padding-top: 10px;">
											          <button id="review-form-submit-btn" class="btn-smal btn-flat btn-register">ตกลง</button>
											          <button id="review-form-cancel-btn" class="btn-smal btn-flat btn-default">ยกเลิก</button>
											        </div>
											    </div>
											    <?php } ?>
											  </div>
											 
											  <div id="reviews_stars_result"></div>
										</div>
										<div id="tab5" class="tab">
											<?php echo Modules::run('course/instructor_detail',$info['instructorId']) ?>
										</div>
									</div>
								</div>
							</div>
							
						</div><!-- row -->
						<?php } ?>

						

					</div><!-- blog-posts -->

					
				</div><!-- col-lg-4 -->

				<div class="col-lg-4 col-md-4 col-ms-12">
					<div class="t-pc">
			         <?php if (empty($this->session->member['userId']) || (empty($info_user) && empty($info_user_regis))){ ?>
				          <div class="sidebar-content">
					        <h3 class="sidebar-title">ราคา ณ เวลานี้</h3>
					        <div class="price">
					        	<?php if(!empty($promotion)){ ?>
					        	   <?php if($promotion['discount']!='0' && $promotion['type']=="1"){ ?>
					        		  <div class="p1 price-nopro"><?php echo number_format($info['price']); ?> บาท</div>
							          <div class="p2 price-pro"><?php echo number_format($promotion['discount']); ?> บาท</div>
							       <?php }else if($promotion['discount']=='0' && $promotion['type']=="2"){ ?>
					        		  <div class="p1 price-nopro"><?php echo number_format($info['price']); ?> บาท</div>
							          <div class="p2 price-pro">จ่ายเท่าไรก็ได้</div>
							      <?php }else{ ?>
							      		 <div class="p2 price-pro"><?php echo number_format($info['price']); ?> บาท</div>
							      <?php } ?>
							          <div class="box-pro" style="display: none;">
							          	หมดโปรใน <br>
							          	<span id="showRemain" class="showRemain"></span>
							          </div>
					        	<?php }else{ ?>
					          	 <div class="p2 price-pro"><?php echo number_format($info['price']); ?> บาท</div>
					         <?php } ?>
					         <!--  <a  class="button-click-2 <?php if(!$isLogin){ echo "modalLogin";}else{ echo "register"; } ?>">ชำระเงินเพื่อเริ่มเรียน</a> -->
					          <a   <?php if(!$isLogin){ echo 'class="button-click-2 modalLogin"';}else{ echo 'class="button-click-2 various5 fancybox.iframe"';?>  href="<?php echo site_url('course/payment/register/'.$info['courseId']);?>" <?php } ?> >ชำระเงินเพื่อเริ่มเรียน</a>
					          <span>ไม่จำกัดเวลา เรียนซ้ำได้ไม่อั้น</span>
					        </div>
					      </div>
					      <?php if(!empty($promotionContent)){ ?>
						      <div class="sidebar-content">
						      <h3 class="sidebar-title">แถมคอร์สฟรี</h3>
						        <div class="box">
						            <?php foreach ($promotionContent as $key => $value) { ?>
							         <div class="">		
										<div class="hover13 column ">
										    <figure>
										    	<img src="<?php echo $value->image ?>" alt="Blog Image" style="width: 100%">
										    </figure>
										</div>
										<div class="">
											<h5><b><?php echo $value->title ?></b></h5>
									    </div>
									</div><!-- single-post -->
									<?php } ?>
						        </div>
						      </div>
						  <?php } ?>
				      <?php }else if(!empty($info_user) ){  ?>

				      	<div class="sidebar-content">
				       
				        <div class="price">
				        	
				          <a  class="button-click-2" href="<?php echo site_url('course/list_video/'.$linkId);?>">อนุมัติเรียบร้อย <br>เรียนให้สนุกนะครับ</a>
				          <span>ไม่จำกัดเวลา เรียนซ้ำได้ไม่อั้น</span>
				        </div>
				      </div>

				      <?php }else{ ?>

				      	<div class="sidebar-content">
				       
				        <div class="price not-buy">
				        	
				          <a  class="button-click-2 " >รออนุมัติ</a>
				          <span>ไม่จำกัดเวลา เรียนซ้ำได้ไม่อั้น</span>
				        </div>
				      </div>
				      <?php } ?>

				 </div>     
                 <?php if($info['receipts']!=""){ ?>
				      <div class="sidebar-content">
				      <h3 class="sidebar-title">สิ่งที่ได้รับ</h3>
				        <div class="box">
				          
				          <?php echo $info['receipts']; ?>

				        </div>
				      </div>
				  <?php } ?>
				    
				      <?php echo Modules::run('course/instructor',$info['instructorId']) ?>

				      <?php echo Modules::run('course/instructor_contact',$info['instructorId']) ?>	



				</div>
				<input type="hidden" name="courseId" id="courseId" value="<?php echo $info['courseId']; ?>">
				<input type="hidden" name="linkId" id="linkId" value="<?php echo $linkId; ?>">


			</div>	
		</div>
</section>