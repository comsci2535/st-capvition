		<script src="https://www.paypalobjects.com/api/checkout.js"></script>
		<script src="https://cdn.omise.co/omise.js"></script>
		
		<div class="col-xl-12 col-lg-12 col-md-12 col-ms-12">
			<div class="payment-2">
				<div class="title text-center">
					<br>
					<h5>สรุปการสั่งซื้อ</h5>
					<!-- <div class="separator"></div> -->
				</div>  
				<div class="block-bg">
					<div class="card">
						<div class="card-image">
							<img src="<?php echo $info['image'];?>" >
						</div>
						<div class="card-content">
							<p class="ng-binding">
								<?php echo $info['title'] ?>
							</p>
							<hr>
							<div class="ins-box">
								<img src="<?php echo $instructor_image;?>" >
								<span class="ins-name ng-binding"><?php echo $instructor['title'];?></span>
							</div>
						</div>
					</div>
					<table style="width: 100%">
						<tbody>
							<tr>
								<td >
									<div class="row" >
										<div class="col-sm-3 col-12">
											รายการสั่งซื้อ
										</div>
										<div class="col-sm-9 col-12">
											<?php echo $info['title'] ?>
										</div>
									</div>
								</td>

							</tr>
							<?php if(!empty($info['promotion']) && $info['promotion']['discount']=='0' && $info['promotion']['type']=='2'){ ?>

								<tr>
									<td>
										<div class="row" >
											<div class="col-sm-3 col-12">
												ใส่ราคา
											</div>
											<div class="col-sm-9 col-12">

												<input type="text" name="price-a" id="price-a" value="" class="amount"> บาท
												<input type="hidden" class="price" name="price" id="price" value=""> 
												<input type="hidden" name="price_paypal" id="price_paypal" value="">
												<input type="hidden" name="couponCode" id="couponCode" style="width: 100px">
												<input type="hidden" name="type" id="type">
												<input type="hidden" name="discount" id="discount">
												<div class="alert_price-a"></div>
											</div>
										</div>
									</td>
								</tr>

							<?php }else{ ?>
								<tr>
									<td>
										<div class="row" >
											<div class="col-sm-3 col-12">
												ราคา
											</div>
											<div class="col-sm-9 col-12">
												<?php if(!empty($info['promotion']) && $info['promotion']['discount']!='0' && $info['promotion']['type']=='1'){ ?>
													<input type="hidden" class="price" name="price" id="price" value="<?php echo $info['promotion']['discount']; ?>"> 
													<input type="hidden" name="price_paypal" id="price_paypal" value="<?php echo $info['promotion']['discount']; ?>">
													<?php echo $total=number_format($info['promotion']['discount'])?> บาท
												<?php }else{ ?>
													<input type="hidden" class="price" name="price" id="price" value="<?php echo $info['price']; ?>">
													<input type="hidden" name="price_paypal" id="price_paypal" value="<?php echo $info['price']; ?>">
													<?php echo $total=number_format($info['price'])?> บาท
												<?php } ?> 
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="row" >
											<div class="col-sm-3 col-12">
												รหัสส่วนลด
											</div>
											<div class="col-sm-9 col-12">
												<input type="text" class="couponCode-a" name="couponCode" id="couponCode" style="width: 100px" value="<?php echo $this->session->code_data['codeId']; ?>">
												<input type="hidden" class="type" name="type" id="type">
												<input type="hidden" class="discount" name="discount" id="discount">

												<span id="coupon-error-div" ></span>

											</div>
										</div>



									</td>

								</tr>
								<tr style="display: none;" id="sl1">
									<td>
										<div class="row" >
											<div class="col-sm-3 col-12">
												ส่วนลด
											</div>
											<div class="col-sm-9 col-12">

												<font color="green"><strong><span id="sl"></span> บาท </strong></font>

											</div>
										</div>



									</td>
								</tr>
								<!-- ngIf: discount > 0 -->
								<tr>
									<td class="normal-hilight">
										<div class="row" >
											<div class="col-sm-3 col-12">
												ราคาสุทธิ (รวมVAT)
											</div>
											<div class="col-sm-9 col-12">
												<font color="green"><strong><span class="total"><?php echo $total?></span> บาท </strong></font>

											</div>
										</div>


									</td>
								</tr>

							<?php } ?>

						</tbody>
					</table>
				</div>
				<br>
				<div class="title text-center">
					<h5>เลือกช่องทางชำระเงิน</h5>
				</div>

				<h5>ยอดที่ท่านต้องชำระ  <span style="color: green; font-size: 24px;font-weight: 600; ">
					<span class="total"><?php echo $total?></span> บาท
				</h5>

				<div class="block-bg">

					<div class="card payment payment ng-scope" style="padding: 20px;" >
						<div class="pay-detail"><div class="col s12 l6 head-text-payment"><span>ชำระผ่านบัตรเครดิต <!-- หรือ PayPal --></span></div>
						<div class="col s12 l6 recommend-text-payment">(เรียนได้ทันทีหลังชำระเงิน)</div>
						<br>
						<!-- <div id="paypal-button-container"></div> -->

						<!-- <form name="checkoutForm" id="checkoutForm" action="<?=$frmAction?>" method="post"> -->
						<?php echo form_open_multipart($frmActionOmise, array('class' => '', 'id'=>'checkoutForm' , 'method' => 'post')) ?>
						<?php if(!empty($info['promotion']) && $info['promotion']['discount']!='0' && $info['promotion']['type']=='1'){ ?>
							<input type="hidden" class="price" name="price" id="price" value="<?php echo $info['promotion']['discount']; ?>"> 
							
						<?php }else{ ?>
							<input type="hidden" class="price" name="price" id="price" value="<?php echo $info['price']; ?>">
							
						<?php } ?> 
						<input type="hidden" class="couponCode" name="couponCode" id="couponCode">
						<input type="hidden" class="type" name="type" id="type">
						<input type="hidden" class="discount" name="discount" id="discount">
						<input type="hidden" name="courseId" id="courseId" value="<?php echo $info['courseId']; ?>">
						<input type="hidden" name="promotionId" id="promotionId" value="<?php echo $info['promotion']['promotionId']; ?>">
						<input type="hidden" class="priceOmise" name="priceOmise" id="priceOmise">

						  <p><button type="button" id="checkout-button-1" >
<img class="pay-logo-large" src="<?php echo base_url("assets/website/images/visa.png") ?>">
						  </button></p>
						 
					<?php echo form_close() ?>
					</div>						
				</div>

				<div class="col s12 m12 card payment ng-scope" style="padding: 15px;" id="ck_open_buy">
					<div class="pay-detail"><div class="col s12 l5 head-text-payment"><span>ชำระโดยการโอนเงิน</span></div>
					<div class="col s12 l7 recommend-text-payment">&nbsp;(เรียนได้ภายใน 30 นาที. หลังจากแจ้งโอน)</div></div>
					<img class="pay-logo-large" src="<?php echo base_url("assets/website/images/bank.png") ?>">
				</div>
			</div>

			<?php echo form_open_multipart($frmAction, array('class' => '', 'id'=>'frm-save' , 'method' => 'post')) ?>

			<div  style="overflow-x: hidden;display: none;margin-top: 20px;" id="open_buy"> 
				<h5>ขั้นตอนที่ 1 โอนเงินผ่านธนาคาร</h5>
				<div class="t-pc">
					<table class="bank-t">
						<tr>
							<th >ธนาคาร</th>
							<th >สาขา</th>
							<th >เลขที่บัญชี</th>
							<th >ประเภทบันชี</th>
							<th >ชื่อบัญชี</th>
						</tr>
						<?php foreach ($bank as $key => $value) { ?>
							<tr>
								<td >
									<img alt="K SME" src="<?php echo $value->image; ?>" style=" width: 25px"> <?php echo $value->title; ?> 
								</td>
								<td ><?php echo $value->branch; ?></td>
								<td ><?php echo $value->accountNumber; ?></td>
								<td ><?php echo $value->type; ?></td>
								<td ><?php echo $value->name; ?></td>
							</tr>

						<?php } ?>



					</table>
				</div>
				<div class="t-mobile">
					<div style="padding-top: 20px"></div>
					<?php foreach ($bank as $key => $value) { ?>
					<div class="row" >
						<div class="col-5 col-md-6" >
							<span style="font-weight: 700;">ธนาคาร : </span>
						</div>
						<div class="col-7  col-md-6">
							<img alt="K SME" src="<?php echo $value->image; ?>" style=" width: 25px"> <?php echo $value->title; ?>
						</div>
					</div>
					<div class="row" >
						<div class="col-5 col-md-6" >
							<span style="font-weight: 700;">สาขา : </span>
						</div>
						<div class="col-7 col-md-6">
							<?php echo $value->branch; ?>
						</div>
					</div>
					<div class="row" >
						<div class="col-5 col-md-6" >
							<span style="font-weight: 700;">เลขที่บัญชี : </span>
						</div>
						<div class="col-7 col-md-6">
							<?php echo $value->accountNumber; ?>
						</div>
					</div>
					<div class="row" >
						<div class="col-5 col-md-6" >
							<span style="font-weight: 700;">ประเภทบัญชี : </span>
						</div>
						<div class="col-7 col-md-6">
							<?php echo $value->type; ?>
						</div>
					</div>
					<div class="row" >
						<div class="col-5 col-md-6" >
							<span style="font-weight: 700;">ชื่อบัญชี : </span>
						</div>
						<div class="col-7 col-md-6">
							<?php echo $value->name; ?>
						</div>
					</div>
					<div style="padding-bottom: 20px;"></div>
					<?php } ?>
				</div>

				<hr >

				<h5>ขั้นตอนที่ 2 แจ้งโอนเงิน</h5>
				<p style="color: #000">เมื่อโอนเงินเรียบร้อยแล้ว โปรดแจ้งการชำระเงิน โดยกรอกแบบฟอร์มข้างล่าง</p>
				<div class="row">
					<span class="col-lg-4 col-md-4 col-12">		
						สลิปหลักฐานการโอน (รูป/PDF) <!-- <span class="required">*</span> --> 
					</span> 
					<span class="col-lg-8 col-md-8 col-12">	

						<input type="file" name="fileUpload" id="fileUpload" onchange="check_file_img(this);">
						<div class="alert_file"></div>

					</span>
				</div>  
				<div class="payment-b">
					<center>
						<?php if(!empty($info['promotion']) && $info['promotion']['discount']!='0' && $info['promotion']['type']=='1'){ ?>
							<input type="hidden" class="price" name="price" id="price" value="<?php echo $info['promotion']['discount']; ?>"> 
							
						<?php }else{ ?>
							<input type="hidden" class="price" name="price" id="price" value="<?php echo $info['price']; ?>">
							
						<?php } ?> 
						<input type="hidden" class="couponCode" name="couponCode" id="couponCode">
						<input type="hidden" class="type" name="type" id="type">
						<input type="hidden" class="discount" name="discount" id="discount">
						<input type="hidden" name="courseId" id="courseId" value="<?php echo $info['courseId']; ?>">
						<input type="hidden" name="promotionId" id="promotionId" value="<?php echo $info['promotion']['promotionId']; ?>">

						<div class="register-form">
							<div id="form-success-div" class="text-success"></div> 
							<div id="form-error-div" class="text-danger"></div>
							<button type="submit" class="button-click-2"><span id="form-img-div"></span>  <font color="#fff">แจ้งชำระเงิน</font></button> 

						</div>
					</center> 
				</div>


				<?php echo form_close() ?>



			</div><!-- blog-posts -->
		</div><!-- col-lg-4 -->
                <!-- <div class="clearfix">
                	
                </div> -->
            </div>    

            

            