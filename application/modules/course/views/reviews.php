
<?php foreach ($reviews as $key => $rs) {?>
<section id="section-team" class="topmargin-lg">
    <div class="clearfix">
        <div class="col_one_sixth center">
            <img src="<?php echo $rs->file['coverImage']  ?>" alt="กอร่าสอนออกแบบ กราฟิก"><br>
            <h5><?php echo $rs->title;?></h5>
        </div>
        <div class="col_two_fifth">
           <?php echo $rs->excerpt;?>
           	
        </div>
        <div class="col_two_fifth col_last">
            <a class="fancybox" href="<?php echo $rs->file['contentImage']  ?>" data-fancybox-group="gallery" title="ผลงานของ <?php echo $rs->title;?>"><img src="<?php echo $rs->file['contentImage']  ?>" alt="กอร่าสอนออกแบบ กราฟิก"></a>
        </div>
      

    </div>
 <?php //if(($key+1)%1==0 && count($reviews)!=($key+1)){ ?>
<div class="divider"></div>
 <?php //} ?>
 
</section>
<?php } ?>