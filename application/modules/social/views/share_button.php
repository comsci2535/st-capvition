
<style>
.custom-share-button {
    color: #fff;
    background-color: #55acee;
    padding: 7px 10px;
    border-radius: 0px;
    display: inline-block;
    width: 97px;
    text-align: center;
}
.custom-share a:hover{
    color: #fff;
   
}
.custom-share-button-icon,
.custom-share-button-label {
  display: inline-block;
  vertical-align: middle;
}
.custom-share-button-icon {
  width: 1em;
  height: 1em;
  margin-right: .2em;
}
.custom-share-button-icon path { fill: #fff; }
.custom-share-button-label {
  font-size: .8em;
  font-weight: 500;
}
.custom-share-button:hover { background-color: #70b7ec; }

.custom-share-button-f {
    color: #fff;
    background-color: #3B5998;
    padding: 7px 10px;
    border-radius: 0px;
    display: inline-block;
    width: 100px;
    text-align: center;
}
.custom-share-button-icon-f,
.custom-share-button-label-f {
  display: inline-block;
  vertical-align: middle;
}
.custom-share-button-icon-f {
  width: 1em;
  height: 1em;
  margin-right: .2em;
}
.custom-share-button-icon-f path { fill: #fff; }
.custom-share-button-label-f {
  font-size: .9em;
  font-weight: 500;
}
.custom-share-button-f:hover { background-color: #3B5998; }

.custom-share-button-g {
    color: #fff;
    background-color: #dc4e41;
    padding: 7px 10px;
    border-radius: 0px;
    display: inline-block;
    width: 97px;
    text-align: center;
}
.custom-share-button-icon-g,
.custom-share-button-label-g {
  display: inline-block;
  vertical-align: middle;
}
.custom-share-button-icon-g {
  width: 1em;
  height: 1em;
  margin-right: .2em;
}
.custom-share-button-icon-g path { fill: #fff; }
.custom-share-button-label-g {
  font-size: .9em;
  font-weight: 500;
}
.custom-share-button-g:hover { background-color: #dc4e41; }
</style>
<div class="custom-share">
<a class="custom-share-button-f js-social-share" href="https://www.facebook.com/sharer/sharer.php
     ?u=<?php echo site_url();?><?php echo $this->ogUrl ?>?code=<?php echo $this->session->member['couponCode']; ?>"
   target="_blank"> <span class="custom-share-button-icon"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
    
     viewBox="0 0 252 252"
     style="fill:#000000;"><path d="M184.0125,94.5h-37.0125v-21c0,-10.836 0.882,-17.661 16.4115,-17.661h19.614v-33.39c-9.5445,-0.987 -19.1415,-1.47 -28.749,-1.449c-28.4865,0 -49.2765,17.3985 -49.2765,49.3395v24.1605h-31.5v42l31.5,-0.0105v94.5105h42v-94.5315l32.193,-0.0105z"></path></g></g></g></svg></span>
  <span class="custom-share-button-label">Facebook</span></a>

<a class="custom-share-button js-social-share" href="https://twitter.com/intent/tweet/
     ?text=Check%20out%20this%20website!
     &url=<?php echo site_url();?><?php echo $this->ogUrl ?>?code=<?php echo $this->session->member['couponCode']; ?>" 
   target="_blank">
  <span class="custom-share-button-icon"><svg viewBox="0 0 12 12" preserveAspectRatio="xMidYMid meet" class="svg-icon svg-icon-twitter"><path class="svg-icon-path" d="M10.8,3.5c0,0.1,0,0.2,0,0.3c0,3.3-2.5,7-7,7c-1.4,0-2.7-0.3-3.8-1c0.2,0,0.4,0,0.6,0c1.1,0,2.2-0.5,3.1-1.2 c-1.1,0-2-0.7-2.3-1.7c0.2,0,0.3,0,0.5,0s0.4,0,0.7-0.1c-1.1-0.2-2-1.2-2-2.4l0,0c0.3,0.2,0.7,0.3,1.1,0.3C1,4.3,0.6,3.5,0.6,2.7 c0-0.5,0.1-0.8,0.3-1.2c1.2,1.6,3,2.5,5,2.6c0-0.2-0.1-0.4-0.1-0.6c0-1.3,1.1-2.4,2.5-2.4c0.7,0,1.4,0.3,1.8,0.8 c0.6-0.1,1.1-0.3,1.6-0.6c-0.2,0.6-0.6,1.1-1.1,1.4c0.5-0.1,1-0.2,1.4-0.4C11.7,2.8,11.2,3.2,10.8,3.5z"/></svg></span>
  <span class="custom-share-button-label">Twitter</span>
</a>

<a class="custom-share-button-g js-social-share" href="https://plus.google.com/share
     ?url=<?php echo site_url();?><?php echo $this->ogUrl ?>?code=<?php echo $this->session->member['couponCode']; ?>"
   target="_blank"> <span class="custom-share-button-icon"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
   
     viewBox="0 0 50 50"
    ><g id="surface1"><path style=" " d="M 17.1875 10.9375 C 9.421875 10.9375 3.125 17.234375 3.125 25 C 3.125 32.765625 9.421875 39.0625 17.1875 39.0625 C 24.953125 39.0625 31.25 32.765625 31.25 25 C 31.25 24.035156 31.144531 23.09375 30.960938 22.1875 L 30.882813 21.875 L 17.1875 21.875 L 17.1875 26.5625 L 26.5625 26.5625 C 25.816406 30.996094 21.832031 34.375 17.1875 34.375 C 12.007813 34.375 7.8125 30.179688 7.8125 25 C 7.8125 19.820313 12.007813 15.625 17.1875 15.625 C 19.53125 15.625 21.667969 16.492188 23.3125 17.914063 L 26.671875 14.625 C 24.171875 12.335938 20.84375 10.9375 17.1875 10.9375 Z M 39.0625 17.1875 L 39.0625 21.875 L 34.375 21.875 L 34.375 25 L 39.0625 25 L 39.0625 29.6875 L 42.1875 29.6875 L 42.1875 25 L 46.875 25 L 46.875 21.875 L 42.1875 21.875 L 42.1875 17.1875 Z "></path></g></svg></span>
  <span class="custom-share-button-label">Google+</span></a>
</div>

<script type="text/javascript">

</script>