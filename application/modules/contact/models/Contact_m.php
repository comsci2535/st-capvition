<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contact_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    public function insert($value) {
        $this->db->insert('contact', $value);
        return $this->db->insert_id();
    }

}
