<div class="widget recent-posts">
						<h3 class="widget-title">ที่ดินแนะนำ</h3>
						<ul class="unstyled clearfix">
						<!-- Recent Post Widget Starts -->
						<?php foreach ($info as $key => $rs) { ?>
							
						<li>
							<div class="posts-thumb pull-left"> 
								<a href="<?php echo site_url("land/detail/{$rs['linkId']}");?>">
									<img alt="img" src="<?php echo $rs['image']; ?>">
								</a>
							</div>
							<div class="post-info">
								<h4 class="entry-title">
									<a href="<?php echo site_url("land/detail/{$rs['linkId']}");?>"><?php echo $rs['title'] ?></a>
								</h4>
								<p class="post-meta second-font">
									<span class="post-date"> สถานที่  <?php echo $rs['location'] ?></span>
								</p>
								<p class="post-meta second-font">
									<span class="post-date"> ราคา <?php echo price_th($rs['price']) ?></span>
								</p>
							</div>
							<div class="clearfix"></div>
						</li>
					<?php } ?>
						<!-- Recent Post Widget Ends -->

						
						<!-- Recent Post Widget Ends -->
						</ul>
					</div>
					<!-- Latest Posts Widget Ends -->