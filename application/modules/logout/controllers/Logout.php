<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MX_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('facebook');
    }
    
    public function index()
    {
        $this->db->where('userId', $this->session->member['userId'])
                ->update('user', array('user_datetime_using'=>'','user_login_status'=>0 ));

        $this->session->unset_userdata('member');
        //$this->session->unset_userdata('user');
        $this->session->unset_userdata('urlreffer');
        $this->session->unset_userdata('code_data');
        //$this->facebook->destroy_session();
        redirect(site_url('home'));
    }

    public function closeConnection(){
        //$this->load->library('session');
        if($this->session->userdata('user')!== 1){
           $this->session->unset_userdata('member');
           //$this->session->unset_userdata('user');
           $this->session->unset_userdata('urlreffer');
        }
    }

    public function logoutFunction()
    {
        $dt=date("Y-m-d H:i:s",strtotime(db_datetime_now()." -20 minutes"));
        //arr($dt);exit();

        $this->db->where('user_datetime_using <=', $dt)
                ->update('user', array('user_datetime_using'=>'','user_login_status'=>0 ));

         $this->db->where('userId', $this->session->member['userId'])
                ->update('user', array('user_datetime_using'=>'','user_login_status'=>0 ));

        $this->session->unset_userdata('member');
        //$this->session->unset_userdata('user');
        $this->session->unset_userdata('code_data');
        //$this->facebook->destroy_session();
        //redirect(site_url('home'));
    }
}
