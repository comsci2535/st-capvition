<style type="text/css">


</style>
<section class="section blog-article">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
                            <div class="image-wrapper">
                                <a href="https://gorradesign.com/course/detail/illustrator-Basic-Pro">
                                  <img src="<?php echo  base_url("assets/website/images/1200x160-v2.jpg"); ?>"  class="img-responsive">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
</section><!-- section -->
<section class="blog-article">
        <div class="container">
            <div class="row">
               
                <div class="col-xl-12 col-lg-12 col-md-12 col-ms-12"> 
                    <div class="blog-posts">
                       <!--  <div class="title">
                            <h3>บทความกราฟฟิก</h3>
                            <div class="separator"></div>
                        </div> -->
                        <div class="row">
                         <div class=" col-sm-12">
                            <span class="topnav-article">
                              <a <?php if(empty($categoryId)){ ?> class="active" <?php } ?> href="<?php echo site_url("article");?>">ทั้งหมด</a>
                              <?php foreach ($category as $key => $value) { ?> 
                              <a <?php if(!empty($categoryId) && $categoryId==$value['categoryId'] ){ ?> class="active" <?php } ?>  href="<?php echo site_url("article/category/{$value['nameLink']}");?>"><?=$value['name']?></a>
                            <?php } ?>
                             
                            </span>
                         <!-- </div>
                         <div class=" col-sm-6"> -->
                        
                          <span class="search-container">
                            <form action="<?php echo site_url('article');?>">
                              <input type="text" placeholder="ค้นหา.." name="keyword" value="<?=$keyword;?>">
                              <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                          </span>

                         </div>
                        </div>
                        <?php if(!empty($info)){ ?>
                         <div class="row">

                        <?php foreach ($info as $key => $rs) { ?>
                          <div class="article-b-i col-md-4 col-ms-12 ">
                       
                           <!--  <div class="col-lg-12 col-md-12 col-ms-12 "> -->
                                <div class="hover13 column">
                                    <a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>">
                                        <figure>
                                            <div class="image-wrapper"><img src="<?php echo $rs['image']; ?>" alt="Blog Image"></div>  
                                        </figure>
                                    </a>
                                </div>
                                
                            <!-- </div>
                            <div class="col-lg-12 col-md-12 col-ms-12 "> -->
                                <div class="content-activity">
                                    <h4 ><a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>"><?php echo $rs['title'] ?></a></h4>
                                     <p>หมวดหมู่ : <?php echo $rs['name'] ?></p>
                                    <span class="post-time">โพสเมื่อ : <?php echo date_language($rs['createDate'],true,'th') ?> อ่าน : <?php echo number_format($rs['view']); ?> </span>
                                    <p><?php echo $rs['excerpt'] ?></p>

                                    <div class="tags-area">
                                        <ul class="tags">
                                            <?php foreach ($rs['tags'] as $tg => $tg_) { 
                                                 $linkTags = str_replace(" ","-",$tg_);
                                                 
                                            ?>
                                                
                                            <li><a href="<?php echo site_url("article/tags/{$linkTags}");?>" class="btn" href="#"><?php echo $tg_; ?></a></li>
                                           
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                           <!--  </div> -->
                       
                        </div>

                        <?php } ?>
                       
                            
                        </div><!-- row -->
                        <div class="text-center">
                            <?php echo $links ;?>   
                        </div>
                        <?php }else{ ?>
                        <div class="text-center">
                           
                            <h4>ไม่พบข้อมูล</h4>
                           
                        </div>
                        <?php } ?>
                        
                    </div><!-- blog-posts -->
                 </div><!--col-lg-4 -->
                 <!-- <div id="get_more"></div> -->
                
            </div>  
        </div>
    </section><!-- section -->