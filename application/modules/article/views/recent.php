


		 <!-- ##### Blog Area Start ##### -->
    <section class="blog-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h3>บทความล่าสุด</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Blog Area -->
                <?php foreach ($info_recent as $key => $rs) { ?>
                <div class="col-12 col-md-6">
                    <div class="single-blog-area mb-100 wow fadeInUp" data-wow-delay="250ms">
                        <img src="<?php echo $rs['image']; ?>" alt="">
                        <!-- Blog Content -->
                        <div class="blog-content">
                            <a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>" class="blog-headline">
                                <h4><?php echo $rs['title'] ?></h4>
                            </a>
                            <div class="meta d-flex align-items-center">
                                <a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>">หมวดหมู่ : <?php echo $rs['name'] ?></a>
                                <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                                <a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>">โพสเมื่อ : <?php echo date_language($rs['createDate'],true,'th') ?></a>
                                <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                                <a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>">อ่าน : <?php echo number_format($rs['view']); ?></a>
                            </div>
                            <p><?php echo $rs['excerpt'] ?></p>
                        </div>
                    </div>
                </div>
                  <?php } ?>
                
            </div>
        </div>
    </section>
    <!-- ##### Blog Area End ##### -->