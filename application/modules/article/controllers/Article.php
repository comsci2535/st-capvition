<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends MX_Controller {
    
    private $_grpContent = 'article';

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('category/category_m');
        $this->load->model('article_m');
        $this->load->model("admin/tags_m");
        $this->load->model('seo_m');
    }
    
    public function index()
    {
        Modules::run('track/front','');

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url('article/index/');
        $this->session->set_userdata('urlreffer', $urlreffer);

        $this->load->module('front');
        $input_ = $this->input->get();
        
        $input_c['categoryType'] = $this->_grpContent;
        $data['category'] = $this->category_m->get_rows($input_c)->result_array();
        
        $input['grpContent'] = $this->_grpContent;

        
        $data['keyword'] = "";
        ///pagination///
        if(!empty($input_['keyword']) && $input_['keyword']!=""){
            $input['keyword'] = $input_['keyword'];
            $data['keyword'] = $input_['keyword'];
            $uri='article?keyword='.$data['keyword'];
        }else{
            $uri='article';
        }
          
        $total=$this->article_m->get_count($input);
        $segment=3;
        $per_page = 15;
        $data["links"] = $this->pagin2($uri, $total, $segment, $per_page);
        ///pagination///
        
        $page = ($this->input->get('per_page')) ? ($per_page*($this->input->get('per_page')-1)) : 0;

        $input['length']=$per_page;
        $input['start']=$page;

        //$input['length']=3;
        //$input['start']=0;
        $data['info'] = "";
        $info=$this->article_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ( $info as $key=>&$rs ) {
                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $rs['contentId'];
                $rs['linkId'] = str_replace(" ","-",$rs['title_link']);
                $rs['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->article_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $rs['image'] = base_url($file['path'].$file['filename']);
                }
                $rs['tags']="";
                if($rs['tagsId']!=''){
                     $tags=explode(',', $rs['tagsId']);
                     $rstags  = array( );
                     foreach ($tags as $key => $value) {
                        $input_t['tagsID'] = $value;
                        $t=$this->tags_m->get_rows($input_t)->row_array();
                        $rstags[$value]=$t['tagsName'];
                     }

                      $rs['tags']=$rstags;
                     
                }    

            }
         
            $data['info'] = $info;
            //arr($data['info']);exit();
           
        }
       
        $data['article_act'] = 'active';
       
        $data['contentView'] = "article/index";
        $data['pageHeader'] = "บทความ";

        $input_seo['grpContent'] ='seo_article';
        $seo = $this->seo_m->get_rows($input_seo);
        $seo_ = $seo->row();

        //arr($seo_);exit();

        $imgSeo = config_item('metaOgImage');
        $metaTitle="บทความ";
        $metaDescription="บทความ";
        $metaKeyword="บทความ";
        
        if(!empty($seo_)){           
            $input_seo['contentId'] = $seo_->seoId;
            $file = $this->seo_m->get_uplode($input_seo)->row_array();
            if (!empty($file) && is_file($file['path'].$file['filename'])) {
                $imgSeo = base_url($file['path'].$file['filename']);
            }
           
            $metaTitle=$seo_->metaTitle;
            $metaDescription=$seo_->metaDescription;
            $metaKeyword=$seo_->metaKeyword;
        }
        
        $data['metaTitle'] = $metaTitle;
        $data['metaDescription_'] = $metaDescription;
        $data['metaKeyword_'] = $metaKeyword;

        $share['ogTitle']=$metaTitle;
        $share['ogDescription']=$metaDescription;
        $share['ogUrl']= 'article';
        $share['ogImage']= $imgSeo;
        $this->_social_share($share);

        $this->front->layout($data);
        
    }

    public function getContentMore()
    {
        

        $length = 3;
        $page =  $_GET['page'];
        $start = $page*$length;
       
        $input['grpContent'] = $this->_grpContent;
        $input['length']=$length;
        $input['start']=$start;
        //$data['info'] = "";
        $info=$this->article_m->get_rows($input)->result_array();
        //arr($input);
        $txt='';
        if (!empty($info)) {
            foreach ( $info as $key=>&$rs ) {
                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $rs['contentId'];
                $rs['linkId'] = str_replace(" ","-",$rs['title_link']);
                $rs['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->article_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $rs['image'] = base_url($file['path'].$file['filename']);
                }
                $rs['tags']="";
                if($rs['tagsId']!=''){
                     $tags=explode(',', $rs['tagsId']);
                     $rstags  = array( );
                     foreach ($tags as $key => $value) {
                        $input_t['tagsID'] = $value;
                        $t=$this->tags_m->get_rows($input_t)->row_array();
                        $rstags[$value]=$t['tagsName'];
                     }

                      $rs['tags']=$rstags;
                     
                }    

            }
            $txt.='<div class="col-xl-12 col-lg-12 col-md-12 col-ms-12"> 
                    <div class="blog-posts">
                    <div class="row">';
            $info_=$info;
            foreach ($info_ as $key => $rs2) {

                $txt.='<div class="article-b-i col-md-4 col-ms-12 ">
                        <div class="hover13 column">
                            <a href="'.site_url("article/detail/{$rs2['linkId']}").'">
                                <figure>
                                    <div class="image-wrapper"><img src="'.$rs2['image'].'" alt="Blog Image"></div>  
                                </figure>
                            </a>
                        </div>
                        <div class="content-activity">
                            <h4 ><a href="'.site_url("article/detail/{$rs2['linkId']}").'">'.$rs2['title'].'</a></h4>
                            
                            <span class="post-time">โพสเมื่อ '.date_language($rs2['createDate'],true,'th').'</span>
                            <p>'.$rs2['excerpt'].'</p>

                            <div class="tags-area">
                                <ul class="tags">';
                                     foreach ($rs2['tags'] as $tg => $tg_) { 
                                     $linkTags = str_replace(" ","-",$tg_); 
                                     $txt.='<li><a href="'.site_url("article/tags/{$linkTags}").'" class="btn">'.$tg_.'</a>
                                    </li>';
                                      }
                                $txt.='</ul>
                            </div>
                        </div>
                    </div>';
            }
            $txt.='</div></div></div>';
           
        }

        

         echo $txt;
         exit;

        
    } 


    public function category($categoryId)
    {
        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url('article/category/'.$categoryId);
        $this->session->set_userdata('urlreffer', $urlreffer);

        $this->load->module('front');
        $input = $this->input->get();
        $input['grpContent'] = $this->_grpContent;
        $input['nameLink'] = $categoryId;
        $data['nameLink']=$categoryId;

        
        $input_c['categoryType'] = $this->_grpContent;
        $data['category'] = $this->category_m->get_rows($input_c)->result_array();

    //arr($data['category']);exit();

        $input_t2['tagsType'] = $this->_grpContent;
        $data['tags_'] = $this->tags_m->get_rows($input_t2)->result_array();

        $data['keyword'] = "";
        ///pagination///
        if(!empty($input['keyword']) && $input['keyword']!=""){
            
            $data['keyword'] = $input['keyword'];
            $uri='article/category/'.$categoryId.'?keyword='.$data['keyword'];
        }else{
            $uri='article/category/'.$categoryId;
        }

        ///pagination///
        
        $total=$this->article_m->get_count($input);
        $segment=3;
        $per_page = 15;
        $data["links"] = $this->pagin2($uri, $total, $segment, $per_page);
        ///pagination///
        
        $page = ($this->input->get('per_page')) ? ($per_page*($this->input->get('per_page')-1)) : 0;

        $input['length']=$per_page;
        $input['start']=$page;

        $info=$this->article_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ( $info as $key=>&$rs ) {
                $rs['linkId'] = str_replace(" ","-",$rs['title_link']);
                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $rs['contentId'];
                $rs['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->article_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $rs['image'] = base_url($file['path'].$file['filename']);
                }
                $rs['tags']="";
                if($rs['tagsId']!=''){
                     $tags=explode(',', $rs['tagsId']);
                      $rstags  = array( );
                     foreach ($tags as $key => $value) {
                        $input_t['tagsID'] = $value;
                        $t=$this->tags_m->get_rows($input_t)->row_array();
                        $rstags[$value]=$t['tagsName'];
                     }

                     $rs['tags']=implode(',', $rstags);
                     
                }
                
            }
         
            $data['info'] = $info;
           
        }
        $data['article_act'] = 'active';
        $data['contentView'] = "article/category";
        
        $input_seo['grpContent'] ='seo_article';
        $seo = $this->seo_m->get_rows($input_seo);
        $seo_ = $seo->row();

        $input_c['nameLink'] = $categoryId;
        $categorySeo = $this->category_m->get_rows($input_c)->row();

        //arr($seo_);exit();

        $imgSeo = config_item('metaOgImage');
        $metaTitle="บทความ";
        $metaDescription="บทความ";
        $metaKeyword="บทความ";
        
        if(!empty($seo_)){           
            $input_seo['contentId'] = $seo_->seoId;
            $file = $this->seo_m->get_uplode($input_seo)->row_array();
            if (!empty($file) && is_file($file['path'].$file['filename'])) {
                $imgSeo = base_url($file['path'].$file['filename']);
            }
           
            
        }

        $metaTitle=$categorySeo->metaTitle;
        $metaDescription=$categorySeo->metaDescription;
        $metaKeyword=$categorySeo->metaKeyword;
        
        $data['metaTitle'] = $metaTitle;
        $data['metaDescription_'] = $metaDescription;
        $data['metaKeyword_'] = $metaKeyword;

        $share['ogTitle']=$metaTitle;
        $share['ogDescription']=$metaDescription;
        $share['ogUrl']= 'article/category/'.$categoryId;
        $share['ogImage']= $imgSeo;
        $this->_social_share($share);
        
        $this->front->layout($data);
        // $data['info'] = '';
        // $this->load->view('index', $data); 
    }

    public function tags($tagsId)
    {
        $this->load->module('front');

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url('article/tags/').$tagsId;
        $this->session->set_userdata('urlreffer', $urlreffer);

        $input = $this->input->get();
        $input['grpContent'] = $this->_grpContent;
        
        
        
        
        $input_c['categoryType'] = $this->_grpContent;
        $data['category'] = $this->category_m->get_rows($input_c)->result_array();

        
        $input_t1['tagsName'] = str_replace("-"," ",$tagsId);
        $input_t1['tagsType'] = $this->_grpContent;
        $tt = $this->tags_m->get_rows($input_t1)->row();


        $input_t['tagsType'] = $this->_grpContent;
        $data['tags_'] = $this->tags_m->get_rows($input_t)->result_array();

        $input['tagsId'] =$tt->tagsID;

        //arr($tagsId);exit();
        $data['tagsId'] = $tagsId;
        $data['keyword'] = "";
        ///pagination///
        if(!empty($input['keyword']) && $input['keyword']!=""){
            $input['keyword'] = $input['keyword'];
            $data['keyword'] = $input['keyword'];
            $uri='article/tags/'.$tagsId.'?keyword='.$data['keyword'];
        }else{
            //$uri='article';
            $uri='article/tags/'.$tagsId;
        }

        ///pagination///
        

        $total=$this->article_m->get_count($input);
        $segment=3;
        $per_page = 15;
        $data["links"] = $this->pagin2($uri, $total, $segment, $per_page);
        ///pagination///
        
        $page = ($this->input->get('per_page')) ? ($per_page*($this->input->get('per_page')-1)) : 0;

        $input['length']=$per_page;
        $input['start']=$page;

        $info=$this->article_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ( $info as $key=>&$rs ) {
                //$rs['linkId'] = str_replace(" ","-",$rs['title']);
                $rs['linkId'] = str_replace(" ","-",$rs['title_link']);
                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $rs['contentId'];
                $rs['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->article_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $rs['image'] = base_url($file['path'].$file['filename']);
                }
                $rs['tags']="";
                if($rs['tagsId']!=''){
                     $tags=explode(',', $rs['tagsId']);
                     $rstags  = array( );
                     foreach ($tags as $key => $value) {
                        $input_t['tagsID'] = $value;
                        $t=$this->tags_m->get_rows($input_t)->row_array();
                        $rstags[$value]=$t['tagsName'];
                     }
                     $rs['tags']=$rstags;
                     
                }    
  

            }
         
            $data['info'] = $info;
           
        }
        $data['article_act'] = 'active';
        $data['contentHeader'] = "article/header";
        $data['contentView'] = "article/tags";

        $data['metaTitle'] = $tt->metaTitle;
        $data['metaDescription_'] = $tt->metaDescription;
        $data['metaKeyword_'] = $tt->metaKeyword;

        $share['ogTitle']= $tt->metaTitle;
        $share['ogDescription']=$tt->metaDescription;
        $share['ogUrl']= 'article/tags/'.$tagsId;
        $share['ogImage']= config_item('metaOgImage');
        $this->_social_share($share);
       
        $this->front->layout($data);
        // $data['info'] = '';
        // $this->load->view('index', $data); 
    }

    public function detail($id=0)
    {
        
        $this->load->module('front');

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url('article/detail/').$id;
        $this->session->set_userdata('urlreffer', $urlreffer);
        $data['title_link']=$id;
        $input['title_link'] = str_replace("-"," ",$id);
        //$input['keyword']=
        $input['grpContent'] = $this->_grpContent;
        $data['keyword'] = "";
        $data['name']=$input['title_link'];

        // $input_c['categoryType'] = $this->_grpContent;
        // $data['category'] = $this->category_m->get_rows($input_c)->result_array();

        // $input_t['tagsType'] = $this->_grpContent;
        // $data['tags'] = $this->tags_m->get_rows($input_t)->result_array();

        $info=$this->article_m->get_rows($input)->row_array();
        if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "The CAP Vision Academy");

        //print"<pre>";print_r($info); exit(); 
        if (!empty($info)) {
            //foreach ( $info as $key=>&$rs ) {

                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $info['contentId'];
                $info['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->article_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $info['image'] = base_url($file['path'].$file['filename']);
                    $imageSeo = base_url($file['path'].$file['filename']);
                }

                $info['tags']="";
                if($info['tagsId']!=''){
                     $tags=explode(',', $info['tagsId']);
                     $info['tags']  = array( );
                     foreach ($tags as $key => $value) {
                        $input_t['tagsID'] = $value;
                        $t=$this->tags_m->get_rows($input_t)->row_array();
                        $rstags[$value]=$t['tagsName'];
                     }

                     $info['tags']=$rstags;
                     
                }

            $data['info'] = $info;

           // }

        }
        $this->article_m->plus_view($info['contentId']);
        Modules::run('track/front',$info['contentId']);
        
        $data['article_act'] = 'active';
       
        $data['contentView'] = "article/detail";
        $data['pageHeader'] = $info['metaTitle'];
        
        $data['metaTitle'] = $info['metaTitle'];
        $data['metaDescription_'] = $info['metaDescription'];
        $data['metaKeyword_'] = $info['metaKeyword'];

        $share['ogTitle']=$info['metaTitle'];
        $share['ogDescription']=$info['metaDescription'];
        $share['ogUrl']= 'article/detail/'.$id;
        $share['ogImage']= $imageSeo;
        $this->_social_share($share);
        $this->front->layout($data);
    }

    public function relate($categoryId=0,$contentId=0)
    {
        $input['grpContent'] = $this->_grpContent;
        $input['length']=5;
        $input['start']=0;
        $input['categoryId']=$categoryId;
        $input['exclude']=$contentId;
        
        $info = $this->article_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ($info as &$rs) {
                $input_u['contentId'] = $rs['contentId'];
                $input_u['grpContent'] = $this->_grpContent;
                $uplode = $this->article_m->get_uplode($input_u)->result_array();
                $rs['linkId'] = str_replace(" ","-",$rs['title_link']);
                $rs['image']=base_url("assets/images/no_image2.png");
                if (!empty($uplode)){
                    foreach ($uplode as $key => $value) {
                        if($value['grpType']=='coverImage'){
                            $coverImage_path=$value['path'];
                            $coverImage_filename=$value['filename'];
                            $rs['image'] = base_url($coverImage_path.$coverImage_filename);
                        }
                    }
                }
                
                $rs['createDate'] = date_language($rs['createDate'], TRUE, 'th');
            }
        }
        $data['info'] = $info;
       //print"<pre>"; print_r($data['info']); exit();
        if (!empty($info)) $this->load->view('relate', $data);
    }

    public function recent()
    {
        $input['grpContent'] = $this->_grpContent;
        $input['length']=5;
        $input['start']=0;
        //$input['categoryId']=$categoryId;
        //$input['exclude']=$contentId;
        
        $info = $this->article_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ($info as &$rs) {
                $input_u['contentId'] = $rs['contentId'];
                $input_u['grpContent'] = $this->_grpContent;
                $uplode = $this->article_m->get_uplode($input_u)->result_array();
                $rs['linkId'] = str_replace(" ","-",$rs['title_link']);
                $rs['image']=base_url("assets/images/no_image2.png");
                if (!empty($uplode)){
                    foreach ($uplode as $key => $value) {
                        if($value['grpType']=='coverImage'){
                            $coverImage_path=$value['path'];
                            $coverImage_filename=$value['filename'];
                            $rs['image'] = base_url($coverImage_path.$coverImage_filename);
                        }
                    }
                }
                
                
                $rs['tags']="";
                if($rs['tagsId']!=''){
                     $tags=explode(',', $rs['tagsId']);
                     $rstags  = array( );
                     foreach ($tags as $key => $value) {
                        $input_t['tagsID'] = $value;
                        $t=$this->tags_m->get_rows($input_t)->row_array();
                        $rstags[$value]=$t['tagsName'];
                     }

                      $rs['tags']=$rstags;
                     
                }    
            }
        }
        $data['info_recent'] = $info;
       //print"<pre>"; print_r($data['info']); exit();
        if (!empty($info)) $this->load->view('recent', $data);
    }

    public function pagin($uri, $total, $segment, $per_page = 30) {

        $config['base_url'] = site_url($uri);
        $config['per_page'] = $per_page;
        $config['total_rows'] = $total;
        $config['uri_segment'] = $segment;
        //$config['num_links'] = $num_links;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination center-align">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = FALSE; //'<span aria-hidden="true">«</span>';
        $config['last_link'] = FALSE; //'<span aria-hidden="true">»</span>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span aria-hidden="true"><i class="fa fa-angle-left"></i></span>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<span aria-hidden="true"><i class="fa fa-angle-right"></i></span>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    public function _social_share($input)
    {
        $param['ogType'] = "webiste";
        $param['ogUrl'] = $input['ogUrl'];
        $param['ogTitle'] = $input['ogTitle'];
        $param['ogDescription'] = $input['ogDescription'];
        $param['ogImage'] = $input['ogImage'];
        $param['twImage'] = $input['ogImage'];
        
        Modules::run('social/set_share', $param);
    }  

    public function pagin2($uri, $total, $segment, $per_page = 30) {

        $config['base_url'] = site_url($uri);
        $config['per_page'] = $per_page;
        $config['total_rows'] = $total;
        $config['uri_segment'] = $segment;
        //$config['num_links'] = $num_links;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = FALSE; //'<span aria-hidden="true">«</span>';
        $config['last_link'] = FALSE; //'<span aria-hidden="true">»</span>';
        // $config['first_tag_open'] = '<a>';
        // $config['first_tag_close'] = '</a>';
        $config['prev_link'] = '&laquo;';
        // $config['prev_tag_open'] = '<a>';
        // $config['prev_tag_close'] = '</a>';
        $config['next_link'] = '&raquo;';
        // $config['next_tag_open'] = '<a>';
        // $config['next_tag_close'] = '</a>';
        // $config['last_tag_open'] = '<a>';
        // $config['last_tag_close'] = '</a>';
        $config['cur_tag_open'] = '<a class="active">';
        $config['cur_tag_close'] = '</a>';
        // $config['num_tag_open'] = '<li>';
        // $config['num_tag_close'] = '</li>';

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    
}
