<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
        <div class="box-tools pull-right">
        </div>
    </div>
    <div class="box-body">
        <?php echo form_open($frmAction, array('class' => 'form-horizontal', 'method' => 'post')) ?>
        <h4 class="block"> ข้อมูลทั่วไป</h4>   
        <div class="form-group">
            <div class="col-sm-7 col-sm-offset-2">
                <label class="icheck-inline"><input type="checkbox" name="recommend" value="1" class="icheck" <?php echo isset($info->recommend) && $info->recommend == 1 ? "checked" : NULL ?>/> รายการแนะนำ</label>
            </div>
        </div>  
       
        <div class="form-group">
            <label class="col-sm-2 control-label" for="title"><font color=red>*</font> ชื่อเรื่อง</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control" name="title" required>
            </div>
        </div>      
        <div class="form-group">
            <label class="col-sm-2 control-label" for="excerpt">คำโปรย</label>
            <div class="col-sm-7">
                <textarea name="excerpt" rows="3" class="form-control"><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
            </div>
        </div>  
        <div class="form-group">
            <label class="col-sm-2 control-label" for="detail">เนื้อหา</label>
            <div class="col-sm-7">
                <?php echo $this->ckeditor->editor("detail", isset($info->detail) ? $info->detail : NULL, "normal", 200); ?>
            </div>
        </div>

         <div class="form-group ">

            <label class="col-sm-2 control-label"><font color=red>*</font> สถานที่</label>

            <div class="col-sm-7">

                <?php echo form_dropdown('location', $location, isset($info->location) ? $info->location : NULL, 'class="form-control select2" required'); ?>

            </div>

        </div> 

        <div class="form-group">
            <label class="col-sm-2 control-label" for="latitude">ละติจูด</label>
            <div class="col-sm-2">
                <input value="<?php echo isset($info->latitude) ? $info->latitude : NULL ?>" type="text" class="form-control" name="latitude" >
            </div>
            <label class="col-sm-1 control-label" for="longitude">ลองจิจูด</label>
            <div class="col-sm-2">
                <input value="<?php echo isset($info->longitude) ? $info->longitude : NULL ?>" type="text" class="form-control" name="longitude" >
            </div>
            
        </div>  

        <div class="form-group">
            <label class="col-sm-2 control-label" for="title"><font color=red>*</font> ราคา</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->price) ? number_format($info->price) : NULL ?>" type="text" class="form-control amount" name="price" required>
            </div>
        </div> 

        <div class="form-group">
            <div class="col-sm-7 col-md-offset-2">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false"><i class="fa fa-star"></i> ภาพปก</a></li>
                        <li class="hidden"><a href="#tab_2" data-toggle="tab" aria-expanded="false"><i class="fa fa-image"></i> ภาพประกอบ</a></li>
                        <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="true"><i class="fa fa-th-large"></i> ภาพแกลเลอรี</a></li>
                        <li class="hidden"><a href="#tab_4" data-toggle="tab" aria-expanded="true"><i class="fa fa-file-text"></i> ไฟล์ดาวน์โหลด</a></li>
                    </ul>

                    <div class="tab-content" style="">
                        <div class="tab-pane active" id="tab_1">
                            <div class="col-sm-12" style="margin-bottom: 15px">
                                <button onclick="set_container($('#cover-image'), 'single', 'tmpl-cover-image')" type="button" class="btn btn btn-flat btn-default" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> อัพโหลดไฟล์</button>
                                <!--<button onclick="set_container($('#cover-image'), 'single', 'tmpl-cover-image')" type="button" class="btn btn btn-flat btn-success" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-folder"></i> คลังรูปภาพ</button>-->
                            </div>
                            <div class="row" id="cover-image" style="margin-bottom: 15px; padding:0px 15px">
                                <script> var dataCoverImage = <?php echo isset($coverImage) ? json_encode($coverImage) : "{}" ?></script>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-pane hidden" id="tab_2">
                            <div class="col-sm-12" style="margin-bottom: 15px">
                                <button onclick="set_container($('#content-image'), 'single', 'tmpl-content-image')" type="button" class="btn btn btn-flat btn-default" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> อัพโหลดไฟล์</button>
                                <!--<button onclick="set_container($('#content-image'), 'single', 'tmpl-content-image')" type="button" class="btn btn btn-flat btn-success" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-folder"></i> คลังรูปภาพ</button>-->
                            </div>
                            <div class="form-group" id="content-image" style="margin-bottom: 15px; padding:0px 15px">
                                <script> var dataContentImage = <?php echo isset($contentImage) ? json_encode($contentImage) : "{}" ?></script>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_3">
                            <div class="col-sm-12" style="margin-bottom: 15px">
                                <button onclick="set_container($('#gallery-image'), 'multiple', 'tmpl-gallery-image')" type="button" class="btn btn btn-flat btn-default" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> อัพโหลดไฟล์</button>
                                <!--<button onclick="set_container($('#gallery-image'), 'multiple', 'tmpl-gallery-image')" type="button" class="btn btn btn-flat btn-success" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-folder"></i> คลังรูปภาพ</button>-->
                            </div>
                            <div class="row" id="gallery-image" style="margin-bottom: 15px; padding:0px 15px">
                                <script> var dataGalleryImage = <?php echo isset($galleryImage) ? json_encode($galleryImage) : "{}" ?></script>
                            </div>
                        </div>
                        <div class="tab-pane hidden" id="tab_4">
                            <div class="col-sm-12" style="margin-bottom: 15px">
                                <button onclick="set_container($('#doc-attach'), 'multiple', 'tmpl-doc-attach')" type="button" class="btn btn btn-flat btn-default" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> อัพโหลดไฟล์</button>
                                <!--<button onclick="set_container($('#doc-attach'), 'multiple', 'tmpl-doc-attach')" type="button" class="btn btn btn-flat btn-success" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-folder"></i> คลังรูปภาพ</button>-->
                            </div>
                            <div class="row" id="doc-attach" style="margin-bottom: 15px; padding:0px 15px">
                                <script> var dataDocAttach = <?php echo isset($docAttach) ? json_encode($docAttach) : "{}" ?></script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <h4 class="block">ข้อมูล SEO</h4>
        <div class="form-group">
            <label class="col-sm-2 control-label" >ชื่อเรื่อง (Title)</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->metaTitle) ? $info->metaTitle : NULL ?>" type="text" id="" class="form-control" name="metaTitle">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">คำอธิบาย (Description)</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="5"  class="form-control" name="metaDescription"><?php echo isset($info->metaDescription) ? $info->metaDescription : NULL ?></textarea>
            </div>
        </div>   
        <div class="form-group">
            <label class="col-sm-2 control-label">คำหลัก (Keyword)</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="3"  class="form-control" name="metaKeyword"><?php echo isset($info->metaKeyword) ? $info->metaKeyword : NULL ?></textarea>
            </div>
        </div>  

        <div class="col-md-7 col-md-offset-2">
            <div class="form-group">

            </div>
        </div>  
    </div>

    <div class="box-footer">
        <div class="col-sm-7 col-sm-offset-3">
            <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->contentId) ? encode_id($info->contentId) : 0 ?>">
    <?php echo form_close() ?>
</div>           
<!--</div>-->

<?php echo Modules::run('admin/upload/modal', $grpContent) ?>
<?php echo Modules::run('admin/upload/modal_crop') ?>

<script type="text/x-tmpl" id="tmpl-cover-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 5px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="coverImageId" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="coverImageTitle" placeholder="ชื่อภาพ">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
            <!--<button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>-->
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-content-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 5px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="contentImageId" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="contentImageTitle" placeholder="ชื่อภาพ">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
            <!--<button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>-->
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-gallery-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2 item" id="image">
        <div style=" margin-bottom: 10px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="galleryImageId[]" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="galleryImageTitle[]" placeholder="ชื่อภาพ">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
            <!--<button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>-->
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    <div class="clearfix"></div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-doc-attach">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 10px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-8" id="info">
        <input type="hidden" id="uploadId" name="docAttachId[]" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="docAttachTitle[]" placeholder="ชื่อไฟล์">
    </div>
    <div class="col-sm-2" id="action">
        <div class="btn-group">
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบไฟล์</button>
        </div>
    </div>
    <div class="clearfix"></div>
    {% } %}
</script>