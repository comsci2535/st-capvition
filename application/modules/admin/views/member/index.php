<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">รายการ</h3>
        <div class="box-tools pull-right">
            <?php echo $boxAction; ?>
        </div>
    </div>
    <div class="box-body filter">
        <form id="frm-filter" role="form">
             
            <div class="col-md-3 form-group">
                <label for="active">สถานะ</label>
                <?php $activeDD = array(""=>'ทั้งหมด', 1=>'เปิด', 0=>'ปิด') ?>
                <?php echo form_dropdown('active', $activeDD, null, 'class="from-control select2"') ?>
            </div> 
            
            <div class="col-md-7 form-group">
                <label for="keyword">คำค้นหา</label>
                <input class="form-control" name="keyword" type="text">
            </div>           
            <div class="col-md-1 form-group">
                <button type="button" class="btn btn-primary btn-flat btn-block btn-filter"><i class="fa fa-filter"></i> ค้นหา</button>
            </div>
        </form>
    </div>
    <div class="box-body">
        <form  role="form">
          <!--   <div style="overflow-x: auto !important; min-width: 700px;"> -->
    
            <table id="data-list" class="table table-hover dataTable  table-striped table-bordered nowrap" width="100%">
                <thead>
                    <tr>
                       
                        <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                        <th >ชื่อนามสกุล</th>
                         <th>คอร์สเรียน</th>
                        <!--<th>เบอร์โทร</th>-->
                       <!--  <th >อีเมล์</th>  -->
                        <th >ใช้งานล่าสุด</th>
                        <th >สร้าง</th>
                        <th >สถานะ</th>
                        <th ></th>
                    </tr>
                </thead>
            </table>
            
            <!-- </div> -->
        </form>
    </div>  
    <div id="overlay-box" class="overlay">
        <i class="fa fa-circle-o-notch fa-spin"></i>
    </div>         
</div>
