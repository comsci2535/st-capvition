<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">ข้อมูลผู้ดูแล</h3>
    </div>
    <div class="box-body">
        <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-edit', 'method' => 'post')) ?>
        <!-- <div class="form-group">
            <label class="col-sm-2 control-label">กลุ่มผู้ดูแล</label>
            <div class="col-sm-7">
                <?php echo form_dropdown('policy', $policyDD, $info->policyId, 'class="form-control select2" required') ?>
            </div>
        </div>   --> 

       

        <div class="form-group">
            <label class="col-sm-2 control-label" >ชื่อ</label>
            <div class="col-sm-7">
                <input value="<?php echo $info->firstname ?>" type="text" id="input-username" class="form-control" name="firstname" required>
            </div>
        </div>  

        <div class="form-group">
            <label class="col-sm-2 control-label" >นามสกุล</label>
            <div class="col-sm-7">
                <input value="<?php echo $info->lastname ?>" type="text" id="input-username" class="form-control" name="lastname">
            </div>
        </div>         
        <div class="form-group">
            <label class="col-sm-2 control-label" >อีเมล์</label>
            <div class="col-sm-7">
                <input type="email" id="input-email" class="form-control" name="email" value="<?php echo $info->email ?>" >
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label" >เบอร์โทร</label>
            <div class="col-sm-7">
                <input type="text" id="input-phone" class="form-control" name="phone" value="<?php echo $info->phone ?>" >
            </div>
        </div>
        
       <div class="form-group">
            <label class="col-sm-2 control-label" >ชื่อผู้ใช้งาน</label>
            <div class="col-sm-7">
                <input value="<?php echo $info->username ?>" type="text" id="input-username" class="form-control" name="username" disabled>
            </div>
        </div>  
        
        <!-- <div class="form-group">
            <label class="col-sm-2 control-label">รูปภาพ</label>
            <div class="col-sm-7">
                <div class="row">
                    <div class="col-sm-12" style="margin-bottom: 10px">
                        <button onclick="set_container($('#cover-image'), 'single', 'tmpl-cover-image')" type="button" class="btn btn btn-flat btn-default" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> อัพโหลดไฟล์</button>
                        <button onclick="set_container($('#cover-image'), 'single', 'tmpl-cover-image')" type="button" class="btn btn btn-flat btn-success" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-folder"></i> คลังรูปภาพ</button>
                    </div>
                    <div class="col-sm-12" id="cover-image" style="padding-left: 0px;">
                        <script> var dataCoverImage = <?php echo isset($coverImage) ? json_encode($coverImage) : "{}" ?> </script>
                    </div>
                </div>
            </div>
        </div> -->
        
    </div>
    <div class="box-footer">
        <div class="col-sm-7 col-sm-offset-2">
            <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->userId) ? encode_id($info->userId) : 0 ?>">
    <?php echo form_close() ?>
</div>           

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">เปลี่ยนรหัสผ่าน</h3>
    </div>
    <?php echo form_open($frmActionPassword, array('class' => 'form-horizontal frm-change-password', 'method' => 'post')) ?>
    <div class="box-body">
        <!-- <div class="form-group hidden">
            <label class="col-sm-2 control-label" for="inputPassword3">รหัสผ่านปัจจุบัน</label>
            <div class="col-sm-7">
                <input type="password" id="input-old-password" class="form-control" name="oldPassword" value="" required>
            </div>
        </div>   -->
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputPassword3">รหัสผ่านใหม่</label>
            <div class="col-sm-7">
                <input type="password" id="input-new-password" class="form-control" name="newPassword"  value="" required>
            </div>
        </div>                      
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputPassword3">ยืนรหัสผ่านใหม่</label>
            <div class="col-sm-7">
                <input type="password" id="input-re-password" class="form-control" name="rePassword"  value="">
            </div>
        </div>                   
    </div>
    <div class="box-footer">
        <div class="col-sm-7 col-sm-offset-2">
            <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->userId) ? encode_id($info->userId) : 0 ?>">
    <?php echo form_close() ?>
</div>

<?php echo Modules::run('admin/upload/modal', $grpContent) ?>
<?php echo Modules::run('admin/upload/modal_crop') ?>

<script type="text/x-tmpl" id="tmpl-cover-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style="">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div id="info">
        <input type="hidden" id="uploadId" name="coverImageId" value="{%=obj.uploadId%}">
    </div>
    <div class="col-sm-4" id="action">
        <div class="btn-group">
            <button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    {% } %}
</script>