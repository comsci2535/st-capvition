<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
    </div>
    <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">    
        
        <div class="form-group">
            <label class="col-sm-2 control-label">ชื่อผู้รีวิว</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" id="input-title" class="form-control" name="title" required>
                <input class="form-control" name="courseId" id="courseId" type="hidden" value="<?php echo $courseId;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">รูปภาพผู้รีวิว</label>
            <div class="col-sm-7">
                <div class="row">
                    <!--  -->
                    <div class="col-sm-4" style="margin-bottom: 10px">
                         <!-- image-preview-filename input [CUT FROM HERE]-->
                        <div class="input-group image-preview">
                            <input type="text" class="form-control image-preview-filename" disabled="disabled" name="fileName"> <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                    <span class="glyphicon glyphicon-remove"></span> ยกเลิก
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                    <span class="glyphicon glyphicon-folder-open"></span>
                                    <span class="image-preview-input-title">อัพโหลด</span>
                                    <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                                </div>
                            </span>


                        </div>
                       
                       <!-- /input-group image-preview [TO HERE]--> 
                    </div>
                    <div class="col-sm-8">
                         <span>172px*172px (กว้าง x สูง) </span>
                    </div>

                    <div class="col-sm-12" id="cover-image" style="padding-left: 0px;">
                        <script> var dataCoverImage = <?php echo isset($coverImage) ? json_encode($coverImage) : "{}" ?> </script>
                    </div>
                    
                     
                </div>
                
                
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="excerpt">เนื้อหา</label>
            <div class="col-sm-7">
                <textarea name="excerpt" rows="3" class="form-control"><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
            </div>
        </div>    

       <div class="form-group">
            <label class="col-sm-2 control-label" for="title">รูปภาพผลงาน</label>
            <div class="col-sm-7">
                <div class="row">
                    <!--  -->
                    <div class="col-sm-4" style="margin-bottom: 10px">
                         <!-- image-preview-filename input [CUT FROM HERE]-->
                        <div class="input-group image-preview-2">
                            <input type="text" class="form-control image-preview-filename-2" disabled="disabled" name="fileName-2"> <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear-2" style="display:none;">
                                    <span class="glyphicon glyphicon-remove"></span> ยกเลิก
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input-2">
                                    <span class="glyphicon glyphicon-folder-open"></span>
                                    <span class="image-preview-input-title-2">อัพโหลด</span>
                                    <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview-2"/> <!-- rename it -->
                                </div>
                            </span>


                        </div>
                       
                       <!-- /input-group image-preview [TO HERE]--> 
                    </div>
                    <div class="col-sm-8">
                         <!-- <span>475px*250px (กว้าง x สูง) </span> -->
                    </div>

                 
                     <div class="col-sm-12"  id="content-image" style="padding-left: 0px;">
                                <script> var dataContentImage = <?php echo isset($contentImage) ? json_encode($contentImage) : "{}" ?></script>
                     </div>
                    
                     
                </div>
                
                
            </div>
        </div>

        
    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->reviewsId) ? encode_id($info->reviewsId) : 0 ?>">
    <?php echo form_close() ?>
</div>
<?php echo Modules::run('admin/upload/modal', $grpContent) ?>
<?php echo Modules::run('admin/upload/modal_crop') ?>

<script type="text/x-tmpl" id="tmpl-cover-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 5px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
       <input type="hidden" id="uploadId" name="coverImageId" value="{%=obj.uploadId%}">
    </div>
    <div class="col-sm-3" id="action">
       
    </div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-content-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 5px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="contentImageId" value="{%=obj.uploadId%}">
    </div>
    <div class="col-sm-3" id="action">
        
    </div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-gallery-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 10px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="galleryImageId[]" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="galleryImageTitle[]" placeholder="ชื่อภาพ">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
            <button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    <div class="clearfix"></div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-doc-attach">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 10px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-8" id="info">
        <input type="hidden" id="uploadId" name="docAttachId[]" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="docAttachTitle[]" placeholder="ชื่อไฟล์">
    </div>
    <div class="col-sm-2" id="action">
        <div class="btn-group">
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบไฟล์</button>
        </div>
    </div>
    <div class="clearfix"></div>
    {% } %}
</script>

