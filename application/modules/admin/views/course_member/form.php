<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
    </div>
    <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">
               
        <div class="form-group">
            <label class="col-sm-2 control-label">คอร์สเรียน</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($courseTitle) ? $courseTitle : NULL ?>" type="text" id="input-title" class="form-control" name="title" disabled>
                <input class="form-control" name="courseId" id="courseId" type="hidden" value="<?php echo $courseId;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label"><font color=red>*</font> ผู้สมัครเรียน</label>
            <div class="col-sm-4">
                   <?php echo form_dropdown('userId', $member, isset($info['userId']) ? $info['userId'] : NULL, 'class="form-control select2" required'); ?>
            </div>
        </div>    

        

    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="courseId" id="input-courseId" value="<?php echo $courseId ?>">
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info['course_memberId']) ? encode_id($info['course_memberId']) : 0 ?>">
    <?php echo form_close() ?>
</div>


<script type="text/x-tmpl" id="tmpl-doc-attach">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 10px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
        <input type="hidden" id="uploadId" name="docAttachId[]" value="{%=obj.uploadId%}">
    </div>
   
    <div class="col-sm-2" id="action">
        <div class="btn-group">
           
        </div>
    </div>
    <div class="clearfix"></div>
    {% } %}
</script>