<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
    </div>
    <?php echo form_open($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">    
         
        
        <div class="form-group">
            <label class="col-sm-2 control-label">คอร์สเรียน</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($courseTitle) ? $courseTitle : NULL ?>" type="text" id="input-title" class="form-control" name="title" disabled>
                <input class="form-control" name="courseId" id="courseId" type="hidden" value="<?php echo $courseId;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">ราคาปกติ</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($price) ? $price : NULL ?>" type="text" id="input-title" class="form-control" name="title" disabled>
              
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">ชื่อโปรโมชั่น</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" id="input-title" class="form-control" name="title" required>
            </div>
        </div>

      <!--   <div class="form-group">
            <label class="col-sm-2 control-label" for="excerpt">คำโปรย</label>
            <div class="col-sm-7">
                <textarea name="excerpt" rows="3" class="form-control"><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
            </div>
        </div> -->  

        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">ประเภท</label>
            <div class="col-sm-7">
                <label class="icheck-inline"><input id="display1"  type="radio" name="type" value="1" class="icheck" <?php if(isset($info->type) && $info->type=="1"){ echo "checked"; }?> <?php if(!isset($info->type)){ echo "checked"; }?> /> ปกติ</label>
                <label class="icheck-inline"><input id="display2" type="radio" name="type" value="2" class="icheck" <?php if(isset($info->type) && $info->type=="2"){ echo "checked"; }?>/> กำหนดราคาเอง</label>
               
            </div>
        </div> 
        <div class="form-group" id="dc">
            <label class="col-sm-2 control-label">ราคาโปรโมชั่น</label>
            <div class="col-sm-3">
                <input value="<?php echo isset($info->discount) ? number_format($info->discount) : NULL ?>" type="text" id="input-discount" class="amount form-control" name="discount" >
                <div id="alert_e" style="color: red;"></div>
            </div>
        </div> 

        <div class="form-group">
            <label class="col-sm-2 control-label">ช่วงเวลาโปรโมชั่น</label>
            <div class="col-sm-3">
                    <input type="text" name="dateRange" class="form-control" value="<?php echo isset($dateRang) ? $dateRang : NULL ?>" />
                    <input type="hidden" name="startDate" value="<?php echo isset($info->startDate) ? $info->startDate : NULL ?>" />
                    <input type="hidden" name="endDate" value="<?php echo isset($info->endDate) ? $info->endDate : NULL ?>" />
            </div>
        </div>     

        <div class="form-group hidden">
            <label class="col-sm-2 control-label" for="descript">รายละเอียด</label>
            <div class="col-sm-7">
                <?php echo $this->ckeditor->editor("detail", isset($info->detail) ? $info->detail : NULL, "normal", 200); ?>
            </div>
        </div>  

        

        <div class="form-group">
            <div class="col-sm-7 col-md-offset-2">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab2_0" data-toggle="tab" aria-expanded="true"><i class="fa fa-list-ul"></i> คอร์สเรียนที่แถม</a></li>
                        
                    </ul>
                    <div class="tab-content" style="">
                        <div class="tab-pane active" id="tab2_0">
                            <div class="row" id="cover-image" style="margin-bottom: 15px; padding:0px 15px">
                                <div class="col-sm-12">
                                    <input value="" type="text" id="filter-category" class="form-control" placeholder="กรองคอร์ส">
                                    <ul class="filter-category" >
                                    
                                                <?php foreach ($courseExclude as $key_ => $rs_) { ?>
                                                    <li><input type="checkbox" class="icheck" name="courseExclude[]" value="<?php echo $key_;?>" <?php if (!empty($promotion) && in_array($key_, $promotion) ) { ?> checked <?php } ?>/>  <?php echo $rs_;?></li>
                                                <?php } ?>
                                                


                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <h4 class="block">ข้อมูล SEO</h4>
        <div class="form-group">
            <label class="col-sm-2 control-label" >ชื่อเรื่อง (Title)</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->metaTitle) ? $info->metaTitle : NULL ?>" type="text" id="" class="form-control" name="metaTitle">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">คำอธิบาย (Description)</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="5"  class="form-control" name="metaDescription"><?php echo isset($info->metaDescription) ? $info->metaDescription : NULL ?></textarea>
            </div>
        </div>   
        <div class="form-group">
            <label class="col-sm-2 control-label">คำหลัก (Keyword)</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="3"  class="form-control" name="metaKeyword"><?php echo isset($info->metaKeyword) ? $info->metaKeyword : NULL ?></textarea>
            </div>
        </div>  

        <div class="col-md-7 col-md-offset-2">
            <div class="form-group">

            </div>
        </div>
    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->promotionId) ? encode_id($info->promotionId) : 0 ?>">
    <?php echo form_close() ?>
</div>
<?php echo Modules::run('admin/upload/modal', $grpContent) ?>
<?php echo Modules::run('admin/upload/modal_crop') ?>

<script type="text/x-tmpl" id="tmpl-cover-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 5px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="coverImageId" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="coverImageTitle" placeholder="ชื่อภาพ">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
            <!--<button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>-->
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-content-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 5px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="contentImageId" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="contentImageTitle" placeholder="ชื่อภาพ">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
            <!--<button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>-->
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-gallery-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2 item" id="image">
        <div style=" margin-bottom: 10px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="galleryImageId[]" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="galleryImageTitle[]" placeholder="ชื่อภาพ">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
            <!--<button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>-->
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    <div class="clearfix"></div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-doc-attach">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 10px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-8" id="info">
        <input type="hidden" id="uploadId" name="docAttachId[]" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="docAttachTitle[]" placeholder="ชื่อไฟล์">
    </div>
    <div class="col-sm-2" id="action">
        <div class="btn-group">
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบไฟล์</button>
        </div>
    </div>
    <div class="clearfix"></div>
    {% } %}
</script>



