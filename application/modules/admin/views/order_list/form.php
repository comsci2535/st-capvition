<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
    </div>
    <?php echo form_open($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">    
        <div class="form-group">
            <div class="col-sm-7 col-sm-offset-1">
                <label class="icheck-inline"><input type="checkbox" name="chk" value="" class="icheck"/> รายการแนะนำ</label>
            </div>
        </div>  
        <div class="form-group">
            <label class="col-sm-1 control-label" for="title">Checkbox</label>
            <div class="col-sm-7">
                <label class="icheck-inline"><input type="checkbox" name="chkB[]" value="" class="icheck"/> option1</label>
                <label class="icheck-inline"><input type="checkbox" name="chkB[]" value="" class="icheck"/> option2</label>
                <label class="icheck-inline"><input type="checkbox" name="chkB[]" value="" class="icheck"/> option3</label>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-1 control-label" for="title">Radio</label>
            <div class="col-sm-7">
                <label class="icheck-inline"><input type="radio" name="chkR" value="" class="icheck"/> option1</label>
                <label class="icheck-inline"><input type="radio" name="chkR" value="" class="icheck"/> option2</label>
                <label class="icheck-inline"><input type="radio" name="chkR" value="" class="icheck"/> option3</label>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-1 control-label">ชื่อ</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" id="input-title" class="form-control" name="title" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-1 control-label" for="excerpt">เนื้อหาย่อ</label>
            <div class="col-sm-7">
                <textarea name="excerpt" rows="3" class="form-control"><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
            </div>
        </div>    

        <div class="form-group">
            <label class="col-sm-1 control-label" for="descript">เนื้อหา</label>
            <div class="col-sm-7">
                <?php echo $this->ckeditor->editor("detail", isset($info->detail) ? $info->detail : NULL, "normal", 200); ?>
            </div>
        </div>  

        <div class="form-group">
            <div class="col-sm-7 col-md-offset-1">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_0" data-toggle="tab" aria-expanded="true"><i class="fa fa-list-ul"></i> หมวดหมุ่</a></li>
                        <li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="false"><i class="fa fa-star"></i> ภาพปก</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"><i class="fa fa-image"></i> ภาพประกอบ</a></li>
                        <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="true"><i class="fa fa-th-large"></i> ภาพแกลเลอรี</a></li>
                        <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="true"><i class="fa fa-file-text"></i> ไฟล์ดาวน์โหลด</a></li>
                        
                    </ul>

                    <div class="tab-content" style="">
                        <div class="tab-pane active" id="tab_0">
                            <div class="row" id="cover-image" style="margin-bottom: 15px; padding:0px 15px">
                                <div class="col-sm-12">
                                    <input value="" type="text" id="filter-category" class="form-control" placeholder="กรองหมวดหมู่">
                                    <ul class="filter-category">
                                        <li><input type="checkbox" class="icheck"/> item1</li>
                                        <li><input type="checkbox" class="icheck"/> item2</li>
                                        <li><input type="checkbox" class="icheck"/> item3
                                          <ul>
                                              <li><input type="checkbox" class="icheck"/> item32
                                                <ul>
                                                    <li><input type="checkbox" class="icheck"/> item323</li>
                                                    <li><input type="checkbox" class="icheck"/> item324</li>
                                                </ul>
                                              </li>
                                              <li><input type="checkbox" class="icheck"/> item33</li>
                                          </ul>
                                        </li>
                                        <li><input type="checkbox" class="icheck"/> item4</li>
                                        <li><input type="checkbox" class="icheck"/> item5</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_1">
                            <div class="col-sm-12" style="margin-bottom: 15px">
                                <button onclick="set_container($('#cover-image'), 'single', 'tmpl-cover-image')" type="button" class="btn btn btn-flat btn-default" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> อัพโหลดไฟล์</button>
                                <button onclick="set_container($('#cover-image'), 'single', 'tmpl-cover-image')" type="button" class="btn btn btn-flat btn-success" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-folder"></i> คลังรูปภาพ</button>
                            </div>
                            <div class="row" id="cover-image" style="margin-bottom: 15px; padding:0px 15px">
                                <script> var dataCoverImage = <?php echo isset($coverImage) ? json_encode($coverImage) : "{}" ?></script>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="col-sm-12" style="margin-bottom: 15px">
                                <button onclick="set_container($('#content-image'), 'single', 'tmpl-content-image')" type="button" class="btn btn btn-flat btn-default" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> อัพโหลดไฟล์</button>
                                <button onclick="set_container($('#content-image'), 'single', 'tmpl-content-image')" type="button" class="btn btn btn-flat btn-success" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-folder"></i> คลังรูปภาพ</button>
                            </div>
                            <div class="form-group" id="content-image" style="margin-bottom: 15px; padding:0px 15px">
                                <script> var dataContentImage = <?php echo isset($contentImage) ? json_encode($contentImage) : "{}" ?></script>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_3">
                            <div class="col-sm-12" style="margin-bottom: 15px">
                                <button onclick="set_container($('#gallery-image'), 'multiple', 'tmpl-gallery-image')" type="button" class="btn btn btn-flat btn-default" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> อัพโหลดไฟล์</button>
                                <button onclick="set_container($('#gallery-image'), 'multiple', 'tmpl-gallery-image')" type="button" class="btn btn btn-flat btn-success" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-folder"></i> คลังรูปภาพ</button>
                            </div>
                            <div class="row" id="gallery-image" style="margin-bottom: 15px; padding:0px 15px">
                                <script> var dataGalleryImage = <?php echo isset($galleryImage) ? json_encode($galleryImage) : "{}" ?></script>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_4">
                            <div class="col-sm-12" style="margin-bottom: 15px">
                                <button onclick="set_container($('#doc-attach'), 'multiple', 'tmpl-doc-attach')" type="button" class="btn btn btn-flat btn-default" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> อัพโหลดไฟล์</button>
                                <button onclick="set_container($('#doc-attach'), 'multiple', 'tmpl-doc-attach')" type="button" class="btn btn btn-flat btn-success" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-folder"></i> คลังรูปภาพ</button>
                            </div>
                            <div class="row" id="doc-attach" style="margin-bottom: 15px; padding:0px 15px">
                                <script> var dataDocAttach = <?php echo isset($docAttach) ? json_encode($docAttach) : "{}" ?></script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->repoId) ? encode_id($info->repoId) : 0 ?>">
    <?php echo form_close() ?>
</div>
<?php echo Modules::run('admin/upload/modal', $grpContent) ?>
<?php echo Modules::run('admin/upload/modal_crop') ?>

<script type="text/x-tmpl" id="tmpl-cover-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 5px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="coverImageId" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="coverImageTitle" placeholder="ชื่อภาพ">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
            <button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-content-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 5px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="contentImageId" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="contentImageTitle" placeholder="ชื่อภาพ">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
            <button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-gallery-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 10px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="galleryImageId[]" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="galleryImageTitle[]" placeholder="ชื่อภาพ">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
            <button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    <div class="clearfix"></div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-doc-attach">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 10px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-8" id="info">
        <input type="hidden" id="uploadId" name="docAttachId[]" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="docAttachTitle[]" placeholder="ชื่อไฟล์">
    </div>
    <div class="col-sm-2" id="action">
        <div class="btn-group">
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบไฟล์</button>
        </div>
    </div>
    <div class="clearfix"></div>
    {% } %}
</script>

