
    <!-- <div class="box-header with-border">
        <h3 class="box-title">รายการ</h3>
        <div class="box-tools pull-right">
            <?php echo $boxAction; ?>
        </div>
    </div>
    <div class="box-body filter">
        <form id="frm-filter" role="form">
            <div class="col-md-3 form-group">
                <label for="startRang">วันที่สร้าง</label>
                <input type="text" name="createDateRange" class="form-control" value="" />
                <input type="hidden" name="createStartDate" value="" />
                <input type="hidden" name="createEndDate" value="" />
            </div>    
            <div class="col-md-3 form-group">
                <label for="startRang">วันที่แก้ไข</label>
                <input type="text" name="updateDateRange" class="form-control" value="" />
                <input type="hidden" name="updateStartDate" value="" />
                <input type="hidden" name="updateEndDate" value="" />
            </div>  
            <div class="col-md-2 form-group">
                <label for="active">สถานะ</label>
                <?php $activeDD = array(""=>'ทั้งหมด', 1=>'เปิด', 0=>'ปิด') ?>
                <?php echo form_dropdown('active', $activeDD, null, 'class="from-control select2"') ?>
            </div> 
            
            <div class="col-md-3 form-group">
                <label for="keyword">คำค้นหา</label>
                <input class="form-control" name="keyword" type="text">
            </div>           
            <div class="col-md-1 form-group">
                <button type="button" class="btn btn-primary btn-flat btn-block btn-filter"><i class="fa fa-filter"></i> ค้นหา</button>
            </div>
        </form>
    </div> -->
    <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <label style="padding-top: 5px" class="col-sm-2 col-md-offset-6 control-label text-right">รายงานประจำเดือน:</label>
                    <div class="col-sm-2">
                        <?php echo form_dropdown('month', $ddMonth, date('m'), "class='form-control daily-month'") ?>
                    </div>                    
                    <div class="col-sm-1">
                        <?php echo form_dropdown('year', $ddYear, current(array_keys($ddYear)), "class='form-control daily-year'") ?>
                    </div>
                    <div class="col-sm-1">
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div id="daily-device" style="min-width: 310px; height: 600px; margin: 10px auto"></div>
                    <div id="dialy-device-data"></div>
                    <div id="daily-page" style="min-width: 310px; height: 600px; margin: 10px auto"></div>
                    <div id="dialy-page-data"></div>
                    <!-- <div id="daily-department" style="min-width: 310px; height: 600px; margin: 10px auto"></div>
                    <div id="dialy-department-data"></div> -->                    
                </div>
            </div>
            
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <label style="padding-top: 5px" class="col-sm-2 col-md-offset-8 control-label text-right">รายงานประจำปี:</label>
                    <div class="col-sm-1">
                        <?php echo form_dropdown('year', $ddYear, current(array_keys($ddYear)), "class='form-control monthly-year'") ?>
                    </div>
                    <div class="col-sm-1">
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div id="monthly-device" style="min-width: 310px; height: 600px; margin: 10px auto"></div>
                    <div id="monthly-device-data"></div>
                    <div id="monthly-page" style="min-width: 310px; height: 600px; margin: 10px auto"></div>
                    <div id="monthly-page-data"></div>
                   <!--  <div id="monthly-department" style="min-width: 310px; height: 600px; margin: 10px auto"></div>
                    <div id="monthly-department-data"></div>            -->         
                </div>
            </div>         

