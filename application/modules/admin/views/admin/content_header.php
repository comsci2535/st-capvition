<section class="content-header">
    <h1><i class="ion-ios-location"></i>
        <?php echo $pageHeader; ?>
        <?php if (isset($pageExcerpt)) : ?>
            <small><?php echo $pageExcerpt; ?></small>
        <?php endif; ?>
    </h1>
    <?php echo $this->breadcrumbs->show(); ?>
</section>
