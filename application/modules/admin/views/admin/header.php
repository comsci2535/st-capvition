<header class="main-header">
  
     <a href="<?php echo site_url('') ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
            <img src="<?php echo base_url("assets/website/") ?>images/logo.png"  style="width: 30px;"/> 
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="<?php echo base_url("assets/website/") ?>images/logo.png"  style="height: 30px;"/> The CAP Vision Academy</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <?php if (count($language) > 1 && config_item('langSwitch')) : ?>
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true" style="padding-top: 14px;padding-bottom: 12px;">
                            <img src="<?php echo base_url("uploads/flag/{$curLang}.png"); ?>"/>
                        </a>
                        <ul class="dropdown-menu" style="width:100px">
                            <li>
                                <ul class="menu">
                                    <?php foreach ($language as $key=>$rs ) : ?>
                                    <li class="text-right">
                                        <a href="<?php echo url_change_lang($this->uri->uri_string(), $this->input->get(), $key); ?>"><?php echo $rs; ?> <img style="margin-left: 10px" src="<?php echo base_url("uploads/flag/{$key}.png"); ?>"></img></a>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo $this->session->user['image'] ?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo $this->session->user['name'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo $this->session->user['image'] ?>" class="img-circle" alt="User Image">
                            <p>
                                <?php echo $this->session->user['name'] ?>
                                <small><?php echo $this->session->user['email'] ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo site_url("admin/user_profile/edit")?>" class="btn btn-info btn-flat"><i class="fa fa-user-circle-o"></i> ข้อมูลส่วนตัว</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo site_url("admin/logout") ?>" class="btn btn-warning btn-flat"><i class="fa fa-sign-out"></i> ออกระบบ</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <!-- <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li> -->
            </ul>
        </div>
    </nav>
</header>
