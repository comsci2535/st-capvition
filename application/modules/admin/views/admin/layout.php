<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url("assets/website/") ?>images/favicon.ico" type="image/x-icon">
    <base href="<?php echo site_url(); ?>">
    <title><?php echo $pageTitle; ?></title>

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/Ionicons/css/ionicons.min.css">

    <?php if (in_array($this->router->class, array('member','order_list'))){ ?>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">

    <style>
    /* The container */
    .chb {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default checkbox */
    .chb input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #d2d6de;
    }

    /* On mouse-over, add a grey background color */
    .chb:hover input ~ .checkmark {
        background-color: #d2d6de;
    }

    /* When the checkbox is checked, add a blue background */
    .chb input:checked ~ .checkmark {
        background-color: #2196F3;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    .chb input:checked ~ .checkmark:after {
        display: block;
    }

    /* Style the checkmark/indicator */
    .chb .checkmark:after {
        left: 9px;
        top: 5px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
    </style>
    <?php }else{ ?>

        <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <?php } ?>



    <link href="<?php echo base_url() ?>assets/plugins/icheck/skins/square/grey.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/toastr/toastr.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/bower_components/select2/dist/css/select2.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/cropper-master/css/cropper.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css"/>

    <?php if (in_array($this->router->method, array('order'))) : ?>  
        <link href="<?php echo base_url() ?>assets/bower_components/nestable2/jquery.nestable.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>

    <?php if (in_array($this->router->method, array('edit','create'))) : ?>
        <link href="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/css/jquery.fileupload.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>

    <link rel="stylesheet" href="<?php echo base_url("assets/") ?>dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/") ?>dist/css/skins/_all-skins.css">

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-toggle-master/css/bootstrap-toggle.css">
    <link href="<?php echo base_url("assets/") ?>css/admin/custom.css" rel="stylesheet" type="text/css"/>



    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/select2/select2.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/select2/select2.custom.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/select2/bootstrap-select.min.css" />

    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/jquery.fancybox.css?v=2.1.5" />
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />

    <!-- Zun Noti -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/push.js/0.0.11/push.min.js"></script>
    <script>

        Push.Permission.request();

    </script>




    <?php //endif; ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    .image-preview-input {
        position: relative;
        overflow: hidden;
        margin: 0px;    
        color: #333;
        background-color: #fff;
        border-color: #ccc;    
    }
    .image-preview-input input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    .image-preview-input-title {
        margin-left:2px;
    }

    .image-preview-input-2 {
        position: relative;
        overflow: hidden;
        margin: 0px;    
        color: #333;
        background-color: #fff;
        border-color: #ccc;    
    }
    .image-preview-input-2 input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    .image-preview-input-title-2 {
        margin-left:2px;
    }
</style>

</head>
<body class="hold-transition skin-yellow sidebar-mini">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <?php $this->load->view('sidebar'); ?>
        <div class="content-wrapper">
            <?php $this->load->view('content_header'); ?>
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <?php $this->load->view($contentView); ?>
                    </div>
                </div>
            </section>
        </div> 
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>เวอร์ชั่น</b> 1.0.0
            </div>
            <strong>Copyright &copy; 2018 <a href="<?php echo site_url();?>">The CAP Vision Academy</a></strong> สงวนลิขสิทธิ์.
        </footer>

        <?php $this->load->view('asside'); ?>
        <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>

    <script src="<?php echo base_url() ?>assets/bower_components/jquery/dist/jquery.min.js"></script>

    <script src="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js" type="text/javascript"></script>

    <?php if (in_array($this->router->class, array('member','order_list'))){ ?>
       <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
       <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
       <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
       <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
       <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <?php }else{ ?>

       <script src="<?php echo base_url() ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
       <script src="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

    <?php } ?>

    


    <script src="<?php echo base_url() ?>assets/bower_components/moment/moment.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/plugins/toastr/toastr.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/plugins/jquery.form.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/jquery-validation/dist/localization/messages_th.js"></script>  
    <script src="<?php echo base_url() ?>assets/plugins/jQueryUI/jquery-ui.min.js" type="text/javascript"></script>


    <script src="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/jquery.fancybox.js?v=2.1.5" ></script>
    <script src="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <script src="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

    <script src="<?php echo base_url() ?>assets/bower_components/highcharts/highcharts.js"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/highcharts/highcharts-3d.js"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/highcharts/modules/exporting.js"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/highcharts/modules/export-data.js"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/highcharts/modules/drilldown.js"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/highcharts/modules/map.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/bootbox/bootbox.min.js"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/highcharts/modules/bullet.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/plugins/th-all.js" type="text/javascript"></script>
    <?php if (in_array($this->router->class, array('social','course','course_content','instructor','rental','new_house','promotion','lesson', 'user_profile', 'user', 'banner', 'room', 'reviews', 'recommend', 'article', 'activity', 'content','seo_article','seo_activity','seo_home','seo_course','bank'))) : ?>
        <?php if (in_array($this->router->method, array('edit','create'))) : ?>      
         <script src="<?php echo base_url() ?>assets/plugins/cropper-master/js/cropper.js" type="text/javascript"></script>
         <!--<script src="<?php echo base_url() ?>assets/plugins/canvas-to-blob.js" type="text/javascript"></script>-->
         <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/vendor/jquery.ui.widget.js" type="text/javascript"></script>
         <script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
         <script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
         <script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
         <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.iframe-transport.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.fileupload.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.fileupload-process.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.fileupload-image.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.fileupload-audio.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.fileupload-video.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.fileupload-validate.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.fileupload-ui.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/main.js" type="text/javascript"></script>
        <!--[if (gte IE 8)&(lt IE 10)]>
        <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/cors/jquery.xdr-transport.js"></script>
    <![endif]-->

    <script src="<?php echo base_url();?>assets/plugins/select2/select2.min.js" ></script>
    <script src="<?php echo base_url();?>assets/plugins/select2/bootstrap-select.min.js"></script>
<?php endif; ?>




<?php endif; ?>


<?php  if (in_array($this->router->class, array('report_website','report_history'))) : ?>
    <script src="<?php echo base_url() ?>assets/plugins/highcharts/highcharts.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/plugins/highcharts/modules/exporting.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/plugins/highcharts/jquery.highchartTable.js" type="text/javascript"></script>
<?php endif; ?>

<script src="<?php echo base_url() ?>assets/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>

<?php if (in_array($this->router->method, array('order'))) : ?>  
    <script src="<?php echo base_url() ?>assets/bower_components/nestable2/jquery.nestable.js" type="text/javascript"></script>
<?php endif; ?>
<!-- AdminLTE App -->
<script src="<?php echo base_url("assets/") ?>dist/js/adminlte.min.js"></script>
<!-- <script src="<?php echo base_url("assets/") ?>dist/js/demo.js"></script> -->
<script src="<?php echo base_url("assets/") ?>scripts/admin/app.js?v=<?php echo rand(0,100) ?>"></script>
<?php echo $pageScript; ?>
<script>
    var siteUrl = "<?php echo site_url(); ?>";
    var baseUrl = "<?php echo base_url(); ?>";
    var controller = "<?php echo $this->router->class ?>";
    var method = "<?php echo $this->router->method ?>";
    $(document).ready(function () {
        <?php if ($this->session->toastr) : ?>
            setTimeout(function () {
                toastr.<?php echo $this->session->toastr['type']; ?>('<?php echo $this->session->toastr['lineTwo']; ?>', '<?php echo $this->session->toastr['lineOne']; ?>');
            }, 500);
            <?php $this->session->unset_userdata('toastr'); ?>
        <?php endif; ?>
    });    

    $(document).on('click', '#close-preview', function(){ 
        $('.image-preview').popover('hide');
                // Hover befor close the preview
                $('.image-preview').hover(
                    function () {
                       $('.image-preview').popover('show');
                   }, 
                   function () {
                       $('.image-preview').popover('hide');
                   }
                   );    
            });

    $(function() {
                // Create the close button
                var closebtn = $('<button/>', {
                    type:"button",
                    text: 'x',
                    id: 'close-preview',
                    style: 'font-size: initial;',
                });
                closebtn.attr("class","close pull-right");
                // Set the popover default content
                $('.image-preview').popover({
                    trigger:'manual',
                    html:true,
                    title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
                    content: "There's no image",
                    placement:'bottom'
                });
                // Clear event
                $('.image-preview-clear').click(function(){
                    $('.image-preview').attr("data-content","").popover('hide');
                    $('.image-preview-filename').val("");
                    $('.image-preview-clear').hide();
                    $('.image-preview-input input:file').val("");
                    $(".image-preview-input-title").text("อัพโหลด"); 
                }); 
                // Create the preview image
                $(".image-preview-input input:file").change(function (){     
                    var img = $('<img/>', {
                        id: 'dynamic',
                        width:250,
                        // height:200
                    });      
                    var file = this.files[0];
                    var reader = new FileReader();
                    // Set preview image into the popover data-content
                    reader.onload = function (e) {
                        $(".image-preview-input-title").text("เปลี่ยน");
                        $(".image-preview-clear").show();
                        $(".image-preview-filename").val(file.name);            
                        img.attr('src', e.target.result);
                        $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                    }        
                    reader.readAsDataURL(file);
                });  


            }); 

    $(document).on('click', '#close-preview-2', function(){ 
        $('.image-preview-2').popover('hide');
                // Hover befor close the preview
                $('.image-preview-2').hover(
                    function () {
                       $('.image-preview-2').popover('show');
                   }, 
                   function () {
                       $('.image-preview-2').popover('hide');
                   }
                   );    
            });

    $(function() {
                // Create the close button
                var closebtn = $('<button/>', {
                    type:"button",
                    text: 'x',
                    id: 'close-preview-2',
                    style: 'font-size: initial;',
                });
                closebtn.attr("class","close pull-right");
                // Set the popover default content
                $('.image-preview-2').popover({
                    trigger:'manual',
                    html:true,
                    title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
                    content: "There's no image",
                    placement:'bottom'
                });
                // Clear event
                $('.image-preview-clear-2').click(function(){
                    $('.image-preview-2').attr("data-content","").popover('hide');
                    $('.image-preview-filename-2').val("");
                    $('.image-preview-clear-2').hide();
                    $('.image-preview-input-2 input:file').val("");
                    $(".image-preview-input-title-2").text("อัพโหลด"); 
                }); 
                // Create the preview image
                $(".image-preview-input-2 input:file").change(function (){     
                    var img = $('<img/>', {
                        id: 'dynamic',
                        width:250,
                        // height:200
                    });      
                    var file = this.files[0];
                    var reader = new FileReader();
                    // Set preview image into the popover data-content
                    reader.onload = function (e) {
                        $(".image-preview-input-title-2").text("เปลี่ยน");
                        $(".image-preview-clear-2").show();
                        $(".image-preview-filename-2").val(file.name);            
                        img.attr('src', e.target.result);
                        $(".image-preview-2").attr("data-content",$(img)[0].outerHTML).popover("show");
                    }        
                    reader.readAsDataURL(file);
                });  


            });       
        </script>
        
    </body>
    </html>
