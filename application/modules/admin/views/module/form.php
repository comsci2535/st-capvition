<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
    </div>
    <?php echo form_open($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label">Separate</label>
            <div class="col-sm-7 icheck-inline">
                <label><input <?php echo $info['type']==2 ? "checked" : NULL ?> type="radio" name="type" class="icheck" value="2" > ใช่</label>
                <label><input <?php echo $info['type']==1 ? "checked" : NULL ?> type="radio" name="type" class="icheck" value="1"> ไม่ใช่</label>
            </div>
        </div>         
        <div class="form-group">
            <label class="col-sm-2 control-label">สถานะ</label>
            <div class="col-sm-7 icheck-inline">
                <label><input <?php echo $info['active']==1 ? "checked" : NULL ?> type="radio" name="active" class="icheck" value="1" > เปิด</label>
                <label><input <?php echo $info['active']==0 ? "checked" : NULL ?> type="radio" name="active" class="icheck" value="0"> ปิด</label>
            </div>
        </div>     
        <div class="form-group">
            <label class="col-sm-2 control-label">เมนูด้านข้าง</label>
            <div class="col-sm-7 icheck-inline">
                <label><input <?php echo $info['isSidebar']==1 ? "checked" : NULL ?> type="radio" name="isSidebar" class="icheck" value="1" > ใช่</label>
                <label><input <?php echo $info['isSidebar']==0 ? "checked" : NULL ?> type="radio" name="isSidebar" class="icheck" value="0"> ไม่ใช่</label>
            </div>
        </div>  
        <?php if ($this->router->method == 'create') : ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">สร้างไฟล์โมดูล</label>
                <div class="col-sm-7 icheck-inline">
                    <label><input <?php echo $info['createModule']==1 ? "checked" : NULL ?> type="radio" name="createModule" class="icheck" value="1"> สร้าง</label>
                    <label><input <?php echo $info['createModule']==0 ? "checked" : NULL ?> type="radio" name="createModule" class="icheck" value="0" > ไม่สร้าง</label>
                </div>
            </div>         
        <?php endif; ?>
        <div class="form-group">
            <label class="col-sm-2 control-label">หมวดหมู่หลัก</label>
            <div class="col-sm-7">
                <?php echo $parentModule ?>
            </div>
        </div> 

        <div class="form-group">
            <label class="col-sm-2 control-label">ชื่อ</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['title']) ? $info['title'] : NULL ?>" type="text" id="input-title" class="form-control" name="title" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Directory</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['directory']) ? $info['directory'] : NULL ?>" type="text" id="input-clss" class="form-control" name="directory">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Class</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['class']) ? $info['class'] : NULL ?>" type="text" id="input-clss" class="form-control" name="class">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="icon">ไอคอน</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['icon']) ? $info['icon'] : "fa fa-circle-o" ?>" type="text" id="input-icon" class="form-control" name="icon">
            </div>
        </div>   

        <div class="form-group">
            <label class="col-sm-2 control-label" for="descript">อธิบาย</label>
            <div class="col-sm-7">
                <textarea name="descript" rows="3" class="form-control"><?php echo isset($info['descript']) ? $info['descript'] : "การจัดการข้อมูลเกี่ยวกับ ___" ?></textarea>
            </div>
        </div>  

    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info['moduleId']) ? encode_id($info['moduleId']) : 0 ?>">
    <?php echo form_close() ?>
</div>

