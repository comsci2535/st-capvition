<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>The CAP Vision Academy | Admin</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/animate.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <style>
    .login-box-body, .register-box-body {background: rgba(255, 255, 255, 0.5);padding: 30px;}   
    .login-box-msg{font-size: 18px}
    </style>
    <body class="hold-transition login-page" style="height: 80%;">
        <div class="login-box">
            <div class="login-logo">
                  <img src="<?php echo base_url("assets/website/") ?>images/logo.png" class="ls-bg" alt="" style="height: 45px;"/> <a href="<?php echo base_url() ?>">The CAP Vision Academy</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">เข้าระบบเพื่อเริ่มต้นใช้งาน</p>
                <div class="form-group">
                    <div class="alert alert-danger alert-dismissible login-error hidden"></div>
                </div>
                <?php echo form_open(base_url('admin/login/check'), 'id="frm-login" class=""'); ?>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" name="username" placeholder="ชื่อผู้ใช้งาน">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" name="password" placeholder="รหัสผ่าน">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="row">
                    <div class="col-xs-4 col-xs-offset-8">
                        <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> เข้าระบบ</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <script src="<?php echo base_url() ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jquery.form.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jquery-validation/dist/localization/messages_th.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- App functions and actions -->
        <script src="<?php echo base_url(); ?>assets/scripts/admin/login.js"></script>

    </body>
</html>
