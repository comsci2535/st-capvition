<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">รายการ</h3>
        <div class="box-tools pull-right">
            <?php echo $boxAction; ?>
        </div>
    </div>
    <div class="box-body filter">
        <form id="frm-filter" role="form">
            <div class="col-md-2 form-group">
                <label for="startRang">วันที่ลงทะเบียน</label>
                <input type="text" name="createDateRange" class="form-control" value="" />
                <input type="hidden" name="createStartDate" value="" />
                <input type="hidden" name="createEndDate" value="" />
            </div>    
            <div class="col-md-3 form-group">
                <label for="active">คอร์สเรียน</label>
                <?php //$activeDD = array(""=>'ทั้งหมด', 1=>'ชำระเงินเรียบร้อย', 0=>'ยังไม่ชำระเงิน') ?>
                <?php echo form_dropdown('courseId', $course, null, 'class="from-control select2"') ?>
            </div> 
            <div class="col-md-2 form-group">
                <label for="active">สถานะ</label>
                <?php $activeDD = array(""=>'ทั้งหมด', 1=>'ชำระเงินเรียบร้อย', 0=>'ยังไม่ชำระเงิน',2=>'ยกเลิก') ?>
                <?php echo form_dropdown('status', $activeDD, null, 'class="from-control select2"') ?>
            </div> 
            
            <div class="col-md-4 form-group">
                <label for="keyword">คำค้นหา</label>
                <input class="form-control" name="keyword" type="text">
            </div>           
            <div class="col-md-1 form-group">
                <button type="button" class="btn btn-primary btn-flat btn-block btn-filter"><i class="fa fa-filter"></i> ค้นหา</button>
            </div>
        </form>
    </div>
    <div class="box-body">
        <form  role="form">
            <div style="overflow-x: scroll;">
            <table id="data-list" class="table table-hover dataTable  table-striped table-bordered nowrap" width="100%">
                <thead>
                    <tr>
                        <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                        <th>เลขที่อ้างอิง</th>
                        <th>สมาชิก</th>
                        <th>จำนวนเงินที่ถอน</th>
                        <th>โอนเงินเข้าธนาคาร</th>
                        <th>ชื่อบัญชี</th>
                        <th>เลขที่บัญชี</th>
                        <th>หลักฐาน</th>
                        <th>วันที่ทำรายการ</th>
                        <th>สถานะ</th>
                    </tr>
                </thead>
            </table>
            </div>
        </form>
    </div>  
    <div id="overlay-box" class="overlay">
        <i class="fa fa-circle-o-notch fa-spin"></i>
    </div>         
</div>
