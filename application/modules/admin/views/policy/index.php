<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">รายการ</h3>
        <div class="box-tools pull-right">
            <?php echo $boxAction; ?>
        </div>
    </div>
    <div class="box-body">
        <form  role="form">
            <table id="data-list" class="table table-hover dataTable" width="100%">
                <thead>
                    <tr>
                        <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                        <th>ชื่อกลุ่มผู้ดูแล</th>
                        <th>หมายเหตุ</th>
                        <th>สร้าง</th>
                        <th>แก้ไข</th>
                        <th>สถานะ</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </form>
    </div>  
    <div id="overlay-box" class="overlay">
        <i class="fa fa-circle-o-notch fa-spin"></i>
    </div>         
</div>