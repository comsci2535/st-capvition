<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sidebar extends MX_Controller {
    
    public $allParent;

    public function __construct() {
        parent::__construct();
        $this->load->model("sidebar_m");
        
    }

    public function gen() {

        $courseRegis = $this->db
                                ->select('a.course_registerId')
                                ->from('course_register a')
                                ->where('a.status',0)
                                ->where('a.recycle',0)
                                ->get()->num_rows();

        $withdrawList = $this->db
                                ->select('a.point_withdrawId')
                                ->from('point_withdraw a')
                                ->where('a.status',0)
                                ->get()->num_rows();
        $sidebar = array();
        $allModuleId = array();
        $info = $this->sidebar_m->get_sidebar();
        $user = $this->session->user;
        $policy = Modules::run('admin/policy/login', $user['policyId']);
        $user['policy'] = $policy;
        $this->session->set_userdata('user', $user);
        $module = $this->sidebar_m->get_module();
        foreach ( $module->result() as $rs ) {
            $modules[$rs->moduleId] = $rs;
        }   
        if ( !empty($policy) ) {
            foreach ( $policy as $key => $rs ) {
                $this->allParent = array();
                $allParent = $this->_all_parent($key, $modules);
                $allModuleId = array_merge($allModuleId ,$allParent);
            }
        }
        foreach ( $info->result() as $rs ) {
            if ( $this->session->user['type'] == 'developer' || in_array($rs->moduleId, $allModuleId) ) {
                $href = site_url("{$rs->directory}/") . $rs->class;
                if ( $rs->method != "" && $rs->method !== "index" ) {
                    $href = $href . '/' . $rs->method;
                }


                $active = FALSE;
                if ( $rs->class == $this->router->class ) {
                    $active = TRUE;
                }
                if ( $rs->class == 'course' && $this->router->class =='reviews' ) {
                    $active = TRUE;
                }
                if ( $rs->class == 'course' && $this->router->class =='course_member' ) {
                    $active = TRUE;
                }
                if ( $rs->class == 'course' && $this->router->class =='course_content' ) {
                    $active = TRUE;
                }
                if ( $rs->class == 'course' && $this->router->class =='promotion' ) {
                    $active = TRUE;
                }

               
                
                $thisref = &$refs[$rs->moduleId];
                $thisref['target'] = "";
                if(substr($rs->class,0,4)=="http"){ 
                    $href = $rs->class;
                    $thisref['target'] = "_blank";
                }
                $thisref['moduleId'] = $rs->moduleId;
                $thisref['parentId'] = $rs->parentId;
                $thisref['title'] = $rs->title;
                $thisref['icon'] = $rs->icon;
                $thisref['type'] = $rs->type;
                $thisref['href'] = $href;
                $thisref['active'] = $active;

                $thisref['aa'] = '';
                if ( $rs->class == 'order_list' && $courseRegis > 0  ) {
                    $thisref['aa'] = '<span class="pull-right-container">
                                  <small class="label pull-right bg-red">'.$courseRegis.'</small>
                                  </span>';
                }
                
                if ( $rs->class == 'withdraw_list' && $withdrawList > 0 ) {
                    $thisref['aa'] = '<span class="pull-right-container">
                                  <small class="label pull-right bg-red">'.$withdrawList.'</small>
                                  </span>';
                }
                if ( $rs->parentId != 0 ) {
                    if ( $active ) {
                        $refs[$rs->parentId]['active'] = true;
                        if ( isset($refs[$refs[$rs->parentId]['parentId']]) ) {
                           $refs[$refs[$rs->parentId]['parentId']]['active'] = true;
                        }
                    }
                    $refs[$rs->parentId]['children'][$rs->moduleId] = &$thisref;
                } else {
                    $sidebar[$rs->moduleId] = &$thisref;
                } 
            }
        }
//        arrx($sidebar);
        return $sidebar;
    }
    
    private function _all_parent($key, $module){
        if ( isset($module[$key]) ) {
            if ( $module[$key]->parentId != 0 ) {
                array_push($this->allParent, $module[$key]->parentId, $module[$key]->moduleId);
                $this->_all_parent($module[$key]->parentId, $module);
            } else {
                array_push($this->allParent, $module[$key]->moduleId);
            }
        } else {
            $this->allParent = array();
        }
        return $this->allParent;
    }

}
