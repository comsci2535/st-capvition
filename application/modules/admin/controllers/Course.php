<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Course extends MX_Controller {

    private $_title = "จัดการข้อมูลคอร์สเรียน";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับข้อมูลคอร์สเรียน";
    private $_grpContent = "course";
    private $_requiredExport = true;
    private $_permission;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("course_m");
        $this->load->model("upload_m");
        $this->load->library('image_moo');
        $this->load->model("Instructor_m");
    }
    
    public function index() {
        $this->load->module('admin/admin');
        
        // toobar
        $export = array(
            'excel' => site_url("admin/{$this->router->class}/excel"),
            'pdf' => site_url("admin/{$this->router->class}/pdf"),
        );
        $action[1][] = action_refresh(site_url("admin/{$this->router->class}"));
        $action[1][] = action_filter();
        $action[2][] = action_add(site_url("admin/{$this->router->class}/create"));
       // $action[2][] = action_export_group($export);
        $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(site_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";
        
        $this->admin->layout($data);
    }    

    public function data_index() {
        $input = $this->input->post();
        parse_str($_POST['frmFilter'], $frmFilter);
        if ( !empty($frmFilter) ) {
            foreach ( $frmFilter as $key => $rs )
                $input[$key] = $rs;
        }
        $input['recycle'] = 0;
        $info = $this->course_m->get_rows($input);
        $infoCount = $this->course_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->courseId);
            $action = array();
            $action[1][] = table_edit(site_url("admin/{$this->router->class}/edit/{$id}"));

            $manage = array();
             $manage[] = action_custom(site_url("admin/reviews/index/{$rs->courseId}"),'btn-default','add','รีวิวคอร์สเรียน','fa-comment','');
             $manage[] = action_custom(site_url("admin/course_member/index/{$rs->courseId}"),'btn-warning','add','ผู้สมัครเรียน','fa-user','');
            $manage[] = action_custom(site_url("admin/course_content/index/{$rs->courseId}"),'btn-success','add','เนื้อหาคอร์ส','fa-book','');
            $manage[] = action_custom(site_url("admin/promotion/index/{$rs->courseId}"),'btn-primary','add','โปรโมชั่น','fa-map-signs','');

            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
           // $column[$key]['excerpt'] = $rs->excerpt;
            $column[$key]['manage'] = implode($manage," "); //Modules::run('admin/utils/build_button_group', $manage);
            $column[$key]['active'] = toggle_active($active, "admin/{$this->router->class}/action/active");
            $column[$key]['createDate'] = datetime_table($rs->createDate);
            $column[$key]['updateDate'] = datetime_table($rs->updateDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create() {
        $this->load->module('admin/admin');

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/save");

        $instructor_['recycle'] = 0;
        $instructor_['active'] = 1;
        $instructor = $this->Instructor_m->get_rows($instructor_)->result();
        $data['instructor'][''] = "เลือก";
        foreach ($instructor as $key => $value) {
            $data['instructor'][$value->instructorId]=$value->title;
        }
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("admin/{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function save() {
        $input = $this->input->post(null, true);
        $images=$this->upload_ci('input-file-preview');
        $input['coverImageId']=$images['insertId'];

        $images=$this->upload_ci2('input-file-preview-2');
        $input['contentImageId']=$images['insertId'];

        $value = $this->_build_data($input);
        $id = $this->course_m->insert($value);
        if ( $id ) {
            $value = $this->_build_upload_content($id, $input);
            Modules::run('admin/upload/update_content', $value);
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    public function edit($id="") {
        $this->load->module('admin/admin');
        
        $id = decode_id($id);
        $input['courseId'] = $id;
        $info = $this->course_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/update");

        $instructor_['recycle'] = 0;
        $instructor_['active'] = 1;
        $instructor = $this->Instructor_m->get_rows($instructor_)->result();
        $data['instructor'][''] = "เลือก";
        foreach ($instructor as $key => $value) {
            $data['instructor'][$value->instructorId]=$value->title;
        }

        $data['coverImage'] = Modules::run('admin/upload/get_upload_tmpl2', $info->courseId, $this->_grpContent, 'coverImage');
        $data['contentImage'] = Modules::run('admin/upload/get_upload_tmpl2', $info->courseId, $this->_grpContent, 'contentImage');
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("admin/{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function update() {
        $input = $this->input->post(null, true);
        if(isset($_FILES['input-file-preview'])&&$_FILES['input-file-preview']['tmp_name']){
            $images=$this->upload_ci('input-file-preview');
            $input['coverImageId']=$images['insertId'];
        }

        if(isset($_FILES['input-file-preview-2'])&&$_FILES['input-file-preview-2']['tmp_name']){
            $images=$this->upload_ci2('input-file-preview-2');
            $input['contentImageId']=$images['insertId'];
        }
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->course_m->update($id, $value);
        if ( $result ) {
           if(isset($_FILES['input-file-preview'])&&$_FILES['input-file-preview']['tmp_name'] || isset($_FILES['input-file-preview-2'])&&$_FILES['input-file-preview-2']['tmp_name']){
                $value = $this->_build_upload_content($id, $input);
                Modules::run('admin/upload/update_content', $value);
            }
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    private function _build_data($input) {
        $value['recommend'] = isset($input['recommend']) ? $input['recommend'] : 0;
        $value['title'] = $input['title'];
        $value['excerpt'] = $input['excerpt'];
        $value['detail'] = $input['detail'];
        $value['receipts'] = $input['receipts'];
        $value['instructorId'] = $input['instructorId'];
        $value['recommendVideo'] = $input['recommendVideo'];
        $value['learner'] = isset($input['learner']) ? $input['learner'] : 0;
        $value['price'] = str_replace(",","",$input['price']);

        if($input['metaTitle']!=""){
            $value['metaTitle'] = $input['metaTitle'];
        }else{
             $value['metaTitle'] = $input['title'];
        }
        if($input['metaDescription']!=""){
            $value['metaDescription'] = $input['metaDescription'];
        }else{
            $value['metaDescription'] = $input['excerpt'];
        }
        if($input['metaKeyword']!=""){
            $value['metaKeyword'] = $input['metaKeyword'];
        }else{
             $value['metaKeyword'] = $input['title'];
        }

        $value['title_link'] = $this->clean($input['title']);


        if ( $input['mode'] == 'create' ) {
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->user['userId'];
        } else {
            $value['updateDate'] = db_datetime_now();
            $value['updateBy'] = $this->session->user['userId'];
        }
        return $value;
    }

    function clean($string) {
       $string = str_replace('-', ' ', $string);
       $string = str_replace('!', '', $string);
       $string = str_replace('/', '', $string);
       $string = str_replace('#', '', $string);
       $string = str_replace('@', '', $string);
       $string = str_replace('?', '', $string);
       $string = str_replace(',', ' ', $string); // Replaces all spaces with hyphens.
       $string = str_replace('%', ' ', $string); // Removes special chars.
       return str_replace('/[^a-z\d]/i', ' ', $string); // Replaces multiple hyphens with single one.
    }
    
    private function _build_upload_content($id, $input) {
        $value = array();
        if ( isset($input['coverImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId'],
                'title' => $input['coverImageTitle'],
            );
        if ( isset($input['contentImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'contentImage',
                'uploadId' => $input['contentImageId'],
                'title' => $input['contentImageTitle'],
            );
        if ( isset($input['galleryImageId']) ) {
            foreach ( $input['galleryImageId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'galleryImage',
                    'uploadId' => $rs,
                    'title' => $input['galleryImageTitle'][$key]
                );
            }
        }
        if ( isset($input['docAttachId']) ) {
            foreach ( $input['docAttachId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'docAttach',
                    'uploadId' => $rs,
                    'title' => $input['docAttachTitle'][$key]
                );
            }
        }
        return $value;
    }    
    
    
    
    public function trash() {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_list_view(site_url("admin/{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("admin/{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/trash";
        
        $this->admin->layout($data);
    }

    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->course_m->get_rows($input);
        $infoCount = $this->course_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->courseId);
            $action = array();
            $action[1][] = table_restore("admin/{$this->router->class}/action/restore");         
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['excerpt'] = $rs->excerpt;
            $column[$key]['recycleDate'] = datetime_table($rs->recycleDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }    
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->course_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycleDate'] = $dateTime;
                $value['recycleBy'] = $this->session->user['userId'];
                $result = $this->course_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->course_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->course_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  

    public function upload_ci($inputFileName="userfile")  {
        $config = $this->_get_config();
        create_dir($config['upload_path']);
        $this->upload->initialize($config);
       // 
        if ( !$this->upload->do_upload($inputFileName) ) {
            $result['result'] = $this->upload->display_errors();
            $result['success'] = false;
        } else {
            $result = $this->upload->data();
            $result['success'] = true;
            $pathinfo = pathinfo("{$config['upload_path']}{$result['file_name']}");
            $result = array_merge($result, $pathinfo);      
            $value = array(
                'grpContent' => $this->_grpContent,
                'filename' => $result['file_name'],
                'path' => $result['dirname'].'/',
                'size' => $result['file_size']*1024,
                'extension' => $result['extension'],
                'mime' => $result['file_type'],
                'createDate' => db_date_now(),
                'createBy' => $this->session->user['userId']
            );
             $this->resizeImage($result['file_name'],'730','487');
            $result['insertId'] = $this->upload_m->insert($value);
        }

        //print_r($result);exit();
        return $result;
    }  

    public function upload_ci2($inputFileName="userfile")  {
        $config = $this->_get_config2();
        create_dir($config['upload_path']);
        $this->upload->initialize($config);
       // 
        if ( !$this->upload->do_upload($inputFileName) ) {
            $result['result'] = $this->upload->display_errors();
            $result['success'] = false;
        } else {
            $result = $this->upload->data();
            $result['success'] = true;
            $pathinfo = pathinfo("{$config['upload_path']}{$result['file_name']}");
            $result = array_merge($result, $pathinfo);      
            $value = array(
                'grpContent' => $this->_grpContent,
                'filename' => $result['file_name'],
                'path' => $result['dirname'].'/',
                'size' => $result['file_size']*1024,
                'extension' => $result['extension'],
                'mime' => $result['file_type'],
                'createDate' => db_date_now(),
                'createBy' => $this->session->user['userId']
            );
             $this->resizeImage($result['file_name'],'30','30');
            $result['insertId'] = $this->upload_m->insert($value);
        }

        //print_r($result);exit();
        return $result;
    }  

    public function resizeImage($filename,$width,$height){
      //   print_r($filename);exit();
      $source_path = 'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/' . $filename;
      //$target_path =  'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/thumbnail/';
      $config_manip = array(
          'image_library' => 'GD2',
          'quality'=> '90%',
          'source_image' => $source_path,
          //'new_image' => $target_path,
          'create_thumb' => FALSE,
          'maintain_ratio' => FALSE,
          //'thumb_marker' => '_thumb',
          'width' => $width,
          'height' => $height
      );


      $this->load->library('image_lib', $config_manip);
      if (!$this->image_lib->resize()) {
          echo $this->image_lib->display_errors();
      }


      $this->image_lib->clear();
   } 
    
    private function _get_config(){

        $config['upload_path'] = 'uploads/';
        $config['max_size'] = file_upload_max_size();
        $config['resize'] = true;
        $config['upload_path'] = 'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/';
        $config['encrypt_name'] = true;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|image/png|svg';
        $config['resizeOverwrite'] = true;
        $config['quality']= '90%';
        $config['resize'] = true;
        $config['width'] = 730;
        $config['height'] = 487;
    
        
        return $config;
    } 
     private function _get_config2(){

        $config['upload_path'] = 'uploads/';
       // $config['max_size'] = file_upload_max_size();
        $config['resize'] = true;
        $config['upload_path'] = 'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/';
        $config['encrypt_name'] = true;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|image/png|svg';
        $config['resizeOverwrite'] = true;
        $config['quality']= '90%';
        $config['resize'] = true;
        $config['width'] = 730;
        $config['height'] = 487;
    
        
        return $config;
    } 

    public function checkTitle()
    {
        $input = $this->input->post();
        $input['recycle'] = array(0,1);
        //$input['grpContent'] = $this->_grpContent;
        //$input['categoryId'] = "";
        $info = $this->course_m->get_rows($input);
       // arr($info); exit();
        if ( $info->num_rows() > 0 ) {
            if ($input['mode'] == 'create') {
                $rs = FALSE;
            } else {
                $row = $info->row();
                if ($row->courseId == decode_id($input['id'])) {
                    $rs = TRUE;
                } else {
                    $rs = FALSE;
                }
            }
        } else {
            $rs =  TRUE;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($rs));
    } 
    
}
