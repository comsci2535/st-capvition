<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {
    
    private $_title = 'title';
    private $_pageExcerpt = 'page excerpt';
    private $_grpContent = 'grpContent';
    
    public function __construct() {
        parent::__construct();
        $this->load->model("home_m");
    }

    public function index() {
        $this->load->module('admin');
        
        if ( $this->session->firstTime )
            Modules::run('admin/utils/toastr', 'info', config_item('appName'), 'ยินดีต้อนรับ');   
        
        // toobar
        $action[1][] = action_refresh(base_url("admin/{$this->router->class}"));
        $action[1][] = action_filter();
        $action[2][] = action_add(base_url("admin/{$this->router->class}/create"));
        $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(base_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "empty";
             
        $this->admin->layout($data);
    }

}
