<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MX_Controller {

    private $_title = 'สมาชิก';
    private $_pageExcerpt = 'การจัดการเกี่ยวกับสมาชิก';
    private $_permission;
    private $_grpContent = 'member';

    public function __construct() {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }

        
        $this->load->library('encryption');
        $this->load->model("member_m");
    }

    public function index() {
        $this->load->module('admin/admin');

        // toobar
        $action[1][] = action_refresh(base_url("admin/{$this->router->class}"));
         $action[1][] = action_filter();
        $action[2][] = action_add(base_url("admin/{$this->router->class}/create"));
        $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(base_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";

        $this->admin->layout($data);
    }

    public function data_index() {
        $input = $this->input->post();
        
        parse_str($_POST['frmFilter'], $frmFilter);
        if ( !empty($frmFilter) ) {
            foreach ( $frmFilter as $key => $rs )
                $input[$key] = $rs;
        }
        $input['recycle'] = 0;
        $input['type'] = 'member';
        $info = $this->member_m->get_rows($input);
        $infoCount = $this->member_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->userId);
            $action = array();
            $action[1][] = table_edit(site_url("admin/{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;

            $course_ = $this->db
                        ->select('*')
                        ->from('course a')
                        ->where('active',1)
                        ->where('recycle',0)
                        ->get();
            $course= $course_->result();
            $c='<div class="col-12"><div class="row">';
            foreach ($course as $key2 => $value_) {
                $course_m = $this->db
                        ->select('*')
                        ->from('course_member a')
                        ->where('active',1)
                        ->where('recycle',0)
                        ->where('courseId',$value_->courseId)
                        ->where('userId',$rs->userId)
                        ->get()->num_rows();
                if($course_m > 0){
                    $active2 = "checked";
                    $act = 1;
                }else{
                    $active2 = "";
                    $act = 0;
                }
            	// $c.= '<div class="col-md-6 col-6 text-right">'.(iconv_substr($value_->title,0,20, "UTF-8")).' <input id="'.$value_->courseId.$rs->userId.'" value="'.$act.'" onclick="javascript:changeActive('.$value_->courseId.','.$rs->userId.')" class="toggle-demo" type="checkbox" '.$active2.' data-toggle="toggle" data-on="เปิด" data-off="ปิด" data-onstyle="success" data-offstyle="danger"></div>';
                // $c.= '<div class="col-md-6 col-6 text-right">'.(iconv_substr($value_->title,0,20, "UTF-8")).' '.toggle_active($active2, "admin/{$this->router->class}/update_course_member/{$rs->userId}/{$value_->courseId}/{$act}").'</div>';
                $c.= '<div class="col-md-6 col-12 "> <label class="chb">'.$value_->title.'
  <input id="'.$value_->courseId.$rs->userId.'" value="'.$act.'" onclick="javascript:changeActive('.$value_->courseId.','.$rs->userId.')" type="checkbox" '.$active2.'>
  <span class="checkmark"></span>
</label></div>';
               
            	if(($key2+1)%2 == 0){
            		$c.='';
            	}
            }
            $c.='</div></div>';
            
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['name'] = $rs->firstname." ".$rs->lastname.'<br>รหัส : '.$rs->couponCode;
            $column[$key]['username'] = $c;//$rs->username;
            $column[$key]['phone'] = $rs->phone;
            $column[$key]['email'] = $rs->email;
            $column[$key]['active'] = toggle_active($active, "admin/{$this->router->class}/action/active");
            $column[$key]['lastLogin'] = datetime_table($rs->lastLogin);
            $column[$key]['updateDate'] = datetime_table($rs->createDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function update_course_member($userId,$courseId,$action){
       
        $action == "true" ? 1 : 0;
        $this->db
                ->where('userId', $userId)
                ->where('courseId', $courseId)
                ->delete('course_member');
        if($action==1){

                $value_i['courseId']=$courseId;
                $value_i['userId']=$userId;
                $value_i['code']="";
                $value_i['active']=1;
                $value_i['recycle']=0;
                $value_i['createDate'] = db_datetime_now();

                $this->db->insert('course_member', $value_i);
               
        }
        $result = true ;
        if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
        } else {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }
        $data['success'] = $result;
        $data['toastr'] = $toastr;

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));  
    }

    public function change_active()
  {
    $courseId = $this->input->post('courseId');
    $userId = $this->input->post('userId');
    $manage_val = $this->input->post('manage_val');

    //print "<pre>";
    //print_r($manage_id);
   // exit();

     $this->db
                ->where('userId', $userId)
                ->where('courseId', $courseId)
                ->delete('course_member');
        if($manage_val==0){

                $value_i['courseId']=$courseId;
                $value_i['userId']=$userId;
                $value_i['code']="";
                $value_i['active']=1;
                $value_i['recycle']=0;
                $value_i['createDate'] = db_datetime_now();

                $this->db->insert('course_member', $value_i);
                $resp_msg = array(
                   "manage_val"=>1,
                   "manage_status"=>1
                );
        }else{
            $resp_msg = array(
                   "manage_val"=>0,
                   "manage_status"=>1
                );
        }
        
        
        echo json_encode($resp_msg);
         return false;
      

  }
    
    public function create() {
        $this->load->module('admin/admin');
        
        $data['grpContent'] = $this->_grpContent;
        $data['policyDD'] = Modules::run('admin/policy/dropdown');
        $data['frmAction'] = site_url("admin/{$this->router->class}/save");
        
        

        // breadcrumb
        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', base_url("admin/{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function save() {
        $input = $this->input->post();
        $value = $this->_build_data($input);
        $insertId = $this->member_m->insert($value);
        if ( $insertId ) {
            $value = $this->_build_upload_content($insertId, $input);
            Modules::run('admin/upload/update_content', $value);
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    public function edit($id="") {
        $this->load->module('admin/admin');
        
        $id = decode_id($id);
        $input['userId'] = $id;
        $input['recycle'] = 0;
        $info = $this->member_m->get_rows($input);
        if ( $info->num_rows() == 0 ){
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบข้อมูลที่ต้องการแก้ไข');
            redirect($this->agent->referrer());
        }
            
        $info = $info->row();
        $data['coverImage'] = Modules::run('admin/upload/get_upload_tmpl', $info->userId, $this->_grpContent, 'coverImage');

        $data['info'] = $info;

       // print"<pre>";print_r($data['info']);exit();
        $data['grpContent'] = $this->_grpContent;
        $data['policyDD'] = Modules::run('admin/policy/dropdown');

        

        $data['frmAction'] = site_url("admin/{$this->router->class}/update");
        $data['frmActionPassword'] = site_url("admin/{$this->router->class}/update_password");
        
        // breadcrumb
        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', base_url("admin/{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/edit";
        
        $this->admin->layout($data);
    }
    
    public function update() {
        $input = $this->input->post();
        $input['userId'] = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->member_m->update($input['userId'], $value);
        if ( $result ) {
            $value = $this->_build_upload_content($input['userId'], $input);
            Modules::run('admin/upload/update_content', $value);
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    public function update_password() {
        $input = $this->input->post();
        $input['userId'] = decode_id($input['id']);
        $value['password'] = $this->encryption->encrypt($input['newPassword']);
        $value['updateDate'] = db_datetime_now();
        $value['updateBy'] = $this->session->user['userId'];
        $result = $this->member_m->update($input['userId'], $value);
        if ( $result ) {
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    public function check_password() {
        $input = $this->input->post();
        $input['userId'] = decode_id($input['id']);
        $input['recycle'] = 0;
        $info = $this->member_m->get_rows($input);
        $row = $info->row();
        if ( $this->encryption->decrypt( $row->password ) == $input['oldPassword']  ) {
            $rs = TRUE;
        } else {
            $rs = FALSE;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($rs));
    }

    private function _build_data($input) {
        
        $value['policyId'] = $input['policy'];
        $value['email'] = $input['email'];
        $value['phone'] = $input['phone'];
        $value['firstname'] = $input['firstname'];
        $value['lastname'] = $input['lastname'];
        $value['sectionId'] = $input['sectionId'];
        $value['partyId'] = $input['partyId'];
        $value['positionId'] = $input['positionId'];
        $value['degree'] = $input['degree'];
        if ( $input['mode'] == 'create' ) {
            $value['username'] = $input['username'];
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->user['userId'];
            $value['password'] = $this->encryption->encrypt($input['password']);
            $value['type'] = "member";
            $value['verify'] = 1;
        } else {
            $value['updateDate'] = db_datetime_now();
            $value['updateBy'] = $this->session->user['userId'];
        }
        return $value;
    }
    
    private function _build_upload_content($id, $input) {
        $value = array();
        if ( isset($input['coverImageId']) ) {
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId']
            );
        } else {
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => null,
            );
        }
        return $value;
    }
    
    public function trash() {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_list_view(base_url("admin/{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", base_url("admin/{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/trash";
        
        $this->admin->layout($data);
    }

    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $input['type'] = 'member';
        $info = $this->member_m->get_rows($input);
        $infoCount = $this->member_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->userId);
            $action = array();
            $action = table_restore("admin/{$this->router->class}/action/restore");
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['name'] = $rs->firstname . ' ' . $rs->lastname;
            $column[$key]['email'] = $rs->email;
            $column[$key]['lastLogin'] = datetime_table($rs->lastLogin);
            $column[$key]['updateDate'] = datetime_table($rs->updateDate);
            $column[$key]['action'] = $action;
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }    
    
    public function action($type=""){
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขออภัยคุณไม่ได้รับสิทธินี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ($input['id'] as &$rs) $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->member_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $result = $this->member_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->member_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->member_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    
    public function check_email()
    {
        $input = $this->input->post();
        $input['recycle'] = array(0,1);
        $info = $this->member_m->get_rows($input);
//        arrx($info);
        if ( $info->num_rows() > 0 ) {
            if ($input['mode'] == 'create') {
                $rs = FALSE;
            } else {
                $row = $info->row();
                if ($row->userId == decode_id($input['id'])) {
                    $rs = TRUE;
                } else {
                    $rs = FALSE;
                }
            }
        } else {
            $rs =  TRUE;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($rs));
    }
    
}
