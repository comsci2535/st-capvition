<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Coupon extends MX_Controller {

    private $_title = "คูปองส่วนลด";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับคูปองส่วนลด";
    private $_grpContent = "coupon";
    private $_requiredExport = true;
    private $_permission;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("coupon_m");
    }
    
    public function index() {
        $this->load->module('admin/admin');
        
        // toobar
       
        $action[1][] = action_refresh(site_url("admin/{$this->router->class}"));
        $action[1][] = action_filter();
        $action[2][] = action_add(site_url("admin/{$this->router->class}/create"));
      
        $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(site_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";
        
        $this->admin->layout($data);
    }    

    public function data_index() {
        $input = $this->input->post();
        parse_str($_POST['frmFilter'], $frmFilter);
        if ( !empty($frmFilter) ) {
            foreach ( $frmFilter as $key => $rs )
                $input[$key] = $rs;
        }
        $input['recycle'] = 0;
        $info = $this->coupon_m->get_rows($input);
        $infoCount = $this->coupon_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->couponId);
            $action = array();
            $action[1][] = table_edit(site_url("admin/{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->couponCode;
            $column[$key]['excerpt'] = $rs->title;
            $column[$key]['active'] = toggle_active($active, "admin/{$this->router->class}/action/active");
            $column[$key]['createDate'] = datetime_table($rs->createDate);
            $column[$key]['updateDate'] = datetime_table($rs->updateDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create() {
        $this->load->module('admin/admin');

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/save");

        $chars = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < 7; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }

        $data['couponCode']=$res;
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("admin/{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function save() {
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->coupon_m->insert($value);
        if ( $result ) {
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    public function edit($id="") {
        $this->load->module('admin/admin');
        
        $id = decode_id($id);
        $input['couponId'] = $id;
        $info = $this->coupon_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/update");

         $data['dateRang'] ="";
        if($info->startDate!="0000-00-00" && $info->endDate!="0000-00-00"){
            $data['dateRang'] = date('d-m-Y', strtotime($info->startDate)).' ถึง '.date('d-m-Y', strtotime($info->endDate));
        }
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("admin/{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function update() {
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->coupon_m->update($id, $value);
        if ( $result ) {
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    private function _build_data($input) {
        
        $value['title'] = $input['title'];//str_replace(","," ",$input['title']);
        $value['excerpt'] = $input['excerpt'];
        $value['detail'] = $input['detail'];
        $value['type'] = $input['type'];
        $value['startDate'] = $input['startDate'];
        $value['endDate'] = $input['endDate'];

        
        $value['isCourse'] = $input['isCourse'];
        $value['isCourseNum'] = $input['isCourseNum'];
        $value['isMember'] = $input['isMember'];
        $value['isMemberNum'] = $input['isMemberNum'];

        $value['discount'] = str_replace(",","",$input['discount']);

        if ( $input['mode'] == 'create' ) {
            $value['couponCode'] = $input['couponCode'];
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->user['userId'];
        } else {
            $value['updateDate'] = db_datetime_now();
            $value['updateBy'] = $this->session->user['userId'];
        }
        return $value;
    }
    
    private function _build_upload_content($id, $input) {
        $value = array();
        if ( isset($input['coverImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId'],
                'title' => $input['coverImageTitle'],
            );
        if ( isset($input['contentImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'contentImage',
                'uploadId' => $input['contentImageId'],
                'title' => $input['contentImageTitle'],
            );
        if ( isset($input['galleryImageId']) ) {
            foreach ( $input['galleryImageId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'galleryImage',
                    'uploadId' => $rs,
                    'title' => $input['galleryImageTitle'][$key]
                );
            }
        }
        if ( isset($input['docAttachId']) ) {
            foreach ( $input['docAttachId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'docAttach',
                    'uploadId' => $rs,
                    'title' => $input['docAttachTitle'][$key]
                );
            }
        }
        return $value;
    }    
    
    
    
    public function trash() {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_list_view(site_url("admin/{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("admin/{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/trash";
        
        $this->admin->layout($data);
    }

    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->coupon_m->get_rows($input);
        $infoCount = $this->coupon_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->couponId);
            $action = array();
            $action[1][] = table_restore("admin/{$this->router->class}/action/restore");         
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->couponCode;
            $column[$key]['excerpt'] = $rs->title;
            $column[$key]['recycleDate'] = datetime_table($rs->recycleDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }    
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->coupon_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycleDate'] = $dateTime;
                $value['recycleBy'] = $this->session->user['userId'];
                $result = $this->coupon_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->coupon_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->coupon_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  
    
}
