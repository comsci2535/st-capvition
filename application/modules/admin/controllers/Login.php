<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("login_m");
        $this->load->library('encryption');
    }

    public function index() {
        $this->load->view('login/index');
    }

    public function check() {
        $input = $this->input->post();
        $info = $this->login_m->get_by_username($input);
        if (!empty($info)) {
            if ($info['verify'] == 1 || $info['type'] == 'developer') {
                if ($info['active'] == 1 && $info['policyActive'] == 1 || $info['type'] == 'developer') {
                    $pass = $this->encryption->decrypt($info['password']);
                   // arr($info);//exit();
                    if ( $input['password'] == $pass ) {
                        $image = base_url('uploads/user.png');
                        $upload = Modules::run('admin/upload/get_upload', $info['userId'], 'user', 'coverImage');
                        if ( $upload->num_rows() != 0 ) {
                            $row = $upload->row();
                            if ( is_file("{$row->path}/{$row->filename}") )
                                $image = base_url("{$row->path}{$row->filename}");
                        }
                        $data['error'] = false;
                        $data['message'] = site_url("admin");
                        $user['userId'] = $info['userId'];
                        $user['name'] = $info['firstname']." ".$info['lastname'];
                        $user['image'] = $image;
                        $user['email'] = $info['email'];
                        $user['type'] = $info['type'];
                        $user['policyId'] = $info['policyId'];
                        $user['isBackend'] = TRUE;
                        $this->session->set_userdata('user', $user);
                        $this->session->set_flashdata('firstTime', '1');
                        $this->login_m->update_last_login();
                    } else {
                        $data['error'] = true;
                        $data['message'] = "รหัสผ่านไม่ถูกต้อง";
                    }
                } else {
                    $data['error'] = true;
                    $data['message'] = "บัญชีของท่านถูกระงับการใช้งาน";
                }
            } else {
                $data['error'] = true;
                $data['message'] = 'กรุณายืนยันตัวตนอีเมล์ <a href="">ส่งอีกครั้ง</a>';
            }
        } else {
            $data['error'] = true;
            $data['message'] = 'ไม่พบบัญขีของท่านกรุณาลงทะเบียน';
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

}
