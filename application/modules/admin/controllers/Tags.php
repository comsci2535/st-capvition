<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Tags extends MX_Controller {

    private $_title = "จัดการ Tags";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับ Tags";
    private $_grpContent = "tags";
    private $_requiredExport = true;
    private $_permission;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("tags_m");
    }
    
    public function index() {
        $this->load->module('admin/admin');
        
        // toobar
        $export = array(
            'excel' => site_url("admin/{$this->router->class}/excel"),
            'pdf' => site_url("admin/{$this->router->class}/pdf"),
        );
        $action[1][] = action_refresh(site_url("admin/{$this->router->class}"));
        $action[1][] = action_filter();
        $action[2][] = action_add(site_url("admin/{$this->router->class}/create"));
        //$action[2][] = action_export_group($export);
        $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(site_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";
        
        $this->admin->layout($data);
    }    

    public function data_index() {
        $input = $this->input->post();
        parse_str($_POST['frmFilter'], $frmFilter);
        if ( !empty($frmFilter) ) {
            foreach ( $frmFilter as $key => $rs )
                $input[$key] = $rs;
        }
        $input['tagsRecycle'] = 0;
        $info = $this->tags_m->get_rows($input);
        $infoCount = $this->tags_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->tagsID);
            $action = array();
            $action[1][] = table_edit(site_url("admin/{$this->router->class}/edit/{$id}"));
            $active = $rs->tagsActive ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['tagsName'] = $rs->tagsName;
            $column[$key]['tagsExcerpt'] = $rs->tagsExcerpt;
            $column[$key]['active'] = toggle_active($active, "admin/{$this->router->class}/action/active");
            $column[$key]['tagsDateCreate'] = datetime_table($rs->tagsDateCreate);
            $column[$key]['tagsDateUpdate'] = datetime_table($rs->tagsDateUpdate);
            $column[$key]['action'] = Modules::run('admin/utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create() {
        $this->load->module('admin/admin');

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/save");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("admin/{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function save() {
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->tags_m->insert($value);
        if ( $result ) {
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    public function edit($id="") {
        $this->load->module('admin/admin');
        
        $id = decode_id($id);
        $input['tagsID'] = $id;
        $info = $this->tags_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("admin/{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function update() {
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->tags_m->update($id, $value);
        if ( $result ) {
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    private function _build_data($input) {
        
        $value['tagsName'] =  $this->clean($input['tagsName']);
        $value['tagsExcerpt'] = $input['tagsExcerpt'];
        $value['tagsType'] = 'article';

        if($input['metaTitle']!=""){
            $value['metaTitle'] = $input['metaTitle'];
        }else{
             $value['metaTitle'] = str_replace(","," ",$input['tagsName']);
        }
        if($input['metaDescription']!=""){
            $value['metaDescription'] = $input['metaDescription'];
        }else{
            $value['metaDescription'] = $input['tagsExcerpt'];
        }
        if($input['metaKeyword']!=""){
            $value['metaKeyword'] = $input['metaKeyword'];
        }else{
             $value['metaKeyword'] = str_replace(","," ",$input['tagsName']);
        }
        
        if ( $input['mode'] == 'create' ) {
            $value['tagsDateCreate'] = db_datetime_now();
            $value['tagsAuthor'] = $this->session->user['userId'];
        } else {
            $value['tagsDateUpdate'] = db_datetime_now();
            $value['tagsEditor'] = $this->session->user['userId'];
        }
        return $value;
    }

     function clean($string) {
       $string = str_replace(',', ' ', $string); // Replaces all spaces with hyphens.
       $string = str_replace('%', ' ', $string); // Removes special chars.
       return str_replace('/[^a-z\d]/i', ' ', $string); // Replaces multiple hyphens with single one.
    }
    
    private function _build_upload_content($id, $input) {
        $value = array();
        if ( isset($input['coverImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId'],
                'title' => $input['coverImageTitle'],
            );
        if ( isset($input['contentImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'contentImage',
                'uploadId' => $input['contentImageId'],
                'title' => $input['contentImageTitle'],
            );
        if ( isset($input['galleryImageId']) ) {
            foreach ( $input['galleryImageId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'galleryImage',
                    'uploadId' => $rs,
                    'title' => $input['galleryImageTitle'][$key]
                );
            }
        }
        if ( isset($input['docAttachId']) ) {
            foreach ( $input['docAttachId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'docAttach',
                    'uploadId' => $rs,
                    'title' => $input['docAttachTitle'][$key]
                );
            }
        }
        return $value;
    }    
    
    
    
    public function trash() {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_list_view(site_url("admin/{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("admin/{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/trash";
        
        $this->admin->layout($data);
    }

    public function data_trash() {
        $input = $this->input->post();
        $input['tagsRecycle'] = 1;
        $info = $this->tags_m->get_rows($input);
        $infoCount = $this->tags_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->tagsID);
            $action = array();
            $action[1][] = table_restore("admin/{$this->router->class}/action/restore");         
            $active = $rs->tagsActive ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['tagsName'] = $rs->tagsName;
            $column[$key]['tagsExcerpt'] = $rs->tagsExcerpt;
            $column[$key]['tagsRecycleDate'] = datetime_table($rs->tagsRecycleDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }    
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['tagsDateUpdate'] = $dateTime;
            $value['tagsEditor'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['tagsActive'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->tags_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['tagsActive'] = 0;
                $value['tagsRecycle'] = 1;
                $value['tagsRecycleDate'] = $dateTime;
                $value['tagsRecycleDel'] = $this->session->user['userId'];
                $result = $this->tags_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['tagsActive'] = 0;
                $value['tagsRecycle'] = 0;
                $result = $this->tags_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['tagsActive'] = 0;
                $value['tagsRecycle'] = 2;
                $result = $this->tags_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  
    
}
