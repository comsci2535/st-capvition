/*
Navicat MySQL Data Transfer

Source Server         : MySQL Local
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : the_capvition_db

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2019-05-01 00:23:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `activityId` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `recommend` int(1) DEFAULT '0' COMMENT '0=ไม่แนะนำ,1=แนะนำ',
  `view` int(15) DEFAULT '0',
  `price` varchar(25) DEFAULT '',
  `promotion` varchar(25) DEFAULT '',
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  `title_link` varchar(250) DEFAULT '',
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `startTime` time DEFAULT NULL,
  `endTime` time DEFAULT NULL,
  `location` varchar(255) DEFAULT '',
  `title_gallery` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`activityId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity
-- ----------------------------
INSERT INTO `activity` VALUES ('1', null, 'Photoshop Super Promote', 'ไอเดียภาพให้หยุดนิ้วมือ แล้วสั่งซื้อทันที !!', 'ไอเดียภาพให้หยุดนิ้วมือแล้วสั่งซื้อทันที !!', '1', '0', '2018-10-03 11:52:14', '3', '2019-04-18 14:54:52', '3', '0', null, null, '1', '0', '9000', '4500', 'Photoshop Super Promote', 'ไอเดียภาพให้หยุดนิ้วมือแล้วสั่งซื้อทันที !!', 'Photoshop Super Promote', 'Photoshop Super Promote', '2019-04-18', '2019-04-30', '09:00:00', '17:00:00', 'โรงแรมพักพิง งามวงวาน', '');
INSERT INTO `activity` VALUES ('2', null, 'ทดสอบ', 'ทดสอบ', 'ทดสอบ', '1', '0', '2018-10-06 17:03:08', '22', '2018-10-11 15:31:02', '3', '0', null, null, '0', '0', '1900', '', 'ทดสอบ', 'ทดสอบ', 'ทดสอบ', 'ทดสอบ', '2018-10-10', '2018-10-11', '08:00:00', '17:30:00', 'โรงแรมพักพิง งามวงวาน', 'ทดสอบ');

-- ----------------------------
-- Table structure for bank
-- ----------------------------
DROP TABLE IF EXISTS `bank`;
CREATE TABLE `bank` (
  `bankId` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `branch` varchar(250) DEFAULT '',
  `accountNumber` varchar(150) DEFAULT '',
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`bankId`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bank
-- ----------------------------
INSERT INTO `bank` VALUES ('1', 'th', 'งานประชุมประจำปี2017', null, null, '0', '0', '2018-03-10 14:25:13', '0', '2018-07-03 04:27:00', '1', '2', '2018-04-25 16:59:31', '1', null, null);
INSERT INTO `bank` VALUES ('2', 'th', 'บริหารจัดการหมู่บ้านจัดสรรและอาคารชุด', null, null, '0', '0', '2018-03-09 20:57:07', '3', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('3', 'th', 'ดูแลสาธารณูปโภคของหมู่บ้านจัดสรรและอาคารชุด', null, null, '0', '0', '2018-03-09 21:16:50', '3', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('4', 'th', 'รับจดทะเบียนจัดตั้งนิติบุคคลหมู่บ้านจัดสรรและอาคารชุด', null, null, '0', '0', '2018-03-09 21:19:06', '3', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('5', 'th', 'test', null, null, '0', '0', '2018-03-09 21:23:54', '3', '2018-03-16 09:16:00', '1', '2', '2018-03-16 09:15:47', '1', null, null);
INSERT INTO `bank` VALUES ('6', 'th', 'test', null, null, '0', '0', '2018-03-09 21:24:16', '3', '2018-03-16 09:15:57', '1', '2', '2018-03-16 09:15:47', '1', null, null);
INSERT INTO `bank` VALUES ('7', 'th', 'test', null, null, '0', '0', '2018-03-09 21:37:20', '3', '2018-03-16 09:16:03', '1', '2', '2018-03-16 09:15:47', '1', null, null);
INSERT INTO `bank` VALUES ('8', 'th', 'โครงการที่ 1', null, null, '0', '0', '2018-03-10 11:39:19', '1', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('9', 'th', 'โครงการที่ 2', null, null, '0', '0', '2018-03-10 12:03:36', '1', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:28', '3', null, null);
INSERT INTO `bank` VALUES ('10', 'th', 'โครงการที่ 3', null, null, '0', '0', '2018-03-10 12:06:06', '1', '2018-10-21 20:17:36', '3', '2', '2018-10-21 19:54:10', '3', null, null);
INSERT INTO `bank` VALUES ('11', 'th', 'จัดตั้งบริษัท', null, null, '0', '0', '2018-03-10 14:04:01', '1', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('12', 'th', 'ให้บริการด้านจัดสวน', null, null, '0', '0', '2018-03-10 15:36:02', '1', '2018-10-21 20:17:36', '3', '2', '2018-10-21 19:54:10', '3', null, null);
INSERT INTO `bank` VALUES ('14', 'th', null, null, null, '0', '0', '2018-03-10 16:49:34', '1', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('15', 'th', null, null, null, '0', '0', '2018-03-10 20:27:49', '1', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('16', 'th', null, null, null, '0', '0', '2018-03-10 20:28:50', '1', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('17', 'th', 'ตกแต่งครัวแบบไทยๆ', null, null, '0', '0', '2018-03-11 15:17:28', '1', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('18', 'th', 'การจัดตั้งนิติบุคคลหมู่บ้านจัดสรร', '', '', '0', '0', '2018-03-11 15:26:16', '1', '2018-07-03 04:27:00', '1', '2', '2018-04-25 16:58:33', '1', null, null);
INSERT INTO `bank` VALUES ('19', null, 'test repo', 'test is test', 'test is test test is test', '0', '0', '2018-03-16 08:34:32', '1', '2018-03-23 11:21:35', '1', '2', '2018-03-23 10:41:36', '1', null, null);
INSERT INTO `bank` VALUES ('20', null, 'กสิกรไทย', '-', '868-2-08221-7', '1', '0', '2018-10-21 20:01:24', '3', '2018-10-21 20:15:12', '3', '0', null, null, 'ออมทรัพย์', 'นาย กรกฎ พรลักษณพิมล');
INSERT INTO `bank` VALUES ('21', null, 'กรุงเทพ', '-', '106-5-40644-7', '1', '0', '2018-10-21 20:16:01', '3', '2018-10-21 20:16:05', '3', '0', null, null, 'ออมทรัพย์', 'กรกฎ พรลักษณพิมล');
INSERT INTO `bank` VALUES ('22', null, 'กรุงศรีอยุธยา', '-', '494-1-10515-7', '1', '0', '2018-10-21 20:16:51', '3', '2018-10-21 20:16:54', '3', '0', null, null, 'ออมทรัพย์', 'กรกฎ พรลักษณพิมล');

-- ----------------------------
-- Table structure for bank_list
-- ----------------------------
DROP TABLE IF EXISTS `bank_list`;
CREATE TABLE `bank_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT '',
  `title` varchar(250) DEFAULT NULL,
  `imgFile` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bank_list
-- ----------------------------
INSERT INTO `bank_list` VALUES ('23', 'BKKBTHBK', 'ธนาคารกรุงเทพ', '002.gif');
INSERT INTO `bank_list` VALUES ('24', 'KASITHBK', 'ธนาคารกสิกรไทย', '004.gif');
INSERT INTO `bank_list` VALUES ('25', 'KRTHTHBK', 'ธนาคารกรุงไทย', '006.gif');
INSERT INTO `bank_list` VALUES ('26', 'TMBKTHB', 'ธนาคารทหารไทย', '011.gif');
INSERT INTO `bank_list` VALUES ('27', 'SICOTHBK', 'ธนาคารไทยพาณิชย์', '014.gif');
INSERT INTO `bank_list` VALUES ('28', 'AYUDTHBK', 'ธนาคารกรุงศรีอยุธยา', '025.gif');
INSERT INTO `bank_list` VALUES ('29', 'KIFITHB1', 'ธนาคารเกียรตินาคิน', '069.gif');
INSERT INTO `bank_list` VALUES ('30', 'UBOBTHBK', 'ธนาคารซีไอเอ็มบีไทย', '022.gif');
INSERT INTO `bank_list` VALUES ('31', 'TFPCTHB1', 'ธนาคารทิสโก้', '067Newlogotisco.jpg');
INSERT INTO `bank_list` VALUES ('32', 'THBKTHBK', 'ธนาคารธนชาต', '065.gif');
INSERT INTO `bank_list` VALUES ('33', 'UOVBTHBK', 'ธนาคารยูโอบี', 'Logo-UOB.jpg');
INSERT INTO `bank_list` VALUES ('34', 'SCBLTHBX', 'ธนาคารสแตนดาร์ดชาร์เตอร์ด (ไทย)', '020.gif');
INSERT INTO `bank_list` VALUES ('35', 'LAHRTHB1', 'ธนาคารแลนด์ แอนด์ เฮาส์', '073.GIF');
INSERT INTO `bank_list` VALUES ('36', 'ICBKTHBK', 'ธนาคารไอซีบีซี (ไทย)', '070.gif');
INSERT INTO `bank_list` VALUES ('37', 'BAABTHBK', 'ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร', 'Baac2.png');
INSERT INTO `bank_list` VALUES ('38', 'GSBATHBK', 'ธนาคารออมสิน', '030.png');
INSERT INTO `bank_list` VALUES ('39', 'TIBTTHBK', 'ธนาคารอิสลามแห่งประเทศไทย', '066.jpg');

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `bannerId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `link` varchar(255) DEFAULT '',
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `type` varchar(30) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `page` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`bannerId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO `banner` VALUES ('1', 'หน้าหลัก', 'หน้าหลัก', '', '0000-00-00', '0000-00-00', '1', 'hero', '2019-04-13 01:00:58', '3', '2019-04-13 01:01:01', '3', '0', null, null, 'home');
INSERT INTO `banner` VALUES ('2', 'tests 2', 'tests 2', '', '0000-00-00', '0000-00-00', '1', 'hero', '2019-04-13 01:43:52', '3', '2019-04-13 01:43:58', '3', '0', null, null, 'home');

-- ----------------------------
-- Table structure for banner_image
-- ----------------------------
DROP TABLE IF EXISTS `banner_image`;
CREATE TABLE `banner_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bannerId` int(11) DEFAULT NULL,
  `uploadId` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `titleBanner` varchar(250) DEFAULT '',
  `excerpt` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of banner_image
-- ----------------------------

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT '0',
  `active` tinyint(1) DEFAULT '1' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `type` varchar(50) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('14', null, '1', '1', 'news', '2018-07-03 07:06:14', '3', '2018-07-03 09:34:08', '3', '0', null, null);
INSERT INTO `category` VALUES ('15', '14', '0', '1', 'news', '2018-07-03 07:07:26', '3', '2018-07-03 07:08:13', '3', '1', '2018-07-03 07:08:13', '3');
INSERT INTO `category` VALUES ('16', '15', '0', '1', 'news', '2018-07-03 07:07:49', '3', '2018-07-03 07:08:13', '3', '1', '2018-07-03 07:08:13', '3');
INSERT INTO `category` VALUES ('17', null, '1', '0', 'media', '2018-07-06 10:26:55', '3', null, '0', '0', null, null);
INSERT INTO `category` VALUES ('18', null, '1', '0', 'media', '2018-07-06 10:27:31', '3', null, '0', '0', null, null);
INSERT INTO `category` VALUES ('19', null, '1', '0', 'news', '2018-07-11 06:20:24', '3', null, '0', '0', null, null);
INSERT INTO `category` VALUES ('20', null, '0', '0', 'article', '2018-08-04 16:44:29', '3', '2018-11-06 12:49:58', '3', '2', '2018-08-04 19:42:54', '3');
INSERT INTO `category` VALUES ('21', null, '0', '0', '', '2018-08-04 19:41:01', '3', '2018-08-04 19:42:50', '3', '1', '2018-08-04 19:42:50', '3');
INSERT INTO `category` VALUES ('22', null, '1', '0', 'article', '2018-08-04 21:25:09', '3', '2018-11-06 12:50:10', '3', '0', null, null);
INSERT INTO `category` VALUES ('23', null, '0', '0', 'article', '2018-08-04 23:47:15', '3', '2018-11-06 12:49:58', '3', '2', '2018-09-18 01:10:50', '3');
INSERT INTO `category` VALUES ('24', null, '1', '0', 'article', '2018-11-05 13:58:37', '3', '2018-11-06 12:59:28', '3', '0', null, null);
INSERT INTO `category` VALUES ('25', null, '0', '0', 'article', '2018-11-05 14:09:12', '3', '2018-11-06 12:49:58', '3', '2', '2018-11-05 14:09:20', '3');
INSERT INTO `category` VALUES ('26', null, '0', '0', 'article', '2018-11-05 14:30:56', '3', '2018-11-06 12:51:10', '3', '1', '2018-11-06 12:51:10', '3');

-- ----------------------------
-- Table structure for category_lang
-- ----------------------------
DROP TABLE IF EXISTS `category_lang`;
CREATE TABLE `category_lang` (
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(2) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `excerpt` text,
  `content` text,
  `nameLink` varchar(255) DEFAULT '',
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  PRIMARY KEY (`categoryId`,`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of category_lang
-- ----------------------------
INSERT INTO `category_lang` VALUES ('1', 'th', 'คณิตศาสตร์', 'คณิตศาสตร์ระดับชั้นประถม', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('2', 'th', 'บวกเลข', null, null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('3', 'th', 'พุทธศาสนา', 'พุทธศาสนาเพื่อชีวติประจำวัน', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('4', 'th', 'คูณเลข', null, null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('5', 'th', 'พระสูตร', 'ปปปปป', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('6', 'th', 'ธรรมดำ', '-', '-', '', null, null, null);
INSERT INTO `category_lang` VALUES ('7', 'en', 'ABC', 'ABC Excerpt', '', '', null, null, null);
INSERT INTO `category_lang` VALUES ('8', 'en', 'DEFG', '', '', '', null, null, null);
INSERT INTO `category_lang` VALUES ('9', 'en', 'HIJ', '', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('10', 'en', 'KLMN', '', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('11', 'th', 'วิธีคิดลัด', '', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('12', 'th', 'xxx', 'xx', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('13', 'th', 'aa', '', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('14', 'th', 'ข่าวประชาสัมพันธ์', 'ข่าวประชาสัมพันธ์', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('15', 'th', 'aa1', 'aa1', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('16', 'th', 'aaa1', '', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('17', 'th', 'หลักสูตรทั่วไป', 'หลักสูตรทั่วไป', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('18', 'th', 'หลักสูตรภาคบังคับ (วิชาชีพ/เฉพาะทาง)', 'หลักสูตรภาคบังคับ (วิชาชีพ/เฉพาะทาง)', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('19', 'th', 'ข่าวแนะนำ', '', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('20', 'th', 'ฟฟ', 'ฟฟ', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('21', 'th', 'aaaa2', 'aa2', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('22', 'th', 'บทความทั่วไป', 'บทความทั่วไป', null, 'บทความทั่วไป', 'บทความทั่วไป', 'บทความทั่วไป', 'บทความทั่วไป');
INSERT INTO `category_lang` VALUES ('23', 'th', 'บทความเกี่ยวกับบ้าน 2', 'บทความเกี่ยวกับบ้าน 2', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('24', 'th', 'บทความสอนสด', 'บทความสอนสด', null, 'บทความสอนสด', 'บทความสอนสด', 'บทความสอนสด', 'บทความสอนสด');
INSERT INTO `category_lang` VALUES ('25', 'th', 'บทความทั่วไป', '', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('26', 'th', 'บทความทั่วไป 2', '', null, 'บทความทั่วไป-2', null, null, null);

-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `id` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `data` longtext,
  PRIMARY KEY (`id`,`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('1p7ir4tl0q9tc256n87cu4721n3abtoi', '127.0.0.1', '1555088973', '__ci_last_regenerate|i:1555088973;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('2emnoochenm72e8fl7j24mucphi1vfd2', '127.0.0.1', '1555091190', '__ci_last_regenerate|i:1555091190;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('2s8ma6unjpq77835cdgj8laq7cvm5pcn', '127.0.0.1', '1555092344', '__ci_last_regenerate|i:1555092344;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:8:\"activity\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updateDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('50q54r0q6se2ff3h0aj62qj1o0sd23c3', '127.0.0.1', '1555092982', '__ci_last_regenerate|i:1555092982;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:8:\"activity\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updateDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('5q9nlk7n8nhfbnb5ej60smlam5dq2omc', '127.0.0.1', '1556641912', '__ci_last_regenerate|i:1556641632;FBRLH_state|s:32:\"7f39bb445022d55c2bac7b26aa85ddc8\";urlreffer|a:1:{s:3:\"url\";s:44:\"http://the-capvition.test:82/activity/index/\";}');
INSERT INTO `ci_sessions` VALUES ('60bf947mjv60b7qlcbes77hk3gjupp5p', '127.0.0.1', '1555090231', '__ci_last_regenerate|i:1555090231;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('749fbb83755km44ampqr16mke5jkk90b', '127.0.0.1', '1555094082', '__ci_last_regenerate|i:1555094082;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:6:\"course\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:5:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:6:\"manage\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('alavvjh17md5gaf1bikgmv3o0m1212qg', '127.0.0.1', '1555093435', '__ci_last_regenerate|i:1555093435;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:6:\"course\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:5:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:6:\"manage\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('bd0olcr0raubossh6mmb8v83i9adh5f2', '127.0.0.1', '1555575636', '__ci_last_regenerate|i:1555575636;FBRLH_state|s:32:\"278e46d33a5e5c26e9acd7e742677b70\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:6:\"course\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:5:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:6:\"manage\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('bmihbevqkn3bb63u8t44dg31mga05f5s', '127.0.0.1', '1555094939', '__ci_last_regenerate|i:1555094939;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:6:\"course\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:5:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:6:\"manage\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('c8pothf2grpch91tu973t0bdibsd1bhp', '127.0.0.1', '1555255426', '__ci_last_regenerate|i:1555255244;urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}FBRLH_state|s:32:\"fc304f16655556ff673e60e52bf2a1aa\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:6:\"course\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:5:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:6:\"manage\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('cbuhgf2pgaumpqsp0mcb8oaje2dfggaf', '127.0.0.1', '1555089602', '__ci_last_regenerate|i:1555089602;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}');
INSERT INTO `ci_sessions` VALUES ('f5cgghcfpk9cjlrprk519r5a2sd569qt', '127.0.0.1', '1555090563', '__ci_last_regenerate|i:1555090563;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('g2ve65uup0indtecmdve6g22ph7hesbb', '127.0.0.1', '1555092023', '__ci_last_regenerate|i:1555092023;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}urlreffer|a:1:{s:3:\"url\";s:43:\"http://the-capvition.test:82/article/index/\";}condition|a:1:{s:8:\"activity\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updateDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('hsktqk7n8d3eaej48i82l9d95iqe35it', '127.0.0.1', '1555089274', '__ci_last_regenerate|i:1555089274;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}');
INSERT INTO `ci_sessions` VALUES ('hsrcspo8r5e11p2h5731hjn0ss9sql99', '127.0.0.1', '1555574056', '__ci_last_regenerate|i:1555574056;FBRLH_state|s:32:\"278e46d33a5e5c26e9acd7e742677b70\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:10:\"order_list\";a:14:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:11:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:4:\"code\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"member\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:5:\"price\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:10:\"couponCode\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:8:\"discount\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:7;a:5:{s:4:\"data\";s:5:\"total\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:8;a:5:{s:4:\"data\";s:4:\"slip\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:9;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:10;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:2:\"10\";s:3:\"dir\";s:4:\"desc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:75:\"createDateRange=&createStartDate=&createEndDate=&courseId=&status=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:8:\"courseId\";s:0:\"\";s:6:\"status\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('jv9gqkc9qcppmtm1t2pop1e6616hc7jk', '127.0.0.1', '1555574919', '__ci_last_regenerate|i:1555574919;FBRLH_state|s:32:\"278e46d33a5e5c26e9acd7e742677b70\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:8:\"activity\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updateDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('nl082ddug0obqm27d48bsnc236mtl7uq', '127.0.0.1', '1555574378', '__ci_last_regenerate|i:1555574378;FBRLH_state|s:32:\"278e46d33a5e5c26e9acd7e742677b70\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:8:\"activity\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updateDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('oav86l99a7dagrd3jqoqni5ui5uiqkv4', '127.0.0.1', '1555091631', '__ci_last_regenerate|i:1555091631;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}urlreffer|a:1:{s:3:\"url\";s:43:\"http://the-capvition.test:82/article/index/\";}');
INSERT INTO `ci_sessions` VALUES ('qpsam6jcrut3i64gtfv3skkpb6rspdd8', '127.0.0.1', '1555092645', '__ci_last_regenerate|i:1555092645;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:8:\"activity\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updateDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('qqpqjbt68472hg4vfj90ugvv4vdcr477', '127.0.0.1', '1555094558', '__ci_last_regenerate|i:1555094558;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:6:\"course\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:5:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:6:\"manage\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('r9roeslsvdg52nkbo5odapfqga1602mp', '127.0.0.1', '1555089905', '__ci_last_regenerate|i:1555089905;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('rddkqckvh2l4nba57djm1h13hc8crnqh', '127.0.0.1', '1555090876', '__ci_last_regenerate|i:1555090876;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}urlreffer|a:1:{s:3:\"url\";s:43:\"http://the-capvition.test:82/article/index/\";}');
INSERT INTO `ci_sessions` VALUES ('s9phdklc5i3i1pr6vfulcq041ilt459a', '127.0.0.1', '1555575687', '__ci_last_regenerate|i:1555575636;FBRLH_state|s:32:\"278e46d33a5e5c26e9acd7e742677b70\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:6:\"course\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:5:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:6:\"manage\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('she76irpl9pju22pe7qk269kc72m3rd1', '127.0.0.1', '1555095154', '__ci_last_regenerate|i:1555094939;FBRLH_state|s:32:\"9dcfee9dd76519bd53eba5d283992996\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:6:\"course\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:5:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:6:\"manage\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');
INSERT INTO `ci_sessions` VALUES ('sqctptfmg5pigp0c725j3qotn02rlqsu', '127.0.0.1', '1555573514', '__ci_last_regenerate|i:1555573514;urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}FBRLH_state|s:32:\"278e46d33a5e5c26e9acd7e742677b70\";');
INSERT INTO `ci_sessions` VALUES ('v60tdcp1l7p13sjp2aha8pid5nrsglc7', '127.0.0.1', '1555575335', '__ci_last_regenerate|i:1555575335;FBRLH_state|s:32:\"278e46d33a5e5c26e9acd7e742677b70\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:45:\"http://the-capvition.test:82/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:8:\"activity\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updateDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}urlreffer|a:1:{s:3:\"url\";s:29:\"http://the-capvition.test:82/\";}');

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `variable` varchar(50) NOT NULL DEFAULT '',
  `value` text,
  `lang` varchar(2) NOT NULL DEFAULT 'th',
  `description` varchar(250) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`variable`,`lang`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('content', 'บริษัทฯ ก่อตั้งโดยคณะผู้บริหารมืออาชีพที่มากด้วยประสบการณ์และความเชี่ยวชาญมาเป็นเวลานาน โดยมีวัตถุประสงค์เพื่อเสริมสร้างคุณภาพชีวิตที่ดีของผู้พักอาศัย และเสริมสร้างภาพลักษณ์ให้กับโครงการ และสร้างมูลค่าเพิ่มของสินทรัพย์ให้แก่เจ้าของ', 'th', null, '2018-03-15 08:23:05', 'about');
INSERT INTO `config` VALUES ('content', 'Chrn Property Management ถนนประชาอุทิศ ตำบลบางแม่นาง อำเภอบางใหญ่ จังหวัดนนทบุรี 11140\r\nโทรศัพท์: 02-225-3654 โทรสาร: 02-225-3654\r\nอีเมล์: admin@admin.com\r\n', 'th', null, '2018-03-15 09:03:35', 'contact');
INSERT INTO `config` VALUES ('content', 'ข่าวสาร บทความ และกิจกรรมของบริษัทฯ', 'th', null, '2018-03-11 14:17:02', 'news');
INSERT INTO `config` VALUES ('content', 'บริหารงานโดยบุคลากรที่มีประสบการณ์ มีความรู้ความชำนาญและนักวิชาการให้คำแนะนำวางแผนและจัดระบบ จึงทำให้การทำงานมีประสิทธิภาพ  โดยคัดสรรบุคลากรที่มีคุณภาพ  รักงานบริการมีการอบรมบุคลากรทั้งภาคทฤษฏีและภาคปฏิบัติ  มีเจ้าหน้าที่ตรวจสอบ  การปฏิบัติงานจากส่วนกลางเข้าทำการตรวจสอบเพื่อตรวจสอบการปฏิบัติงานของพนักงาน ให้ได้คุณภาพมาตรฐาน  มีหน่วยปฏิบัติการเข้า พบลูกค้าเพื่อแก้ไขปัญหาต่าง ๆ ของระบบงาน', 'th', null, '2018-03-15 08:47:18', 'portfolio');
INSERT INTO `config` VALUES ('content', 'บริหารงานโดยบุคลากรที่มีประสบการณ์ มีความรู้ความชำนาญและนักวิชาการให้คำแนะนำวางแผนและจัดระบบ จึงทำให้การทำงานมีประสิทธิภาพ  โดยคัดสรรบุคลากรที่มีคุณภาพ  รักงานบริการมีการอบรมบุคลากรทั้งภาคทฤษฏีและภาคปฏิบัติ  มีเจ้าหน้าที่ตรวจสอบ  การปฏิบัติงานจากส่วนกลางเข้าทำการตรวจสอบเพื่อตรวจสอบการปฏิบัติงานของพนักงาน ให้ได้คุณภาพมาตรฐาน  มีหน่วยปฏิบัติการเข้า พบลูกค้าเพื่อแก้ไขปัญหาต่าง ๆ ของระบบงาน', 'th', null, '2018-03-15 09:18:38', 'service');
INSERT INTO `config` VALUES ('content', 'ส่วนหนึ่งของทีมงานบริหารที่มีประสบการณ์การทำงานที่เชี่ยวชาญ', 'th', null, '2018-03-10 16:42:36', 'team');
INSERT INTO `config` VALUES ('facebook', 'gorradesign', 'th', null, '2018-12-15 22:25:12', 'general');
INSERT INTO `config` VALUES ('idLine', '@gorradesign', 'th', null, '2018-12-15 22:25:12', 'general');
INSERT INTO `config` VALUES ('mailAuthenticate', '1', 'th', null, '2018-10-19 21:06:01', 'mail');
INSERT INTO `config` VALUES ('mailDefault', 'gorradesign.info@gmail.com', 'th', null, '2018-10-19 21:06:01', 'mail');
INSERT INTO `config` VALUES ('mailMethod', '1', 'th', null, '2018-10-19 21:06:01', 'mail');
INSERT INTO `config` VALUES ('metaDescription', 'Gorra Design', 'th', null, '2018-12-15 22:25:12', 'general');
INSERT INTO `config` VALUES ('metaKeyword', 'Gorra Design', 'th', null, '2018-12-15 22:25:12', 'general');
INSERT INTO `config` VALUES ('payment', '', 'th', null, '2018-12-15 22:25:12', 'general');
INSERT INTO `config` VALUES ('phoneContact', '(ทุกวัน 10:00 - 04:00 น.)', 'th', null, '2018-12-15 22:25:12', 'general');
INSERT INTO `config` VALUES ('phoneNumber', '0971456536', 'th', null, '2018-12-15 22:25:12', 'general');
INSERT INTO `config` VALUES ('senderEmail', 'gorradesign.info@gmail.com', 'th', null, '2018-10-19 21:06:01', 'mail');
INSERT INTO `config` VALUES ('senderName', 'The CAP Vision Academy', 'th', null, '2018-10-19 21:06:01', 'mail');
INSERT INTO `config` VALUES ('siteTitle', 'The CAP Vision Academy', 'th', null, '2018-12-15 22:25:12', 'general');
INSERT INTO `config` VALUES ('SMTPpassword', 'P@ssw0rdGorra', 'th', null, '2018-10-19 21:06:01', 'mail');
INSERT INTO `config` VALUES ('SMTPport', '465', 'th', null, '2018-10-19 21:06:01', 'mail');
INSERT INTO `config` VALUES ('SMTPserver', 'ssl://smtp.gmail.com', 'th', null, '2018-10-19 21:06:01', 'mail');
INSERT INTO `config` VALUES ('SMTPusername', 'gorradesign.info@gmail.com', 'th', null, '2018-10-19 21:06:01', 'mail');

-- ----------------------------
-- Table structure for contact
-- ----------------------------
DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `contactId` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(250) DEFAULT '',
  `lname` varchar(250) DEFAULT '',
  `email` varchar(150) DEFAULT '',
  `phone` varchar(25) DEFAULT NULL,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`contactId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of contact
-- ----------------------------

-- ----------------------------
-- Table structure for content
-- ----------------------------
DROP TABLE IF EXISTS `content`;
CREATE TABLE `content` (
  `contentId` int(11) NOT NULL AUTO_INCREMENT,
  `grpContent` varchar(200) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `period` varchar(250) DEFAULT NULL,
  `client` varchar(250) DEFAULT NULL,
  `service` varchar(250) DEFAULT NULL,
  `contact` varchar(250) DEFAULT NULL,
  `position` varchar(250) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `recommend` int(1) DEFAULT '0' COMMENT '0=ไม่แนะนำ,1=แนะนำ',
  `view` int(15) DEFAULT '0',
  `tagsId` varchar(255) DEFAULT '',
  `statusBuy` int(1) DEFAULT '0' COMMENT '0=ยังไม่ขาย,1=ขายแล้ว',
  `location` varchar(255) DEFAULT NULL,
  `price` int(25) DEFAULT NULL,
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  `title_link` varchar(250) DEFAULT '',
  PRIMARY KEY (`contentId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content
-- ----------------------------
INSERT INTO `content` VALUES ('1', 'article', null, 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi fermentum laoreet elit, sit amet tincidunt mauris ultrices vitae.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi fermentum laoreet elit, sit amet tincidunt mauris ultrices vitae.', null, null, null, null, null, '1', '0', '2019-04-18 14:47:14', '3', null, '0', '0', null, null, '22', '1', '3', '1', '0', null, null, 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi fermentum laoreet elit, sit amet tincidunt mauris ultrices vitae.', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet');

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
  `couponId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `detail` text,
  `type` varchar(30) DEFAULT '' COMMENT '1=ราคา,2=%',
  `discount` int(15) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `excerpt` text,
  `titleLink` varchar(255) DEFAULT NULL,
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  `couponCode` varchar(15) DEFAULT NULL,
  `isCourse` int(1) DEFAULT '0' COMMENT '0=ไม่จำกัดคอร์ส,1=จำกัดจำนวนคอร์ส',
  `isCourseNum` int(10) DEFAULT NULL COMMENT 'ระบุจำนวน',
  `isMember` int(1) DEFAULT '0' COMMENT '0=ไม่จำกัดจำนวนคน,1=จำกัดจำนวนคน',
  `isMemberNum` int(10) DEFAULT NULL COMMENT 'ระบุจำนวน',
  PRIMARY KEY (`couponId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coupon
-- ----------------------------

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `courseId` int(11) NOT NULL AUTO_INCREMENT,
  `instructorId` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `recommend` int(1) DEFAULT '0' COMMENT '0=ไม่แนะนำ,1=แนะนำ',
  `view` int(15) DEFAULT '0',
  `price` int(25) DEFAULT NULL,
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  `title_link` varchar(250) DEFAULT '',
  `learner` int(11) DEFAULT NULL,
  `recommendVideo` text,
  `receipts` text COMMENT 'สิ่งที่ได้รับ',
  PRIMARY KEY (`courseId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('1', '1', 'English Grammar', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis', 'Sed elementum lacus a risus luctus suscipit. Aenean sollicitudin sapien neque, in fermentum lorem dignissim a. Nullam eu mattis quam. Donec porttitor nunc a diam molestie blandit. Maecenas quis ultrices ex. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam eget vehicula lorem, vitae porta nisi. Ut vel quam erat. Ut vitae erat tincidunt, tristique mi ac, pharetra dolor. In et suscipit ex. Pellentesque aliquet velit tortor, eget placerat mi scelerisque a. Aliquam eu dui efficitur purus posuere viverra. Proin ut elit mollis, euismod diam et, fermentum enim.', '1', '0', '2019-04-13 01:27:41', '3', '2019-04-14 22:21:34', '3', '0', null, null, '1', '2', '1200', 'English Grammar', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis', 'English Grammar', 'English Grammar', '16', '8697260', 'test');

-- ----------------------------
-- Table structure for course_content
-- ----------------------------
DROP TABLE IF EXISTS `course_content`;
CREATE TABLE `course_content` (
  `course_contentId` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` int(11) DEFAULT NULL,
  `parentId` smallint(6) DEFAULT NULL,
  `type` int(11) DEFAULT '1' COMMENT '1menu, 2separate',
  `active` tinyint(1) DEFAULT '0' COMMENT '0Disabled, 1Enabled',
  `title` varchar(150) DEFAULT '',
  `order` int(10) DEFAULT '0',
  `param` varchar(150) DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateBy` int(11) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `modify` int(11) DEFAULT '0',
  `view` int(11) DEFAULT '0',
  `print` int(11) DEFAULT '0',
  `import` int(11) DEFAULT '0',
  `export` int(11) DEFAULT '0',
  `descript` text,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `videoLink` varchar(255) DEFAULT '',
  `videoLength` varchar(10) DEFAULT NULL,
  `fileTypeUploadType` int(1) DEFAULT '1' COMMENT '1=ผ่านเว็บ,2=url',
  `fileUrl` text,
  PRIMARY KEY (`course_contentId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of course_content
-- ----------------------------
INSERT INTO `course_content` VALUES ('1', '1', '0', '0', '1', 'บทที่ 1', '0', null, '3', '0', '2019-04-14 22:21:48', null, '0', '0', '0', '0', '0', '', '0', null, null, '', '', '1', '');
INSERT INTO `course_content` VALUES ('2', '1', '1', '0', '1', 'tests', '0', null, '3', '0', '2019-04-14 22:22:06', null, '0', '0', '0', '0', '0', 'test', '0', null, null, '8697260', '12:01 ', '1', '');
INSERT INTO `course_content` VALUES ('3', '1', '1', '1', '1', 'tests 2', '0', null, '3', '0', '2019-04-14 22:22:29', null, '0', '0', '0', '0', '0', 'test 2', '0', null, null, '76979871', '12:01 ', '1', '');

-- ----------------------------
-- Table structure for course_content_member
-- ----------------------------
DROP TABLE IF EXISTS `course_content_member`;
CREATE TABLE `course_content_member` (
  `course_contentId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `status` int(1) DEFAULT '0' COMMENT '0=ดูแล้วยังไม่จบ ,1=ดูจบแล้ว'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of course_content_member
-- ----------------------------
INSERT INTO `course_content_member` VALUES ('9', '23', '0', '2019-02-13 02:12:47', '1');

-- ----------------------------
-- Table structure for course_member
-- ----------------------------
DROP TABLE IF EXISTS `course_member`;
CREATE TABLE `course_member` (
  `course_memberId` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `code` varchar(150) DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT '1',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) unsigned DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) unsigned DEFAULT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`course_memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course_member
-- ----------------------------

-- ----------------------------
-- Table structure for course_payment
-- ----------------------------
DROP TABLE IF EXISTS `course_payment`;
CREATE TABLE `course_payment` (
  `course_paymentId` int(11) NOT NULL AUTO_INCREMENT,
  `course_registerId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `courseId` int(11) DEFAULT NULL,
  `code` varchar(15) DEFAULT '',
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `transferDdate` date DEFAULT NULL,
  `transferTime` time DEFAULT NULL,
  `amount` varchar(25) DEFAULT NULL,
  `note` text,
  `phone` varchar(25) DEFAULT NULL,
  `bankId` int(11) DEFAULT NULL,
  PRIMARY KEY (`course_paymentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course_payment
-- ----------------------------

-- ----------------------------
-- Table structure for course_register
-- ----------------------------
DROP TABLE IF EXISTS `course_register`;
CREATE TABLE `course_register` (
  `course_registerId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `courseId` int(11) NOT NULL,
  `promotionId` int(11) DEFAULT NULL,
  `price` varchar(15) DEFAULT '',
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '0' COMMENT '0=ยังไม่ชำระเงิน,1=ชำระเงินแล้ว',
  `code` varchar(150) DEFAULT NULL,
  `couponCode` varchar(15) DEFAULT '',
  `type` varchar(30) DEFAULT '' COMMENT '1=ราคา,2=%',
  `discount` int(15) DEFAULT NULL,
  `isPaypal` int(1) DEFAULT NULL,
  `codePaypal` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`course_registerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course_register
-- ----------------------------

-- ----------------------------
-- Table structure for instructor
-- ----------------------------
DROP TABLE IF EXISTS `instructor`;
CREATE TABLE `instructor` (
  `instructorId` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `email` varchar(250) DEFAULT '',
  `phoneNumber` varchar(250) DEFAULT '',
  `phoneContact` varchar(250) DEFAULT '',
  `idLine` varchar(250) DEFAULT '',
  `facebook` varchar(250) DEFAULT '',
  PRIMARY KEY (`instructorId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of instructor
-- ----------------------------
INSERT INTO `instructor` VALUES ('1', null, 'Sarah Parker', null, 'test', '1', '0', '2019-04-13 01:25:43', '3', '2019-04-13 01:25:46', '3', '0', null, null, 'SarahParker', 'xxxxx', 'xxxxx', 'SarahParker', 'SarahParker');

-- ----------------------------
-- Table structure for language
-- ----------------------------
DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `languageId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `default` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `order` int(3) NOT NULL DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  PRIMARY KEY (`languageId`),
  KEY `fk_language_idx` (`default`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of language
-- ----------------------------
INSERT INTO `language` VALUES ('1', 'ไทย', 'th', 'ไทย', null, '1', '1', '1', '2015-06-10 09:57:05', null, '2015-09-23 14:58:11', null);
INSERT INTO `language` VALUES ('2', 'อังกฤษ', 'en', 'English', null, '1', '0', '2', '2015-06-10 09:48:36', null, '2015-09-23 14:58:11', null);

-- ----------------------------
-- Table structure for module
-- ----------------------------
DROP TABLE IF EXISTS `module`;
CREATE TABLE `module` (
  `moduleId` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` smallint(6) DEFAULT NULL,
  `type` int(11) DEFAULT '1' COMMENT '1menu, 2separate',
  `active` tinyint(1) DEFAULT '0' COMMENT '0Disabled, 1Enabled',
  `title` varchar(150) DEFAULT NULL,
  `directory` varchar(200) DEFAULT NULL,
  `class` varchar(50) DEFAULT NULL,
  `order` int(10) DEFAULT '0',
  `method` varchar(50) DEFAULT NULL,
  `param` varchar(150) DEFAULT NULL,
  `isSidebar` int(11) DEFAULT '1' COMMENT '0:hide,1show',
  `isDev` tinyint(1) DEFAULT '0',
  `icon` varchar(50) DEFAULT 'fa fa-angle-double-right',
  `createBy` int(11) DEFAULT '0',
  `updateBy` int(11) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `modify` int(11) DEFAULT '0',
  `view` int(11) DEFAULT '0',
  `print` int(11) DEFAULT '0',
  `import` int(11) DEFAULT '0',
  `export` int(11) DEFAULT '0',
  `descript` text,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`moduleId`)
) ENGINE=InnoDB AUTO_INCREMENT=336 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of module
-- ----------------------------
INSERT INTO `module` VALUES ('1', '0', '1', '1', 'แผงควบคุม', 'admin', 'dashboard', '2', '', null, '1', '0', 'fa fa-dashboard', '0', '3', '0000-00-00 00:00:00', '2018-08-05 00:03:14', '0', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('2', '0', '1', '1', 'ตั้งค่า', 'admin', '', '16', '', '', '1', '0', 'fa fa-cogs', '0', '7', '0000-00-00 00:00:00', '2016-12-10 10:37:56', '0', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('8', '2', '1', '1', 'ผู้ใช้งาน', 'admin', 'user', '3', '', '', '1', '0', 'fa fa-user', '0', '1', '0000-00-00 00:00:00', '2018-03-23 16:14:13', '1', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('9', '2', '1', '1', 'พื้นฐาน', 'admin', 'config_general', '4', null, '', '1', '0', 'fa fa-circle-o', '0', '7', '0000-00-00 00:00:00', '2016-07-17 09:37:06', '1', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('10', '2', '1', '1', 'อีเมล์', 'admin', 'config_mail', '6', '', '', '1', '0', 'fa fa-circle-o', '0', '1', '0000-00-00 00:00:00', '2018-03-16 10:22:10', '1', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('20', '2', '1', '1', 'กลุ่มผู้ใช้งาน', 'admin', 'policy', '2', '', '', '1', '0', 'fa fa-users', '0', '1', '0000-00-00 00:00:00', '2018-03-23 16:14:02', '1', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('30', '0', '1', '1', 'โมดูล', 'admin', 'module', '19', '', '', '1', '1', 'fa fa-cog', '0', '7', '0000-00-00 00:00:00', '2016-07-16 13:04:45', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('166', '0', '1', '1', 'คลังไฟล์', 'admin', 'library', '18', '', '', '0', '0', 'fa fa-briefcase', '7', '1', '2016-07-23 08:54:42', '2018-03-23 14:48:52', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('193', '0', '1', '0', 'รายงาน', 'admin', '', '7', '', '', '1', '0', 'fa fa-bar-chart', '7', '3', '2017-01-19 13:06:36', '2018-09-23 22:48:27', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('195', '0', '1', '0', 'ประวัติการใช้งาน', 'admin', 'stat_admin', '14', '', '', '1', '0', 'fa fa-calendar-o', '7', '3', '2017-01-19 13:11:57', '2018-10-13 00:20:58', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('267', '0', '1', '0', 'Repo', 'admin', 'repo', '21', null, null, '1', '1', 'fa fa-circle-o', '1', '3', '2018-03-16 21:32:34', '2018-07-03 04:28:33', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับrepo', '0', null, null);
INSERT INTO `module` VALUES ('270', '0', '2', '1', 'รายการเมนู', 'admin', '', '1', null, null, '1', '0', '', '1', '3', '2018-03-23 13:17:03', '2018-08-05 00:07:24', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('271', '0', '2', '1', 'นักพัฒนา', 'admin', '', '17', null, null, '1', '1', '', '1', '1', '2018-03-23 16:15:05', '2018-03-23 16:15:16', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('272', '0', '1', '0', 'สำรองข้อมูล', 'admin', 'backup', '16', null, null, '1', '0', 'fa fa-database', '1', '3', '2018-03-23 17:12:35', '2018-07-03 04:29:21', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับสำรองข้อมูล', '0', null, null);
INSERT INTO `module` VALUES ('294', '0', '2', '1', 'สำหรับผู้ดูแลระบบ', 'admin', '', '14', null, null, '1', '0', '', '3', '0', '2018-07-03 03:19:39', null, '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('299', '0', '1', '1', 'แบนเนอร์', 'admin', 'banner', '3', null, null, '1', '0', 'fa fa-map-signs', '3', '3', '2018-07-05 08:08:59', '2018-08-04 22:07:27', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับป้ายโฆษณาหน้าแรก', '0', null, null);
INSERT INTO `module` VALUES ('304', '193', '1', '0', 'รายงานสรุปการเข้าชม', 'admin', 'report_website', '1', null, null, '1', '0', 'fa fa-bar-chart', '3', '3', '2018-07-15 20:09:02', '2018-09-23 22:48:29', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับรายงานสรุปการเข้าชม', '0', null, null);
INSERT INTO `module` VALUES ('305', '193', '1', '0', 'รายงานประวัติการเข้าชม', 'admin', 'report_history', '2', null, null, '1', '0', 'fa fa-line-chart', '3', '3', '2018-07-16 00:24:34', '2018-09-23 22:48:31', '0', '0', '0', '0', '0', 'รายงานประวัติการเข้าชม', '0', null, null);
INSERT INTO `module` VALUES ('307', '308', '1', '1', 'หมวดหมู่บทความ', 'admin', 'category', '1', 'type/article', null, '1', '0', 'fa fa-circle-o', '3', '3', '2018-08-04 16:36:05', '2018-11-05 13:56:03', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับหมวดหมู่บทความ', '0', '2018-08-04 16:46:46', '3');
INSERT INTO `module` VALUES ('308', '0', '1', '1', 'บทความ', '', '', '9', null, null, '1', '0', 'fa fa-newspaper-o', '3', '3', '2018-08-04 21:24:12', '2018-11-05 13:56:46', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับบทความ', '0', null, null);
INSERT INTO `module` VALUES ('309', '308', '1', '1', 'บทความ', 'admin', 'article', '2', null, null, '1', '0', 'fa fa-newspaper-o', '3', '3', '2018-08-04 21:26:39', '2018-09-22 20:47:31', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับรายการบทความ', '0', null, null);
INSERT INTO `module` VALUES ('310', '2', '1', '1', 'โซเชียล', 'admin', 'social', '5', null, null, '1', '0', '', '3', '3', '2018-08-04 23:01:17', '2018-10-06 23:09:56', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับโซเชียล', '0', null, null);
INSERT INTO `module` VALUES ('311', '0', '1', '0', 'จัดการข้อมูลบ้านมือสอง', 'admin', 'house', '5', null, null, '1', '0', 'fa fa-home', '3', '3', '2018-08-04 23:26:02', '2018-09-14 17:09:13', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับบ้าน', '0', null, null);
INSERT INTO `module` VALUES ('312', '0', '1', '0', 'จัดการข้อมูลบ้านเช่า', 'admin', 'rental', '6', null, null, '1', '0', 'fa fa-home', '3', '3', '2018-08-04 23:58:11', '2018-09-14 17:09:15', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับบ้านเช่า', '0', null, null);
INSERT INTO `module` VALUES ('313', '0', '1', '0', 'จัดการข้อมูลที่ดิน', 'admin', 'land', '7', null, null, '1', '0', 'fa fa-navicon', '3', '3', '2018-08-05 00:06:24', '2018-09-14 17:09:16', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับที่ดิน', '0', null, null);
INSERT INTO `module` VALUES ('314', '0', '1', '0', 'จัดการข้อมูลบ้านใหม่', 'admin', 'new_house', '4', null, null, '1', '0', 'fa fa-home', '3', '3', '2018-08-28 19:11:07', '2018-09-14 17:09:12', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับบ้านมือหนึ่ง', '0', null, null);
INSERT INTO `module` VALUES ('316', '0', '1', '0', 'จัดการข้อมูลโปรโมชั่น', 'admin', 'promotion', '4', null, null, '1', '0', 'fa fa-map-signs', '3', '3', '2018-08-29 10:37:33', '2018-09-23 22:47:59', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับโปรโมชั่น', '0', null, null);
INSERT INTO `module` VALUES ('317', '0', '1', '1', 'จัดการข้อมูลคอร์สเรียน', 'admin', '', '4', null, null, '1', '0', 'fa fa-book', '3', '3', '2018-09-25 19:48:15', '2018-10-05 02:20:28', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับข้อมูลคอร์สเรียน', '0', null, null);
INSERT INTO `module` VALUES ('318', '317', '1', '1', 'จัดการเนื้อหาคอร์สเรียน', 'admin', 'course_content', '3', null, null, '0', '0', 'fa fa-circle-o', '3', '0', '2018-09-26 23:25:25', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '0', null, null);
INSERT INTO `module` VALUES ('319', '317', '1', '1', 'คอร์สเรียน', 'admin', 'course', '2', null, null, '1', '0', 'fa fa-circle-o', '3', '3', '2018-10-01 13:25:55', '2018-10-03 14:45:42', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับคอร์สเรียน', '0', null, null);
INSERT INTO `module` VALUES ('320', '317', '1', '1', 'ผู้สอน', 'admin', 'instructor', '1', null, null, '1', '0', 'fa fa-circle-o', '3', '3', '2018-10-01 13:27:16', '2018-10-01 20:41:16', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับผู้สอน', '0', null, null);
INSERT INTO `module` VALUES ('321', '317', '1', '1', 'รีวิว', 'admin', 'reviews', '4', null, null, '0', '0', 'fa fa-circle-o', '3', '3', '2018-10-01 13:29:27', '2018-10-10 21:24:26', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับรีวิวคอร์สเรียน', '0', null, null);
INSERT INTO `module` VALUES ('322', '0', '1', '1', 'จัดการข้อมูลสมาชิก', 'admin', 'member', '11', null, null, '1', '0', 'fa fa-user', '3', '0', '2018-10-02 22:50:04', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับสมาชิก', '0', null, null);
INSERT INTO `module` VALUES ('323', '0', '1', '1', 'กิจกรรม', 'admin', 'activity', '8', null, null, '1', '0', 'fa fa-calendar', '3', '0', '2018-10-03 10:44:58', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกิจกรรม', '0', null, null);
INSERT INTO `module` VALUES ('324', '0', '1', '1', 'รายการสั่งซื้อ', 'admin', 'order_list', '6', null, null, '1', '0', 'fa fa-list', '3', '0', '2018-10-04 20:56:36', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับรายการสั่งซื้อคอร์สเรียน', '0', null, null);
INSERT INTO `module` VALUES ('325', '0', '1', '1', 'ติดต่อเรา', 'admin', 'contact', '12', null, null, '1', '0', 'fa fa-hand-paper-o', '3', '0', '2018-10-07 23:14:06', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับติดต่อเรา', '0', null, null);
INSERT INTO `module` VALUES ('326', '0', '1', '1', 'จัดการ Tags', 'admin', 'tags', '10', null, null, '1', '0', 'fa fa-tags', '3', '0', '2018-10-11 15:57:05', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ Tags', '0', null, null);
INSERT INTO `module` VALUES ('327', '0', '1', '1', 'คูปองส่วนลด', 'admin', 'coupon', '5', null, null, '1', '0', 'fa fa-circle-o', '3', '0', '2018-10-11 22:26:51', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับคูปองส่วนลด', '0', null, null);
INSERT INTO `module` VALUES ('328', '0', '1', '1', 'จัดการข้อมูล SEO', '', '', '15', null, null, '1', '0', 'fa fa-tags', '3', '0', '2018-10-13 02:40:47', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '0', null, null);
INSERT INTO `module` VALUES ('329', '328', '1', '1', 'SEO บทความ', 'admin', 'seo_article', '4', 'edit', null, '1', '0', 'fa fa-circle-o', '3', '0', '2018-10-13 02:41:21', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '0', null, null);
INSERT INTO `module` VALUES ('330', '0', '1', '1', 'จัดการ Video', '', 'https://gorradesign.com', '13', null, null, '1', '0', 'fa fa-circle-o', '3', '3', '2018-10-13 02:50:48', '2018-10-13 02:58:07', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '0', null, null);
INSERT INTO `module` VALUES ('331', '328', '1', '1', 'SEO หน้าหลักเว็บไซต์', 'admin', 'seo_home', '1', 'edit', null, '1', '0', 'fa fa-circle-o', '3', '0', '2018-10-13 03:30:01', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '0', null, null);
INSERT INTO `module` VALUES ('332', '328', '1', '1', 'SEO คอร์สเรียน', 'admin', 'seo_course', '2', 'edit', null, '1', '0', 'fa fa-circle-o', '3', '0', '2018-10-13 03:31:34', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '0', null, null);
INSERT INTO `module` VALUES ('333', '328', '1', '1', 'SEO กิจกรรม', 'admin', 'seo_activity', '3', 'edit', null, '1', '0', 'fa fa-circle-o', '3', '3', '2018-10-13 03:33:21', '2019-04-13 00:58:21', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ SEO กิจกรรม', '0', null, null);
INSERT INTO `module` VALUES ('334', '2', '1', '1', 'ธนาคาร', 'admin', 'bank', '1', null, null, '1', '0', 'fa fa-bank ', '3', '0', '2018-10-21 19:48:21', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับธนาคารโอนเงิน', '0', null, null);
INSERT INTO `module` VALUES ('335', '0', '1', '1', 'รายการถอนเงินสะสมสมาชิก', 'admin', 'withdraw_list', '7', null, null, '1', '0', 'fa fa-money', '3', '0', '2018-11-28 14:54:19', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับรายการถอนเงินสะสมสมาชิก', '0', null, null);

-- ----------------------------
-- Table structure for point_log
-- ----------------------------
DROP TABLE IF EXISTS `point_log`;
CREATE TABLE `point_log` (
  `point_logId` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) NOT NULL,
  `point` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `course_registerId` varchar(255) DEFAULT '',
  `courseId` int(11) DEFAULT NULL,
  `note` text NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `point_withdrawId` int(11) DEFAULT NULL,
  PRIMARY KEY (`point_logId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of point_log
-- ----------------------------

-- ----------------------------
-- Table structure for point_withdraw
-- ----------------------------
DROP TABLE IF EXISTS `point_withdraw`;
CREATE TABLE `point_withdraw` (
  `point_withdrawId` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) DEFAULT '',
  `userId` int(11) DEFAULT NULL,
  `bankCode` varchar(100) DEFAULT '',
  `bankName` varchar(250) DEFAULT '',
  `bankNo` varchar(150) DEFAULT '',
  `price` varchar(15) DEFAULT '',
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '0' COMMENT '0=ยังไม่ชำระเงิน,1=ชำระเงินแล้ว',
  `filePath` varchar(255) DEFAULT NULL,
  `fileBookbank` varchar(255) DEFAULT NULL,
  `fileEvidence` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`point_withdrawId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of point_withdraw
-- ----------------------------

-- ----------------------------
-- Table structure for policy
-- ----------------------------
DROP TABLE IF EXISTS `policy`;
CREATE TABLE `policy` (
  `policyId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `remark` text,
  `active` tinyint(1) unsigned DEFAULT '0',
  `policy` text,
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) unsigned DEFAULT NULL,
  `updateDate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updateBy` int(11) unsigned DEFAULT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `visible` int(11) DEFAULT '1',
  PRIMARY KEY (`policyId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of policy
-- ----------------------------
INSERT INTO `policy` VALUES ('1', 'ผู้ดูแลระบบ', 'บริหารจัดการระบบ', '1', '{\"270\":{\"1\":1,\"2\":0},\"1\":{\"1\":1,\"2\":1},\"299\":{\"1\":1,\"2\":1},\"320\":{\"1\":1,\"2\":1},\"319\":{\"1\":1,\"2\":1},\"321\":{\"1\":1,\"2\":1},\"324\":{\"1\":1,\"2\":1},\"323\":{\"1\":1,\"2\":1},\"309\":{\"1\":1,\"2\":1},\"322\":{\"1\":1,\"2\":1},\"294\":{\"1\":1,\"2\":0},\"20\":{\"1\":1,\"2\":1},\"8\":{\"1\":1,\"2\":1},\"9\":{\"1\":1,\"2\":1},\"310\":{\"1\":1,\"2\":1},\"10\":{\"1\":1,\"2\":1},\"195\":{\"1\":1,\"2\":1}}', '2016-07-13 10:55:21', null, '2018-10-05 02:19:05', '22', '0', null, null, '0');
INSERT INTO `policy` VALUES ('2', 'Exclusive', 'เข้าถึงรายงานได้ทุกรายงาน', '0', '{\"269\":{\"1\":1,\"2\":1},\"1\":{\"1\":1,\"2\":1},\"267\":{\"1\":1,\"2\":0}}', '2016-07-13 10:55:24', null, '2018-08-04 23:18:55', '3', '1', null, null, '1');
INSERT INTO `policy` VALUES ('4', 'นักศึกษาฝึกงาน', '', '0', null, '2017-01-03 08:23:17', '7', '2018-04-26 09:40:12', '1', '1', null, null, '1');
INSERT INTO `policy` VALUES ('5', 'Data Manager', 'บริหารจัดการข้อมูล', '0', '{\"1\":{\"1\":1,\"2\":1},\"270\":{\"1\":1,\"2\":0},\"299\":{\"1\":1,\"2\":1},\"186\":{\"1\":1,\"2\":1},\"185\":{\"1\":1,\"2\":1},\"301\":{\"1\":1,\"2\":1}}', '2018-01-19 06:17:41', '1', '2018-08-04 23:18:55', '3', '1', null, null, '1');
INSERT INTO `policy` VALUES ('6', 'test', 'test', '0', '{\"1\":{\"1\":1,\"2\":0},\"185\":{\"1\":1,\"2\":0},\"186\":{\"1\":1,\"2\":0},\"199\":{\"1\":1,\"2\":1},\"200\":{\"1\":1,\"2\":1}}', '2018-01-19 07:01:10', '1', '2018-04-25 16:53:04', '1', '2', null, null, '1');
INSERT INTO `policy` VALUES ('7', 'asdfa', 'adfadf', '0', '{\"1\":{\"1\":1,\"2\":1}}', '2018-01-19 07:01:32', '1', '2018-03-23 11:27:56', '1', '2', null, null, '1');
INSERT INTO `policy` VALUES ('8', 'adfadf', '', '0', '{\"201\":{\"1\":1,\"2\":1}}', '2018-01-19 07:03:59', '1', '2018-03-23 11:27:56', '1', '2', null, null, '1');
INSERT INTO `policy` VALUES ('9', 'aaaa', '', '0', '{\"30\":{\"1\":1,\"2\":1}}', '2018-01-19 07:05:14', '1', '2018-01-26 16:45:28', '3', '2', null, null, '1');
INSERT INTO `policy` VALUES ('10', 'aaaa', '', '0', '{\"30\":{\"1\":1,\"2\":1}}', '2018-01-19 07:05:45', '1', '2018-01-26 16:45:28', '3', '2', null, null, '1');
INSERT INTO `policy` VALUES ('11', 'aaaa', '', '0', '{\"30\":{\"1\":1,\"2\":1}}', '2018-01-19 07:06:49', '1', '2018-01-26 16:45:28', '3', '2', null, null, '1');
INSERT INTO `policy` VALUES ('12', 'aaaa', '', '0', '{\"30\":{\"1\":1,\"2\":1}}', '2018-01-19 07:07:25', '1', '2018-01-26 16:45:28', '3', '2', null, null, '1');
INSERT INTO `policy` VALUES ('13', 'aaaa', '', '0', '{\"30\":{\"1\":1,\"2\":1}}', '2018-01-19 07:07:33', '1', '2018-04-25 16:53:04', '1', '2', null, null, '1');
INSERT INTO `policy` VALUES ('14', 'aaaa', '', '0', '{\"30\":{\"1\":1,\"2\":1}}', '2018-01-19 07:07:43', '1', '2018-04-25 16:53:04', '1', '2', null, null, '1');
INSERT INTO `policy` VALUES ('15', 'Guest', 'ผู้ใช้ทั่วไปที่เข้าถึงรายงานที่กำหนดให้อ่านได้', '0', '{\"1\":{\"1\":1,\"2\":1}}', '2018-03-26 14:39:28', '1', '2018-08-04 23:18:55', '3', '1', null, null, '1');
INSERT INTO `policy` VALUES ('16', 'ผู้อัพเดทข้อมูลเว็บไซต์', 'บริหารจัดการข้อมูลเว็บไซต์', '1', '{\"270\":{\"1\":1,\"2\":0},\"1\":{\"1\":1,\"2\":1},\"299\":{\"1\":1,\"2\":1},\"311\":{\"1\":1,\"2\":1},\"312\":{\"1\":1,\"2\":1},\"313\":{\"1\":1,\"2\":1},\"307\":{\"1\":1,\"2\":1},\"309\":{\"1\":1,\"2\":1},\"304\":{\"1\":1,\"2\":1},\"305\":{\"1\":1,\"2\":1}}', '2018-08-05 00:20:00', '22', '2018-08-05 00:20:09', '22', '0', null, null, '1');

-- ----------------------------
-- Table structure for promotion
-- ----------------------------
DROP TABLE IF EXISTS `promotion`;
CREATE TABLE `promotion` (
  `promotionId` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `detail` text,
  `type` varchar(30) DEFAULT '' COMMENT '1=ปกติ,2=จ่ายเท่าไรก็ได้',
  `discount` int(15) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `excerpt` text,
  `titleLink` varchar(255) DEFAULT NULL,
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  PRIMARY KEY (`promotionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of promotion
-- ----------------------------

-- ----------------------------
-- Table structure for promotion_content
-- ----------------------------
DROP TABLE IF EXISTS `promotion_content`;
CREATE TABLE `promotion_content` (
  `promotionId` int(11) DEFAULT NULL,
  `courseId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of promotion_content
-- ----------------------------
INSERT INTO `promotion_content` VALUES ('4', '1');

-- ----------------------------
-- Table structure for repo
-- ----------------------------
DROP TABLE IF EXISTS `repo`;
CREATE TABLE `repo` (
  `repoId` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`repoId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of repo
-- ----------------------------
INSERT INTO `repo` VALUES ('1', 'th', 'งานประชุมประจำปี2017', null, null, '0', '0', '2018-03-10 14:25:13', '0', '2018-07-03 04:27:00', '1', '2', '2018-04-25 16:59:31', '1');
INSERT INTO `repo` VALUES ('2', 'th', 'บริหารจัดการหมู่บ้านจัดสรรและอาคารชุด', null, null, '1', '0', '2018-03-09 20:57:07', '3', '2018-03-19 09:03:36', '1', '0', null, null);
INSERT INTO `repo` VALUES ('3', 'th', 'ดูแลสาธารณูปโภคของหมู่บ้านจัดสรรและอาคารชุด', null, null, '1', '0', '2018-03-09 21:16:50', '3', '2018-03-23 21:29:19', '3', '0', null, null);
INSERT INTO `repo` VALUES ('4', 'th', 'รับจดทะเบียนจัดตั้งนิติบุคคลหมู่บ้านจัดสรรและอาคารชุด', null, null, '1', '0', '2018-03-09 21:19:06', '3', '2018-03-10 17:26:39', '1', '0', null, null);
INSERT INTO `repo` VALUES ('5', 'th', 'test', null, null, '0', '0', '2018-03-09 21:23:54', '3', '2018-03-16 09:16:00', '1', '2', '2018-03-16 09:15:47', '1');
INSERT INTO `repo` VALUES ('6', 'th', 'test', null, null, '0', '0', '2018-03-09 21:24:16', '3', '2018-03-16 09:15:57', '1', '2', '2018-03-16 09:15:47', '1');
INSERT INTO `repo` VALUES ('7', 'th', 'test', null, null, '0', '0', '2018-03-09 21:37:20', '3', '2018-03-16 09:16:03', '1', '2', '2018-03-16 09:15:47', '1');
INSERT INTO `repo` VALUES ('8', 'th', 'โครงการที่ 1', null, null, '1', '0', '2018-03-10 11:39:19', '1', '2018-03-11 09:09:58', '1', '0', null, null);
INSERT INTO `repo` VALUES ('9', 'th', 'โครงการที่ 2', null, null, '1', '0', '2018-03-10 12:03:36', '1', '2018-03-11 09:12:47', '1', '0', null, null);
INSERT INTO `repo` VALUES ('10', 'th', 'โครงการที่ 3', null, null, '1', '0', '2018-03-10 12:06:06', '1', '2018-03-11 09:06:32', '1', '0', null, null);
INSERT INTO `repo` VALUES ('11', 'th', 'จัดตั้งบริษัท', null, null, '1', '0', '2018-03-10 14:04:01', '1', '2018-03-22 09:58:00', '1', '0', null, null);
INSERT INTO `repo` VALUES ('12', 'th', 'ให้บริการด้านจัดสวน', null, null, '1', '0', '2018-03-10 15:36:02', '1', '2018-03-15 08:24:54', '1', '0', null, null);
INSERT INTO `repo` VALUES ('14', 'th', null, null, null, '1', '0', '2018-03-10 16:49:34', '1', '2018-03-10 17:06:45', '1', '0', null, null);
INSERT INTO `repo` VALUES ('15', 'th', null, null, null, '1', '0', '2018-03-10 20:27:49', '1', '2018-03-11 15:58:24', '1', '0', null, null);
INSERT INTO `repo` VALUES ('16', 'th', null, null, null, '1', '0', '2018-03-10 20:28:50', '1', '2018-03-10 20:29:34', '1', '0', null, null);
INSERT INTO `repo` VALUES ('17', 'th', 'ตกแต่งครัวแบบไทยๆ', null, null, '1', '0', '2018-03-11 15:17:28', '1', '2018-03-22 09:57:58', '1', '0', null, null);
INSERT INTO `repo` VALUES ('18', 'th', 'การจัดตั้งนิติบุคคลหมู่บ้านจัดสรร', '', '', '0', '0', '2018-03-11 15:26:16', '1', '2018-07-03 04:27:00', '1', '2', '2018-04-25 16:58:33', '1');
INSERT INTO `repo` VALUES ('19', null, 'test repo', 'test is test', 'test is test test is test', '0', '0', '2018-03-16 08:34:32', '1', '2018-03-23 11:21:35', '1', '2', '2018-03-23 10:41:36', '1');

-- ----------------------------
-- Table structure for reviews
-- ----------------------------
DROP TABLE IF EXISTS `reviews`;
CREATE TABLE `reviews` (
  `reviewsId` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '1' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `courseId` int(11) DEFAULT NULL,
  PRIMARY KEY (`reviewsId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of reviews
-- ----------------------------

-- ----------------------------
-- Table structure for reviews_stars
-- ----------------------------
DROP TABLE IF EXISTS `reviews_stars`;
CREATE TABLE `reviews_stars` (
  `courseId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `score` int(1) DEFAULT NULL,
  `comment` text,
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of reviews_stars
-- ----------------------------
INSERT INTO `reviews_stars` VALUES ('1', '23', '4', ' ', '2018-11-10 00:33:24', '2018-11-10 00:33:24');
INSERT INTO `reviews_stars` VALUES ('1', '26', '5', ' ', '2018-11-10 00:47:25', '2018-11-10 00:47:25');
INSERT INTO `reviews_stars` VALUES ('1', '33', '5', ' ', '2018-11-10 00:48:01', '2018-11-10 00:48:01');
INSERT INTO `reviews_stars` VALUES ('2', '33', '5', ' ', '2018-11-11 19:21:07', '2018-11-11 19:21:07');
INSERT INTO `reviews_stars` VALUES ('3', '23', '3', ' ', '2018-11-12 22:32:22', '2018-11-12 22:32:22');
INSERT INTO `reviews_stars` VALUES ('3', '33', '2', ' ', '2018-11-12 22:48:38', '2018-11-12 22:48:38');
INSERT INTO `reviews_stars` VALUES ('2', null, '4', 'k', '2018-11-17 09:48:31', '2018-11-17 09:48:31');
INSERT INTO `reviews_stars` VALUES ('2', '23', '2', ' ', '2018-11-17 11:59:01', '2018-11-17 11:59:01');

-- ----------------------------
-- Table structure for seo
-- ----------------------------
DROP TABLE IF EXISTS `seo`;
CREATE TABLE `seo` (
  `seoId` int(11) NOT NULL AUTO_INCREMENT,
  `grpContent` varchar(250) DEFAULT '',
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`seoId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of seo
-- ----------------------------
INSERT INTO `seo` VALUES ('1', 'seo_article', 'บทความ 1', 'บทความ 1', 'บทความ 1', '0', '0', '2018-10-13 03:25:36', '3', '2018-10-13 03:25:36', '3', '0', null, null);
INSERT INTO `seo` VALUES ('2', 'seo_promotin', 'โปร', 'โปร', 'โปร', '0', '0', '2018-09-17 22:16:37', '3', '2018-09-17 22:16:37', '3', '0', null, null);
INSERT INTO `seo` VALUES ('3', 'seo_new_house', 'a', 'a', 'a', '0', '0', '2018-09-17 22:21:54', '3', '2018-09-17 22:21:54', '3', '0', null, null);
INSERT INTO `seo` VALUES ('4', 'seo_house', 'aaa', 'aaaaa', 'aaaa', '0', '0', '2018-09-17 22:22:24', '3', '2018-09-17 22:22:24', '3', '0', null, null);
INSERT INTO `seo` VALUES ('5', 'seo_rental', 'gg', 'gg', 'gg', '0', '0', '2018-09-17 22:22:53', '3', '2018-09-17 22:22:53', '3', '0', null, null);
INSERT INTO `seo` VALUES ('6', 'seo_land', 'fsd', 'fasfa', 'fsadfa', '0', '0', '2018-09-17 22:23:19', '3', '2018-09-17 22:23:19', '3', '0', null, null);
INSERT INTO `seo` VALUES ('7', 'seo_promotion', 'โปรโมชั่น', 'โปรโมชั่น', 'โปรโมชั่น', '0', '0', '2018-09-18 00:09:45', '3', '2018-09-18 00:09:45', '3', '0', null, null);
INSERT INTO `seo` VALUES ('8', 'seo_home', 'aaaa', 'aaa', 'aaa', '0', '0', '2018-10-13 03:30:50', '3', '2018-10-13 03:30:50', '3', '0', null, null);
INSERT INTO `seo` VALUES ('9', 'seo_course', 'bbb', 'bb', 'bb', '0', '0', '2018-10-13 03:32:19', '3', '2018-10-13 03:32:19', '3', '0', null, null);
INSERT INTO `seo` VALUES ('10', 'seo_activity', 'สอนสด', 'สอนสด', 'สอนสด', '0', '0', '2018-10-13 03:34:06', '3', '2018-10-13 03:34:06', '3', '0', null, null);

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `tagsID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสแท็ก',
  `tagsName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tagsType` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ประเภทแท็ก video,sound,picture',
  `tagsActive` int(11) DEFAULT '1' COMMENT 'สถานะ 1เปิด,0ปิด',
  `tagsDateCreate` datetime DEFAULT NULL COMMENT 'วันที่สร้าง',
  `tagsDateUpdate` datetime DEFAULT NULL COMMENT 'วันที่แก้ไข',
  `tagsAuthor` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ผู้สร้าง',
  `tagsEditor` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ผู้แก้ไข',
  `tagsRecycle` int(11) DEFAULT '0' COMMENT 'สถานะการลบ 1ลบ,0ปกติ',
  `tagsRecycleDel` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ผู้ลบ',
  `tagsRecycleDate` datetime DEFAULT NULL COMMENT 'วันที่ลบ',
  `tagsParent` int(11) NOT NULL,
  `tagsLang` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ภาษา th,en',
  `tagsExcerpt` text COLLATE utf8_unicode_ci,
  `metaTitle` text CHARACTER SET utf8,
  `metaDescription` text CHARACTER SET utf8,
  `metaKeyword` text CHARACTER SET utf8,
  PRIMARY KEY (`tagsID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tags
-- ----------------------------
INSERT INTO `tags` VALUES ('1', 'ทดสอบบทความ', 'article', '1', '2018-09-22 21:05:45', '2018-10-11 16:27:12', '3', '3', '0', null, null, '0', '', 'ทดสอบบทความ', 'ทดสอบบทความ', 'ทดสอบบทความ', 'ทดสอบบทความ');
INSERT INTO `tags` VALUES ('2', 'อ.กอร่า', 'article', '1', '2018-09-22 23:00:41', '2018-10-11 16:55:09', '3', '3', '0', null, null, '0', '', 'อ.กอร่า', 'อ.กอร่า', 'อ.กอร่า', 'อ.กอร่า');
INSERT INTO `tags` VALUES ('3', 'aaaa', null, '0', '2018-10-11 16:22:23', '2018-10-11 16:26:47', '3', '3', '1', '3', '2018-10-11 16:26:47', '0', '', 'aaaa', 'aa', 'aaa', 'aaa');

-- ----------------------------
-- Table structure for track
-- ----------------------------
DROP TABLE IF EXISTS `track`;
CREATE TABLE `track` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `controller` varchar(100) DEFAULT NULL,
  `method` varchar(100) DEFAULT NULL,
  `contentId` int(11) DEFAULT NULL,
  `memberId` int(11) DEFAULT NULL,
  `isMobile` int(11) DEFAULT NULL,
  `referrer` varchar(255) DEFAULT NULL,
  `platform` varchar(100) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `sessionId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of track
-- ----------------------------
INSERT INTO `track` VALUES ('1', 'front', '2019-04-13 00:03:35', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', '1p7ir4tl0q9tc256n87cu4721n3abtoi');
INSERT INTO `track` VALUES ('2', 'front', '2019-04-13 00:03:56', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/login', 'Windows 10', '127.0.0.1', '/', '1p7ir4tl0q9tc256n87cu4721n3abtoi');
INSERT INTO `track` VALUES ('3', 'front', '2019-04-13 00:07:58', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/login', 'Windows 10', '127.0.0.1', '/', '1p7ir4tl0q9tc256n87cu4721n3abtoi');
INSERT INTO `track` VALUES ('4', 'front', '2019-04-13 00:08:01', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/login', 'Windows 10', '127.0.0.1', '/', '1p7ir4tl0q9tc256n87cu4721n3abtoi');
INSERT INTO `track` VALUES ('5', 'front', '2019-04-13 00:09:33', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/login', 'Windows 10', '127.0.0.1', '/', 'hsktqk7n8d3eaej48i82l9d95iqe35it');
INSERT INTO `track` VALUES ('6', 'front', '2019-04-13 00:09:35', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/login', 'Windows 10', '127.0.0.1', '/', 'hsktqk7n8d3eaej48i82l9d95iqe35it');
INSERT INTO `track` VALUES ('7', 'front', '2019-04-13 00:09:57', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/login', 'Windows 10', '127.0.0.1', '/', 'hsktqk7n8d3eaej48i82l9d95iqe35it');
INSERT INTO `track` VALUES ('8', 'front', '2019-04-13 00:20:02', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', 'r9roeslsvdg52nkbo5odapfqga1602mp');
INSERT INTO `track` VALUES ('9', 'front', '2019-04-13 00:20:26', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', 'r9roeslsvdg52nkbo5odapfqga1602mp');
INSERT INTO `track` VALUES ('10', 'front', '2019-04-13 00:20:41', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', 'r9roeslsvdg52nkbo5odapfqga1602mp');
INSERT INTO `track` VALUES ('11', 'front', '2019-04-13 00:22:05', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', 'r9roeslsvdg52nkbo5odapfqga1602mp');
INSERT INTO `track` VALUES ('12', 'front', '2019-04-13 00:22:43', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', 'r9roeslsvdg52nkbo5odapfqga1602mp');
INSERT INTO `track` VALUES ('13', 'front', '2019-04-13 00:25:05', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', '60bf947mjv60b7qlcbes77hk3gjupp5p');
INSERT INTO `track` VALUES ('14', 'front', '2019-04-13 00:25:07', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', '60bf947mjv60b7qlcbes77hk3gjupp5p');
INSERT INTO `track` VALUES ('15', 'front', '2019-04-13 00:26:15', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', '60bf947mjv60b7qlcbes77hk3gjupp5p');
INSERT INTO `track` VALUES ('16', 'front', '2019-04-13 00:27:23', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', '60bf947mjv60b7qlcbes77hk3gjupp5p');
INSERT INTO `track` VALUES ('17', 'front', '2019-04-13 00:27:50', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', '60bf947mjv60b7qlcbes77hk3gjupp5p');
INSERT INTO `track` VALUES ('18', 'front', '2019-04-13 00:30:32', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', 'f5cgghcfpk9cjlrprk519r5a2sd569qt');
INSERT INTO `track` VALUES ('19', 'front', '2019-04-13 00:30:54', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', 'f5cgghcfpk9cjlrprk519r5a2sd569qt');
INSERT INTO `track` VALUES ('20', 'front', '2019-04-13 00:32:26', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', 'f5cgghcfpk9cjlrprk519r5a2sd569qt');
INSERT INTO `track` VALUES ('21', 'front', '2019-04-13 00:33:22', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', 'f5cgghcfpk9cjlrprk519r5a2sd569qt');
INSERT INTO `track` VALUES ('22', 'front', '2019-04-13 00:34:19', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', 'f5cgghcfpk9cjlrprk519r5a2sd569qt');
INSERT INTO `track` VALUES ('23', 'front', '2019-04-13 00:35:02', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', 'f5cgghcfpk9cjlrprk519r5a2sd569qt');
INSERT INTO `track` VALUES ('24', 'front', '2019-04-13 00:36:03', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/banner/create', 'Windows 10', '127.0.0.1', '/', 'rddkqckvh2l4nba57djm1h13hc8crnqh');
INSERT INTO `track` VALUES ('25', 'front', '2019-04-13 00:36:57', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/', 'rddkqckvh2l4nba57djm1h13hc8crnqh');
INSERT INTO `track` VALUES ('26', 'front', '2019-04-13 00:36:59', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article', 'rddkqckvh2l4nba57djm1h13hc8crnqh');
INSERT INTO `track` VALUES ('27', 'front', '2019-04-13 00:41:19', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/course', 'Windows 10', '127.0.0.1', '/article', '2emnoochenm72e8fl7j24mucphi1vfd2');
INSERT INTO `track` VALUES ('28', 'front', '2019-04-13 00:41:24', 'activity', 'index', '0', null, '0', 'http://the-capvition.test:82/course', 'Windows 10', '127.0.0.1', '/activity', '2emnoochenm72e8fl7j24mucphi1vfd2');
INSERT INTO `track` VALUES ('29', 'front', '2019-04-13 00:41:34', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/contact', 'Windows 10', '127.0.0.1', '/', '2emnoochenm72e8fl7j24mucphi1vfd2');
INSERT INTO `track` VALUES ('30', 'front', '2019-04-13 00:42:48', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/contact', 'Windows 10', '127.0.0.1', '/', '2emnoochenm72e8fl7j24mucphi1vfd2');
INSERT INTO `track` VALUES ('31', 'front', '2019-04-13 00:44:42', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/contact', 'Windows 10', '127.0.0.1', '/', '2emnoochenm72e8fl7j24mucphi1vfd2');
INSERT INTO `track` VALUES ('32', 'front', '2019-04-13 00:45:32', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/contact', 'Windows 10', '127.0.0.1', '/', '2emnoochenm72e8fl7j24mucphi1vfd2');
INSERT INTO `track` VALUES ('33', 'front', '2019-04-13 00:45:54', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/contact', 'Windows 10', '127.0.0.1', '/', '2emnoochenm72e8fl7j24mucphi1vfd2');
INSERT INTO `track` VALUES ('34', 'front', '2019-04-13 00:46:01', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/contact', 'Windows 10', '127.0.0.1', '/', '2emnoochenm72e8fl7j24mucphi1vfd2');
INSERT INTO `track` VALUES ('35', 'front', '2019-04-13 00:46:04', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/contact', 'Windows 10', '127.0.0.1', '/', '2emnoochenm72e8fl7j24mucphi1vfd2');
INSERT INTO `track` VALUES ('36', 'front', '2019-04-13 00:46:07', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article', '2emnoochenm72e8fl7j24mucphi1vfd2');
INSERT INTO `track` VALUES ('37', 'front', '2019-04-13 00:46:10', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/article', '2emnoochenm72e8fl7j24mucphi1vfd2');
INSERT INTO `track` VALUES ('38', 'front', '2019-04-13 00:46:13', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', '2emnoochenm72e8fl7j24mucphi1vfd2');
INSERT INTO `track` VALUES ('39', 'front', '2019-04-13 00:46:30', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article', 'oav86l99a7dagrd3jqoqni5ui5uiqkv4');
INSERT INTO `track` VALUES ('40', 'front', '2019-04-13 00:46:35', 'activity', 'index', '0', null, '0', 'http://the-capvition.test:82/course', 'Windows 10', '127.0.0.1', '/activity', 'oav86l99a7dagrd3jqoqni5ui5uiqkv4');
INSERT INTO `track` VALUES ('41', 'front', '2019-04-13 00:46:40', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/contact', 'Windows 10', '127.0.0.1', '/', 'oav86l99a7dagrd3jqoqni5ui5uiqkv4');
INSERT INTO `track` VALUES ('42', 'front', '2019-04-13 00:47:25', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article', 'oav86l99a7dagrd3jqoqni5ui5uiqkv4');
INSERT INTO `track` VALUES ('43', 'front', '2019-04-13 01:04:20', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', '2s8ma6unjpq77835cdgj8laq7cvm5pcn');
INSERT INTO `track` VALUES ('44', 'front', '2019-04-13 01:04:38', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', '2s8ma6unjpq77835cdgj8laq7cvm5pcn');
INSERT INTO `track` VALUES ('45', 'front', '2019-04-13 01:05:09', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', '2s8ma6unjpq77835cdgj8laq7cvm5pcn');
INSERT INTO `track` VALUES ('46', 'front', '2019-04-13 01:05:12', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', '2s8ma6unjpq77835cdgj8laq7cvm5pcn');
INSERT INTO `track` VALUES ('47', 'front', '2019-04-13 01:05:20', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', '2s8ma6unjpq77835cdgj8laq7cvm5pcn');
INSERT INTO `track` VALUES ('48', 'front', '2019-04-13 01:05:51', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'qpsam6jcrut3i64gtfv3skkpb6rspdd8');
INSERT INTO `track` VALUES ('49', 'front', '2019-04-13 01:06:33', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'qpsam6jcrut3i64gtfv3skkpb6rspdd8');
INSERT INTO `track` VALUES ('50', 'front', '2019-04-13 01:06:46', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'qpsam6jcrut3i64gtfv3skkpb6rspdd8');
INSERT INTO `track` VALUES ('51', 'front', '2019-04-13 01:07:50', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'qpsam6jcrut3i64gtfv3skkpb6rspdd8');
INSERT INTO `track` VALUES ('52', 'front', '2019-04-13 01:09:13', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'qpsam6jcrut3i64gtfv3skkpb6rspdd8');
INSERT INTO `track` VALUES ('53', 'front', '2019-04-13 01:09:23', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'qpsam6jcrut3i64gtfv3skkpb6rspdd8');
INSERT INTO `track` VALUES ('54', 'front', '2019-04-13 01:10:08', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'qpsam6jcrut3i64gtfv3skkpb6rspdd8');
INSERT INTO `track` VALUES ('55', 'front', '2019-04-13 01:10:18', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'qpsam6jcrut3i64gtfv3skkpb6rspdd8');
INSERT INTO `track` VALUES ('56', 'front', '2019-04-13 01:10:45', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', '50q54r0q6se2ff3h0aj62qj1o0sd23c3');
INSERT INTO `track` VALUES ('57', 'front', '2019-04-13 01:10:54', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', '50q54r0q6se2ff3h0aj62qj1o0sd23c3');
INSERT INTO `track` VALUES ('58', 'front', '2019-04-13 01:11:13', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', '50q54r0q6se2ff3h0aj62qj1o0sd23c3');
INSERT INTO `track` VALUES ('59', 'front', '2019-04-13 01:11:29', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', '50q54r0q6se2ff3h0aj62qj1o0sd23c3');
INSERT INTO `track` VALUES ('60', 'front', '2019-04-13 01:12:09', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', '50q54r0q6se2ff3h0aj62qj1o0sd23c3');
INSERT INTO `track` VALUES ('61', 'front', '2019-04-13 01:12:20', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', '50q54r0q6se2ff3h0aj62qj1o0sd23c3');
INSERT INTO `track` VALUES ('62', 'front', '2019-04-13 01:16:22', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'alavvjh17md5gaf1bikgmv3o0m1212qg');
INSERT INTO `track` VALUES ('63', 'front', '2019-04-13 01:16:50', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'alavvjh17md5gaf1bikgmv3o0m1212qg');
INSERT INTO `track` VALUES ('64', 'front', '2019-04-13 01:17:13', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'alavvjh17md5gaf1bikgmv3o0m1212qg');
INSERT INTO `track` VALUES ('65', 'front', '2019-04-13 01:17:37', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'alavvjh17md5gaf1bikgmv3o0m1212qg');
INSERT INTO `track` VALUES ('66', 'front', '2019-04-13 01:17:43', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/', 'alavvjh17md5gaf1bikgmv3o0m1212qg');
INSERT INTO `track` VALUES ('67', 'front', '2019-04-13 01:17:45', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article', 'alavvjh17md5gaf1bikgmv3o0m1212qg');
INSERT INTO `track` VALUES ('68', 'front', '2019-04-13 01:17:49', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'alavvjh17md5gaf1bikgmv3o0m1212qg');
INSERT INTO `track` VALUES ('69', 'front', '2019-04-13 01:23:55', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', '749fbb83755km44ampqr16mke5jkk90b');
INSERT INTO `track` VALUES ('70', 'front', '2019-04-13 01:27:48', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', '749fbb83755km44ampqr16mke5jkk90b');
INSERT INTO `track` VALUES ('71', 'front', '2019-04-13 01:34:42', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'qqpqjbt68472hg4vfj90ugvv4vdcr477');
INSERT INTO `track` VALUES ('72', 'front', '2019-04-13 01:36:50', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'qqpqjbt68472hg4vfj90ugvv4vdcr477');
INSERT INTO `track` VALUES ('73', 'front', '2019-04-13 01:43:55', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('74', 'front', '2019-04-13 01:44:00', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('75', 'front', '2019-04-13 01:44:09', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('76', 'front', '2019-04-13 01:44:57', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('77', 'front', '2019-04-13 01:44:58', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('78', 'front', '2019-04-13 01:44:58', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('79', 'front', '2019-04-13 01:44:59', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('80', 'front', '2019-04-13 01:44:59', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('81', 'front', '2019-04-13 01:44:59', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('82', 'front', '2019-04-13 01:45:00', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('83', 'front', '2019-04-13 01:45:00', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('84', 'front', '2019-04-13 01:45:00', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('85', 'front', '2019-04-13 01:45:00', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('86', 'front', '2019-04-13 01:45:53', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('87', 'front', '2019-04-13 01:45:53', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('88', 'front', '2019-04-13 01:45:53', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('89', 'front', '2019-04-13 01:45:54', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('90', 'front', '2019-04-13 01:45:54', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('91', 'front', '2019-04-13 01:45:54', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('92', 'front', '2019-04-13 01:45:55', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('93', 'front', '2019-04-13 01:45:55', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('94', 'front', '2019-04-13 01:45:55', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('95', 'front', '2019-04-13 01:45:55', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('96', 'front', '2019-04-13 01:45:55', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('97', 'front', '2019-04-13 01:45:56', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('98', 'front', '2019-04-13 01:45:56', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('99', 'front', '2019-04-13 01:45:56', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('100', 'front', '2019-04-13 01:45:56', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('101', 'front', '2019-04-13 01:45:57', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('102', 'front', '2019-04-13 01:45:57', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('103', 'front', '2019-04-13 01:45:57', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('104', 'front', '2019-04-13 01:45:57', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('105', 'front', '2019-04-13 01:45:57', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('106', 'front', '2019-04-13 01:45:57', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('107', 'front', '2019-04-13 01:45:58', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('108', 'front', '2019-04-13 01:45:58', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('109', 'front', '2019-04-13 01:45:58', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('110', 'front', '2019-04-13 01:45:58', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('111', 'front', '2019-04-13 01:45:58', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('112', 'front', '2019-04-13 01:45:58', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('113', 'front', '2019-04-13 01:45:59', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('114', 'front', '2019-04-13 01:45:59', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('115', 'front', '2019-04-13 01:45:59', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('116', 'front', '2019-04-13 01:45:59', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('117', 'front', '2019-04-13 01:46:00', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('118', 'front', '2019-04-13 01:47:28', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('119', 'front', '2019-04-13 01:47:37', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'bmihbevqkn3bb63u8t44dg31mga05f5s');
INSERT INTO `track` VALUES ('120', 'front', '2019-04-13 01:48:59', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'she76irpl9pju22pe7qk269kc72m3rd1');
INSERT INTO `track` VALUES ('121', 'front', '2019-04-13 01:49:02', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'she76irpl9pju22pe7qk269kc72m3rd1');
INSERT INTO `track` VALUES ('122', 'front', '2019-04-13 01:50:05', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 'she76irpl9pju22pe7qk269kc72m3rd1');
INSERT INTO `track` VALUES ('123', 'front', '2019-04-13 01:51:06', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article', 'she76irpl9pju22pe7qk269kc72m3rd1');
INSERT INTO `track` VALUES ('124', 'front', '2019-04-13 01:51:13', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article', 'she76irpl9pju22pe7qk269kc72m3rd1');
INSERT INTO `track` VALUES ('125', 'front', '2019-04-13 01:51:17', 'activity', 'index', '0', null, '0', 'http://the-capvition.test:82/contact', 'Windows 10', '127.0.0.1', '/activity', 'she76irpl9pju22pe7qk269kc72m3rd1');
INSERT INTO `track` VALUES ('126', 'front', '2019-04-13 01:51:25', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/activity', 'Windows 10', '127.0.0.1', '/', 'she76irpl9pju22pe7qk269kc72m3rd1');
INSERT INTO `track` VALUES ('127', 'front', '2019-04-13 01:51:28', 'activity', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/activity', 'she76irpl9pju22pe7qk269kc72m3rd1');
INSERT INTO `track` VALUES ('128', 'front', '2019-04-13 01:51:38', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/activity', 'Windows 10', '127.0.0.1', '/', 'she76irpl9pju22pe7qk269kc72m3rd1');
INSERT INTO `track` VALUES ('129', 'front', '2019-04-13 01:52:33', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/activity', 'Windows 10', '127.0.0.1', '/', 'she76irpl9pju22pe7qk269kc72m3rd1');
INSERT INTO `track` VALUES ('130', 'front', '2019-04-14 22:20:45', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/activity', 'Windows 10', '127.0.0.1', '/', 'c8pothf2grpch91tu973t0bdibsd1bhp');
INSERT INTO `track` VALUES ('131', 'front', '2019-04-18 14:37:41', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'sqctptfmg5pigp0c725j3qotn02rlqsu');
INSERT INTO `track` VALUES ('132', 'front', '2019-04-18 14:45:14', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'hsrcspo8r5e11p2h5731hjn0ss9sql99');
INSERT INTO `track` VALUES ('133', 'front', '2019-04-18 14:47:18', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/article', 'Windows 10', '127.0.0.1', '/', 'hsrcspo8r5e11p2h5731hjn0ss9sql99');
INSERT INTO `track` VALUES ('134', 'front', '2019-04-18 14:47:31', 'article', 'detail', '1', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article/detail/Lorem-ipsum-dolor-sit-amet', 'hsrcspo8r5e11p2h5731hjn0ss9sql99');
INSERT INTO `track` VALUES ('135', 'front', '2019-04-18 14:47:35', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/article', 'Windows 10', '127.0.0.1', '/', 'hsrcspo8r5e11p2h5731hjn0ss9sql99');
INSERT INTO `track` VALUES ('136', 'front', '2019-04-18 14:55:00', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/article', 'Windows 10', '127.0.0.1', '/', 'nl082ddug0obqm27d48bsnc236mtl7uq');
INSERT INTO `track` VALUES ('137', 'front', '2019-04-18 14:55:12', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/article', 'Windows 10', '127.0.0.1', '/', 'nl082ddug0obqm27d48bsnc236mtl7uq');
INSERT INTO `track` VALUES ('138', 'front', '2019-04-18 14:55:51', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/admin/article', 'Windows 10', '127.0.0.1', '/', 'nl082ddug0obqm27d48bsnc236mtl7uq');
INSERT INTO `track` VALUES ('139', 'front', '2019-04-18 14:56:38', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'nl082ddug0obqm27d48bsnc236mtl7uq');
INSERT INTO `track` VALUES ('140', 'front', '2019-04-18 14:59:38', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'jv9gqkc9qcppmtm1t2pop1e6616hc7jk');
INSERT INTO `track` VALUES ('141', 'front', '2019-04-18 15:01:58', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'jv9gqkc9qcppmtm1t2pop1e6616hc7jk');
INSERT INTO `track` VALUES ('142', 'front', '2019-04-18 15:02:09', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'jv9gqkc9qcppmtm1t2pop1e6616hc7jk');
INSERT INTO `track` VALUES ('143', 'front', '2019-04-18 15:02:15', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'jv9gqkc9qcppmtm1t2pop1e6616hc7jk');
INSERT INTO `track` VALUES ('144', 'front', '2019-04-18 15:08:39', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'v60tdcp1l7p13sjp2aha8pid5nrsglc7');
INSERT INTO `track` VALUES ('145', 'front', '2019-04-18 15:09:23', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'v60tdcp1l7p13sjp2aha8pid5nrsglc7');
INSERT INTO `track` VALUES ('146', 'front', '2019-04-18 15:09:30', 'activity', 'detail', null, null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/activity/detail/Photoshop-Super-Promote', 'v60tdcp1l7p13sjp2aha8pid5nrsglc7');
INSERT INTO `track` VALUES ('147', 'front', '2019-04-18 15:09:39', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'v60tdcp1l7p13sjp2aha8pid5nrsglc7');
INSERT INTO `track` VALUES ('148', 'front', '2019-04-18 15:10:00', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article', 'v60tdcp1l7p13sjp2aha8pid5nrsglc7');
INSERT INTO `track` VALUES ('149', 'front', '2019-04-18 15:10:03', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'v60tdcp1l7p13sjp2aha8pid5nrsglc7');
INSERT INTO `track` VALUES ('150', 'front', '2019-04-18 15:11:23', 'article', 'detail', '1', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article/detail/Lorem-ipsum-dolor-sit-amet', 'v60tdcp1l7p13sjp2aha8pid5nrsglc7');
INSERT INTO `track` VALUES ('151', 'front', '2019-04-18 15:11:29', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 'v60tdcp1l7p13sjp2aha8pid5nrsglc7');
INSERT INTO `track` VALUES ('152', 'front', '2019-04-18 15:15:35', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 's9phdklc5i3i1pr6vfulcq041ilt459a');
INSERT INTO `track` VALUES ('153', 'front', '2019-04-18 15:17:02', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 's9phdklc5i3i1pr6vfulcq041ilt459a');
INSERT INTO `track` VALUES ('154', 'front', '2019-04-18 15:17:18', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 's9phdklc5i3i1pr6vfulcq041ilt459a');
INSERT INTO `track` VALUES ('155', 'front', '2019-04-18 15:18:07', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 's9phdklc5i3i1pr6vfulcq041ilt459a');
INSERT INTO `track` VALUES ('156', 'front', '2019-04-18 15:19:58', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 's9phdklc5i3i1pr6vfulcq041ilt459a');
INSERT INTO `track` VALUES ('157', 'front', '2019-04-18 15:20:35', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 's9phdklc5i3i1pr6vfulcq041ilt459a');
INSERT INTO `track` VALUES ('158', 'front', '2019-04-18 15:20:38', 'course', 'detail', '1', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/course/detail/English-Grammar', 's9phdklc5i3i1pr6vfulcq041ilt459a');
INSERT INTO `track` VALUES ('159', 'front', '2019-04-18 15:20:58', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', 's9phdklc5i3i1pr6vfulcq041ilt459a');
INSERT INTO `track` VALUES ('160', 'front', '2019-04-18 15:21:13', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article', 's9phdklc5i3i1pr6vfulcq041ilt459a');
INSERT INTO `track` VALUES ('161', 'front', '2019-04-18 15:21:15', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 's9phdklc5i3i1pr6vfulcq041ilt459a');
INSERT INTO `track` VALUES ('162', 'front', '2019-04-18 15:21:21', 'course', 'detail', '1', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/course/detail/English-Grammar', 's9phdklc5i3i1pr6vfulcq041ilt459a');
INSERT INTO `track` VALUES ('163', 'front', '2019-04-18 15:21:26', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/', 's9phdklc5i3i1pr6vfulcq041ilt459a');
INSERT INTO `track` VALUES ('164', 'front', '2019-04-30 23:27:13', 'home', 'index', '0', null, '0', '', 'Windows 10', '127.0.0.1', '/', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('165', 'front', '2019-04-30 23:27:27', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('166', 'front', '2019-04-30 23:27:38', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('167', 'front', '2019-04-30 23:27:41', 'activity', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/activity', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('168', 'front', '2019-04-30 23:27:48', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/activity', 'Windows 10', '127.0.0.1', '/', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('169', 'front', '2019-04-30 23:28:04', 'activity', 'detail', null, null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/activity/detail/Photoshop-Super-Promote', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('170', 'front', '2019-04-30 23:28:11', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/activity/detail/Photoshop-Super-Promote', 'Windows 10', '127.0.0.1', '/', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('171', 'front', '2019-04-30 23:30:27', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/activity/detail/Photoshop-Super-Promote', 'Windows 10', '127.0.0.1', '/', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('172', 'front', '2019-04-30 23:30:29', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('173', 'front', '2019-04-30 23:30:31', 'activity', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/activity', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('174', 'front', '2019-04-30 23:30:37', 'activity', 'index', '0', null, '0', 'http://the-capvition.test:82/contact', 'Windows 10', '127.0.0.1', '/activity', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('175', 'front', '2019-04-30 23:30:46', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/activity', 'Windows 10', '127.0.0.1', '/', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('176', 'front', '2019-04-30 23:30:52', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('177', 'front', '2019-04-30 23:30:59', 'article', 'index', '0', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('178', 'front', '2019-04-30 23:31:01', 'activity', 'index', '0', null, '0', 'http://the-capvition.test:82/article', 'Windows 10', '127.0.0.1', '/activity', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('179', 'front', '2019-04-30 23:31:07', 'home', 'index', '0', null, '0', 'http://the-capvition.test:82/contact', 'Windows 10', '127.0.0.1', '/', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('180', 'front', '2019-04-30 23:31:16', 'article', 'detail', '1', null, '0', 'http://the-capvition.test:82/', 'Windows 10', '127.0.0.1', '/article/detail/Lorem-ipsum-dolor-sit-amet', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('181', 'front', '2019-04-30 23:31:42', 'activity', 'index', '0', null, '0', 'http://the-capvition.test:82/article/detail/Lorem-ipsum-dolor-sit-amet', 'Windows 10', '127.0.0.1', '/activity', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('182', 'front', '2019-04-30 23:31:48', 'activity', 'detail', null, null, '0', 'http://the-capvition.test:82/activity', 'Windows 10', '127.0.0.1', '/activity/detail/Photoshop-Super-Promote', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');
INSERT INTO `track` VALUES ('183', 'front', '2019-04-30 23:31:51', 'activity', 'index', '0', null, '0', 'http://the-capvition.test:82/activity/detail/Photoshop-Super-Promote', 'Windows 10', '127.0.0.1', '/activity', '5q9nlk7n8nhfbnb5ej60smlam5dq2omc');

-- ----------------------------
-- Table structure for upload
-- ----------------------------
DROP TABLE IF EXISTS `upload`;
CREATE TABLE `upload` (
  `uploadId` int(11) NOT NULL AUTO_INCREMENT,
  `grpContent` varchar(30) DEFAULT NULL,
  `filename` varchar(200) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `size` int(11) DEFAULT NULL COMMENT 'KB',
  `extension` varchar(100) DEFAULT NULL,
  `mime` varchar(100) DEFAULT NULL,
  `private` int(11) DEFAULT '2' COMMENT '1:private, 2:published',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT NULL,
  `modifyDate` datetime DEFAULT NULL,
  `modifyBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`uploadId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of upload
-- ----------------------------
INSERT INTO `upload` VALUES ('1', 'banner', 'd21aaae18e798d7adc703e66009649ae.jpg', 'uploads/2019/04/banner/', '91648', 'jpg', 'image/jpeg', '2', '2019-04-13 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('2', 'banner', 'db786c0d19b3f7345cedab77c8a01428.jpg', 'uploads/2019/04/banner/', '91648', 'jpg', 'image/jpeg', '2', '2019-04-13 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('3', 'instructor', 'fe0244cf2a3f79c9621951fbb660b9ff.png', 'uploads/2019/04/instructor/', '14193', 'png', 'image/png', '2', '2019-04-13 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('4', 'course', 'd83d2005c35cbcf547477b2c11bd30b1.jpg', 'uploads/2019/04/course/', '8325', 'jpg', 'image/jpeg', '2', '2019-04-13 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('5', 'course', 'e42da8d283fb6ee0cc85ef9cf9c28fc4.jpg', 'uploads/2019/04/course/', '8325', 'jpg', 'image/jpeg', '2', '2019-04-13 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('6', 'banner', 'eb7dd4aa8c82fe3eb3f2db87b7a48e78.jpg', 'uploads/2019/04/banner/', '81265', 'jpg', 'image/jpeg', '2', '2019-04-13 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('7', 'article', '3d25f5fd9a304885295bb40f3ea71cb1.jpg', 'uploads/2019/04/article/', '8325', 'jpg', 'image/jpeg', '2', '2019-04-18 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('8', 'activity', '76b55a5bcdfe4be8321733f6b594e4ba.jpg', 'uploads/2019/04/activity/', '72397', 'jpg', 'image/jpeg', '2', '2019-04-18 00:00:00', '3', null, null);

-- ----------------------------
-- Table structure for upload_content
-- ----------------------------
DROP TABLE IF EXISTS `upload_content`;
CREATE TABLE `upload_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grpContent` varchar(50) NOT NULL,
  `grpType` varchar(50) NOT NULL,
  `contentId` int(11) NOT NULL,
  `uploadId` int(11) NOT NULL,
  `lang` varchar(3) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of upload_content
-- ----------------------------
INSERT INTO `upload_content` VALUES ('1', 'banner', 'coverImage', '1', '1', null, null);
INSERT INTO `upload_content` VALUES ('2', 'banner', 'contentImage', '1', '2', null, null);
INSERT INTO `upload_content` VALUES ('3', 'instructor', 'coverImage', '1', '3', null, null);
INSERT INTO `upload_content` VALUES ('4', 'course', 'coverImage', '1', '4', null, null);
INSERT INTO `upload_content` VALUES ('5', 'course', 'contentImage', '1', '5', null, null);
INSERT INTO `upload_content` VALUES ('6', 'banner', 'coverImage', '2', '6', null, null);
INSERT INTO `upload_content` VALUES ('7', 'article', 'coverImage', '1', '7', null, null);
INSERT INTO `upload_content` VALUES ('8', 'activity', 'coverImage', '1', '8', null, null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT '1',
  `verify` int(11) DEFAULT '0',
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `policyId` int(11) unsigned DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) unsigned DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) unsigned DEFAULT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` varchar(255) DEFAULT NULL,
  `sectionId` int(11) DEFAULT NULL,
  `partyId` int(11) DEFAULT NULL,
  `positionId` int(11) DEFAULT NULL,
  `degree` int(2) DEFAULT NULL,
  `oauth_provider` varchar(150) DEFAULT NULL,
  `oauth_uid` varchar(150) DEFAULT '',
  `picture` text,
  `phone` varchar(25) DEFAULT '',
  `user_login_status` int(1) DEFAULT '0',
  `user_datetime_using` datetime DEFAULT NULL,
  `couponCode` varchar(150) DEFAULT NULL,
  `isCommunity` int(1) DEFAULT '0',
  `session_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userId`),
  KEY `oem_users_ibfk_1` (`policyId`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'administrator', '03f7d1801a07265823dfb824d34b167cca7c8a6e8f6c3627675d4e7446e65abf9ed45d771a59639767d5ee23a542e1669b24ba273b677fb89d64cbb1c9ab2818gCxgV8dToDExXM3ROcarkyChVrZiy8Tsq8nweyvzEwc=', 'admin@admin.com', 'admin', '2018-07-03 04:20:26', '1', '1', 'Admin', 'istrator', '1', '2016-07-10 09:06:53', null, '2018-07-09 08:58:31', '3', '2', '2018-01-18 10:00:51', '1', '14', '17', '18', '10', null, null, null, '', '0', null, null, '0', null);
INSERT INTO `user` VALUES ('3', 'super_admin', '03f7d1801a07265823dfb824d34b167cca7c8a6e8f6c3627675d4e7446e65abf9ed45d771a59639767d5ee23a542e1669b24ba273b677fb89d64cbb1c9ab2818gCxgV8dToDExXM3ROcarkyChVrZiy8Tsq8nweyvzEwc=', 'programmer@ait.com', 'developer', '2019-04-18 14:45:29', '1', '1', 'โปรแกรมเมอร์', 'เดฟ', '1', '2016-07-14 10:17:38', '1', '2018-07-09 09:47:51', '3', '0', '2018-01-18 08:09:05', null, null, null, null, null, null, null, null, '', '0', null, null, '0', null);
INSERT INTO `user` VALUES ('22', 'Admin', 'cebd18473e3808ce458cc4522af2a6d1e7e0561edd0b64c225cc25486cd5cf4c14073316037e7cee6b1fe5383915e193d850c432d6ff71a8f5600e39af4fa29bMdjnsRU/2f3hyKzVqlTFdhvP7jr7K04HXibG1IfvKT4=', 'administrator@admin.com', 'officer', '2018-10-06 20:28:12', '1', '1', 'Admin', 'Administrator', '1', '2018-08-04 23:17:50', '3', '2018-10-05 02:13:06', '3', '0', null, null, null, null, null, null, null, null, null, '', '0', null, null, '0', null);
INSERT INTO `user` VALUES ('23', 'aaa', 'dcb86b518e6b85d7ad0dfab447e1f28f85d9ef52d17f247b01bcd509be9dfb3873b7352d4fa3d5c6ed4cd83db704bf0bb7104f0ec911f40bb1050697e27f59eaQ2IA0KesWJWvoAVpZ2OUZVNpBVDrtSVM+t/xy1+lPU8=', 'satari.dop@gmail.com', 'member', '2019-03-30 21:12:44', '1', '1', 'aaaa', 'aaaa', '0', '2018-10-02 22:02:19', null, '2018-10-19 23:27:12', '3', '0', null, null, null, null, null, null, null, null, null, '0857997445', '1', '2019-03-30 21:12:44', 'DKCVC1523', '0', 'b56fdcd8ff2fb98a4f0508aafe9e4b6b65c75c32');
INSERT INTO `user` VALUES ('24', 'aaaa', '5d6d5820f0a1a502194013fd60bb6c663facd2ad5c56996d0161f12747946881624b435f3b5c97e03a8986756136908bcbf57b602ef1f297598991d99f9aa7b5Ls3PNOy3NKBg0/wjSfN89q0fiycw86+raAlzlZ2rk8Q=', '', 'member', null, '1', '1', 'aa', 'aa', '0', '2018-10-02 22:12:50', null, null, null, '0', null, null, null, null, null, null, null, null, null, '', '0', null, '', '0', null);
INSERT INTO `user` VALUES ('25', 'ddddd', '9a8f044c035f2387b46749a604dbc15144b6447c82ea4778df9245b6a3ca26281beb7c7751e23389bcc0912866badc99bffb5ac372df4372ca54fc3cb6de9292pe4Oa7tqPbN+ub0kzVyzyVU9DQ8ZSHIxPnHoJoa3uow=', '', 'member', null, '1', '1', 'ddddd', 'dddd', '0', '2018-10-03 09:50:32', null, null, null, '0', null, null, null, null, null, null, null, null, null, '', '0', null, null, '0', null);
INSERT INTO `user` VALUES ('26', 'cccc', 'ba63f337c9fd5b4f751797408c83c4e348535b2941e443fba064012b1c09a6996c7dd9c8e9d9cca45bb054b7d59c6956b766c31a29432f6f82879d931992c443Cs556dCcc6I4fXJLh5HlleceGPe/JK059WE2OWVhvNo=', '', 'member', '2018-11-29 01:28:02', '1', '1', 'ccccc', 'ccccc', '0', '2018-10-03 09:55:25', null, null, null, '0', null, null, null, null, null, null, null, null, null, '', '0', '0000-00-00 00:00:00', '5T9J74X26', '0', null);
INSERT INTO `user` VALUES ('32', null, '', 'satari_pas@hotmail.com', 'member', null, '1', '1', 'Satari', 'Ma\'seng', '0', '2018-10-03 11:16:25', null, null, null, '0', null, null, null, null, null, null, 'facebook', '10205227098394244', null, '', '0', null, null, '0', null);
INSERT INTO `user` VALUES ('33', 'bbb', '1ea3fbf088f08bb1ed28d1b2d6ab00d6090823ccda6a6379d531329ecc4af8ac57f6a6b6ed88e69ea7fe70581b5adcc4a9b6e28e646ee56d88d95d8196694b0ajgnmJ/msNWePcx4eEi8dux9MfXJnNib7LuQiZh1ftds=', '', 'member', '2019-01-27 15:34:43', '1', '1', 'bbb', 'bbb', '0', '2018-10-06 22:52:31', null, null, null, '0', null, null, null, null, null, null, 'register', '', null, '', '0', '0000-00-00 00:00:00', '52CPAXW33', '1', '14ec600cf5c3fa1b489c1c4cb846a136650607d7');
INSERT INTO `user` VALUES ('34', 'aaa1', '64b46d5c4508e2d225a579e6875e3f3ed0bea8ffb567bb3d39f0280a6836f6881077940d5761c6415773aa140f42fd23dd553d838a4403f6532e80e539a30a8fgTzETqsej3SB/kWIquAcHlrEvZgLV2RqWXdbb0zrv1w=', '', 'member', '2018-10-10 20:11:04', '1', '1', 'aaa', 'aaa', '0', '2018-10-10 20:10:49', null, null, null, '0', null, null, null, null, null, null, 'register', '', null, '', '0', null, null, '0', null);
INSERT INTO `user` VALUES ('35', 'aaaasasasas', 'ae4d24acca5baea2862d576173a8ddfd2c3c7d5da97d7252b6ba1aad6bc70fb9fcb2122dec3e1b5e89186686b5f255a5809ab5075062869e95b929e738b1f6c3Ufbx5U5qjbWkmvjZkKWV1Xzxs3aVYSjAYWFqJzxoX/I=', '', 'member', null, '1', '1', 'aa', 'fas', '0', '2018-10-12 22:11:13', null, null, null, '0', null, null, null, null, null, null, 'register', '', null, '', '0', null, null, '0', null);
INSERT INTO `user` VALUES ('36', 'aa1234', '18601624195b0bd931c5cc2f54defc2f78cd3f6730ffd2341a9b2762c2f46007c14ae6f74b9793d6e4d43a8192f5267f74a2d5c5cbf895aa6136619a5b731ca9XENE+3jPr4a1+lVSMjjzM2CFmEYimkwKdl/tI21Lzn0=', '', 'member', null, '1', '1', 'สาทาริ', 'มะเซ็ง', '0', '2018-10-12 22:19:06', null, '2018-11-19 22:22:26', '3', '0', null, null, null, null, null, null, 'register', '', null, '', '0', null, null, '0', null);
INSERT INTO `user` VALUES ('37', 'sasss', 'cee3c3a7f88d3b51c3cfcb3837b8122970ccdc4f1deb5cb7d1bfbccfd9f574e146bd06ee0dd6dcfea0f31db263845707634d3f2969a937f0e1f4faf33957ea50npgP6DfFDqQ2nBs2kQfshWaOvUisNcDWcp9A4oFkVgE=', '', 'member', '2018-10-13 23:19:41', '1', '1', 'sass', 'sass', '0', '2018-10-13 23:19:41', null, null, null, '0', null, null, null, null, null, null, 'register', '', null, '', '0', '0000-00-00 00:00:00', null, '0', null);
INSERT INTO `user` VALUES ('38', 'aaasss', 'e03ed006094853a0d0c514f8e12a01802cee741bbf0fea5e47b969eb2f8956420839f29dbccb04376ac9e6421448343782b09523ed6472343cdb8e77a174854e76iIcCSR+GkUOHjvTE+P9OdsDmrPvA9U1gg8ldM6Dq8=', '', 'member', '2018-10-13 23:20:55', '1', '1', 'saaasss', 'sssasasa', '0', '2018-10-13 23:20:55', null, null, null, '0', null, null, null, null, null, null, 'register', '', null, '', '0', '0000-00-00 00:00:00', null, '0', null);
INSERT INTO `user` VALUES ('39', 'aaasssbb', '29d2fb94d5037f6dfea2d83eec5f1a9321fef20b5154a4c89990f033666e378481faa41efd5d40027989edd46a19dd041b40725fdbae7375b64173374f279055MS549uwm1rC7uhTynXUmAZ3ASZpgqWV0PyAHo1iyzec=', 'satari.dop@gmail.com', 'member', '2018-10-19 23:28:23', '1', '1', 'sa', 'sa', '0', '2018-10-19 23:28:23', null, '2018-10-19 23:29:02', null, '0', null, null, null, null, null, null, 'register', '', null, '0857997445', '0', '0000-00-00 00:00:00', null, '0', null);
INSERT INTO `user` VALUES ('40', 'ssaassaa', '4b637ae57a2d6fe27d0d27b5a535d121cdae4388fc95989003a7ca62e86aff4dbc3ffda1565d948f0dea82c32fe9e9c30f9adc49e783df62ccd353a81f73aba1rW6MqQ7gb4dE5bhLM802m3XcnQ1tWjc3VR7UjZnCRHU=', '', 'member', '2018-10-20 19:45:58', '1', '1', 'sass', 'ssasasa', '0', '2018-10-20 19:45:58', null, null, null, '0', null, null, null, null, null, null, 'register', '', null, '', '0', '0000-00-00 00:00:00', null, '0', null);
INSERT INTO `user` VALUES ('41', 'aaaaa', '9590f205377b811fab1d2f6b2a381de4610a9985fce873681f1d57c815db1adc98b2d8bdb4a07b4ddd7e67e219d15605d43bdf936b04f20b1df75b84e8e7810ePEsgPbeLT6aTBe2rDOOcaSzucmWL5e8m4iwmrcRtzFg=', 'test1@ait.com', 'member', '2018-12-15 22:04:46', '1', '1', 'sa', 'sa', '0', '2018-12-15 22:04:45', null, null, null, '0', null, null, null, null, null, null, 'register', '', null, '', '0', '0000-00-00 00:00:00', 'TAHUI2H41', '0', null);
INSERT INTO `user` VALUES ('42', 'satari1', 'bd2875392048a4d128d0ae9c7508c32175efd4f168af3415473961467808ae83f82035d28cb81046833c021081b65bdc1aaebd38645e6f9b6d31a2028f24bdd0QJCldIhsrE8JW74xoINCEwwPPhQo3IcwzN0ht4gz4to=', 'satari.dop@gmail.com', 'member', '2019-03-04 16:18:35', '1', '1', 'satari', 'maseng', '0', '2019-03-04 16:18:34', null, '2019-03-04 16:18:34', null, '0', null, null, null, null, null, null, 'register', '', null, '0857997445', '0', '0000-00-00 00:00:00', '75LYXNC42', '0', 'd394316d7b02aa2515575d95061bc5ff5a0ff892');
INSERT INTO `user` VALUES ('43', 'satari1234', 'a7fe4aac127c7865f424348a90e1d1a95f06754ef179267a734066822930122413f672d5a7bb8c889a8a341c7e5d6bde5b0348e1c97a57ec516b13f02d382678OdfkZNeyOeahmcVhEhWiwQIz3oe7xOw9LtsypdDiS7I=', 'satari.dop@gmail.com', 'member', '2019-03-04 16:23:02', '1', '1', 'satari', 'maseng', '0', '2019-03-04 16:23:02', null, '2019-03-04 16:23:02', null, '0', null, null, null, null, null, null, 'register', '', null, '0857997445', '0', '0000-00-00 00:00:00', 'YBMDRMC43', '0', 'd394316d7b02aa2515575d95061bc5ff5a0ff892');
INSERT INTO `user` VALUES ('44', 'satariaaa', 'f76f51e4e15037f717a8b0ba43eb72857887b5b7b9dac159c448077df92ccd9aea0389ce65a6ec52a9d5136e685762dae0ea765d6faa467def5c47541efb91bb/yFzsnpHizajQrs/2hARIdxGOANWRAOh5YGzBHREBeA=', 'satari.dop@gmail.com', 'member', '2019-03-04 21:56:47', '1', '1', 'satari', 'maseng', '0', '2019-03-04 21:56:47', null, '2019-03-04 21:56:47', null, '0', null, null, null, null, null, null, 'register', '', null, '0857997445', '0', '0000-00-00 00:00:00', 'UDJ2T5G44', '0', '335816d6dee52c9768d0861bad0f05971ea41d6f');
INSERT INTO `user` VALUES ('45', '123456', 'c35200ead56af7a889fb3de1ad439886cd6c8aebfdd84bfde0a83871698ac54c9b428204ac4630ad0386fe6e77dcd1475a100e863919ad470bd64603e17dbb95Gwdfn5K1WI4mJ5Wt5JEZ4080sctK+Wqk7CmUJanaWfk=', '', 'member', '2019-04-06 21:12:27', '1', '1', 'aa', 'aa', '0', '2019-04-06 21:12:14', null, '2019-04-06 21:12:14', null, '0', null, null, null, null, null, null, 'register', '', null, '', '0', '0000-00-00 00:00:00', 'YNEJE6V45', '0', '58f36ad93876bb155f259a2a9145f2b3238d465e');

-- ----------------------------
-- Table structure for usertracking
-- ----------------------------
DROP TABLE IF EXISTS `usertracking`;
CREATE TABLE `usertracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT '0',
  `class` varchar(50) DEFAULT NULL,
  `function` varchar(50) DEFAULT NULL,
  `method` varchar(20) DEFAULT NULL,
  `sessionId` varchar(100) DEFAULT NULL,
  `identifier` varchar(255) DEFAULT NULL,
  `requestUri` text,
  `timestamp` varchar(20) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `agent` text,
  `isBrowser` int(11) DEFAULT NULL,
  `browser` varchar(50) DEFAULT NULL,
  `isMobile` int(11) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `isRobot` tinyint(4) DEFAULT NULL,
  `robot` varchar(50) DEFAULT NULL,
  `platform` varchar(50) DEFAULT NULL,
  `isBackend` tinyint(4) DEFAULT '0',
  `referer` text,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` varchar(255) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usertracking
-- ----------------------------
